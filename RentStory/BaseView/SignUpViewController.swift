//
//  SignUpViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 23/10/20.
//

import UIKit
import Firebase
import FirebaseAuth
import CountryList

class SignUpViewController: UIViewController, CountryListDelegate
{
    var countryList = CountryList()
    
    @IBOutlet weak var mobileView: UIView!
    var verificationCode = ""
    var countryCode = "91"
    
    
    @IBOutlet weak var requestOTPButton: UIButton!
    
    @IBOutlet weak var lblRentStory: UILabel!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblMobileTitle: UILabel!
    @IBOutlet weak var lblTnC: UILabel!
    
    @IBOutlet weak var txtMobileNo: UITextField!
    
    @IBOutlet weak var btnCountry: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnTnC: UIButton!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        let center = NotificationCenter.default
//        center.addObserver(self, selector: #selector(keyboardWillBeShown(note:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//        center.addObserver(self, selector: #selector(keyboardWillBeHidden(note:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        countryList.delegate = self
        
        self.view.backgroundColor = .white //Constants.bgColor
        
        self.mainView.backgroundColor = Constants.bgColor
        self.mainView.layer.cornerRadius = 35
        
        self.btnTnC.layer.cornerRadius = 5
        
        self.txtMobileNo.layer.cornerRadius = 8
        
        self.mobileView.layer.cornerRadius = 8
        
        let passwordViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtMobileNo.leftView = passwordViewObj
        self.txtMobileNo.leftViewMode = .always
        
        let doneToolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 40.0))
        doneToolBar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonClicked(sender:)))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolBar.items = items as? [UIBarButtonItem]
        doneToolBar.sizeToFit()
        
        self.txtMobileNo!.inputAccessoryView = doneToolBar
        
        self.requestOTPButton.layer.cornerRadius = 12
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.lblRentStory.font = UIFont(name: "SofiaPro-Medium", size: 38)
            self.lblSignUp.font = UIFont(name: "SofiaPro-Medium", size: 32)
            self.lblSubTitle.font = UIFont(name: "SofiaPro", size: 25)
            
            self.lblMobileTitle.font = UIFont(name: "SofiaPro-Medium", size: 23)
            
            self.txtMobileNo.font = UIFont(name: "SofiaPro", size: 22)
            
            self.lblTnC.font = UIFont(name: "SofiaPro", size: 15)
            
            self.requestOTPButton.titleLabel?.font = UIFont(name: "SofiaPro-Medium", size: 30)
        }
    }
    
//    //MARK: - Keyboard Notification
//    @objc func keyboardWillBeShown(note: Notification)
//    {
//        if let keyboardSize = (note.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
//        {
//            if self.view.frame.origin.y == 0
//            {
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
//    }
//
//    @objc func keyboardWillBeHidden(note: Notification)
//    {
//        if ((note.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil
//        {
//            if self.view.frame.origin.y != 0
//            {
//                self.view.frame.origin.y  = 0 // += keyboardSize.height
//            }
//        }
//    }
    
    //MARK: - Button Actions
    @IBAction func requestOTPButtonClicked(_ sender: UIButton)
    {
        if self.btnTnC.isSelected
        {
            if !self.txtMobileNo.text!.isEmpty
            {
                PhoneAuthProvider.provider().verifyPhoneNumber("\(self.btnCountry.currentTitle ?? "")\(self.txtMobileNo.text ?? "")", uiDelegate: nil) { (verificationID, error) in

                    if let error = error
                    {
                        print(error.localizedDescription)
                        return
                    }

                    self.verificationCode = "\(verificationID ?? "")"

                    // Sign in using the verificationID and the code sent to the user
                    print("Sign in using the verificationID and the code sent to the user")
                    let otpVC = OTPViewController(nibName: "OTPViewController", bundle: nil)
                    otpVC.verificationIdStr = self.verificationCode
                    otpVC.phoneNumberStr = "\(self.countryCode)\(self.txtMobileNo.text ?? "")"
                    otpVC.countryCodeStr = "\(self.countryCode)"
                    self.present(otpVC, animated: true, completion: nil)
                }
            }
            else
            {
                let alertViewController = UIAlertController(title: "Error", message: "Please Enter PhoneNumber", preferredStyle: .alert)

                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                }))

                DispatchQueue.main.async {
                    self .present(alertViewController, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Click on T&C To Proceed", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    func selectedCountry(country: Country)
    {
        print("\(country.flag!) \(country.name!), \(country.countryCode), \(country.phoneExtension)")
        
        self.countryCode = "\(country.phoneExtension)"
        self.btnCountry.setTitle("+\(country.phoneExtension)", for: .normal)
    }
    
    @IBAction func countryButtonClicked(_ sender: UIButton)
    {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func doneButtonClicked(sender : UIButton)
    {
        self.txtMobileNo?.resignFirstResponder()
        
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func tncButtonClicked(_ sender: UIButton)
    {
        if self.btnTnC.isSelected
        {
            self.btnTnC.isSelected = false
            self.btnTnC.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
        else
        {
            self.btnTnC.isSelected = true
            self.btnTnC.setBackgroundImage(UIImage(named: "checkmark"), for: .normal)
        }
    }
    
    
}
