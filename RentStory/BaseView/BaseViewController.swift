//
//  BaseViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 23/10/20.
//

import UIKit
import FSPagerView

class BaseViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, FSPagerViewDataSource,FSPagerViewDelegate
{
    var imageArray = [String]()
    var contentArray = [String]()
    
//    var pageController: UIPageViewController!
    var controllers = [UIViewController]()
    
    @IBOutlet weak var getStartedButton: UIButton!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var view1Width: NSLayoutConstraint!
    @IBOutlet weak var view2Width: NSLayoutConstraint!
    @IBOutlet weak var view3Width: NSLayoutConstraint!
    @IBOutlet weak var view4Width: NSLayoutConstraint!
    @IBOutlet weak var view5Width: NSLayoutConstraint!
    
    @IBOutlet weak var baseView: FSPagerView! {
        didSet {
            self.baseView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("BaseViewController")
        
        self.view.backgroundColor = Constants.bgColor
        
        self.imageArray = ["onboarding1", "onboarding2", "onboarding3", "onboarding4", "onboarding5"]
        self.contentArray = [
            "Maintain your real estate property listings in one place ",
            "Add a rent contract if leased",
            "Automatic reminders and easy communication between all parties",
            "Manage leased/ unleased properties for reference",
            "Your personal real estate management tool (weather you are a Broker or Owner)"
        ]
        
        self.baseView.backgroundColor = .white
        self.baseView.delegate = self
        self.baseView.dataSource = self
        self.baseView.transformer = FSPagerViewTransformer(type: .depth)
        self.baseView.itemSize = FSPagerView.automaticSize
        self.baseView.decelerationDistance = 1
    
        self.getStartedButton.isHidden = true
        self.contentLabel.text = "\(self.contentArray[0])"
        
        
//        self.pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
//        self.pageController.view.frame = self.view.bounds
//        self.pageController.dataSource = self
//        self.pageController.delegate = self
        
//        self.addChild(self.pageController)
//        self.view.addSubview(self.pageController.view)
        
        let firstVC = FirstViewController(nibName: "FirstViewController", bundle: nil)
        self.controllers.append(firstVC)
        
        let secondVC = SecondViewController(nibName: "SecondViewController", bundle: nil)
        self.controllers.append(secondVC)
        
        let signUpVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.controllers.append(signUpVC)
        
//        self.pageController.setViewControllers([self.controllers[0]], direction: .forward, animated: true)
        
        for fontFamily in UIFont.familyNames
        {
            for fontName in UIFont.fontNames(forFamilyName: fontFamily)
            {
                print("Fonts : \(fontName)")
            }
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.baseView.layer.cornerRadius = 30
            self.baseView.layer.masksToBounds = true
            
            self.view1Width.constant = 30
        }
    }
    
    //MARK: - Button Actions
    @IBAction func getStartedButtonClicked(_ sender: UIButton)
    {
        let signUpVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        
        let appDelegate = UIApplication .shared .delegate as! AppDelegate
        appDelegate.window?.rootViewController = signUpVC
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    // MARK: - FSPagerViewDataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int
    {
        return self.imageArray.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell
    {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: (self.imageArray[index]))
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int)
    {
        if targetIndex == 0
        {
            self.view1Width.constant = 30
            self.view2Width.constant = 15
            
            self.getStartedButton.isHidden = true
            self.contentLabel.text = "\(self.contentArray[0])"
        }
        else if targetIndex == 1
        {
            
            self.view2Width.constant = 30
            self.view1Width.constant = 15
            
            
            
            self.getStartedButton.isHidden = true
            
            self.contentLabel.text = "\(self.contentArray[1])"
        }
        else if targetIndex == 2
        {
            self.view3Width.constant = 30
            self.view2Width.constant = 15
            
            self.getStartedButton.isHidden = true
            
            self.contentLabel.text = "\(self.contentArray[2])"
        }
        else if targetIndex == 3
        {
            self.view4Width.constant = 30
            self.view3Width.constant = 15
            
            
            self.getStartedButton.isHidden = true
            
            self.contentLabel.text = "\(self.contentArray[3])"
        }
        else if targetIndex == 4
        {
            self.view5Width.constant = 30
            self.view4Width.constant = 15
            
            self.getStartedButton.fadeTransition(0.4)
            self.getStartedButton.isHidden = false
            
            self.contentLabel.text = "\(self.contentArray[4])"
        }
        
    }
    
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool
    {
        return false
    }
    
    //MARK: - PageView Delegated
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        if let index = self.controllers.firstIndex(of: viewController)
        {
            if index > 0
            {
                return self.controllers[index - 1]
            }
            else
            {
                return nil
            }
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        if let index = self.controllers.firstIndex(of: viewController)
        {
            if index < self.controllers.count - 1
            {
                return self.controllers[index + 1]
            }
            else
            {
                return nil
            }
        }
        
        return nil
    }
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true)
    {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func dropShadowAllSites()
    {
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.cornerRadius = 20
    }
    
    func setRadiusWithShadow(_ radius: CGFloat? = nil)
    {
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5.5)
        self.layer.shadowRadius = 10.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func dropShadowForDashBoard()
    {
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowColor = UIColor.lightGray.cgColor //UIColor.init(named: "#cbcbf0")?.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3
        layer.masksToBounds = false
    }
    
    func dropShadowOpt()
    {
        //working and for All Sides of the view
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowColor = UIColor.init(named: "#cbcbf0")?.cgColor
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 20
        layer.masksToBounds = false
    }
    
    
}

extension UIView {
    
    func addTopViewBorder(_ color: UIColor, height: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        border.addConstraint(NSLayoutConstraint(item: border,
                                                attribute: NSLayoutConstraint.Attribute.height,
                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                toItem: nil,
                                                attribute: NSLayoutConstraint.Attribute.height,
                                                multiplier: 1, constant: height))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.top,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.top,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.leading,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.leading,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.trailing,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.trailing,
                                              multiplier: 1, constant: 0))
    }
    
    func addBottomViewBorder(_ color: UIColor, height: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        border.addConstraint(NSLayoutConstraint(item: border,
                                                attribute: NSLayoutConstraint.Attribute.height,
                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                toItem: nil,
                                                attribute: NSLayoutConstraint.Attribute.height,
                                                multiplier: 1, constant: height))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.bottom,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.bottom,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.leading,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.leading,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.trailing,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.trailing,
                                              multiplier: 1, constant: 0))
    }
    func addLeftViewBorder(_ color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        border.addConstraint(NSLayoutConstraint(item: border,
                                                attribute: NSLayoutConstraint.Attribute.width,
                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                toItem: nil,
                                                attribute: NSLayoutConstraint.Attribute.width,
                                                multiplier: 1, constant: width))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.leading,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.leading,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.bottom,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.bottom,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.top,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.top,
                                              multiplier: 1, constant: 0))
    }
    func addRightViewBorder(_ color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        border.addConstraint(NSLayoutConstraint(item: border,
                                                attribute: NSLayoutConstraint.Attribute.width,
                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                toItem: nil,
                                                attribute: NSLayoutConstraint.Attribute.width,
                                                multiplier: 1, constant: width))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.trailing,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.trailing,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.bottom,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.bottom,
                                              multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
                                              attribute: NSLayoutConstraint.Attribute.top,
                                              relatedBy: NSLayoutConstraint.Relation.equal,
                                              toItem: self,
                                              attribute: NSLayoutConstraint.Attribute.top,
                                              multiplier: 1, constant: 0))
    }
}

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval)
    {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}
