//
//  SecondViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 23/10/20.
//

import UIKit

class SecondViewController: UIViewController
{
    @IBOutlet weak var bottomLeftView: UIView!
    @IBOutlet weak var bottomMiddleView: UIView!
    @IBOutlet weak var bottomRightView: UIView!
    
    @IBOutlet weak var rightButton: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("SecondViewController")
        
        self.view.backgroundColor = Constants.bgColor
        
        self.bottomLeftView.layer.cornerRadius = self.bottomLeftView.frame.height/2
        self.bottomMiddleView.layer.cornerRadius = self.bottomMiddleView.frame.height/2
        self.bottomRightView.layer.cornerRadius = self.bottomRightView.frame.height/2
        
        self.rightButton.layer.cornerRadius = self.rightButton.frame.height/2
        self.rightButton.setRadiusWithShadow(self.rightButton.frame.height/2)
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.lblTitle.font = UIFont(name: "SofiaPro-Bold", size: 35)
            self.lblSubTitle.font = UIFont(name: "SofiaPro", size: 25)
        }
    }
}
