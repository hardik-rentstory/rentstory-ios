//
//  FirstViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 23/10/20.
//

import UIKit

class FirstViewController: UIViewController
{
    @IBOutlet weak var bottomLeftView: UIView!
    @IBOutlet weak var bottomMiddleView: UIView!
    @IBOutlet weak var bottomRightView: UIView!
    
    @IBOutlet weak var lblProjectManager: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("FirstViewController")

        self.view.backgroundColor = Constants.bgColor
        
        self.bottomLeftView.layer.cornerRadius = self.bottomLeftView.frame.height/2
        self.bottomMiddleView.layer.cornerRadius = self.bottomMiddleView.frame.height/2
        self.bottomRightView.layer.cornerRadius = self.bottomRightView.frame.height/2
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.lblProjectManager.font = UIFont(name: "SofiaPro-Bold", size: 35)
            self.lblSubTitle.font = UIFont(name: "SofiaPro", size: 25)
        }
    }
    
    //MARK: - Button Actions
    @IBAction func skipButtonClicked(_ sender: UIButton)
    {
        let signUpVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        
        let appDelegate = UIApplication .shared .delegate as! AppDelegate
//        let navController = UINavigationController(rootViewController: signUpVC)
//        appDelegate.window?.rootViewController = navController
        
        appDelegate.window?.rootViewController = signUpVC
        appDelegate.window?.makeKeyAndVisible()
    }
}
