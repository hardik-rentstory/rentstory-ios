//
//  ActivityDetailsViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 28/01/21.
//

import UIKit
import KRProgressHUD
import DatePickerDialog

class ActivityDetailsViewController: UIViewController
{
    var alertTitleVal = ""
    var alertMsg = ""
    
    var fromDateTimeStr = ""
    var toDateTimeStr = ""
    var dateStr = ""
    var endDateStr = ""
    
    var contactType = ""
    var propertyID = ""
    var mobileNumber = ""
    
    var contactListArray : NSMutableArray?
    
    var detailDict : NSDictionary?
    var rentInvoiceDict : NSDictionary?
    
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var contactTypeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var propertyNameHeaderLabel: UILabel!
    @IBOutlet weak var propertyNameLabel: UILabel!
    @IBOutlet weak var rentAmountHeaderLabel: UILabel!
    @IBOutlet weak var rentAmountLabel: UILabel!
    @IBOutlet weak var rentDueDateHeaderLabel: UILabel!
    @IBOutlet weak var rentDueDateLabel: UILabel!
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        DispatchQueue.main.async {
          
            if self.detailDict != nil
            {
                self.contactTypeLabel.text = "\(self.detailDict?.value(forKey: "contact_type") ?? "")"
                self.dateLabel.text = ("\(self.detailDict?.value(forKey: "notification_date") ?? "")".components(separatedBy: "T").first ?? "")
                self.contentLabel.text = "\(self.detailDict?.value(forKey: "notification_content") ?? "")"
                
                if "\(self.detailDict?.value(forKey: "property_name") ?? "")".isEmpty != true
                {
                    self.propertyNameLabel.text = "\(self.detailDict?.value(forKey: "property_name") ?? "")"
                }
                
                //Need To Handle
                if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Rent Alert"
                {
                    self.button1.isHidden = false
                    self.button1.setTitle("Received, Confirm Receipt", for: .normal)

                    self.button2.isHidden = false
                    self.button2.setTitle("Send Reminder To Tenant", for: .normal)
                    
                    self.getRentInvoice()
                }
                else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Rent Receipt"
                {
                    self.button1.isHidden = false
                    self.button1.setTitle("Issue Rent Receipt", for: .normal)

                    self.button2.isHidden = false
                    self.button2.setTitle("Will Do It Later", for: .normal)
                }
                else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Renew Lease Agreement" || "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice of Vacate property"
                {
                    self.button1.isHidden = false
                    self.button1.setTitle("Accept", for: .normal)

                    self.button2.isHidden = false
                    self.button2.setTitle("Decline", for: .normal)
                }
                else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Rent Invoice"
                {
                    self.button1.isHidden = false
                    self.button1.setTitle("Received Receipt", for: .normal)

                    self.button2.isHidden = false
                    self.button2.setTitle("Send Reminder To Owner", for: .normal)
                }
                else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Vacate Property"
                {
                    if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
                    {
                        self.button1.isHidden = false
                        self.button1.setTitle("Accept", for: .normal)

                        self.button2.isHidden = false
                        self.button2.setTitle("Chat With Owner", for: .normal)
                    }
                    else
                    {
                        self.button1.isHidden = false
                      
                        if "\(self.detailDict?.value(forKey: "btn_status") ?? "")" == "Yes"
                        {
                            self.button1.isEnabled = false
                            self.button1.setTitle("Accepted", for: .normal)
                        }
                        else
                        {
                            self.button1.isEnabled = true
                            self.button1.setTitle("Accept", for: .normal)
                        }
                        
                        self.button2.isHidden = false
                        self.button2.setTitle("Chat With Tenant", for: .normal)
                    }
                }
                else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Inspect property" || "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Inspect property"
                {
                    if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
                    {
                        self.button1.isHidden = false
                        self.button1.setTitle("Accept", for: .normal)

                        self.button2.isHidden = false
                        self.button2.setTitle("Reschedule", for: .normal)
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
            
        self.mainView.layer.cornerRadius = 40
        
        self.button1.layer.cornerRadius = 10
        self.button2.layer.cornerRadius = 10
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.getPropertyDetail()
        self.getRentalInfo(propertyID: "\(self.detailDict?.value(forKey: "property_id") ?? "")")
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionButtonClicked(_ sender: UIButton)
    {
        if sender.currentTitle == "Received, Confirm Receipt"
        {
            print("Received, Confirm Receipt")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let dateVal = dateFormatter.date(from: "\(self.rentInvoiceDict?.value(forKey: "start_month") ?? "")".components(separatedBy: "T").first ?? "")
            
            dateFormatter.dateFormat = "MMMM"
            let monthVal = dateFormatter.string(from: dateVal!)
            
            self.sendingNotifications(title: "Rent Receipt", msg: "\(AppUtility.getUserData(keyVal: "name")) has issued the rent receipt for month \(monthVal) of the property \(self.detailDict?.value(forKey: "property_name") ?? "")")
        }
        else if sender.currentTitle == "Send Reminder To Tenant"
        {
            print("Send Reminder To Tenant")
        }
        else if sender.currentTitle == "Issue Rent Receipt"
        {
            if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Rent Receipt"
            {
                print("Request to Rent Receipt")
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMMM"
                
                self.sendingNotifications(title: "Request to Rent Receipt", msg: "Your Tenant \(AppUtility.getUserData(keyVal: "name")) of Property \(self.detailDict?.value(forKey: "property_name") ?? "") has requested a receipt being the Rent paid for Month \(dateFormatter.string(from: Date()))")
            }
        }
        else if sender.currentTitle == "Will Do It Later"
        {
            print("Will Do It Later")
        }
        else if sender.currentTitle == "Accept"
        {
            // Notice To Vacate Property
            
            if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
            {
                if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Vacate Property"
                {
                    self.sendingNotifications(title: "Notice of Vacate property", msg: "We will vacate the property \(self.detailDict?.value(forKey: "property_name") ?? "")")
                }
                else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Inspect property"
                {
                    self.sendingNotifications(title: "Notice to Inspect property", msg: "Confirm! You Can Inspect the Property \(self.detailDict?.value(forKey: "property_name") ?? "") on following date \(("\(self.detailDict?.value(forKey: "notification_content") ?? "")".components(separatedBy: "date ").last ?? "").components(separatedBy: " ").first ?? "")")
                }
            }
            else if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
            {
                if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Vacate Property"
                {
                    self.sendingNotifications(title: "Notice of Vacate property", msg: "Confirm, You can vacate the property \(self.detailDict?.value(forKey: "property_name") ?? "")")
                }
            }
            
        }
        else if sender.currentTitle == "Decline"
        {
            print("Decline")
        }
        else if sender.currentTitle == "Received Receipt"
        {
            print("Received Receipt")
        }
        else if sender.currentTitle == "Send Reminder To Owner"
        {
            print("Received Receipt")
        }
        else if sender.currentTitle == "Chat With Owner"
        {
            print("Chat With Owner")
        }
        else if sender.currentTitle == "Chat With Tenant"
        {
            print("Chat With Tenant")
        }
        else if sender.currentTitle == "Reschedule"
        {
            print("Reschedule")
            
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()

            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)

            datePicker.show("Request to Inspect property", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in

                if let dt = date
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                   
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.dateStr = ("\(formatter.string(from: dt))")
                    
                    self.showDateTimePicker(titleStr: "From")
                }
            }
        }
    }
    
    //MARK: - Show Date/Time Picker
    func showDateTimePicker(titleStr : String)
    {
        let datePicker = DatePickerDialog()
        let currentDate = Date()

        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

        var futureDateComponents = DateComponents()
        futureDateComponents.year = 100
        let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)

        datePicker.show("Select \(titleStr) Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .time) { (date) in

            if let dt = date
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                print("Selected Date : \(formatter.string(from: dt))")
                
                
                if titleStr == "From"
                {
                    self.fromDateTimeStr = ("\(formatter.string(from: dt))")
                    
                    self.showDateTimePicker(titleStr: "To")
                }
                else
                {
                    self.toDateTimeStr = ("\(formatter.string(from: dt))")
                    
                    self.sendingNotifications(title: "Notice to Inspect property", msg: "Reschedule, Request to Inspect property \(self.detailDict?.value(forKey: "property_name") ?? "") on following date \(self.dateStr) time between \(self.fromDateTimeStr) to \(self.toDateTimeStr)")
                    
                }
                
            }
        }
    }
    
//    //MARK: - Owner Action
//    func ownerAction()
//    {
//        print("ownerAction")
//    }
//
//    //MARK: - Tenant Action
//    func tenantAction()
//    {
//        print("tenantAction")
//    }
    
    //MARK: - Send Notification
    func sendingNotifications(title : String, msg : String)
    {
        print("sendingNotifications")
        
        if self.contactListArray!.count > 0
        {
            var contact_type = ""
            
            for currentObj in self.contactListArray!
            {
                print(currentObj)
                
                if "\(self.detailDict?.value(forKey: "header") ?? "")" != "Notice to Vacate Property" && "\(self.detailDict?.value(forKey: "header") ?? "")" != "Notice to Inspect property" && title != "Request to Rent Receipt" && title != "Rent Receipt" && "\(self.detailDict?.value(forKey: "header") ?? "")" != "Request to Renew Lease Agreement" && title != "Payment Reminder" && "\(self.detailDict?.value(forKey: "header") ?? "")" != "Rent invoice"
                {
                    if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
                    {
                        if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
                        {
                            self.mobileNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                        }
                    }
                    else if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
                    {
                        if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
                        {
                            self.mobileNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                        }
                    }
                }
                else
                {
                    if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
                    {
                        for currentObj in self.contactListArray!
                        {
                            print(currentObj)
                            
                            if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
                            {
                                self.mobileNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                contact_type = "Owner"
                            }
                        }
                    }
                    else if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
                    {
                        for currentObj in self.contactListArray!
                        {
                            print(currentObj)
                            
                            if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
                            {
                                self.mobileNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                contact_type = "Tenant"
                            }
                        }
                    }
                }
            }
            
            
            if Reachability.isConnectedToNetwork()
            {
                KRProgressHUD.show()
                
                let messageDict = NSMutableDictionary()
                messageDict.setValue("\(msg)", forKey: "message")
                messageDict.setValue("\(title)", forKey: "header")
                messageDict.setValue("\(AppUtility.getCurrentTS())", forKey: "time")
                messageDict.setValue("\(detailDict?.value(forKey: "property_id") ?? "")", forKey: "propertyid")
                messageDict.setValue("\(self.detailDict?.value(forKey: "propertyName") ?? "")", forKey: "property_name")
                messageDict.setValue("\(contact_type)", forKey: "role")
                messageDict.setValue("\(self.detailDict?.value(forKey: "property_type") ?? "")", forKey: "property_type")
                messageDict.setValue("\(self.detailDict?.value(forKey: "invoice_id") ?? "")" == "<null>" ? "" : "\(self.detailDict?.value(forKey: "invoice_id") ?? "")", forKey: "invoice_id") // Need To Send
                messageDict.setValue("\(mobileNumber)", forKey: "mobile_number")
                messageDict.setValue("", forKey: "pic")
                
                let paramDict = [
                    "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                    "sendingto" : (Int64("\(mobileNumber)") ?? 0), //Long
                    "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0), //Long
                    "header" : "\(title)", //String
                    "property_name" : "\(self.detailDict?.value(forKey: "propertyName") ?? "")", //String
                    "property_type" : "\(self.detailDict?.value(forKey: "property_type") ?? "")", //String
                    "notification_content" : "\(msg)", //String
                    "message" : "flag:action/\(AppUtility.getStringFromDictionary(data: messageDict))", //String
                ] as [String : Any]
                
                DispatchQueue.global(qos: .background).async {
                    WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/notificationAction", headerVal: "", parameters: paramDict) { (status, response) in
                        
                        if status
                        {
                            print("Notifications Action Response : \(response)")
                            
                            if response.count > 0
                            {
                                let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                                
                                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                    
                                }))
                                
                                DispatchQueue.main.async {
                                    self .present(alertViewController, animated: true, completion: nil)
                                }
                            }
                            else
                            {
    //                            let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
    //
    //                            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
    //
    //                            }))
    //
    //                            DispatchQueue.main.async {
    //                                self .present(alertViewController, animated: true, completion: nil)
    //                            }
                            }
                            
                        }
                        else
                        {
    //                        let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
    //
    //                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
    //
    //                        }))
    //
    //                        DispatchQueue.main.async {
    //                            self .present(alertViewController, animated: true, completion: nil)
    //                        }
                        }
                        
                        KRProgressHUD.dismiss()
                    }
                }
                
//                if mobileNumber != AppUtility.getUserProfileMobileNumber()
//                {
                    DispatchQueue.global(qos: .background).async {
                        WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/sendNotification", headerVal: "", parameters: paramDict) { (status, response) in
                            
                            if status
                            {
                                print("Send Notifications Response : \(response)")
                                
                                if response.count > 0
                                {
                                    let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                                    
                                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                        
                                    }))
                                    
                                    DispatchQueue.main.async {
                                        self .present(alertViewController, animated: true, completion: nil)
                                    }
                                }
                                else
                                {
                                    let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                                    
                                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                        
                                    }))
                                    
                                    DispatchQueue.main.async {
                                        self .present(alertViewController, animated: true, completion: nil)
                                    }
                                }
                                
                            }
                            else
                            {
                                let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                                
                                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                    
                                }))
                                
                                DispatchQueue.main.async {
                                    self .present(alertViewController, animated: true, completion: nil)
                                }
                            }
                            
                            KRProgressHUD.dismiss()
                        }
                    }
//                }
                
            }
            else
            {
                let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
                
                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                    
                }))
                
                DispatchQueue.main.async {
                    self .present(alertViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - Notification Action
    func notificationAction(title : String, msg : String, mobileNo : String)
    {
        
    }
    
    //MARK: - Property Detail Info
    func getPropertyDetail()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                "pid" : (Int64("\(self.detailDict?.value(forKey: "property_id") ?? "")") ?? 0)
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getSingleProperty", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Single Property Info Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableArray)
                        {
                            print(responseObj)
                            
                            self.contactListArray = NSMutableArray()
                            self.contactListArray = responseObj
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - Rental Info
    func getRentalInfo(propertyID : String)
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "property_id":(Int64("\(propertyID)") ?? 0), //Long
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getRentalInfo", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject).object(at: 0) as? NSDictionary)
                        {
                            print("Rental Info Response : \(responseObj)")
                            
                            if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Inspect property to Tenant" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Rent Receipt to Owner" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Payment Reminder to Owner" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice of Vacate property to Owner" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Renew Lease Agreement to Tenant" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Vacate Property to Tenant" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Rent Receipt to Owner" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Inspect property" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Property Contact" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Inspect property" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Request to Rent Receipt" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Rent Alert" ||
                                "\(self.detailDict?.value(forKey: "header") ?? "")" == "Escalation Property"
                            {
                                DispatchQueue.main.async {
                                    self.propertyNameHeaderLabel.text = "Property Name"
                                    
                                    self.rentAmountHeaderLabel.text = "Start Date"
                                    self.rentAmountLabel.text = "\((responseObj as AnyObject).value(forKey: "rent_start_date") ?? "")".components(separatedBy: "T").first ?? ""
                                
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "MMM"
                                    
                                    self.rentDueDateHeaderLabel.text = "Rent Due Date"
                                    self.rentDueDateLabel.text = "\((responseObj as AnyObject).value(forKey: "rent_due_date") ?? "") - \(dateFormatter.string(from: Date()))"
                                    
//                                    let dateFormatter = DateFormatter()
//                                    dateFormatter.dateFormat = "yyyy-MM-dd"
//
//                                    var rentStartDateComponents = DateComponents()
//                                    rentStartDateComponents.month = Int("\((responseObj as AnyObject).value(forKey: "rent_due_date") ?? "")")
//                                    let rentStartDate = Calendar.current.date(byAdding: rentStartDateComponents, to: dateFormatter.date(from: "\((responseObj as AnyObject).value(forKey: "rent_start_date") ?? "")".components(separatedBy: "T").first ?? "")!)
//
//                                    let rentStartDateformatter = DateFormatter()
//                                    rentStartDateformatter.dateFormat = "yyyy-MM-dd"
//                                    print("Rent Start Date : \(rentStartDateformatter.string(from: rentStartDate!))")
//                                    self.rentDueDateLabel.text = "\(rentStartDateformatter.string(from: rentStartDate!))"
                                }
                            }
                            else if "\(self.detailDict?.value(forKey: "header") ?? "")" == "Notice to Vacate Property"
                            {
                                DispatchQueue.main.async {
                                    self.rentAmountHeaderLabel.text = "Start Date"
                                    self.rentAmountLabel.text = "\((responseObj as AnyObject).value(forKey: "rent_start_date") ?? "")".components(separatedBy: "T").first ?? ""
                                    
                                    self.rentDueDateHeaderLabel.isHidden = true
                                    self.rentDueDateLabel.isHidden = true
                                }
                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Rent Invoice
    func getRentInvoice()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "property_id":(Int64("\(self.detailDict?.value(forKey: "property_id") ?? "")") ?? 0), //Long
                "invoice_id" : (Int64("\(self.detailDict?.value(forKey: "invoice_id") ?? "")") ?? 0), //Long
                "sqlType" : "SelectInvoiceIdWise"
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getRentInvoice", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject).object(at: 0) as? NSDictionary)
                        {
                            print("Rent Invoice Response : \(responseObj)")
                            self.rentInvoiceDict = NSDictionary()
                            self.rentInvoiceDict = responseObj
                            
//                            if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
//                            {
//                                self.ownerAction()
//                            }
//                            else if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
//                            {
//                                self.tenantAction()
//                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
}
