//
//  ActivityViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 27/01/21.
//

import UIKit
import KRProgressHUD
import SDWebImage

class ActivityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var isFromDashboard = false
    
    var propertyId = ""
    var contactType = ""
    
    
    var yourActivityArray : NSMutableArray?
    var otherActivityArray : NSMutableArray?
    var contactList : NSMutableArray?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var baseView: UIView!
 
    @IBOutlet weak var yourActivityButton: UIButton!
    @IBOutlet weak var otherActivityButton: UIButton!
    
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.baseView.backgroundColor = Constants.bgColor
        
        self.yourActivityButton.isSelected = true
        self.yourActivityButton.backgroundColor = Constants.darkBlueColor
        self.yourActivityButton.setTitleColor(.white, for: .normal)
        
        self.otherActivityButton.isSelected = false
        self.otherActivityButton.backgroundColor = .white
        self.otherActivityButton.setTitleColor(Constants.darkGrayColor, for: .normal)
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.separatorColor = .clear
        
        let activityCellNib = UINib(nibName: "ActivityTableViewCell", bundle: nil)
        self.tableViewObj.register(activityCellNib, forCellReuseIdentifier: "activityCell")
        
        self.tableViewObj.rowHeight = UITableView.automaticDimension
        self.tableViewObj.estimatedRowHeight = 100
        self.tableViewObj.backgroundColor = .clear
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.baseView.layer.cornerRadius = 40
            
            self.headerView.layer.cornerRadius = 20
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if self.isFromDashboard
        {
            self.yourActivityResponseList()
        }
        else
        {
            self.yourActivityResponse()
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func yourActivityButtonClicked(_ sender: UIButton)
    {
        self.yourActivityButton.isSelected = true
        self.yourActivityButton.backgroundColor = Constants.darkBlueColor
        self.yourActivityButton.setTitleColor(.white, for: .normal)
        
        self.otherActivityButton.isSelected = false
        self.otherActivityButton.backgroundColor = .white
        self.otherActivityButton.setTitleColor(Constants.darkGrayColor, for: .normal)
        
        if self.isFromDashboard
        {
            self.yourActivityResponseList()
        }
        else
        {
            self.yourActivityResponse()
        }
    }
    
    @IBAction func otherActivityButtonClicked(_ sender: UIButton)
    {
        self.otherActivityButton.isSelected = true
        self.otherActivityButton.backgroundColor = Constants.darkBlueColor
        self.otherActivityButton.setTitleColor(.white, for: .normal)
        
        self.yourActivityButton.isSelected = false
        self.yourActivityButton.backgroundColor = .white
        self.yourActivityButton.setTitleColor(Constants.darkGrayColor, for: .normal)
        
        if self.isFromDashboard
        {
            self.otherActivityResponseList()
        }
        else
        {
            self.otherActivityResponse()
        }
    }
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.yourActivityButton.isSelected
        {
            if self.yourActivityArray != nil
            {
                if self.yourActivityArray!.count > 0
                {
                    return self.yourActivityArray!.count
                }
            }
        }
        else if self.otherActivityButton.isSelected
        {
            if self.otherActivityArray != nil
            {
                if self.otherActivityArray!.count > 0
                {
                    return self.otherActivityArray!.count
                }
            }
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        // Swift 4.2 onwards
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {        
        let myCell = tableView.dequeueReusableCell(withIdentifier: "activityCell", for: indexPath) as! ActivityTableViewCell
        
        myCell.selectionStyle = .none
        myCell.backgroundColor = .clear
        
        DispatchQueue.main.async {
            myCell.mainView.layer.cornerRadius = 15
            myCell.mainView.backgroundColor = .white
        }
        
        if self.yourActivityButton.isSelected
        {
            myCell.messageLabel.text =  "\((self.yourActivityArray?.object(at: indexPath.row) as AnyObject).value(forKey: "notification_content") ?? "")"
           
            myCell.dateTimeLabel.text = ("\((self.yourActivityArray?.object(at: indexPath.row) as AnyObject).value(forKey: "notification_date") ?? "")".components(separatedBy: "T").first ?? "")
        }
        else
        {
            myCell.messageLabel.text =  "\((self.otherActivityArray?.object(at: indexPath.row) as AnyObject).value(forKey: "notification_content") ?? "") For The Property  \((self.otherActivityArray?.object(at: indexPath.row) as AnyObject).value(forKey: "property_name") ?? "")"
           
            myCell.dateTimeLabel.text = ("\((self.otherActivityArray?.object(at: indexPath.row) as AnyObject).value(forKey: "notification_date") ?? "")".components(separatedBy: "T").first ?? "")
        }
        
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let activityDetailsVC = ActivityDetailsViewController(nibName: "ActivityDetailsViewController", bundle: nil)
        activityDetailsVC.contactType = self.contactType
        
        if self.yourActivityButton.isSelected
        {
            activityDetailsVC.detailDict = (self.yourActivityArray?.object(at: indexPath.row) as? NSDictionary)
        }
        else
        {
            activityDetailsVC.detailDict = (self.otherActivityArray?.object(at: indexPath.row) as? NSDictionary)
        }
        
        self.navigationController?.pushViewController(activityDetailsVC, animated: true)
    }
    
    //MARK: - Your's Activity Response
    func yourActivityResponse()
    {
        print("yourActivityResponse")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "pid":(Int64("\(self.propertyId)") ?? 0), //Long
                "sqlType" : "SelectAllSentData"
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getNotifications", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject) as? NSMutableArray)
                        {
                            print("Your Activity Response : \(responseObj)")
                          
                            self.yourActivityArray = NSMutableArray()
                            self.yourActivityArray = responseObj
                            
                            DispatchQueue.main.async {
                                self.tableViewObj.reloadData()
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.tableViewObj.reloadData()
                        }
                        
                        let alertViewController = UIAlertController(title: "No Data", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    func yourActivityResponseList()
    {
        print("yourActivityResponseList")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "sqlType" : "SelectAllPropertySentData"
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getNotifications", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject) as? NSMutableArray)
                        {
                            print("Your Activity List Response : \(responseObj)")
                          
                            self.yourActivityArray = NSMutableArray()
                            self.yourActivityArray = responseObj
                            
//                            self.getPropertyDetail(propertyID: "0")
                            
                            DispatchQueue.main.async {
                                self.tableViewObj.reloadData()
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.tableViewObj.reloadData()
                        }
                        
                        let alertViewController = UIAlertController(title: "No Data", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Other's Activity Response
    func otherActivityResponse()
    {
        print("otherActivityResponse")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "pid":(Int64("\(self.propertyId)") ?? 0), //Long
                "sqlType" : "SelectAllReceivedData"
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getNotifications", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject) as? NSMutableArray)
                        {
                            print("Other Activity Response : \(responseObj)")
                          
                            self.otherActivityArray = NSMutableArray()
                            self.otherActivityArray = responseObj
                            
                            DispatchQueue.main.async {
                                self.tableViewObj.reloadData()
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.tableViewObj.reloadData()
                        }
                        
                        let alertViewController = UIAlertController(title: "No Data", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
//                            DispatchQueue.main.async {
//                                self.navigationController?.popViewController(animated: true)
//                            }
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    func otherActivityResponseList()
    {
        print("otherActivityResponseList")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "sqlType" : "SelectAllPropertyReceivedData"
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getNotifications", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject) as? NSMutableArray)
                        {
                            print("Other Activity List Response : \(responseObj)")
                          
                            self.otherActivityArray = NSMutableArray()
                            self.otherActivityArray = responseObj
                            
//                            self.getPropertyDetail(propertyID: "0")
                            
                            DispatchQueue.main.async {
                                self.tableViewObj.reloadData()
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.tableViewObj.reloadData()
                        }
                        
                        let alertViewController = UIAlertController(title: "No Data", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
//                            DispatchQueue.main.async {
//                                self.navigationController?.popViewController(animated: true)
//                            }
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
}
