//
//  ActivityTableViewCell.swift
//  RentStory
//
//  Created by Hardik Pithadia on 27/01/21.
//

import UIKit

class ActivityTableViewCell: UITableViewCell
{
    @IBOutlet weak var mainView: UIView!

    @IBOutlet weak var imageViewObj: UIImageView!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
