//
//  RentHistoryTableViewCell.swift
//  RentStory
//
//  Created by Hardik Pithadia on 25/01/21.
//

import UIKit

class RentHistoryTableViewCell: UITableViewCell
{
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblRentAmount: UILabel!
    @IBOutlet weak var lblRentDueDate: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblRentPeriod: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
