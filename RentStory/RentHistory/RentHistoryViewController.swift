//
//  RentHistoryViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 25/01/21.
//

import UIKit
import KRProgressHUD

class RentHistoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource
{
    var propertyID = ""
    var propertyName = ""
    var createByName = ""
    
    var yearArray : NSMutableArray?
    var rentHistoryArray : NSMutableArray?
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var yearView: UIView!
    
    @IBOutlet weak var propertyNameLabel: UILabel!
    @IBOutlet weak var headerNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
  
    @IBOutlet weak var tableViewObj: UITableView!
    @IBOutlet weak var yearCollectionView: UICollectionView!
    
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.yearArray = NSMutableArray()
        self.yearArray?.add("2019")
        self.yearArray?.add("2020")
        self.yearArray?.add("2021")
        
        self.mainView.backgroundColor = Constants.bgColor
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.separatorColor = .clear
        self.tableViewObj.backgroundColor = .clear
        
        let rentHistoryTableCell = UINib(nibName: "RentHistoryTableViewCell", bundle: nil)
        self.tableViewObj.register(rentHistoryTableCell, forCellReuseIdentifier: "rentHistoryCell")
        
        self.yearCollectionView.dataSource = self
        self.yearCollectionView.delegate = self
        
        let propertyTypeCellNib = UINib(nibName: "NewPropertyTypeCollectionViewCell", bundle: nil)
        self.yearCollectionView.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.yearCollectionView.showsHorizontalScrollIndicator = false
        
        self.propertyNameLabel.text = "\(self.propertyName)"
        self.nameLabel.text = "\(self.createByName)"
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.rentHistoryResponse()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.mainView.layer.cornerRadius = 35
        self.yearView.layer.cornerRadius = 25
        self.yearCollectionView.clipsToBounds = true
    }

    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - CollectionView DataSource & Delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if self.yearArray != nil
        {
            if self.yearArray!.count > 0
            {
                return self.yearArray!.count
            }
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
        
        DispatchQueue.main.async {
            myCell.titleButton.layer.cornerRadius = 10
            myCell.titleButton.layer.masksToBounds = true

            myCell.titleButton.titleLabel?.font = UIFont(name: "SofiaPro", size: 20)
                
            if (self.yearArray!.count - 1) == indexPath.item
            {
                myCell.titleButton.backgroundColor = Constants.darkBlueColor
                myCell.titleButton.setTitleColor(.white, for: .normal)
            }
            else
            {
                myCell.titleButton.backgroundColor = .white
                myCell.titleButton.setTitleColor(.darkGray, for: .normal)
            }
        }
        
        myCell.titleButton.setTitle("\(self.yearArray?.object(at: indexPath.item) ?? "")", for: .normal)
        myCell.titleButton.tag = indexPath.item
        //            myCell.titleButton.addTarget(self, action: #selector(newPropertyTypeButtonClicked(sender:)), for: .touchUpInside)
        
        return myCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 120.0, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 15, left: 10, bottom: 5, right: 10)
    }
    
    //MARK: - TableView DataSource & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.rentHistoryArray != nil
        {
            if self.rentHistoryArray!.count > 0
            {
                return self.rentHistoryArray!.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 260
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = (tableView.dequeueReusableCell(withIdentifier: "rentHistoryCell", for: indexPath) as! RentHistoryTableViewCell)

        myCell.selectionStyle = .none
        
        myCell.lblRentPeriod.text = "\("\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "start_month") ?? "")".components(separatedBy: "T").first ?? "") To \("\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "end_month") ?? "")".components(separatedBy: "T").first ?? "")"
        
        myCell.lblPaymentStatus.text = "\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "payment_status") ?? "")"
        
        myCell.lblRentAmount.text = "\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "monthly_rent") ?? "")"
        
        myCell.lblRentDueDate.text =  "\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "start_month") ?? "")".components(separatedBy: "T").first ?? ""
        
        myCell.lblNote.text = "\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "payment_status") ?? "")"
        
        myCell.statusButton.setTitle("\((self.rentHistoryArray?.object(at: indexPath.row) as AnyObject).value(forKey: "payment_mode") ?? "")", for: .normal)

        DispatchQueue.main.async {
            myCell.baseView.backgroundColor = .white
            myCell.baseView.layer.cornerRadius = 25
            myCell.baseView.layer.masksToBounds = true
        }
        
        return myCell
    }
    
    //MARK: - Get Rent History Response
    func rentHistoryResponse()
    {
        print("rentHistoryResponse")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "property_id":(Int64("\(self.propertyID)") ?? 0), //Long
                "sqlType" : "SelectAllRentInvoice"
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getRentInvoice", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject) as? NSMutableArray)
                        {
                            print("Rental History Response : \(responseObj)")
                          
                            self.rentHistoryArray = NSMutableArray()
                            self.rentHistoryArray = responseObj
                            
                            DispatchQueue.main.async {
                                self.tableViewObj.reloadData()
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.tableViewObj.reloadData()
                        }
                        
                        let alertViewController = UIAlertController(title: "No Data", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
}


