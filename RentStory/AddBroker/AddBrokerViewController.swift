//
//  AddBrokerViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 21/01/21.
//

import UIKit
import KRProgressHUD
import AWSS3
import PromiseKit
import AddressBook
import AddressBookUI
import Contacts
import ContactsUI
import DatePickerDialog
import SDWebImage
import MobileCoreServices

class AddBrokerViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ABPeoplePickerNavigationControllerDelegate, CNContactPickerDelegate, UIDocumentPickerDelegate
{
    var isEditBroker = false
    
    var isProfileSelected = false
    var isAadharSelected = false
    var isPanSelected = false
    
    var brokerProfileImagePath = ""
    var brokerAadharImagePath = ""
    var brokerPanImagePath = ""
    var genderString = ""
    var isTenantTemporary = ""
    var propertyIdStr = ""
    var propertyName = ""
    var brokerTypeStr = ""
    
    var companyInfoArray = [String]()
    var bankInfoArray = [String]()
    
    var brokerAadharImage : UIImage?
    var brokerPanImage : UIImage?
    
    var detailDict : NSDictionary?
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet var brokerView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var txtAddress1: UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtAadhar: UITextField!
    @IBOutlet weak var txtPan: UITextField!
    
    
    @IBOutlet weak var aadharButton: UIButton!
    @IBOutlet weak var panButton: UIButton!
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    @IBOutlet weak var ownerButton: UIButton!
    @IBOutlet weak var tenantButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    
    
    @IBOutlet weak var topHeaderLabel: UILabel!
    
    @IBOutlet weak var mobileLabelTop: NSLayoutConstraint!
    @IBOutlet weak var mobileLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var phoneBookButton: UIButton!
    
    @IBOutlet weak var mobileHeight: NSLayoutConstraint!
    @IBOutlet weak var mobileTop: NSLayoutConstraint!
    
    @IBOutlet weak var profileButton: UIButton!
    
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.brokerView.backgroundColor = .clear
        self.brokerView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
        self.mainScrollView.addSubview(self.brokerView)
        
        self.mainScrollView.contentSize.height = self.brokerView.frame.height
        
        self.txtMobileNumber.delegate = self
        let txtMobileNumberViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtMobileNumber.leftView = txtMobileNumberViewObj
        self.txtMobileNumber.leftViewMode = .always
        
        self.txtFirstName.delegate = self
        let txtFirstNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFirstName.leftView = txtFirstNameViewObj
        self.txtFirstName.leftViewMode = .always
        
        self.txtLastName.delegate = self
        let txtLastNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLastName.leftView = txtLastNameViewObj
        self.txtLastName.leftViewMode = .always
        
        self.txtEmail.delegate = self
        let txtEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtEmail.leftView = txtEmailViewObj
        self.txtEmail.leftViewMode = .always
        
        self.txtDOB.delegate = self
        let txtDOBViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtDOB.leftView = txtDOBViewObj
        self.txtDOB.leftViewMode = .always
        
        self.txtCompany.delegate = self
        let txtCompanyViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCompany.leftView = txtCompanyViewObj
        self.txtCompany.leftViewMode = .always
        
        self.txtAddress1.delegate = self
        let txtAddress1ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress1.leftView = txtAddress1ViewObj
        self.txtAddress1.leftViewMode = .always
        
        self.txtAddress2.delegate = self
        let txtAddress2ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress2.leftView = txtAddress2ViewObj
        self.txtAddress2.leftViewMode = .always
        
        self.txtCity.delegate = self
        let txtCityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCity.leftView = txtCityViewObj
        self.txtCity.leftViewMode = .always
        
        self.txtZipCode.delegate = self
        let txtZipCodeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtZipCode.leftView = txtZipCodeViewObj
        self.txtZipCode.leftViewMode = .always
        
        self.txtState.delegate = self
        let txtStateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtState.leftView = txtStateViewObj
        self.txtState.leftViewMode = .always
        
        self.txtCountry.delegate = self
        let txtCountryViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCountry.leftView = txtCountryViewObj
        self.txtCountry.leftViewMode = .always
        
        self.txtAadhar.delegate = self
        let txtAadharViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAadhar.leftView = txtAadharViewObj
        self.txtAadhar.leftViewMode = .always
        
        self.txtPan.delegate = self
        let txtPanViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtPan.leftView = txtPanViewObj
        self.txtPan.leftViewMode = .always
        
        if self.detailDict != nil
        {
            self.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
            self.brokerProfileImagePath = "\(self.detailDict?.value(forKey: "contact_photo") ?? "")"
            self.isProfileSelected = ("\(self.detailDict?.value(forKey: "contact_photo") ?? "")".isEmpty == true) ? false : true
            
            let aadharImgView = UIImageView()
            aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "adhar_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.aadharButton.setImage(aadharImgView.image, for: .normal)
            self.brokerAadharImagePath = "\(self.detailDict?.value(forKey: "adhar_doc") ?? "")"
            self.isAadharSelected = ("\(self.detailDict?.value(forKey: "adhar_doc") ?? "")".isEmpty == true) ? false : true
            self.aadharButton.imageView?.contentMode = .scaleAspectFill
            self.aadharButton.layer.masksToBounds = true
            
            let panImgView = UIImageView()
            panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.panButton.setImage(panImgView.image, for: .normal)
            self.brokerPanImagePath = "\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")"
            self.isPanSelected = ("\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")".isEmpty == true) ? false : true
            self.panButton.imageView?.contentMode = .scaleAspectFill
            self.panButton.layer.masksToBounds = true
            
            self.txtMobileNumber.text = "\(self.detailDict?.value(forKey: "mobileno") ?? "")"
            self.txtFirstName.text = "\(self.detailDict?.value(forKey: "firstName") ?? "")"
            self.txtLastName.text = "\(self.detailDict?.value(forKey: "lastName") ?? "")"
            self.txtEmail.text = "\(self.detailDict?.value(forKey: "email") ?? "")"
            self.txtDOB.text = "\(self.detailDict?.value(forKey: "dob") ?? "")"
            
            self.txtAadhar.text = "\(self.detailDict?.value(forKey: "adhar_number") ?? "")"
            self.txtPan.text = "\(self.detailDict?.value(forKey: "pan_number") ?? "")"
            
            if "\(self.detailDict?.value(forKey: "gender") ?? "")".lowercased() == "male"
            {
                self.genderString = "male"
                self.maleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.maleButton.isSelected = true
                
            }
            else if "\(self.detailDict?.value(forKey: "gender") ?? "")".lowercased() == "female"
            {
                self.genderString = "female"
                self.femaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.femaleButton.isSelected = true
            }
            else
            {
                self.genderString = "other"
                self.otherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.otherButton.isSelected = true
            }
            
            if "\(self.detailDict?.value(forKey: "broker_type") ?? "")" == "Owner"
            {
                self.brokerTypeStr = "Owner"
                self.ownerButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.ownerButton.isSelected = true
                
            }
            else if "\(self.detailDict?.value(forKey: "broker_type") ?? "")" == "Tenant"
            {
                self.brokerTypeStr = "Tenant"
                self.tenantButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.tenantButton.isSelected = true
            }
            else
            {
                self.brokerTypeStr = "Both"
                self.bothButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.bothButton.isSelected = true
            }
            
            if "\(self.detailDict?.value(forKey: "companyinfo") ?? "")".isEmpty != true
            {
                if "\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",").count > 0
                {
                    self.txtCompany.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[0]))"
                    self.txtAddress1.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[1]))"
                    self.txtAddress2.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[2]))"
                    self.txtCity.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[3]))"
                    self.txtZipCode.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[4]))"
                    self.txtState.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[5]))"
                    self.txtCountry.text = "\(("\(self.detailDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[6]))"
                }
                
            }
            
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if self.isEditBroker
        {
            self.topHeaderLabel.text = "Edit Broker"
            
            self.mobileLabelTop.constant = 0
            self.mobileLabelHeight.constant = 0
            self.mobileTop.constant = 0
            self.mobileHeight.constant = 0
            self.phoneBookButton.isHidden = true
        }
        else
        {
            self.topHeaderLabel.text = "Add Broker"
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profileButtonClicked(_ sender: UIButton)
    {
        self.isProfileSelected = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func phoneButtonClicked(_ sender: UIButton)
    {
        let peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        
        DispatchQueue.main.async {
            self.present(peoplePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func aadharButtonClicked(_ sender: UIButton)
    {
        self.isAadharSelected = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func panButtonClicked(_ sender: UIButton)
    {
        self.isPanSelected = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func maleButtonClicked(_ sender: UIButton)
    {
        self.femaleButton.isSelected = false
        self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.otherButton.isSelected = false
        self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.maleButton.isSelected
        {
            self.maleButton.isSelected = false
            self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderString = ""
        }
        else
        {
            self.maleButton.isSelected = true
            self.maleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderString = "male"
        }
    }
    
    @IBAction func femaleButtonClicked(_ sender: UIButton)
    {
        self.maleButton.isSelected = false
        self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.otherButton.isSelected = false
        self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.femaleButton.isSelected
        {
            self.femaleButton.isSelected = false
            self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderString = ""
        }
        else
        {
            self.femaleButton.isSelected = true
            self.femaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderString = "female"
        }
    }
    
    @IBAction func otherButtonClicked(_ sender: UIButton)
    {
        self.maleButton.isSelected = false
        self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.femaleButton.isSelected = false
        self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.otherButton.isSelected
        {
            self.otherButton.isSelected = false
            self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderString = ""
        }
        else
        {
            self.otherButton.isSelected = true
            self.otherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderString = "other"
        }
    }
    
    @IBAction func ownerButtonClicked(_ sender: UIButton)
    {
        self.tenantButton.isSelected = false
        self.tenantButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.bothButton.isSelected = false
        self.bothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.ownerButton.isSelected
        {
            self.ownerButton.isSelected = false
            self.ownerButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerTypeStr = ""
        }
        else
        {
            self.ownerButton.isSelected = true
            self.ownerButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerTypeStr = "Owner"
        }
    }
    
    @IBAction func tenantButtonClicked(_ sender: UIButton)
    {
        self.ownerButton.isSelected = false
        self.ownerButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.bothButton.isSelected = false
        self.bothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.tenantButton.isSelected
        {
            self.tenantButton.isSelected = false
            self.tenantButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerTypeStr = ""
        }
        else
        {
            self.tenantButton.isSelected = true
            self.tenantButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerTypeStr = "Tenant"
        }
    }
    
    @IBAction func bothButtonClicked(_ sender: UIButton)
    {
        self.ownerButton.isSelected = false
        self.ownerButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.tenantButton.isSelected = false
        self.tenantButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.bothButton.isSelected
        {
            self.bothButton.isSelected = false
            self.bothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerTypeStr = ""
        }
        else
        {
            self.bothButton.isSelected = true
            self.bothButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerTypeStr = "Both"
        }
    }
    
    @IBAction func submitButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertyIdStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Property Not Created", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }

        else if self.txtMobileNumber.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtFirstName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's First Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLastName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Last Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtEmail.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.genderString.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Gender", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtAddress1.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Address 1", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCity.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's City", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtZipCode.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's ZipCode", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtState.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's State", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCountry.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Country", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            
            if isEditBroker
            {
                self.updateBrokerResponse()
            }
            else
            {
                self.addBrokerDetailsResponse()
            }
        }
    }
    
    //MARK: - Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])
    {
        if urls.first != nil
        {
            if self.isAadharSelected
            {
                self.isAadharSelected = false
                
                self.brokerAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.aadharButton.setImage(self.brokerAadharImage, for: .normal)
                self.aadharButton.layer.masksToBounds = true
                self.aadharButton.imageView?.contentMode = .scaleAspectFill
                
                
                self.brokerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isPanSelected
            {
                self.isPanSelected = false
                
                self.brokerPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.panButton.setImage(self.brokerPanImage, for: .normal)
                self.panButton.imageView?.contentMode = .scaleAspectFill
                self.panButton.layer.masksToBounds = true
                
                self.brokerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if self.isProfileSelected == true
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isProfileSelected = false
                self.profileImageView.image = pickedImage
                
                self.brokerProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isAadharSelected
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isAadharSelected = false
                
                self.brokerAadharImage = pickedImage
                
                self.aadharButton.setImage(pickedImage, for: .normal)
                self.aadharButton.imageView?.contentMode = .scaleAspectFill
                self.aadharButton.layer.masksToBounds = true
                
                self.brokerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isPanSelected
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isPanSelected = false
                
                self.brokerPanImage = pickedImage
                
                self.panButton.setImage(pickedImage, for: .normal)
                self.panButton.imageView?.contentMode = .scaleAspectFill
                self.panButton.layer.masksToBounds = true
                
                self.brokerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.isProfileSelected = false
        self.isAadharSelected = false
        self.isPanSelected = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Phonebook Delegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact])
    {
        if contacts.count > 0
        {
            let selectedPhoneNumber = contacts[0].phoneNumbers[0].value.stringValue
          
            self.txtMobileNumber.text = "\(selectedPhoneNumber)"
//            self.txtFirstName.text = "\(contacts[0].givenName)"
//            self.txtLastName.text = "\(contacts[0].familyName)"
            
            self.getUserProfile()
        }
    }
    
    //MARK: - TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if self.txtDOB == textField
        {
//            textField.resignFirstResponder()
//            self.txtDOB.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtDOB.text = "\(formatter.string(from: dt))"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if self.txtMobileNumber == textField
        {
            self.getUserProfile()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if textField == self.txtMobileNumber ||  textField == self.txtAadhar
        {
            if string == ""
            {
                return true
            }
            else if (textField.text?.count)! > 11
            {
                return false
            }
            
            return true
        }
        else if textField == self.txtPan
        {
            if string == ""
            {
                return true
            }
            else if (textField.text?.count)! > 9
            {
                return false
            }
            
            return true
        }
        
        return true
    }
    
    //MARK: - Get User Profile
    func getUserProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtMobileNumber.text ?? ""))") ?? 0),
                             "searchResult":"",
                             "emailId":"",
                             "property_name":"",
                             "property_id":"",
                             "data":""
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        DispatchQueue.main.async {
                           
                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")").isEmpty != true
                            {
                                self.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
                            }
                            
                            self.profileButton.isEnabled = false
                            
                            self.txtFirstName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "firstName") ?? "")"
                            self.txtFirstName.isEnabled = false
                            
                            self.txtLastName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "lastName") ?? "")"
                            self.txtLastName.isEnabled = false
                            
                            self.txtEmail.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "email") ?? "")"
                            self.txtEmail.isEnabled = false
                            
                            self.txtDOB.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "dob") ?? "")"
                            self.txtDOB.isEnabled = false
                            
                            self.maleButton.isEnabled = false
                            self.femaleButton.isEnabled = false
                            self.otherButton.isEnabled = false
                            
                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "male"
                            {
                                self.genderString = "male"
                                self.maleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "female"
                            {
                                self.genderString = "female"
                                self.femaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else
                            {
                                self.genderString = "other"
                                self.otherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            
                            self.txtAadhar.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_no") ?? "")"
                            self.txtAadhar.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")".isEmpty != true
                            {
                                let aadharImgView = UIImageView()
                                aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
                                self.aadharButton.setBackgroundImage(aadharImgView.image, for: .normal)
                            }
                            
                            self.aadharButton.isEnabled = false
                            self.panButton.isEnabled = false
                            
                            self.txtPan.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_no") ?? "")"
                            self.txtPan.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")".isEmpty != true
                            {
                                let panImgView = UIImageView()
                                panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
                                self.panButton.setBackgroundImage(panImgView.image, for: .normal)
                            }
                            
                            self.isTenantTemporary = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "is_temporary") ?? "")"
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                           
                            self.profileImageView.image = UIImage(named: "avatar")
                            self.brokerProfileImagePath = ""
                            self.isProfileSelected = false
                            self.profileButton.isEnabled = true
                            
                            self.txtFirstName.text = ""
                            self.txtFirstName.isEnabled = true
                            
                            self.txtLastName.text = ""
                            self.txtLastName.isEnabled = true
                            
                            self.txtEmail.text = ""
                            self.txtEmail.isEnabled = true
                            
                            self.txtDOB.text = ""
                            self.txtDOB.isEnabled = true
                            
                            self.txtAadhar.text = ""
                            self.txtAadhar.isEnabled = true
                            
                            self.txtPan.text = ""
                            self.txtPan.isEnabled = true
                            
                            self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.maleButton.isSelected = false
                            self.maleButton.isEnabled = true
                            
                            self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.femaleButton.isSelected = false
                            self.femaleButton.isEnabled = true
                            
                            self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.otherButton.isSelected = false
                            self.otherButton.isEnabled = true
                            
                            self.aadharButton.setBackgroundImage(UIImage(named: ""), for: .normal)
                            self.aadharButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.aadharButton.isSelected = false
                            self.aadharButton.isEnabled = true
                            
                            self.panButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.panButton.isSelected = false
                            self.panButton.isEnabled = true
                            
                            self.isTenantTemporary = "0"
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Upload Registration Images
    func uploadImageToServer(uploadImg : UIImage, imageName : String)
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: uploadImg, imageName: "\(imageName)")
            
            req.done { (url) in
                print("Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Add Broker Details
    func addBrokerDetailsResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.brokerProfileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.profileImageView.image!, imageName: "\(self.brokerProfileImagePath)")
            }
            
            if self.brokerAadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerAadharImage!, imageName: "\(self.brokerAadharImagePath)")
            }
            
            if self.brokerPanImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerPanImage!, imageName: "\(self.brokerPanImagePath)")
            }
            
            KRProgressHUD.show()
            
            self.companyInfoArray.append("\(self.txtCompany.text ?? "")")
            self.companyInfoArray.append("\(self.txtAddress1.text ?? "")")
            self.companyInfoArray.append("\(self.txtAddress2.text ?? "")")
            self.companyInfoArray.append("\(self.txtCity.text ?? "")")
            self.companyInfoArray.append("\(self.txtZipCode.text ?? "")")
            self.companyInfoArray.append("\(self.txtState.text ?? "")")
            self.companyInfoArray.append("\(self.txtCountry.text ?? "")")
            
            self.bankInfoArray.append("")
            
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_type":"Broker", //String
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno":(Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtMobileNumber.text ?? ""))") ?? 0) as Any, //Long
                "firstName":"\(self.txtFirstName.text ?? "")", //String
                "lastName":"\(self.txtLastName.text ?? "")", //String
                "email":"\(self.txtEmail.text ?? "")", //String
                "adhar_doc":"\(self.brokerAadharImagePath)", //String
                "pan_card_doc":"\(self.brokerPanImagePath)", //String
                "contact_photo":"\(self.brokerProfileImagePath)", //String
                "companyinfo":self.companyInfoArray, //String Array
                "Nationality":"",
                "istenantbroker":false, //Boolean
                "iscompanyoccupant":false, //Boolean
                "bank_info":self.bankInfoArray, //String Array
                "dob":"\(self.txtDOB.text ?? "")", //String
                "pan_number":"\(self.txtPan.text ?? "")", //String
                "adhar_number":"\(self.txtAadhar.text ?? "")", //String
                "gender":"\(self.genderString)", //String
                "doa":"", //String
                "propertyName":"\(self.propertyName)", //String
                "property_progress_state":Int(0), //Int
                "is_temporary":"\(self.isTenantTemporary)", //String
                "broker_type":"\(self.brokerTypeStr)"
            ] as [String : Any]
            
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/addPropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Update Tenant Response
    func updateBrokerResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.brokerProfileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.profileImageView.image!, imageName: self.brokerProfileImagePath)
            }
            
            if self.brokerAadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerAadharImage!, imageName: self.brokerAadharImagePath)
            }
            
            if self.brokerPanImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerPanImage!, imageName: self.brokerPanImagePath)
            }
            
            self.companyInfoArray.append("\(self.txtCompany.text ?? "")")
            self.companyInfoArray.append("\(self.txtAddress1.text ?? "")")
            self.companyInfoArray.append("\(self.txtAddress2.text ?? "")")
            self.companyInfoArray.append("\(self.txtCity.text ?? "")")
            self.companyInfoArray.append("\(self.txtZipCode.text ?? "")")
            self.companyInfoArray.append("\(self.txtState.text ?? "")")
            self.companyInfoArray.append("\(self.txtCountry.text ?? "")")
            
            KRProgressHUD.show()

            
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_id" : Int64("\(self.detailDict?.value(forKey: "contact_id") ?? "")") as Any, //Long
                "contact_type" : "Broker", //String
                "mobileno" : (Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno" : (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtMobileNumber.text ?? ""))") ?? 0) as Any, //Long
                "firstName" : "\(self.txtFirstName.text ?? "")", //String
                "lastName" : "\(self.txtLastName.text ?? "")", //String
                "email" : "\(self.txtEmail.text ?? "")", //String
                "Nationality" : "", //String
                "adhar_doc" : "\(self.brokerAadharImagePath)", //String
                "pan_card_doc" : "\(self.brokerPanImagePath)", //String
                "contact_photo" : "\(self.brokerProfileImagePath)", //String
                "companyinfo" : self.companyInfoArray, //String Array
                "istenantbroker" : false, //Boolean
                "iscompanyoccupant" : false, //Boolean
//                "bank_info" : [String](), //String Array
                "dob" : "\(self.txtDOB.text ?? "")", //String
                "pan_number" : "\(self.txtPan.text ?? "")", //String
                "adhar_number" : "\(self.txtAadhar.text ?? "")", //String
                "gender" : "\(self.genderString)", //String
                "doa" : "", //String
                "propertyName" : "\(self.propertyName)", //String
                "property_progress_state" : Int(0), //Int
                "is_temporary" : "\(self.detailDict?.value(forKey: "is_temporary") ?? "")", //String
                "broker_type" : "\(self.detailDict?.value(forKey: "broker_type") ?? "")", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updatePropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    

    
}
