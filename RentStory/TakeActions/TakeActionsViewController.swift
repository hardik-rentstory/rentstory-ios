//
//  TakeActionsViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 08/03/21.
//

import UIKit
import DatePickerDialog
import KRProgressHUD

class TakeActionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var alertTitleVal = ""
    var alertMsg = ""
    
    var fromDateTimeStr = ""
    var toDateTimeStr = ""
    var dateStr = ""
    var endDateStr = ""
    
    var tableArray : NSMutableArray?
    var detailDict : NSDictionary?
    var contactListArray : NSMutableArray?
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var tableViewObj: UITableView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        self.baseView.backgroundColor = Constants.bgColor
        
        if self.detailDict != nil
        {
            if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
            {
                self.tableArray = NSMutableArray()
                self.tableArray?.add("Notice to Inspect property")
                self.tableArray?.add("Notice to Vacate Property")
                
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                
                let currentDateStr = "\(formatter.string(from: Date()))"
                let currentDate = formatter.date(from: currentDateStr)!
                let endDate = formatter.date(from: endDateStr)!

                let diffs = Calendar.current.dateComponents([.year, .month, .day], from: currentDate, to: endDate)
                
                if diffs.day! <= 30
                {
                    self.tableArray?.add("Request to Renew Lease Agreement")
                }
            }
            else if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant"
            {
                self.tableArray = NSMutableArray()
                self.tableArray?.add("Request to Rent Receipt")
                self.tableArray?.add("Notice to Vacate Property")
                self.tableArray?.add("Request to Inspect property")
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                
                let currentDateStr = "\(formatter.string(from: Date()))"
                let currentDate = formatter.date(from: currentDateStr)!
                let endDate = formatter.date(from: endDateStr)!

                let diffs = Calendar.current.dateComponents([.year, .month, .day], from: currentDate, to: endDate)
                
                if diffs.day! <= 30
                {
                    self.tableArray?.add("Request to Renew Lease Agreement")
                }
            }
        }
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.backgroundColor = .clear
        self.tableViewObj.separatorColor = .white
        
        let myNib = UINib(nibName: "UserProfileTableViewCell", bundle: nil)
        self.tableViewObj.register(myNib, forCellReuseIdentifier: "userProfileCell")
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.baseView.layer.cornerRadius = 40
        self.baseView.layer.masksToBounds = true
        
        self.tableViewObj.layer.cornerRadius = 40
        self.tableViewObj.layer.masksToBounds = true
        
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.tableArray != nil
        {
            if self.tableArray!.count > 0
            {
                return self.tableArray!.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "userProfileCell", for: indexPath) as! UserProfileTableViewCell
        
        myCell.selectionStyle = .none
        
        myCell.nameLabel.text = "\(self.tableArray?.object(at: indexPath.row) ?? "")"
        
        DispatchQueue.main.async {
            myCell.mainView.layer.cornerRadius = 20
            myCell.mainView.backgroundColor = .white
        }
        
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if "\(self.tableArray?.object(at: indexPath.row) ?? "")" == "Notice to Inspect property"
        {
            print("Notice to Inspect property")
            
            self.alertTitleVal = "Notice to Inspect property"
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()

            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)

            datePicker.show("Notice to Inspect property", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in

                if let dt = date
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                   
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.dateStr = ("\(formatter.string(from: dt))")
                    
                    self.showDateTimePicker(titleStr: "From")
                }
            }
        }
        else if "\(self.tableArray?.object(at: indexPath.row) ?? "")" == "Notice to Vacate Property"
        {
            print("Notice to Vacate Property")
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()

            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)

            datePicker.show("Notice to Vacate Property", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in

                if let dt = date
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                   
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.dateStr = ("\(formatter.string(from: dt))")
                    
                    if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
                    {
                        self.showAlert(title: "Notice to Vacate Property", message: "Please Vacate Property \(self.detailDict?.value(forKey: "propertyName") ?? "") on date \(self.dateStr)", role: "Tenant")
                    }
                    else
                    {
                        self.showAlert(title: "Notice to Vacate Property", message: "Please Vacate Property \(self.detailDict?.value(forKey: "propertyName") ?? "") on date \(self.dateStr)", role: "Owner")
                    }
                }
            }
        }
        else if "\(self.tableArray?.object(at: indexPath.row) ?? "")" == "Request to Renew Lease Agreement"
        {
            print("Request to Renew Lease Agreement")
            
            self.alertTitleVal = "Request to Renew Lease Agreement"
            self.alertMsg = "\(AppUtility.getUserData(keyVal: "name")) has request to renew the lease agreement of the Property \(self.detailDict?.value(forKey: "propertyName") ?? "")"
            
            if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
            {
                self.showAlert(title: "\(self.alertTitleVal)", message: "\(self.alertMsg)", role: "Tenant")
            }
            else
            {
                self.showAlert(title: "\(self.alertTitleVal)", message: "\(self.alertMsg)", role: "Owner")
            }
        }
        else if "\(self.tableArray?.object(at: indexPath.row) ?? "")" == "Request to Rent Receipt"
        {
            print("Request to Rent Receipt")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM"
            
            self.alertTitleVal = "Request to Rent Receipt"
            self.alertMsg = "Your Tenant \(AppUtility.getUserData(keyVal: "name")) of Property \(self.detailDict?.value(forKey: "propertyName") ?? "") has requested a receipt of Rs.\("\(self.detailDict?.value(forKey: "monthly_rent") ?? "")" == "<null>" ? "" : self.detailDict?.value(forKey: "monthly_rent") ?? "") being the Rent paid for Month \(dateFormatter.string(from: Date()))"
            
            if "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner"
            {
                self.showAlert(title: "\(self.alertTitleVal)", message: "\(self.alertMsg)", role: "Tenant")
            }
            else
            {
                self.showAlert(title: "\(self.alertTitleVal)", message: "\(self.alertMsg)", role: "Owner")
            }
        }
        else if "\(self.tableArray?.object(at: indexPath.row) ?? "")" == "Request to Inspect property"
        {
            print("Request to Inspect property")
            self.alertTitleVal = "Request to Inspect property"
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()

            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)

            datePicker.show("Request to Inspect property", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in

                if let dt = date
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                   
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.dateStr = ("\(formatter.string(from: dt))")
                    
                    self.showDateTimePicker(titleStr: "From")
                }
            }
        }
    }
    
    //MARK: - Show Date/Time Picker
    func showDateTimePicker(titleStr : String)
    {
        let datePicker = DatePickerDialog()
        let currentDate = Date()

        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

        var futureDateComponents = DateComponents()
        futureDateComponents.year = 100
        let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)

        datePicker.show("Select \(titleStr) Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .time) { (date) in

            if let dt = date
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                print("Selected Date : \(formatter.string(from: dt))")
                
                
                if titleStr == "From"
                {
                    self.fromDateTimeStr = ("\(formatter.string(from: dt))")
                    
                    self.showDateTimePicker(titleStr: "To")
                }
                else
                {
                    self.toDateTimeStr = ("\(formatter.string(from: dt))")
                    
                    if self.alertTitleVal == "Notice to Inspect property"
                    {
                        self.showAlert(title: "Notice to Inspect property", message: "We are coming for Inspection of Property \(self.detailDict?.value(forKey: "propertyName") ?? "") on following date \(self.dateStr) time between \(self.fromDateTimeStr) to \(self.toDateTimeStr)", role: "Tenant")
                    }
                    else
                    {
                        self.showAlert(title: "Request to Inspect property", message: "Request to Inspect property \(self.detailDict?.value(forKey: "propertyName") ?? "") on following date \(self.dateStr) time between \(self.fromDateTimeStr) to \(self.toDateTimeStr)", role: "Owner")
                    }
                    
                }
                
            }
        }
    }
    
    //MARK: - Show Alert
    func showAlert(title : String, message : String, role : String)
    {
        let alertViewController = UIAlertController(title: "\(title)", message: "\(message)", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
           
            self.alertTitleVal = "\(title)"
            self.alertMsg = "\(message)"
            
            if self.alertTitleVal == "Request to Renew Lease Agreement" || self.alertTitleVal == "Notice to Vacate Property"
            {
                self.sendNotification(title: "\(self.alertTitleVal) to \(role)", msg: "\(self.alertMsg)", role: "All", mobileNumber: "\(AppUtility.getUserProfileMobileNumber())")
            }
            else
            {
                self.sendNotification(title: "\(self.alertTitleVal) to \(role)", msg: "\(self.alertMsg)", role: "\(role)", mobileNumber: "\(AppUtility.getUserProfileMobileNumber())")
            }
            
            for currentObj in self.contactListArray!
            {
                print("Current Object : \(currentObj)")
                
                if "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")" != AppUtility.getUserProfileMobileNumber()
                {
                    if title == "Request to Renew Lease Agreement" || title == "Notice to Vacate Property" && "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                    {
                        self.sendNotification(title: "\(title)", msg: "\(message)", role: "\(role)", mobileNumber: "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")")
                    }
                    else if title == "Request to Inspect property" && "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Property Manager" && "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                    {
                        self.sendNotification(title: "\(title)", msg: "\(message)", role: "\(role)", mobileNumber: "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")")
                    }
                    else if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner" || "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
                    {
                        self.sendNotification(title: "\(title)", msg: "\(message)", role: "\(role)", mobileNumber: "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")")
                    }
                    
                }
            }
        }))
        
        DispatchQueue.main.async {
            self .present(alertViewController, animated: true, completion: nil)
        }
    }
    
    //MARK: - Send Notification
    func sendNotification(title : String, msg : String, role : String, mobileNumber : String)
    {
        print("sendNotification")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            var noticeSender = "sender"
            var readFlag = "0"
            
            if mobileNumber == AppUtility.getUserProfileMobileNumber()
            {
                noticeSender = "sender"
                readFlag = "1"
            }
            else
            {
                noticeSender = "receiver"
            }
            
            let messageDict = NSMutableDictionary()
            messageDict.setValue("\(msg)", forKey: "message")
            messageDict.setValue("\(title)", forKey: "header")
            messageDict.setValue("\(AppUtility.getCurrentTS())", forKey: "time")
            messageDict.setValue("\(detailDict?.value(forKey: "property_id") ?? "")", forKey: "propertyid")
            messageDict.setValue("\(self.detailDict?.value(forKey: "propertyName") ?? "")", forKey: "property_name")
            messageDict.setValue("\(role)", forKey: "role")
            messageDict.setValue("\(self.detailDict?.value(forKey: "property_type") ?? "")", forKey: "property_type")
            messageDict.setValue("\(mobileNumber)", forKey: "mobile_number")
            messageDict.setValue("", forKey: "pic")
            
            let paramDict = [
                "noticeSender" : "\(noticeSender)", //String
                "readFlag" : "\(readFlag)", //String
                "contact_type" : "\(role)", //String
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                "sendingto" : (Int64("\(mobileNumber)") ?? 0), //Long
                "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0), //Long
                "header" : "\(title)", //String
                "property_name" : "\(self.detailDict?.value(forKey: "propertyName") ?? "")", //String
                "property_type" : "\(self.detailDict?.value(forKey: "property_type") ?? "")", //String
                "notification_content" : "\(msg)", //String
                "message" : "flag:action/\(AppUtility.getStringFromDictionary(data: messageDict))", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/notificationAction", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Notifications Action Response : \(response)")
                        
                        if response.count > 0
                        {
                            let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                            
                            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                
                            }))
                            
                            DispatchQueue.main.async {
                                self .present(alertViewController, animated: true, completion: nil)
                            }
                        }
                        else
                        {
//                            let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
//
//                            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//                            }))
//
//                            DispatchQueue.main.async {
//                                self .present(alertViewController, animated: true, completion: nil)
//                            }
                        }
                        
                    }
                    else
                    {
//                        let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
//
//                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//                        }))
//
//                        DispatchQueue.main.async {
//                            self .present(alertViewController, animated: true, completion: nil)
//                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
            if mobileNumber != AppUtility.getUserProfileMobileNumber()
            {
                DispatchQueue.global(qos: .background).async {
                    WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/sendNotification", headerVal: "", parameters: paramDict) { (status, response) in
                        
                        if status
                        {
                            print("Send Notifications Response : \(response)")
                            
                            if response.count > 0
                            {
                                let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                                
                                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                    
                                }))
                                
                                DispatchQueue.main.async {
                                    self .present(alertViewController, animated: true, completion: nil)
                                }
                            }
                            else
                            {
                                let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                                
                                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                    
                                }))
                                
                                DispatchQueue.main.async {
                                    self .present(alertViewController, animated: true, completion: nil)
                                }
                            }
                            
                        }
                        else
                        {
                            let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)
                            
                            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                                
                            }))
                            
                            DispatchQueue.main.async {
                                self .present(alertViewController, animated: true, completion: nil)
                            }
                        }
                        
                        KRProgressHUD.dismiss()
                    }
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
}
