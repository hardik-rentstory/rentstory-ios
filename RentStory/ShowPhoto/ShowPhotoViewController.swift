//
//  ShowPhotoViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 08/02/21.
//

import UIKit
import SDWebImage

class ShowPhotoViewController: UIViewController
{
    var photoURL = ""
    
    @IBOutlet weak var imageViewObj: UIImageView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear.withAlphaComponent(0.8)
        
        self.imageViewObj.sd_setImage(with: (NSURL(string: "\(self.photoURL)")! as URL), placeholderImage: UIImage(named: "homeLogo"))
    }
    
    //MARK: - Button Actions
    @IBAction func crossButtonClicked(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
