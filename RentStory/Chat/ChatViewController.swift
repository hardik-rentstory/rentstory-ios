//
//  ChatViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 22/03/21.
//

import UIKit
import Firebase
import FirebaseFirestore

class ChatViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    var isGroupChat = false
    
    var senderId = ""
    var senderName = ""
    var senderMob = ""
    var senderDict : NSDictionary?
    
    var receiverId = ""
    var receiverName = ""
    var receiverMob = ""
    var receiverDict : NSDictionary?
    
    var finalID = ""
    var headerString = ""

    var msgArray : NSMutableArray?
    var userDeviceDict : NSDictionary?
    
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var msgTextView: UITextView!
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    var refreshController : UIRefreshControl?
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveMessageListNotification(notification:)), name: Notification.Name("MessageNotification"), object: nil)
        
        self.msgTextView.delegate = self
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        
        self.msgTextView.layer.borderWidth = 1.0
        self.msgTextView.layer.borderColor = UIColor.init(red: 236.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0).cgColor
        self.msgTextView.layer.cornerRadius = 10
        
        let sendNib = UINib(nibName: "SendMessageTableViewCell", bundle: nil)
        self.tableViewObj.register(sendNib, forCellReuseIdentifier: "sendMessageCell")
        
        let sendImageNib = UINib(nibName: "SendImageMessageTableViewCell", bundle: nil)
        self.tableViewObj.register(sendImageNib, forCellReuseIdentifier: "sendImageCell")
        
        
        let receiveNib = UINib(nibName: "ReceiveMessageTableViewCell", bundle: nil)
        self.tableViewObj.register(receiveNib, forCellReuseIdentifier: "receiveMessageCell")
        
        let receiveImageNib = UINib(nibName: "ReceiveImageMessageTableViewCell", bundle: nil)
        self.tableViewObj.register(receiveImageNib, forCellReuseIdentifier: "receiveImageCell")
        
        self.tableViewObj.estimatedRowHeight = 100;
        self.tableViewObj.rowHeight = UITableView.automaticDimension
        self.tableViewObj.separatorColor = UIColor.clear
        self.tableViewObj.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        self.refreshController = UIRefreshControl()
        self.refreshController!.addTarget(self, action: #selector(refreshSync), for: .valueChanged)
        self.tableViewObj.addSubview(self.refreshController!)
        
        self.senderId = "\(self.senderDict?.value(forKey: "contact_id") ?? "")"
        self.senderName = "\(self.senderDict?.value(forKey: "firstName") ?? "") \(self.senderDict?.value(forKey: "lastName") ?? "")"
        self.senderMob = "\(self.senderDict?.value(forKey: "mobileno") ?? "")"
        
        self.receiverId = "\(self.receiverDict?.value(forKey: "contact_id") ?? "")"
        self.receiverName = "\(self.receiverDict?.value(forKey: "firstName") ?? "") \(self.receiverDict?.value(forKey: "lastName") ?? "")"
        self.receiverMob = "\(self.receiverDict?.value(forKey: "mobileno") ?? "")"
        
        if self.receiverName == "Group Chat"
        {
            self.isGroupChat = true
            self.finalID = "\(self.receiverDict?.value(forKey: "property_id") ?? "")"
        }
        else
        {
            self.isGroupChat = false
            self.finalID = "\(self.senderId)_\(self.receiverId)"
        }
        
//        self.updateTableContentInset()
        self.getChatMessages()
        self.getUserDeviceToken()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.lblHeader.text = self.headerString
    }
    
    func updateTableContentInset()
    {
        let numRows = self.tableViewObj.numberOfRows(inSection: 0)
        var contentInsetTop = self.tableViewObj.bounds.size.height
        for i in 0..<numRows {
            let rowRect = self.tableViewObj.rectForRow(at: IndexPath(item: i, section: 0))
            contentInsetTop -= rowRect.size.height
            if contentInsetTop <= 0 {
                contentInsetTop = 0
                break
            }
        }
        self.tableViewObj.contentInset = UIEdgeInsets(top: contentInsetTop,left: 0,bottom: 0,right: 0)
    }
    
    //MARK: - Message Notification
    @objc func receiveMessageListNotification(notification : NSNotification)
    {
        print("receiveMessageListNotification")
        
        self.getChatMessages()
    }
    
    //MARK: - Load Chat Data
    func getChatMessages()
    {
        let db = Firestore.firestore()
        self.msgArray = NSMutableArray()
        
        db.collection("Smartrental-Dev/Chats/\(self.finalID)").getDocuments { (querySnapShot, error) in
            
            if let err = error
            {
                print("Error : \(err.localizedDescription)")
            }
            else
            {
                if let snapShotDocuments = querySnapShot?.documents
                {
                    if snapShotDocuments.count > 0
                    {
                        for doc in snapShotDocuments
                        {
                            print(doc.data())
                            
                            let messageObj = Messages(
                                _id: "\("\((doc.data() as AnyObject).value(forKey: "id") ?? "")" == "<null>" ? "" : "\((doc.data() as AnyObject).value(forKey: "id") ?? "")")",
                                _read: "\((doc.data() as AnyObject).value(forKey: "read") ?? "")" == "0" ? false : true,
                                _message: "\((doc.data() as AnyObject).value(forKey: "message") ?? "")",
                                _receiver: "\((doc.data() as AnyObject).value(forKey: "receiver") ?? "")",
                                _receiverName: "\((doc.data() as AnyObject).value(forKey: "recieverName") ?? "")",
                                _sender: "\((doc.data() as AnyObject).value(forKey: "sender") ?? "")",
                                _senderName: "\((doc.data() as AnyObject).value(forKey: "senderName") ?? "")",
                                _type: "\((doc.data() as AnyObject).value(forKey: "type") ?? "")",
                                _timestamp: Int64("\((doc.data() as AnyObject).value(forKey: "timestamp") ?? "")") ?? 0,
                                _groupmessage: "\((doc.data() as AnyObject).value(forKey: "groupmessage") ?? "")" == "0" ? false : true,
                                _unread: "\((doc.data() as AnyObject).value(forKey: "unread") ?? "")" == "0" ? false : true,
                                _userunread: Int("\((doc.data() as AnyObject).value(forKey: "userunread") ?? "")") ?? 0)
                            
                            self.msgArray?.add(messageObj)
                        }
                        
                        let reversedArray : NSArray = ((self.msgArray?.reverseObjectEnumerator().allObjects)! as NSArray)
                        self.msgArray = NSMutableArray(array: reversedArray)
                    }
                    else
                    {
                        if self.receiverName != "Group Chat"
                        {
                            self.finalID = "\(self.receiverId)_\(self.senderId)"
                            self.loadChatMessages()
                        }
                    }
                    
                    DispatchQueue.main.async {
                       
                        self.tableViewObj.reloadData()

                        if self.msgArray!.count > 0
                        {
                            let indexPath : IndexPath = IndexPath(row: (self.msgArray?.count ?? 0) - 1, section: 0)
                            self.tableViewObj.scrollToRow(at: indexPath, at: .bottom, animated: false)
                        }
                    }
                }
                else
                {
                    print("No Chat Messages")
                }
            }
        }
    }
    
    func loadChatMessages()
    {
        let db = Firestore.firestore()
        self.msgArray = NSMutableArray()
        
        db.collection("Smartrental-Dev/Chats/\(self.finalID)").getDocuments { (querySnapShot, error) in

            if let err = error
            {
                print("Error : \(err.localizedDescription)")
            }
            else
            {
                if let snapShotDocuments = querySnapShot?.documents
                {
                    if snapShotDocuments.count > 0
                    {
                        for doc in snapShotDocuments
                        {
                            print(doc.data())
                            
                            let messageObj = Messages(
                                _id: "\("\((doc.data() as AnyObject).value(forKey: "id") ?? "")" == "<null>" ? "" : "\((doc.data() as AnyObject).value(forKey: "id") ?? "")")",
                                _read: "\((doc.data() as AnyObject).value(forKey: "read") ?? "")" == "0" ? false : true,
                                _message: "\((doc.data() as AnyObject).value(forKey: "message") ?? "")",
                                _receiver: "\((doc.data() as AnyObject).value(forKey: "receiver") ?? "")",
                                _receiverName: "\((doc.data() as AnyObject).value(forKey: "recieverName") ?? "")",
                                _sender: "\((doc.data() as AnyObject).value(forKey: "sender") ?? "")",
                                _senderName: "\((doc.data() as AnyObject).value(forKey: "senderName") ?? "")",
                                _type: "\((doc.data() as AnyObject).value(forKey: "type") ?? "")",
                                _timestamp: Int64("\((doc.data() as AnyObject).value(forKey: "timestamp") ?? "")") ?? 0,
                                _groupmessage: "\((doc.data() as AnyObject).value(forKey: "groupmessage") ?? "")" == "0" ? false : true,
                                _unread: "\((doc.data() as AnyObject).value(forKey: "unread") ?? "")" == "0" ? false : true,
                                _userunread: Int("\((doc.data() as AnyObject).value(forKey: "userunread") ?? "")") ?? 0)
                            
                            self.msgArray?.add(messageObj)
                          
                        }
                    }
                    
                    let reversedArray : NSArray = ((self.msgArray?.reverseObjectEnumerator().allObjects)! as NSArray)
                    self.msgArray = NSMutableArray(array: reversedArray)

                    
                    DispatchQueue.main.async {
                       
                        self.tableViewObj.reloadData()

                        if self.msgArray!.count > 0
                        {
                            let indexPath : IndexPath = IndexPath(row: (self.msgArray?.count ?? 0) - 1, section: 0)
                            self.tableViewObj.scrollToRow(at: indexPath, at: .bottom, animated: false)
                        }
                    }
                }
                else
                {
                    print("No Chat Messages")
                }
            }
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonClicked(_ sender: UIButton)
    {
        print("sendButtonClicked")
        
        if self.msgTextView.text != "Type Message" || self.msgTextView.text.isEmpty != true
        {
            let messageVal = self.msgTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let paramDict = [
                "groupmessage":self.isGroupChat,
                "id":"",
                "message":"\(messageVal)",
                "read":false,
                "receiver":"\(self.receiverMob)",
                "recieverName":"\(self.receiverName)",
                "sender":"\(self.senderMob)",
                "senderName":"\(self.senderName)",
                "timestamp":(Int64("\(AppUtility.getCurrentTS())") ?? 0),
                "type":"text",
                "unread":true,
                "user":"",
                "userunread":0
            ] as [String : Any]
            
            let db = Firestore.firestore()
            
            db.collection("Smartrental-Dev").document("Chats").collection("\(self.finalID)").document("\(AppUtility.getCurrentTS())").setData(paramDict) { (error) in
                
                if error == nil
                {
                    let pushNotificationObj = PushNotification()
                    pushNotificationObj.message = "\(messageVal)"
                    pushNotificationObj.deviceType = "0"
                    pushNotificationObj.type = "Chat"
//                    pushNotificationObj.title = "\(self.receiverDict?.value(forKey: "firstName") ?? "") \(self.receiverDict?.value(forKey: "lastName") ?? "")"
                    pushNotificationObj.title = "\(AppUtility.getUserData(keyVal: "name"))"
                    
                    pushNotificationObj.token = "\(self.userDeviceDict?.value(forKey: "device_token") ?? "")"
                    pushNotificationObj.tokens = "IOS"
                    pushNotificationObj.sender_id = "\(AppUtility.getMobileNumber(mobileNumber: "\(self.userDeviceDict?.value(forKey: "mobileno") ?? "")"))"
                    
                    self.sendPushNotification(pushNotification: pushNotificationObj)
                    
                    self.getChatMessages()
                }
                else
                {
                    print("Data Not Saved")
                }
                
                self.msgTextView.text = ""
            }
            
//            db.collection("Smartrental-Dev").document("Chats").collection("\(self.finalID)").addDocument(data: paramDict) { (error) in
//
//                if error == nil
//                {
//                    self.getChatMessages()
//                }
//                else
//                {
//                    print("Data Not Saved")
//                }
//
//                self.msgTextView.text = ""
//
////                DispatchQueue.main.async {
////                    self.tableViewObj.reloadData()
////
////                    if self.msgArray!.count > 0
////                    {
////                        let indexPath : IndexPath = IndexPath(row: (self.msgArray?.count ?? 0) - 1, section: 0)
////                        self.tableViewObj.scrollToRow(at: indexPath, at: .bottom, animated: false)
////                    }
////                }
//            }
            
        }
        
    }
    
    @IBAction func plusButtonClicked(_ sender: UIButton)
    {
        print("plusButtonClicked")
    }
    
    //MARK: - Refresh Controller
    @objc func refreshSync()
    {
        print("refreshSync Called")
        
        self.getChatMessages()
        
        self.refreshController?.endRefreshing()
    }
    
    //MARK: - TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Type Message"
        {
            self.msgTextView.text = ""
            self.msgTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            self.msgTextView.text = "Type Message"
            self.msgTextView.textColor = UIColor.lightGray
        }
    }
    
    //MARK: - TableView DataSource & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.msgArray != nil
        {
            if self.msgArray!.count > 0
            {
                return self.msgArray!.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let msgObj : Messages = self.msgArray?.object(at: indexPath.row) as! Messages
        
//        if self.sender_uTeam == "1"
//        {
//            DBFileManager().insertInTo_WithValues(insertQuery: "UPDATE tblMessages set isOpen = '1' where messageId = \((msgObj._messageId.isEmpty ? "0" : msgObj._messageId)) AND teamId = \(self.sender_uId)")
//        }
//        else
//        {
//            if self.sender_isTg == "1"
//            {
//                DBFileManager().insertInTo_WithValues(insertQuery: "UPDATE tblMessages set isOpen = '1' where messageId = \((msgObj._messageId.isEmpty ? "0" : msgObj._messageId)) AND groupId = \(self.sender_uId)")
//            }
//            else
//            {
//                DBFileManager().insertInTo_WithValues(insertQuery: "UPDATE tblMessages set isOpen = '1' where messageId = \((msgObj._messageId.isEmpty ? "0" : msgObj._messageId)) AND sendByUserId = \(self.sender_uId)")
//            }
//        }
        
        if msgObj._type == "text"
        {
            if msgObj._sender == AppUtility.getUserProfileMobileNumber()
            {
                let myCell = tableView.dequeueReusableCell(withIdentifier: "sendMessageCell", for: indexPath) as! SendMessageTableViewCell
                
                myCell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)

                myCell.selectionStyle = .none

                myCell.mainView.layer.cornerRadius = 8
                myCell.mainView.layer.masksToBounds = true
                
                if self.receiverName == "Group Chat"
                {
                    myCell.lblTitle.text = "\(msgObj._senderName)"
                    myCell.lblNameHeight.constant = 21
                }
                else
                {
                    myCell.lblTitle.text = ""
                    myCell.lblNameHeight.constant = 0
                }
                
                myCell.lblMessage.text = "\(msgObj._message)"
                
                myCell.lblTime.text = AppUtility.timestampToDate(timestampValue: "\(msgObj._timestamp)")
                myCell.lblTime.textColor = Constants.goldenColor

                return myCell
            }
            else
            {
                let myCell = tableView.dequeueReusableCell(withIdentifier: "receiveMessageCell", for: indexPath) as! ReceiveMessageTableViewCell

                myCell.selectionStyle = .none
                myCell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)

                myCell.mainView.layer.cornerRadius = 8
                myCell.mainView.layer.masksToBounds = true
                
                if self.receiverName == "Group Chat"
                {
                    myCell.lblTitle.text = "\(msgObj._senderName)"
                    myCell.lblNameHeight.constant = 21
                }
                else
                {
                    myCell.lblTitle.text = ""
                    myCell.lblNameHeight.constant = 0
                }
                
                myCell.lblMessage.text = "\(msgObj._message)"
                myCell.lblTime.text = AppUtility.timestampToDate(timestampValue: "\(msgObj._timestamp)")
                myCell.lblTime.textColor = Constants.goldenColor
                
                return myCell
            }
            
            
//            if msgObj._isMultimedia == "1"
//            {
//                let myCell = tableView.dequeueReusableCell(withIdentifier: "sendImageCell", for: indexPath) as! SendImageMessageTableViewCell
//
//                myCell.selectionStyle = .none
//
//                myCell.mainView.layer.cornerRadius = 8
//                myCell.mainView.layer.masksToBounds = true
//
//                myCell.imgView.image = UIImage(data: (Data(base64Encoded: msgObj._media ))!)
//
//                myCell.lblTitleMsg.text = "\(msgObj._description)"
//                myCell.lblTime.text = "\(dateString)"
//                myCell.lblTime.textColor = Constants.goldenColor
//
//                return myCell
//            }
//            else
//            {
//                let myCell = tableView.dequeueReusableCell(withIdentifier: "sendMessageCell", for: indexPath) as! SendMessageTableViewCell
//
//                myCell.selectionStyle = .none
//
//                myCell.mainView.layer.cornerRadius = 8
//                myCell.mainView.layer.masksToBounds = true
//
//                myCell.lblTitle.text = "\(msgObj._title)"
//                myCell.lblMessage.text = "\(msgObj._description)"
//                myCell.lblTime.text = "\(dateString)"
//                myCell.lblTime.textColor = Constants.goldenColor
//
//                return myCell
//            }
        }
//        else
//        {
//
//
////            if msgObj._isMultimedia == "1"
////            {
////                let myCell = tableView.dequeueReusableCell(withIdentifier: "receiveImageCell", for: indexPath) as! ReceiveImageMessageTableViewCell
////
////                myCell.selectionStyle = .none
////
////                myCell.mainView.layer.cornerRadius = 8
////                myCell.mainView.layer.masksToBounds = true
////
////                myCell.imageViewObj.image = UIImage(data: (Data(base64Encoded: msgObj._media ))!)
////
////                myCell.lblTitle.text = "\(msgObj._description)"
////                myCell.lblTime.text = "\(dateString)"
////                myCell.lblTime.textColor = Constants.goldenColor
////
////                return myCell
////            }
////            else
////            {
////                let myCell = tableView.dequeueReusableCell(withIdentifier: "receiveMessageCell", for: indexPath) as! ReceiveMessageTableViewCell
////
////                myCell.selectionStyle = .none
////
////                myCell.mainView.layer.cornerRadius = 8
////                myCell.mainView.layer.masksToBounds = true
////
////                myCell.lblTitle.text = "\(msgObj._title)"
////                myCell.lblMessage.text = "\(msgObj._description)"
////                myCell.lblTime.text = "\(dateString)"
////                myCell.lblTime.textColor = Constants.goldenColor
////
////                return myCell
////            }
//        }
        
        return UITableViewCell()
    }
    
    //MARK: - Send Device Token
    func getUserDeviceToken()
    {
        if Reachability.isConnectedToNetwork()
        {
            let paramDict = [
                "mobileno":(Int64("\(AppUtility.getMobileNumber(mobileNumber: "\(self.receiverDict?.value(forKey: "mobileno") ?? "")"))") ?? 0),
                "device_id":0,
                "device_token" : "",
                "otp" : 0,
                
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserDeviceToken", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Get User Device Token Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableArray)
                        {
                            self.userDeviceDict = NSDictionary()
                            self.userDeviceDict = (responseObj.object(at: 0) as! NSDictionary)
                        }
                    }
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - Send Push Notification
    func sendPushNotification(pushNotification : PushNotification)
    {
        let jsonObj = NSMutableDictionary()
        
        // iOS = 0, Android = 1
//        if pushNotification.deviceType == "1"
        if "\(self.userDeviceDict?.value(forKey: "device_id") ?? "")" == "1"
        {
            jsonObj.setValue("\(pushNotification.token)", forKey: "to")
            
            let jsonDict = NSMutableDictionary()
            jsonDict.setValue("\(pushNotification.message)", forKey: "message")
            jsonDict.setValue("\(pushNotification.type)", forKey: "notificationType")
            jsonDict.setValue("\(pushNotification.title)", forKey: "notificationTitle")
            jsonDict.setValue("\(pushNotification.title)", forKey: "title")
            jsonDict.setValue("\(pushNotification.sender_id)", forKey: "sender")
            
            jsonObj.setValue(jsonDict, forKey: "data")
        }
        else
        {
            let notification = NSMutableDictionary()
            notification.setValue("\(pushNotification.message)", forKey: "body")
            notification.setValue("\(pushNotification.type)", forKey: "title")
            notification.setValue("\(pushNotification.title)", forKey: "notificationTitle")
            notification.setValue("default", forKey: "sound")
            
            let data = NSMutableDictionary()
            data.setValue("\(pushNotification.sender_id)", forKey: "sender")
            notification.setValue(data, forKey: "data")
            notification.setValue("\(pushNotification.type)", forKey: "notificationType")
            
            jsonObj.setValue("\(pushNotification.token)", forKey: "to")
            jsonObj.setValue(notification, forKey: "notification")
        }
        
        WebRequestManager.sendPushNotification(urlString: Constants.fcmURL, parameters: jsonObj as! [String : Any]) { (status, response) in
            
            print("Status : \(status)")
            print("response : \(response)")
        }
    }
}
