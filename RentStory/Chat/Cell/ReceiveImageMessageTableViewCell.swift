//
//  ReceiveImageMessageTableViewCell.swift
//  GP Global
//
//  Created by Hardik on 26/03/20.
//  Copyright © 2020 Hardik. All rights reserved.
//

import UIKit

class ReceiveImageMessageTableViewCell: UITableViewCell
{
    @IBOutlet weak var mainView: UIView!
 
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imageViewObj: UIImageView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
