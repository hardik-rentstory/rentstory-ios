//
//  ChatListTableViewCell.swift
//  RentStory
//
//  Created by Hardik Pithadia on 19/03/21.
//

import UIKit

class ChatListTableViewCell: UITableViewCell
{
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
