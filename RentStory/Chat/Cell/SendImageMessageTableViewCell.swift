//
//  SendImageMessageTableViewCell.swift
//  GP Global
//
//  Created by Hardik on 26/03/20.
//  Copyright © 2020 Hardik. All rights reserved.
//

import UIKit

class SendImageMessageTableViewCell: UITableViewCell
{
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblTitleMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
