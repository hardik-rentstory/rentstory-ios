//
//  ChatListViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 19/03/21.
//

import UIKit
import SDWebImage

class ChatListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    var senderId = ""
    var senderName = ""
    var senderMob = ""
    var propertyPic = ""
    
    var senderDict : NSDictionary?
    
    var chatListArray : NSMutableArray?
    var peopleInvolvedArray : NSMutableArray?
    
    @IBOutlet weak var searchBarObj: UISearchBar!
    @IBOutlet weak var tableViewObj: UITableView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.searchBarObj.delegate = self
        
        self.chatListArray = NSMutableArray()
        self.chatListArray?.add(["contact_photo" : self.propertyPic, "firstName" : "Group", "lastName" : "Chat", "property_id" : "\((self.peopleInvolvedArray?.object(at: 0) as AnyObject).value(forKey: "property_id") ?? "")"])
        
        for currentObj in self.peopleInvolvedArray!
        {
            if "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")" == AppUtility.getUserProfileMobileNumber()
            {
                self.senderDict = NSDictionary()
                self.senderDict = (currentObj as! NSDictionary)
            }
            else
            {
                self.chatListArray?.add(currentObj)
            }
        }
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        
        let chatListCellNib = UINib(nibName: "ChatListTableViewCell", bundle: nil)
        self.tableViewObj.register(chatListCellNib, forCellReuseIdentifier: "chatListCell")
        self.tableViewObj.separatorColor = .clear
        self.tableViewObj.backgroundColor = .clear
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - SearchBar Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.searchTextField.resignFirstResponder()
    }
    
    //MARK: - TableView DataSource & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.chatListArray != nil
        {
            if self.chatListArray!.count > 0
            {
                return self.chatListArray!.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "chatListCell", for: indexPath) as! ChatListTableViewCell
        
        myCell.selectionStyle = .none
        myCell.backgroundColor = .clear
        myCell.baseView.backgroundColor = .white
        
        DispatchQueue.main.async {
            myCell.baseView.layer.cornerRadius = 10
            myCell.baseView.layer.masksToBounds = true
            
            myCell.profileImgView.layer.cornerRadius = 35
            myCell.baseView.layer.masksToBounds = true
        }
        
        myCell.profileImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_photo") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
        myCell.profileImgView.clipsToBounds = true
        myCell.profileImgView.contentMode = .scaleAspectFill
        
        myCell.lblName.text = "\((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "firstName") ?? "") \((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "lastName") ?? "")"
                
        myCell.lblTime.text = "\("\((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "updated_date") ?? "")".components(separatedBy: "T").first ?? "") \(("\((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "updated_date") ?? "")".components(separatedBy: "T").last ?? "").components(separatedBy: ".").first ?? "")"
        
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let chatVC = ChatViewController(nibName: "ChatViewController", bundle: nil)
        chatVC.headerString = "\((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "firstName") ?? "") \((self.chatListArray?.object(at: indexPath.row) as AnyObject).value(forKey: "lastName") ?? "")"
        chatVC.senderDict = self.senderDict
        chatVC.receiverDict = (self.chatListArray?.object(at: indexPath.row) as! NSDictionary)
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
}
