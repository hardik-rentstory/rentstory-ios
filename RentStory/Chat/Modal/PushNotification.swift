//
//  PushNotification.swift
//  RentStory
//
//  Created by Hardik Pithadia on 26/03/21.
//

import UIKit

class PushNotification: NSObject
{
    var message = ""
    var deviceType = ""
    var type = ""
    var title = ""
    var token = ""
    var tokens = ""
    var sender_id = ""
}
