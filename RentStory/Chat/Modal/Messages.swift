//
//  Messages.swift
//  GP Global
//
//  Created by Hardik on 17/03/20.
//  Copyright © 2020 Hardik. All rights reserved.
//

import UIKit

class Messages: NSObject
{    
    var _id = ""
    var _read = false
    var _message = ""
    var _receiver = ""
    var _receiverName = ""
    var _sender = ""
    var _senderName = ""
    var _type = ""
    var _timestamp = Int64(0) // Long
    var _groupmessage = false;
    var _unread = true;
    var _userunread = 0;
    
    init(_id : String, _read : Bool, _message : String, _receiver : String, _receiverName : String, _sender : String, _senderName : String, _type : String, _timestamp : Int64, _groupmessage : Bool, _unread : Bool, _userunread : Int)
    {
        self._id = _id
        self._read = _read
        self._message = _message
        self._receiver = _receiver
        self._receiverName = _receiverName
        self._sender = _sender
        self._senderName = _senderName
        self._type = _type
        self._timestamp = _timestamp // Long
        self._groupmessage = _groupmessage
        self._unread = _unread
        self._userunread = _userunread
    }
}
