//
//  AppDelegate.swift
//  RentStory
//
//  Created by Hardik Pithadia on 23/10/20.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import FirebaseCore
import IQKeyboardManagerSwift
import AWSCore
import GooglePlaces


@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate
{
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        IQKeyboardManager.shared.enable = true
        GMSPlacesClient.provideAPIKey("AIzaSyCef35C0A6wb8lOU47bswhtrgN5UXVcdtU")
        
        // Initialised AWS S3
        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: Constants.poolIdValue)
        
        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
        
        AWSServiceManager.default()?.defaultServiceConfiguration = configuration
        
        if AppUtility.getUserData(keyVal: "isLogin").isEmpty
        {
            let baseVC = BaseViewController(nibName: "BaseViewController", bundle: nil)
            self.window?.rootViewController = baseVC
            self.window?.makeKeyAndVisible()
        }
        else
        {
            //            if AppUtility.getUserData(keyVal: "isLogin") == "1"
            //            {
            //                let homeVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
            //                let navController = UINavigationController(rootViewController: homeVC)
            //                self.window?.rootViewController = navController
            //                self.window?.makeKeyAndVisible()
            //            }
            //            else
            //            {
            //                let homeVC = CreateProfileViewController(nibName: "CreateProfileViewController", bundle: nil)
            //                let navController = UINavigationController(rootViewController: homeVC)
            //                self.window?.rootViewController = navController
            //                self.window?.makeKeyAndVisible()
            //
            //            }
            
            let homeVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
            let navController = UINavigationController(rootViewController: homeVC)
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
        }
        
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
      
        if #available(iOS 10.0, *)
        {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        }
        else
        {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
    
        Messaging.messaging().delegate = self
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        UIApplication.shared.windows.forEach { window in
            if #available(iOS 13.0, *) {
                window.overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
        }
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("Received fcmToken: \(fcmToken)")
        AppUtility.setUserData(keyString: "fcmToken", valueString: "\(fcmToken)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        print("didRegisterForRemoteNotificationsWithDeviceToken")
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        if let messageID = userInfo["gcm.message_id"]
        {
          print("Message ID: \(messageID)")
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print("Entire message \(userInfo)")
//        print("Article avaialble for download: \(userInfo["articleId"]!)")
        
        if Auth.auth().canHandleNotification(userInfo)
        {
            completionHandler(.noData)
            return
        }
        
        let state : UIApplication.State = application.applicationState
        switch state {
        case UIApplicationState.active:
            print("If needed notify user about the message")
        default:
            print("Run code to download content")
        }
        
//        completionHandler(UIBackgroundFetchResult.newData)
        
        if let messageID = userInfo["gcm.message_id"]
        {
          print("Message ID: \(messageID)")
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // Called When App is in Foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("willPresent Notification Info : \(notification.request.content.userInfo)")
        
        if "\((notification.request.content.userInfo as AnyObject).value(forKey: "gcm.notification.notificationType") ?? "")" == "chat"
        {
            NotificationCenter.default.post(name: Notification.Name("MessageNotification"), object: nil)
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
                
        completionHandler([.alert, .badge, .sound])
//        completionHandler([])
    }
    
    // Called When App is in Background
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("didReceive Notification Called")
        
        let userInfo = response.notification.request.content.userInfo

        UIApplication.shared.applicationIconBadgeNumber = 0
        
        print(userInfo)

        completionHandler()
//        completionHandler([.alert, .badge, .sound])
    }
}
