//
//  AppUtility.swift
//  Quran Daily
//
//  Created by Hardik Pithadia on 09/06/20.
//  Copyright © 2020 Hardik Pithadia. All rights reserved.
//

import UIKit

class AppUtility: NSObject
{
    public static func isValidEmail(emailString : String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if emailTest.evaluate(with: emailString)
        {
            return true
        }
        return false
    }
    
    public static func getFormattedDate(fromFormate : String, toFormate : String, dateStr : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormate
        
        let dateVal = dateFormatter.date(from: dateStr)
        
        dateFormatter.dateFormat = toFormate
        
        return "\(dateFormatter.string(from: dateVal!))"
    }
    
    public static func timestampToDate(timestampValue : String) -> String
    {
        let timeStamp  = Double(timestampValue)
        let timeInterval : TimeInterval = (timeStamp ?? 0) / 1000
        let date : Date = Date.init(timeIntervalSince1970: timeInterval)
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        return dateformatter.string(from: date)
    }
    
    public static func isValidPassword(passwordString : String) -> Bool
    {
        let passwordRegEx = "(?=.*[A-Z])(?=.*[!@#$&*<>])(?=.*[0-9])(?=.*[a-z]).{8,}"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        
        if passwordTest.evaluate(with: passwordString)
        {
            return true
        }
        
        return false
    }
    
    public static func getUserProfileMobileNumber() -> String
    {
        var mobNum = ""
        if AppUtility.getUserData(keyVal: "mobileNo").count <= 10
        {
            mobNum = "\(AppUtility.getUserData(keyVal: "countryCode"))\(AppUtility.getUserData(keyVal: "mobileNo"))"
        }
        else
        {
            mobNum = "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        }
        
        if mobNum.contains("+")
        {
            mobNum = mobNum.replacingOccurrences(of: "+", with: "")
        }
        
        return mobNum
    }
    
    public static func getMobileNumber(mobileNumber : String) -> String
    {
        var mobNum = ""
        if mobileNumber.count <= 10
        {
            mobNum = "\(AppUtility.getUserData(keyVal: "countryCode"))\(mobileNumber)"
        }
        else
        {
            mobNum = "\(mobileNumber)"
        }
        
        if mobNum.contains("+")
        {
            mobNum = mobNum.replacingOccurrences(of: "+", with: "")
        }
        
        return mobNum
    }
    
    public static func resizeImage(imageObj : UIImage) -> UIImage
    {
        var w : Float = 00.0000000000
        var h : Float = 00.0000000000
        
        w = Float((imageObj.size.width / imageObj.size.height) * 420)
        h = Float((imageObj.size.height / imageObj.size.width) * 380)
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(w), height: CGFloat(h)), false, 0.0)
        imageObj.draw(in: CGRect(x: 0.0, y: 0.0, width: CGFloat(w), height: CGFloat(h)))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    public static func getUserData(keyVal : String) -> String
    {
        let preferences = UserDefaults.standard
        
        if preferences.object(forKey: keyVal as String) == nil
        {
            return ""
        }
        else
        {
            //            let valueString : String = preferences.value(forKey: keyVal as String) as! String
            
            //            let valueString : String = "\(preferences.value(forKey: keyVal as String) ?? "")"
            
            let valueString = preferences.value(forKey: keyVal)
            
            return "\(valueString ?? "")"
        }
    }
    
    public static func setUserData(keyString : String, valueString : String)
    {
        UserDefaults.standard.set("\(valueString)", forKey: "\(keyString)")
        UserDefaults.standard.synchronize()
    }
    
    public static func deleteAllUserDefaults()
    {
        for currentObj in UserDefaults.standard.dictionaryRepresentation().keys
        {
            if !(currentObj == "deviceToken")
            {
                UserDefaults.standard.removeObject(forKey: currentObj)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    public static func getCurrentTS() -> String
    {
        let ts : Int = Int(Date().timeIntervalSince1970)
        return String(format: "%d000", ts)
    }
    
    public static func getTimestamp() -> String
    {
//        let ts : Int = Int(Date().timeIntervalSince1970)
        let ts : Int = Int(Date().timeIntervalSinceNow)
        return "\(ts)"
    }
    
    public static func getCities() -> NSArray
    {
        if let path = Bundle.main.path(forResource: "CityNames", ofType: "json")
        {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                
                return (jsonResult as! NSArray)
            } catch {
                // handle error
                print("Error while reading City Names")
            }
        }
        
        return []
    }
    
    public static func getStringFromDictionary(data : NSDictionary) -> NSString
    {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            if jsonData.count == 0 {
                return ""
            }
            else
            {
                let jsonString : String = (String(data: jsonData, encoding: .utf8)!).replacingOccurrences(of: "\\/", with: "/")
                
                let finalStr = String(format: "%@",jsonString) as NSString
                
                let escapeSet = NSCharacterSet(charactersIn: "+")
                
                let escapedString = finalStr.addingPercentEncoding(withAllowedCharacters: escapeSet.inverted)
                
                return escapedString! as NSString
            }
            
        } catch {
            print(error.localizedDescription)
        }
        
        return ""
    }
    
    public static func drawPDFfromURL(url: URL) -> UIImage?
    {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }

        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)

            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)

            ctx.cgContext.drawPDFPage(page)
        }

        return img
    }
}
