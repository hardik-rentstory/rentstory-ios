//
//  NetworkManager.swift
//  TableDemo
//
//  Created by Hardik Pithadia on 29/12/20.
//  Copyright © 2020 Hardik Pithadia. All rights reserved.
//

import UIKit
import Alamofire
import PromiseKit
import AWSS3
import Foundation

class NetworkManager
{
    static let api = NetworkManager()
    
    private init()
    {
        
    }
    
    func uploadImage(image : UIImage, imageName : String) -> Promise<URL>
    {
        return Promise { resolver in
            
            let progressBlock : AWSS3TransferUtilityProgressBlock = {
                task, progress in
                
                print("Upload Image Progress : \(progress.fractionCompleted)")
            }
            
            let  transferUtility = AWSS3TransferUtility.default()
            let imageData = image.jpegData(compressionQuality: 0.5)
//            let imageData = image.pngData()
            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock = progressBlock
            
            
            transferUtility.uploadData(imageData!, bucket: Constants.bucketName, key: imageName, contentType: "image/png", expression: expression) { (task, error) in
                 
                if let error = error
                {
                    resolver.reject(error)
                }
                else
                {
                    let imageURL = URL(string: Constants.imagePath+"/"+imageName)
                    resolver.fulfill(imageURL!)
                }
             }
        }
    }
}
