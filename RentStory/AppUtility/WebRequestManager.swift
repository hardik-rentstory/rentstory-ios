//
//  WebRequestManager.swift
//  Field-O
//
//  Created by Hardik Pithadia on 12/01/19.
//  Copyright © 2019 Hardik Pithadia. All rights reserved.
//

import UIKit

class WebRequestManager: NSObject
{
    class func parseJsonWebService(urlString : String, headerVal : String ,parameters : [String : Any] ,completionHandlor:@escaping(Bool, AnyObject) -> ())
    {
        print("URL : \(urlString)")
        print("Parameters : \(parameters)")
        
        let myURL = URL(string: urlString)
        
        //set the Request
        var urlRequest = URLRequest(url: myURL!)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.addValue("\(Constants.apiKey)", forHTTPHeaderField: "x-api-key")
        
        
//        if !(UserDefaults.standard.value(forKey: "token") as? String ?? "").isEmpty
//        {
//            urlRequest.addValue("Bearer \(UserDefaults.standard.value(forKey: "token") as? String ?? "")", forHTTPHeaderField: "Authorization")
//        }
        
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        urlRequest.httpBody = json.data(using: .utf8)
        
        urlRequest.timeoutInterval = 60
        
        let session = URLSession.shared
        
        let dataTast = session.dataTask(with: urlRequest) { (data, response, error) in
            
            if response != nil
            {
                let httpStatusCode = response as! HTTPURLResponse
                let statusCode = httpStatusCode.statusCode
                
                print("Status Code : \(statusCode)")
                
                if statusCode == 200
                {
                    do {
                        guard let data = data else {
                            
                            completionHandlor(false, [] as AnyObject)
                            
                            return
                        }
                        
                        let jsonResponse = try(JSONSerialization.jsonObject(with: data, options: .mutableContainers))

                        completionHandlor(true, jsonResponse as AnyObject)
                        
                    } catch {
                        
                        completionHandlor(false, [] as AnyObject)
                    }
                }
                else
                {
                    do {
                        guard let data = data else {
                            
                            completionHandlor(false, [] as AnyObject)
                            
                            return
                        }
                        
                        let jsonResponse = try(JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                        
                        completionHandlor(false, jsonResponse as AnyObject)
                        
                    } catch {
                        completionHandlor(false, [] as AnyObject)
                    }
                }
            }
            else
            {
                do {
                    guard let data = data else {
                        
                        completionHandlor(false, [] as AnyObject)
                        
                        return
                    }
                    
                    let jsonResponse = try(JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                    
                    completionHandlor(false, jsonResponse as AnyObject)
                    
                } catch {
                    completionHandlor(false, [] as AnyObject)
                }
            }
        }
        
        dataTast.resume()
    }
    
    class func sendPushNotification(urlString : String, parameters : [String : Any] ,completionHandlor:@escaping(Bool, AnyObject) -> ())
    {
        print("URL : \(urlString)")
        print("Parameters : \(parameters)")
        
        let myURL = URL(string: urlString)
        
        //set the Request
        var urlRequest = URLRequest(url: myURL!)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("key=\(Constants.serverKey)", forHTTPHeaderField: "Authorization")
        
        
//        if !(UserDefaults.standard.value(forKey: "token") as? String ?? "").isEmpty
//        {
//            urlRequest.addValue("Bearer \(UserDefaults.standard.value(forKey: "token") as? String ?? "")", forHTTPHeaderField: "Authorization")
//        }
        
        let data = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        urlRequest.httpBody = json.data(using: .utf8)
        
        urlRequest.timeoutInterval = 60
        
        let session = URLSession.shared
        
        let dataTast = session.dataTask(with: urlRequest) { (data, response, error) in
            
            if response != nil
            {
                let httpStatusCode = response as! HTTPURLResponse
                let statusCode = httpStatusCode.statusCode
                
                print("Status Code : \(statusCode)")
                
                if statusCode == 200
                {
                    do {
                        guard let data = data else {
                            
                            completionHandlor(false, [] as AnyObject)
                            
                            return
                        }
                        
                        let jsonResponse = try(JSONSerialization.jsonObject(with: data, options: .mutableContainers))

                        completionHandlor(true, jsonResponse as AnyObject)
                        
                    } catch {
                        
                        completionHandlor(false, [] as AnyObject)
                    }
                }
                else
                {
                    do {
                        guard let data = data else {
                            
                            completionHandlor(false, [] as AnyObject)
                            
                            return
                        }
                        
                        let jsonResponse = try(JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                        
                        completionHandlor(false, jsonResponse as AnyObject)
                        
                    } catch {
                        completionHandlor(false, [] as AnyObject)
                    }
                }
            }
            else
            {
                do {
                    guard let data = data else {
                        
                        completionHandlor(false, [] as AnyObject)
                        
                        return
                    }
                    
                    let jsonResponse = try(JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                    
                    completionHandlor(false, jsonResponse as AnyObject)
                    
                } catch {
                    completionHandlor(false, [] as AnyObject)
                }
            }
        }
        
        dataTast.resume()
    }
    
    //    class func parsePostJsonWebServiceWithHeader(urlString : String, parameters : [String : String] ,completionHandlor:@escaping(Bool, AnyObject) -> ())
    //    {
    //        let myURL = URL(string: urlString)
    //
    //        var urlRequest = URLRequest(url: myURL!)
    //        urlRequest.httpMethod = "POST"
    //        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
    //        urlRequest.addValue("4bbf7bc6da91c9c341c4fea3a6b70526", forHTTPHeaderField: "apiToken")
    //
    //        let data = try! JSONSerialization.data(withJSONObject: parameters, options: [])
    //        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    //        //        urlRequest.urlRequest = json.dataUsingEncoding(NSUTF8StringEncoding)
    //        urlRequest.httpBody = json.data(using: .utf8)
    ////        urlRequest.allHTTPHeaderFields = ["key":"value"]
    //
    //        /*
    //
    //         let json = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
    //         request.HTTPBody = json.dataUsingEncoding(NSUTF8StringEncoding)
    //         request.allHTTPHeaderFields = ["key":"value"]
    //
    //         */
    //
    //        //        urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
    //
    //
    //        let config = URLSessionConfiguration.default
    //        let session = URLSession(configuration: config)
    //
    //        let dataTast = session.dataTask(with: urlRequest) { (data, response, error) in
    //            let httpStatusCode = response as! HTTPURLResponse
    //            let statusCode = httpStatusCode.statusCode
    //
    //            if statusCode == 200 {
    //                do {
    //                    let jsonResponse = try(JSONSerialization.jsonObject(with: data!, options: .mutableContainers))
    //                    completionHandlor(true, jsonResponse as AnyObject)
    //                } catch {
    //
    //                }
    //            } else {
    //                completionHandlor(false, [] as AnyObject)
    //            }
    //        }
    //
    //        dataTast.resume()
    //
    //    }
}
