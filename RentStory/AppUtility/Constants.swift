//
//  Constants.swift
//  RentStory
//
//  Created by Hardik Pithadia on 27/10/20.
//

import UIKit

class Constants: NSObject
{
    // Test Server API URL
    public static let mainURL = "https://7a19wkxe2a.execute-api.us-east-1.amazonaws.com/qa"
    public static let fcmURL = "https://fcm.googleapis.com/fcm/send"
    
    // Server API Key
    public static let apiKey = "8wvTIaggbH6lOAwd4OhyP2yhFBdlnDkt8lQxNhju"
    public static let serverKey = "AAAAk2U5V4Q:APA91bG6HYLocEld3xdncIQ3YwqaOyDQCxj387_GYJcJC5-Jwg_qD9SRGC42ql5r0WDVgeYFqFXayh_0kXqxp7cNbM-KwaYmGey9z9pS0txiFN3Qs0NzXfW20xfsD3GDgkxTGkmG-Siy"
    
    // Upload Image to AWS S3 Path
    public static let imagePath = "https://rentstory-upload.s3.amazonaws.com"
   
    public static let aadharImageFolder = "aadhar_img"
    public static let panCardImageFolder = "pan_img"
    public static let profileImageFolder = "profile_img"
    
    public static let propertyImageFolder = "property_img"
    public static let registrationImageFolder = "registration_img"
    public static let lightBillImageFolder = "light_bill_img"
    public static let ownerIdImageFolder = "owner_id_img"
    
    public static let agreementFolder = "agreement_img"
    public static let policeVerificationFolder = "police_verification_img"
    public static let nocFolder = "society_noc_img"
    
    
    // AWS S3 Configurations
    public static let poolIdValue = "us-east-1:a92e07ad-3090-419f-9606-bc806ad98f2b"
    public static let bucketName = "rentstory-upload"
    
    
    // Background Color 198, 153, 96
    public static let bgColor = UIColor.init(red: 243.0/255.0, green: 248.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    
    public static let darkBlueColor = UIColor.init(red: 9.0/255.0, green: 20.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    
    public static let darkGrayColor = UIColor.init(red: 118.0/255.0, green: 124.0/255.0, blue: 139.0/255.0, alpha: 1.0)
    
    public static let goldenColor = UIColor.init(red: 203.0/255.0, green: 156.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    
}
