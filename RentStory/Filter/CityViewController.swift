//
//  CityViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 17/02/21.
//

import UIKit

protocol CityDelegate
{
    func sendSelectedCity(cityName : String)
}

class CityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    var cityDelegate : CityDelegate?
    
    var isSearch = false
    var filterArray = NSArray()
    
    var cityArray = NSArray()
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    @IBOutlet weak var searchBarObj: UISearchBar!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.cityArray = AppUtility.getCities()
        print(self.cityArray)
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.searchBarObj.delegate = self
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isSearch
        {
            if self.filterArray.count > 0
            {
                return self.filterArray.count
            }
        }
        else
        {
            if self.cityArray.count > 0
            {
                return self.cityArray.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var myCell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if myCell == nil
        {
            myCell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        if self.isSearch
        {
            myCell?.textLabel?.text = "\(self.filterArray[indexPath.row])"
        }
        else
        {
            myCell?.textLabel?.text = "\(self.cityArray[indexPath.row])"
        }
        
        return myCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.isSearch
        {
            if self.cityDelegate != nil
            {
                self.cityDelegate?.sendSelectedCity(cityName: "\(self.filterArray[indexPath.row])")
            }
        }
        else
        {
            if self.cityDelegate != nil
            {
                self.cityDelegate?.sendSelectedCity(cityName: "\(self.cityArray[indexPath.row])")
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UISearchBar Delegates
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.count == 0
        {
            self.isSearch = false;
            
            DispatchQueue.main.async {
                self.tableViewObj.reloadData()
            }
        }
        else
        {
            self.filterArray = self.cityArray.filter({ (text) -> Bool in
                let tmp: NSString = "\(text)" as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            }) as NSArray
            
            isSearch = true
            
            DispatchQueue.main.async {
                self.tableViewObj.reloadData()
            }
        }
    }
}
