//
//  PlaceAutocompleteViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 17/02/21.
//

import UIKit
import GooglePlaces

protocol SelectedAddressDelegate
{
    func sendSelectedAddress(place: GMSPlace)
}

class PlaceAutocompleteViewController: UIViewController
{
    var selectedAddressDelegate : SelectedAddressDelegate?
    
    private var tableView: UITableView!
    private var tableDataSource: GMSAutocompleteTableDataSource!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 80, width: self.view.frame.size.width, height: 44.0))
        searchBar.delegate = self
        view.addSubview(searchBar)
        
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        filter.country = "IN"
        
        tableDataSource = GMSAutocompleteTableDataSource()
        tableDataSource.autocompleteFilter = filter
        tableDataSource.delegate = self
        
        tableView = UITableView(frame: CGRect(x: 0, y: searchBar.frame.origin.y + searchBar.frame.height + 5, width: self.view.frame.size.width, height: self.view.frame.size.height - 100))
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        
        view.addSubview(tableView)
    }
}

extension PlaceAutocompleteViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        // Update the GMSAutocompleteTableDataSource with the search text.
        tableDataSource.sourceTextHasChanged(searchText)
    }
}

extension PlaceAutocompleteViewController: GMSAutocompleteTableDataSourceDelegate
{
    func didUpdateAutocompletePredictions(for tableDataSource: GMSAutocompleteTableDataSource)
    {
        // Turn the network activity indicator off.
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        // Reload table data.
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func didRequestAutocompletePredictions(for tableDataSource: GMSAutocompleteTableDataSource)
    {
        // Turn the network activity indicator on.
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // Reload table data.
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didAutocompleteWith place: GMSPlace)
    {
        // Do something with the selected place.
        print("Place name: \(place.name ?? "")")
        print("Place address: \(place.formattedAddress ?? "")")
//        print("Place attributions: \(place.attributions!)")
        
        if self.selectedAddressDelegate != nil
        {
            self.selectedAddressDelegate?.sendSelectedAddress(place: place)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didFailAutocompleteWithError error: Error)
    {
        // Handle the error.
        print("Error: \(error.localizedDescription)")
    }
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didSelect prediction: GMSAutocompletePrediction) -> Bool
    {
        return true
    }
}
