//
//  FilterViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 05/02/21.
//

import UIKit
import TagListView
import GooglePlaces

class FilterViewController: UIViewController, CityDelegate, SelectedAddressDelegate, TagListViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    var propertyTypeStr = "Residential"
    var propertySubType = "Flat"
    
    var newPropertyTypeArray : NSMutableArray?
    var selectedNewPropertyTypeArray : NSMutableArray?
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityButton: UIButton!
    
    @IBOutlet weak var searchBarObj: UISearchBar!
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var roleTagList: TagListView!

    @IBOutlet weak var residentialButton: UIButton!
    @IBOutlet weak var commercialButton: UIButton!
    
    
    @IBOutlet weak var propertyTypeCollection: UICollectionView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.newPropertyTypeArray = NSMutableArray()
        self.newPropertyTypeArray?.add("Flat")
        self.newPropertyTypeArray?.add("Duplex")
        self.newPropertyTypeArray?.add("Bunglow")
        self.newPropertyTypeArray?.add("Row House")
        
        self.selectedNewPropertyTypeArray = NSMutableArray()
        for _ in 0..<self.newPropertyTypeArray!.count
        {
            self.selectedNewPropertyTypeArray?.add("0")
        }
        self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
        
        self.propertyTypeCollection.delegate = self
        self.propertyTypeCollection.dataSource = self
        
        let propertyTypeCellNib = UINib(nibName: "NewPropertyTypeCollectionViewCell", bundle: nil)
        self.propertyTypeCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.propertyTypeCollection.showsHorizontalScrollIndicator = false
        
        self.searchBarObj.backgroundColor = .clear
        
        self.tagListView.delegate = self
        self.tagListView.textFont = UIFont(name: "SofiaPro", size: 15)!
        self.tagListView.shadowOpacity = 0.4
        self.tagListView.shadowColor = UIColor.black
        self.tagListView.alignment = .left

        self.roleTagList.delegate = self
        self.roleTagList.textFont = UIFont(name: "SofiaPro", size: 15)!
        self.roleTagList.shadowOpacity = 0.4
        self.roleTagList.shadowColor = UIColor.black
        self.roleTagList.alignment = .left
        
        self.roleTagList.addTag("Owner")
        self.roleTagList.addTag("Tenant")
        self.roleTagList.addTag("Broker")
        self.roleTagList.addTag("Property Manager")
        self.roleTagList.addTag("All")
        
        self.residentialButton.layer.cornerRadius = 15
        self.residentialButton.backgroundColor = Constants.darkBlueColor
        self.residentialButton.layer.masksToBounds = true
        
        self.commercialButton.layer.cornerRadius = 15
        self.commercialButton.backgroundColor = .white
        self.commercialButton.layer.masksToBounds = true
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.cityView.layer.cornerRadius = 10
    }
    
    //MARK: - TagList Delegates
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView)
    {
        print("Tag pressed: \(title)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView)
    {
        print("Tag Remove pressed: \(title)")
        
        sender.removeTagView(tagView)
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cityButtonClicked(_ sender: UIButton)
    {
        let cityVC  = CityViewController(nibName: "CityViewController", bundle: nil)
        cityVC.cityDelegate = self
        self.navigationController?.pushViewController(cityVC, animated: true)
    }
    
    @IBAction func addressSearchButtonClicked(_ sender: UIButton)
    {
        let placesVc = PlaceAutocompleteViewController(nibName: "PlaceAutocompleteViewController", bundle: nil)
        placesVc.selectedAddressDelegate = self
        self.navigationController?.pushViewController(placesVc, animated: true)
    }
    
    @IBAction func residentialButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            self.propertyTypeStr = self.residentialButton.currentTitle ?? ""
            self.propertySubType = "Flat"
            
            self.residentialButton.layer.cornerRadius = 15
            self.residentialButton.backgroundColor = Constants.darkBlueColor
            self.residentialButton.setTitleColor(.white, for: .normal)
            
            self.commercialButton.backgroundColor = .white
            self.commercialButton.setTitleColor(.lightGray, for: .normal)
            
            self.newPropertyTypeArray?.removeAllObjects()

            self.newPropertyTypeArray?.add("Flat")
            self.newPropertyTypeArray?.add("Duplex")
            self.newPropertyTypeArray?.add("Bungalow")
            self.newPropertyTypeArray?.add("Row House")
            self.newPropertyTypeArray?.add("Pent House")
            
            self.selectedNewPropertyTypeArray?.removeAllObjects()
            for _ in 0..<self.newPropertyTypeArray!.count
            {
                self.selectedNewPropertyTypeArray?.add("0")
            }
            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
            
            DispatchQueue.main.async {
                self.propertyTypeCollection.reloadData()
            }
        }
    }
    
    @IBAction func commercialButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            
            self.propertyTypeStr = self.commercialButton.currentTitle ?? ""
            self.propertySubType = "Shop or Showroom"
            
            self.commercialButton.layer.cornerRadius = 15
            self.commercialButton.backgroundColor = Constants.darkBlueColor
            self.commercialButton.setTitleColor(.white, for: .normal)
            
            self.residentialButton.backgroundColor = .white
            self.residentialButton.setTitleColor(.lightGray, for: .normal)

            self.newPropertyTypeArray?.removeAllObjects()

            self.newPropertyTypeArray?.add("Shop or Showroom")
            self.newPropertyTypeArray?.add("Industrial")
            self.newPropertyTypeArray?.add("Office")
            self.newPropertyTypeArray?.add("IT")
            
            self.selectedNewPropertyTypeArray?.removeAllObjects()
            for _ in 0..<self.newPropertyTypeArray!.count
            {
                self.selectedNewPropertyTypeArray?.add("0")
            }
            
            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
            
            self.propertyTypeCollection.reloadData()
        }
    }
    
    @objc func newPropertyTypeButtonClicked(sender : UIButton)
    {
        print("newPropertyTypeButtonClicked")
        
        self.propertySubType = (sender.currentTitle ?? "")
        
        self.selectedNewPropertyTypeArray?.removeAllObjects()
        for _ in 0..<self.newPropertyTypeArray!.count
        {
            self.selectedNewPropertyTypeArray?.add("0")
        }
        
        self.selectedNewPropertyTypeArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.propertyTypeCollection.reloadData()
        }
    }
    
    //MARK: - Collection Delegate & DataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if self.newPropertyTypeArray != nil
        {
            return self.newPropertyTypeArray!.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
        
        DispatchQueue.main.async {
            myCell.titleButton.layer.cornerRadius = 12
            myCell.titleButton.layer.masksToBounds = true
            
            if ("\(self.selectedNewPropertyTypeArray?.object(at: indexPath.item) as? String ?? "")" == "1")
            {
                myCell.titleButton.backgroundColor = Constants.darkBlueColor
                myCell.titleButton.setTitleColor(.white, for: .normal)
            }
            else
            {
                myCell.titleButton.backgroundColor = .white
                myCell.titleButton.setTitleColor(.lightGray, for: .normal)
            }
        }
        
        myCell.titleButton.setTitle("\(self.newPropertyTypeArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
        myCell.titleButton.tag = indexPath.item
        myCell.titleButton.addTarget(self, action: #selector(newPropertyTypeButtonClicked(sender:)), for: .touchUpInside)
        
        return myCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 120.0, height: 40.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 15)
    }
    
    //MARK: - City Delegate
    func sendSelectedCity(cityName: String)
    {
        DispatchQueue.main.async {
            self.cityButton.setTitle("\(cityName)", for: .normal)
        }
    }
    
    //MARK: - Selected Address
    func sendSelectedAddress(place: GMSPlace)
    {
        self.tagListView.addTag("\(place.name ?? "")")
    }
}
