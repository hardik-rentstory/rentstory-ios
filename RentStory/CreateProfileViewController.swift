//
//  CreateProfileViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 11/11/20.
//

import UIKit
import DatePickerDialog
import KRProgressHUD
import AWSS3
import PromiseKit
import MobileCoreServices

class CreateProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate
{
    var profileDict : NSMutableDictionary?

    var profileNameStr = ""
    var aadharNameStr = ""
    var panNameStr = ""
    
    var genderStr = "male"
    
    var isProfileSelect = false
    var isAadharCard = false
    var isPanCard = false
    var isFromEditProfile = false
    
    var selectedProfileImage : UIImage?
    var selectedAadharImage : UIImage?
    var selectedPanCardImage : UIImage?
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var profileImgView: UIImageView!
    
    @IBOutlet weak var btnProfile: UIButton!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtAddress1: UITextField!
    
    @IBOutlet weak var txtAddress2: UITextField!
    
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var txtPostCode: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtAadhar: UITextField!
    
    @IBOutlet weak var txtPan: UITextField!
    
    
    @IBOutlet weak var btnDOB: UIButton!
    
    @IBOutlet weak var btnAadhar: UIButton!
    @IBOutlet weak var btnPan: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblPostCode: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblAadhar: UILabel!
    @IBOutlet weak var lblAadharAttach: UILabel!
    @IBOutlet weak var lblPan: UILabel!
    @IBOutlet weak var lblPanAttach: UILabel!
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    @IBOutlet weak var headerNameLabel: UILabel!
    
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipLabel: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.lblFirstName.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblLastName.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblEmail.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblDOB.font = UIFont(name: "SofiaPro-Medium", size: 25)
            
            self.lblGender.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblMale.font = UIFont(name: "SofiaPro-Medium", size: 22)
            self.lblFemale.font = UIFont(name: "SofiaPro-Medium", size: 22)
            self.lblOther.font = UIFont(name: "SofiaPro-Medium", size: 22)
            
            self.lblAddress1.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblAddress2.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblCity.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblCity.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblPostCode.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblState.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblAadhar.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblAadharAttach.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblPan.font = UIFont(name: "SofiaPro-Medium", size: 25)
            self.lblPanAttach.font = UIFont(name: "SofiaPro-Medium", size: 25)
            
            self.btnSubmit.titleLabel?.font = UIFont(name: "SofiaPro-Medium", size: 25)
        }
        
        let txtFnameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFirstName.leftView = txtFnameViewObj
        self.txtFirstName.leftViewMode = .always
        
        let txtlnameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLastName.leftView = txtlnameViewObj
        self.txtLastName.leftViewMode = .always
        
        let txtEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtEmail.leftView = txtEmailViewObj
        self.txtEmail.leftViewMode = .always
        
        let txtAddress1ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress1.leftView = txtAddress1ViewObj
        self.txtAddress1.leftViewMode = .always
        
        let txtAddress2ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress2.leftView = txtAddress2ViewObj
        self.txtAddress2.leftViewMode = .always
        
        let txtCityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCity.leftView = txtCityViewObj
        self.txtCity.leftViewMode = .always
        
        let txtPostCodeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtPostCode.leftView = txtPostCodeViewObj
        self.txtPostCode.leftViewMode = .always
        
        let txtStateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtState.leftView = txtStateViewObj
        self.txtState.leftViewMode = .always
        
        let txtAadharViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAadhar.leftView = txtAadharViewObj
        self.txtAadhar.leftViewMode = .always
        
        let txtPanNoViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtPan.leftView = txtPanNoViewObj
        self.txtPan.leftViewMode = .always
        
        self.backButton.isHidden = true
        
        if self.isFromEditProfile
        {
            self.backButton.isHidden = false
            self.headerNameLabel.text = "Edit Profile"
            self.btnSubmit.setTitle("Update", for: .normal)
            
            self.displayProfileInfo()
        }
        else
        {
            DispatchQueue.main.async {
                self.btnSubmit.setTitle("Update", for: .normal)
            }
            
//            self.getUserProfile()
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.topView.layer.cornerRadius = 40
        self.profileImgView.layer.cornerRadius = 10
        self.btnProfile.layer.cornerRadius = self.btnProfile.frame.height/2
        
        self.txtFirstName.layer.cornerRadius = 10
        self.txtLastName.layer.cornerRadius = 10
        self.txtEmail.layer.cornerRadius = 10
        self.txtAddress1.layer.cornerRadius = 10
        self.txtAddress2.layer.cornerRadius = 10
        self.txtCity.layer.cornerRadius = 10
        self.txtPostCode.layer.cornerRadius = 10
        self.txtState.layer.cornerRadius = 10
        self.txtAadhar.layer.cornerRadius = 10
        self.txtPan.layer.cornerRadius = 10
        
        self.btnDOB.layer.cornerRadius = 10
        self.btnAadhar.layer.cornerRadius = 10
        self.btnPan.layer.cornerRadius = 10
        
        self.btnSubmit.layer.cornerRadius = 12
        
        self.mainScrollView.contentSize.height = self.btnSubmit.frame.origin.y + self.btnSubmit.frame.height + 100
    }
    
    //MARK: - Display Profile Info
    func displayProfileInfo()
    {
        self.profileImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "profile_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
        
        self.txtFirstName.text = "\(self.profileDict?.value(forKey: "firstName") ?? "")"
        self.txtLastName.text = "\(self.profileDict?.value(forKey: "lastName") ?? "")"
        self.txtEmail.text = "\(self.profileDict?.value(forKey: "email") ?? "")"
        self.btnDOB.setTitle("\(self.profileDict?.value(forKey: "dob") ?? "")", for: .normal)
        
        if "\(self.profileDict?.value(forKey: "gender") ?? "")".lowercased() == "male"
        {
            self.femaleButton.isSelected = false
            self.femaleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.otherButton.isSelected = false
            self.otherButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.maleButton.isSelected = true
            self.maleButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderStr = "male"
        }
        else if "\(self.profileDict?.value(forKey: "gender") ?? "")".lowercased() == "female"
        {
            self.femaleButton.isSelected = true
            self.femaleButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.otherButton.isSelected = false
            self.otherButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.maleButton.isSelected = false
            self.maleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderStr = "female"
        }
        else
        {
            self.femaleButton.isSelected = false
            self.femaleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.otherButton.isSelected = true
            self.otherButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.maleButton.isSelected = false
            self.maleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderStr = "other"
        }
        
        
        self.txtAddress1.text = "\(self.profileDict?.value(forKey: "address1") ?? "")"
        self.txtAddress2.text = "\(self.profileDict?.value(forKey: "address2") ?? "")"
        self.txtCity.text = "\(self.profileDict?.value(forKey: "city") ?? "")"
        self.txtPostCode.text = "\(self.profileDict?.value(forKey: "zipCode") ?? "")"
        self.txtState.text = "\(self.profileDict?.value(forKey: "state") ?? "")"
        self.txtAadhar.text = "\(self.profileDict?.value(forKey: "adhar_no") ?? "")"
        self.txtPan.text = "\(self.profileDict?.value(forKey: "pan_no") ?? "")"
        
        self.profileImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "profile_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
        
        DispatchQueue.main.async {
            let aadharImg = UIImageView()
            aadharImg.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "adhar_doc_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
            self.btnAadhar.setImage(aadharImg.image, for: .normal)
            self.btnAadhar.layer.masksToBounds = true
            
            let panImg = UIImageView()
            panImg.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "pan_doc_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
            self.btnPan.setImage(panImg.image, for: .normal)
            self.btnPan.layer.masksToBounds = true
        }
        
        
        self.aadharNameStr = "\(self.profileDict?.value(forKey: "adhar_doc_path") ?? "")"
        self.profileNameStr = "\(self.profileDict?.value(forKey: "profile_path") ?? "")"
        self.panNameStr = "\(self.profileDict?.value(forKey: "pan_doc_path") ?? "")"
    }
    
    //MARK: - Button Actions
    @IBAction func submitButtonClicked(_ sender: UIButton)
    {
        if self.txtFirstName.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter First Name", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLastName.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Last Name", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtEmail.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Email Address", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if !AppUtility.isValidEmail(emailString: "\(self.txtEmail.text ?? "")")
            {
                let alertViewController = UIAlertController(title: "Error", message: "Please Enter Valid Email Address", preferredStyle: .alert)
                
                alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                    
                }))
                
                DispatchQueue.main.async {
                    self .present(alertViewController, animated: true, completion: nil)
                }
            }
            else
            {
                if Reachability.isConnectedToNetwork()
                {
                    self.createProfileResponse()
                }
                else
                {
                    let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
                    
                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                        
                    }))
                    
                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    
                }
                
            }
        }
    }
    
    @IBAction func profileButtonClicked(_ sender: UIButton)
    {
        self.isProfileSelect = true
        self.isAadharCard = false
        self.isPanCard = false
        
        let alert = UIAlertController(title: "Profile Picture", message: "Select Option To Change Profile Picture", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        self.present(alert, animated: true)
    }
    
    @IBAction func aadharButtonClicked(_ sender: UIButton)
    {
        self.isProfileSelect = false
        self.isAadharCard = true
        self.isPanCard = false
        
        let alert = UIAlertController(title: "Upload Aadhar Card", message: "Select Option To Take Aadhar Card Photo", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        self.present(alert, animated: true)
    }
    
    @IBAction func panButtonClicked(_ sender: UIButton)
    {
        self.isProfileSelect = false
        self.isAadharCard = false
        self.isPanCard = true
        
        let alert = UIAlertController(title: "Upload Pan Card", message: "Select Option To Take Pan Card Photo", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        self.present(alert, animated: true)
    }
    
    @IBAction func dobButtonClicked(_ sender: UIButton)
    {
        let datePicker = DatePickerDialog()
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        datePicker.show("Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
            
            if let dt = date
            {
                let formatter = DateFormatter()
//                formatter.dateFormat = "dd MMM - yyyy"
                formatter.dateFormat = "yyyy-MM-dd"
                print("Selected Date : \(formatter.string(from: dt))")
                
                self.btnDOB.setTitle("\(formatter.string(from: dt))", for: .normal)
            }
        }
    }
    
    
    @IBAction func maleButtonClicked(_ sender: UIButton)
    {
        self.femaleButton.isSelected = false
        self.femaleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.otherButton.isSelected = false
        self.otherButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.maleButton.isSelected
        {
            self.maleButton.isSelected = false
            self.maleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderStr = ""
        }
        else
        {
            self.maleButton.isSelected = true
            self.maleButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderStr = "male"
        }
    }
    
    @IBAction func femaleButtonClicked(_ sender: UIButton)
    {
        self.maleButton.isSelected = false
        self.maleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.otherButton.isSelected = false
        self.otherButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.femaleButton.isSelected
        {
            self.femaleButton.isSelected = false
            self.femaleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderStr = ""
        }
        else
        {
            self.femaleButton.isSelected = true
            self.femaleButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderStr = "female"
        }
    }
    
    @IBAction func otherButtonClicked(_ sender: UIButton)
    {
        self.maleButton.isSelected = false
        self.maleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.femaleButton.isSelected = false
        self.femaleButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.otherButton.isSelected
        {
            self.otherButton.isSelected = false
            self.otherButton.setImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderStr = ""
        }
        else
        {
            self.otherButton.isSelected = true
            self.otherButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderStr = "other"
        }
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skipButtonClicked(_ sender: UIButton)
    {
        AppUtility.setUserData(keyString: "isLogin", valueString: "1")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        let homeVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
        let navController = UINavigationController(rootViewController: homeVC)

        appDelegate.window?.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    //MARK: - Create Profile Response
    func createProfileResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": Int64(AppUtility.getUserProfileMobileNumber()) ?? 0,
                             "property_name":0,
                             "firstName":"\(self.txtFirstName.text ?? "")",
                             "lastName":"\(self.txtLastName.text ?? "")",
                             "dob":"\(self.btnDOB.currentTitle ?? "")",
                             "address1":"\(self.txtAddress1.text ?? "")",
                             "address2":"\(self.txtAddress2.text ?? "")",
                             "email":"\(self.txtEmail.text ?? "")",
                             "city":"\(self.txtCity.text ?? "")",
                             "zipCode": Int64(self.txtPostCode.text ?? "") ?? 0,
                             "state":"\(self.txtState.text ?? "")",
                             "adhar_doc_path":"\(self.aadharNameStr)",
                             "pan_doc_path":"\(self.panNameStr)",
                             "profile_path":"\(self.profileNameStr)",
                             "adhar_no":"\(self.txtAadhar.text ?? "")",
                             "pan_no":"\(self.txtPan.text ?? "")",
                             "gender":"\(self.genderStr)",
                             "is_temporary":"0",
                             "broker_type":"0",
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updateUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if self.profileNameStr.isEmpty != true && self.selectedProfileImage != nil
                        {
                            self.uploadProfileImage()
                        }
                        
                        if self.aadharNameStr.isEmpty != true && self.selectedAadharImage != nil
                        {
                            self.uploadAadharCardImage()
                        }
                        
                        if self.panNameStr.isEmpty != true && self.selectedPanCardImage != nil
                        {
                            self.uploadPanCardImage()
                        }
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            DispatchQueue.main.async {
                                
                                if self.isFromEditProfile
                                {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                else
                                {
                                    AppUtility.setUserData(keyString: "isLogin", valueString: "1")
                                    
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate

                                    let homeVC = HomeViewController(nibName: "HomeViewController", bundle: nil)
                                    let navController = UINavigationController(rootViewController: homeVC)

                                    
                                    appDelegate.window?.rootViewController = navController
                                    appDelegate.window?.makeKeyAndVisible()
                                }
                            }
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    func uploadProfileImage()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.selectedProfileImage!, imageName: self.profileNameStr)

            req.done { (url) in
                print("Profile Success URL : \(url)")
                
                NotificationCenter.default.post(name: Notification.Name("UpdateProfilePicNotification"), object: "\(url)")
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    func uploadAadharCardImage()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.selectedAadharImage!, imageName: self.aadharNameStr)

            req.done { (url) in
                print("AadharCard Success URL : \(url)")
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    func uploadPanCardImage()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.selectedPanCardImage!, imageName: self.panNameStr)

            req.done { (url) in
                print("PanCard Success URL : \(url)")
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
            
        }
    }
    
    //MARK: - Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])
    {
        if urls.first != nil
        {
            if self.isAadharCard
            {
                self.selectedAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.aadharNameStr = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                
                self.btnAadhar.setImage(self.selectedAadharImage, for: .normal)
                self.btnAadhar.layer.masksToBounds = true
            }
            else if self.isPanCard
            {
                self.selectedPanCardImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.panNameStr = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                
                self.btnPan.setImage(self.selectedPanCardImage, for: .normal)
                self.btnPan.layer.masksToBounds = true
            }
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            if self.isProfileSelect
            {
                self.selectedProfileImage = pickedImage
                        
                self.profileNameStr = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                
                self.profileImgView.image = self.selectedProfileImage
                self.profileImgView.layer.masksToBounds = true
            }
            else if self.isAadharCard
            {
                self.selectedAadharImage = pickedImage
                
                self.aadharNameStr = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                
                self.btnAadhar.setImage(pickedImage, for: .normal)
                self.btnAadhar.layer.masksToBounds = true
            }
            else if self.isPanCard
            {
                self.selectedPanCardImage = pickedImage
                
                self.panNameStr = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                
                self.btnPan.setImage(pickedImage, for: .normal)
                self.btnPan.layer.masksToBounds = true
            }
        }
        
        self.isProfileSelect = false
        self.isAadharCard = false
        self.isPanCard = false
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.isProfileSelect = false
        self.isAadharCard = false
        self.isPanCard = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Get User Profile
    func getUserProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno":(Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                             "searchResult" : "",
                             "emailId" : "",
                             "property_name" : "",
                             "property_id" : "",
                             "data" : "",
                             
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("User Profile Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableDictionary)
                        {
                            self.profileDict = NSMutableDictionary()
                            self.profileDict = responseObj
                        }
                        
                        DispatchQueue.main.async {

                            self.skipButton.isHidden = true
                            self.skipLabel.isHidden = true
                            
                            self.lblHeader.isHidden = false
                            
                            self.profileImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "profile_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            
                            let aadharImgView = UIImageView()
                            aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.profileDict?.value(forKey: "adhar_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
                            self.btnAadhar.setImage(aadharImgView.image, for: .normal)
                            self.btnAadhar.imageView?.contentMode = .scaleAspectFill
                            self.btnAadhar.layer.masksToBounds = true
                            
                            let panImgView = UIImageView()
                            panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.profileDict?.value(forKey: "pan_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
                            self.btnPan.setImage(panImgView.image, for: .normal)
                            self.btnPan.imageView?.contentMode = .scaleAspectFill
                            self.btnPan.layer.masksToBounds = true
                            
                            
                            self.txtFirstName.text = "\(self.profileDict?.value(forKey: "firstName") ?? "")"
                            self.txtLastName.text = "\(self.profileDict?.value(forKey: "lastName") ?? "")"
                            self.txtEmail.text = "\(self.profileDict?.value(forKey: "email") ?? "")"
                            self.btnDOB.setTitle("\(self.profileDict?.value(forKey: "dob") ?? "")", for: .normal)
                            
                            if "\(self.profileDict?.value(forKey: "gender") ?? "")" == "male"
                            {
                                self.maleButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
                                self.genderStr = "male"
                            }
                            else if "\(self.profileDict?.value(forKey: "gender") ?? "")" == "female"
                            {
                                self.femaleButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
                                self.genderStr = "female"
                            }
                            else
                            {
                                self.otherButton.setImage(UIImage(named: "selectedRadio"), for: .normal)
                                self.genderStr = "other"
                            }

                            self.txtAddress1.text = "\(self.profileDict?.value(forKey: "address1") ?? "")"
                            self.txtAddress2.text = "\(self.profileDict?.value(forKey: "address2") ?? "")"
                            self.txtCity.text = "\(self.profileDict?.value(forKey: "city") ?? "")"
                            self.txtPostCode.text = "\(self.profileDict?.value(forKey: "zipCode") ?? "")"
                            self.txtState.text = "\(self.profileDict?.value(forKey: "state") ?? "")"
                            self.txtAadhar.text = "\(self.profileDict?.value(forKey: "adhar_no") ?? "")"
                            self.txtPan.text = "\(self.profileDict?.value(forKey: "pan_no") ?? "")"
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
}
