//
//  SelectLocationViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 02/02/21.
//

import UIKit
import CoreLocation
import MapKit
import GooglePlacesSearchController

protocol SelectedAreaDelegate {
    func sendSelectedArea(placesDict : PlaceDetails)
}

class SelectLocationViewController: UIViewController
{
    let GoogleSearchPlaceAPIKey = "AIzaSyCef35C0A6wb8lOU47bswhtrgN5UXVcdtU"
    var placesSearchController: GooglePlacesSearchController!
    
    var areaDelegate : SelectedAreaDelegate!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        placesSearchController = {
            let controller = GooglePlacesSearchController(delegate: self, apiKey: GoogleSearchPlaceAPIKey, placeType: .establishment)
            controller.searchBar.delegate = self
            return controller
        }()
        
        placesSearchController.view.backgroundColor = Constants.bgColor
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.present(placesSearchController, animated: true, completion: nil)
    }
}

extension SelectLocationViewController: GooglePlacesAutocompleteViewControllerDelegate, UISearchBarDelegate
{
    func viewController(didAutocompleteWith place: PlaceDetails)
    {
        placesSearchController.isActive = false
        
        if self.areaDelegate != nil {
           
            self.areaDelegate.sendSelectedArea(placesDict: place)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        placesSearchController.isActive = false
        self.navigationController?.popViewController(animated: true)
    }
}
