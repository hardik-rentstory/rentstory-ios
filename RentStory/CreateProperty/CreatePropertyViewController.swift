//
//  CreatePropertyViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 04/12/20.
//

import UIKit
import KRProgressHUD
import AWSS3
import PromiseKit
import CoreLocation
import MapKit
import AddressBook
import AddressBookUI
import Contacts
import ContactsUI
import DatePickerDialog
import SDWebImage
import GooglePlacesSearchController
import MobileCoreServices


class CreatePropertyViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate, ABPeoplePickerNavigationControllerDelegate, CNContactPickerDelegate, SelectedAreaDelegate, UIDocumentPickerDelegate
{
    var addPropertyBrokerRepresentingBy = ""
    
    var selectedLeaseFreePeriod = "0"
    var selectedFloor = "0"
    var selectedBasement = "0"
    
    var selectedLatValue = ""
    var selectedLonValue = ""
    
    var isPropertyRented = "1"
    
    var isTenantTemporary = "0"
    var roleString = "Owner"
    
    var selectedLeaseStartDate = Date()
    var selectedLeaseRentStartDate = Date()
    
    var isPropertyPhotoSelect = false
    
    @IBOutlet weak var lblTopHeader: UILabel!
    @IBOutlet weak var mapViewObj: MKMapView!
    var annotation : MKPointAnnotation?
    var locationManager: CLLocationManager!
    
    var newPropertyTypeArray : NSMutableArray?
    var selectedNewPropertyTypeArray : NSMutableArray?
    
    var bedroomArray : NSMutableArray?
    var selectedBedroomArray : NSMutableArray?
    
    var bathroomArray : NSMutableArray?
    var selectedBathroomArray : NSMutableArray?
    
    var carParkArray : NSMutableArray?
    var selectedCarParkArray : NSMutableArray?
    
    var amenitiesArray : NSMutableArray?
    var amenitiesImageArray : NSMutableArray?
    var selectedAmenitiesArray : NSMutableArray?
    
    var propertyDocArray : NSMutableArray?
    var propertyDocImageArray : NSMutableArray?
    var propertyPhotosArray : NSMutableArray?
    var propertyPhotosNameArray : NSMutableArray?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerSubView: UIView!
    
    @IBOutlet var newPropertyView: UIView!
    
    @IBOutlet weak var headerScrollView: UIScrollView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblPageCount: UILabel!
    
    @IBOutlet weak var residentailButton: UIButton!
    @IBOutlet weak var commercialButton: UIButton!
    
    @IBOutlet weak var propertyTypeCollection: UICollectionView!
    
    @IBOutlet weak var bedroomCollection: UICollectionView!
    
    @IBOutlet weak var bathroomCollection: UICollectionView!
    
    @IBOutlet weak var lblBuiltUpArea: UILabel!
    @IBOutlet weak var builtUpAreaSlider: UISlider!
    
    @IBOutlet weak var carpetAreaSlider: UISlider!
    
    @IBOutlet weak var lblCarpetArea: UILabel!
    
    @IBOutlet weak var furnishedButton: UIButton!
    @IBOutlet weak var nonFurnishedButton: UIButton!
    @IBOutlet weak var partialButton: UIButton!
    
    @IBOutlet weak var carParkCollection: UICollectionView!
    
    @IBOutlet weak var amenitiesCollection: UICollectionView!
    
    @IBOutlet weak var propertyPhotosCollection: UICollectionView!
    
//    @IBOutlet weak var propertyDocumentCollection: UICollectionView!
    
    @IBOutlet var propertyLocationView: UIView!
    
    @IBOutlet var addTenantView: UIView!
    
    @IBOutlet var leaseView: UIView!
    
    @IBOutlet weak var leaseCompanyButton: UIButton!
    
    @IBOutlet weak var leaseIndividualButton: UIButton!
    
//    @IBOutlet weak var leasePropertyDocCollectionView: UICollectionView!
    @IBOutlet var brokerDetailView: UIView!
    
    @IBOutlet var propertyManagerView: UIView!
    
    @IBOutlet weak var skipButton: UIButton!
    
    var addNewPropertyViewObj : UIView?
    var propertyLocationViewObj : UIView?
    var addTenantViewObj : UIView?
    var leaseAgreementViewObj : UIView?
    var brokerDetailsViewObj : UIView?
    var propertyManagerDetailsViewObj : UIView?
    
    var addNewPropertyImgViewObj : UIImageView?
    var propertyLocationImgViewObj : UIImageView?
    var addTenantImgViewObj : UIImageView?
    var leaseAgreementImgViewObj : UIImageView?
    var brokerDetailsImgViewObj : UIImageView?
    var propertyManagerDetailsImgViewObj : UIImageView?
    
    var addNewPropertyLabel : UILabel?
    var propertyLocationLabel : UILabel?
    var addTenantLabel : UILabel?
    var leaseAgreementLabel : UILabel?
    var brokerDetailsLabel : UILabel?
    var propertyManagerDetailsLabel : UILabel?
    
    //Add New Property
    @IBOutlet weak var txtPropertyName: UITextField!
    @IBOutlet weak var roleView: UIView!
    @IBOutlet weak var roleButton: UIButton!
    @IBOutlet weak var propertyPhotoHeightConstraint: NSLayoutConstraint!
    
    var isRegistrationSelect = false
    var isLightBillSelect = false
    var isOwnerId = false
    
    var registrationImage : UIImage?
    var registrationDocPath = ""
    
    var lightBillImage : UIImage?
    var lightBillDocPath = ""
    
    var ownerIdImage : UIImage?
    var ownerIdDocPath = ""

    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var lightBillButton: UIButton!
    @IBOutlet weak var ownerIdButton: UIButton!
    
    
    var propertyTypeStr = "Residential"
    var propertySubType = "Flat"
    
    var noOfRoomsStr = ""
    var noOfBathroomsStr = ""
    var builtUpAreaStr = ""
    var carpetAreaStr = ""
    var furnishedTypeStr = "Furnished"
    var noOfCarParkStr = ""
    var amenitiesStr = ""
    
    //Property Location
    @IBOutlet weak var txtFlatNo: UITextField!
    @IBOutlet weak var txtBuildingName: UITextField!
    @IBOutlet weak var txtLocality: UITextField!
    @IBOutlet weak var txtFloor: UITextField!
    @IBOutlet weak var txtWing: UITextField!
    @IBOutlet weak var txtAddress1: UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    
    
    @IBOutlet weak var txtBuiltUpArea: UITextField!
    @IBOutlet weak var txtCarpetArea: UITextField!
    // Add Tenant
    @IBOutlet weak var tenantProfileImageView: UIImageView!
    
    @IBOutlet weak var txtTenantMobileNumber: UITextField!
    @IBOutlet weak var txtTenantFirstName: UITextField!
    @IBOutlet weak var txtTenantLastName: UITextField!
    @IBOutlet weak var txtTenantEmail: UITextField!
    @IBOutlet weak var txtTenantDOB: UITextField!
    @IBOutlet weak var txtTenantAadhar: UITextField!
    @IBOutlet weak var txtTenantPan: UITextField!
    @IBOutlet weak var tenantAadharButton: UIButton!
    @IBOutlet weak var tenantPanButton: UIButton!
    @IBOutlet weak var tenantMaleButton: UIButton!
    @IBOutlet weak var tenantFemaleButton: UIButton!
    @IBOutlet weak var tenantOtherButton: UIButton!
    
    var isTenantProfileSelect = false
    var isTenantAadharSelect = false
    var isTenantPanSelect = false
    
    var tenantProfileImagePath = ""
    var tenantAadharImagePath = ""
    var tenantPanImagePath = ""
    var propertyIdStr = ""
    var tenantGenderString = ""
    
    var tenantAadharImage : UIImage?
    var tenantPanImage : UIImage?
    
    var companyInfoArray = [String]()
    var bankInfoArray = [String]()
    
    
    // Lease Agreement Details
    @IBOutlet weak var txtLeaseCompanyName: UITextField!
    @IBOutlet weak var txtLeaseAddress: UITextField!
    @IBOutlet weak var txtLeaseContactPerson: UITextField!
    @IBOutlet weak var txtLeaseDesignation: UITextField!
    @IBOutlet weak var txtLeaseContactNumber: UITextField!
    @IBOutlet weak var txtLeaseEmail: UITextField!
    @IBOutlet weak var txtLeaseFreePeriod: UITextField!
    @IBOutlet weak var txtLeaseStartDate: UITextField!
    @IBOutlet weak var txtLeaseMonthlyRentAmt: UITextField!
    @IBOutlet weak var txtLeaseSecurityDeposite: UITextField!
    @IBOutlet weak var txtLeaseRentStartDate: UITextField!
    
    
    @IBOutlet weak var leaseYearButton: UIButton!
    @IBOutlet weak var leaseMonthButton: UIButton!
    @IBOutlet weak var leaseRentDueDate: UIButton!
    @IBOutlet weak var leaseEscalationYearButton: UIButton!
    @IBOutlet weak var leaseEscalationMonthButton: UIButton!
    @IBOutlet weak var leaseEscalationPercentageButton: UIButton!
    @IBOutlet weak var leaseOwnerLockInButton: UIButton!
    @IBOutlet weak var leaseTenantLockInButton: UIButton!
    @IBOutlet weak var leaseNoticeButton: UIButton!
    @IBOutlet weak var leaseAgreementButton: UIButton!
    @IBOutlet weak var leasePoliceVerificationButton: UIButton!
    @IBOutlet weak var leaseNOCButton: UIButton!
    
    var leaseCompanyInfoArray = [String]()
    var leaseDurationArray = [String]()
    var leaseEscalationArray = [String]()
    
    var leaseRentImagePath = ""
    var leasePoliceImagePath = ""
    var leaseSocietyNOCImagePath = ""
    var leaseRentDueDateStr = ""
    
    var leaseAgreementImage : UIImage?
    var leasePoliceVerificationImage : UIImage?
    var leaseNOCImage : UIImage?
    
    var leaseTypeStr = "Company"
    var applyGST : Int = 0
    
    var isLeaseAgreementSelect = false
    var isLeasePoliceVerificationSelect = false
    var isLeaseNOCSelect = false
    
    var leaseAgreementImagePath = ""
    var leasePoliceVerificationImagePath = ""
    var leaseNOCImagePath = ""
    
    // Broker Details
    @IBOutlet weak var brokerProfilePic: UIImageView!
    
    @IBOutlet weak var txtBrokerMobileNumber: UITextField!
    @IBOutlet weak var txtBrokerFirstName: UITextField!
    @IBOutlet weak var txtBrokerLastName: UITextField!
    @IBOutlet weak var txtBrokerEmail: UITextField!
    @IBOutlet weak var txtBrokerDOB: UITextField!
    @IBOutlet weak var txtBrokerCompanyName: UITextField!
    @IBOutlet weak var txtBrokerAddress1: UITextField!
    @IBOutlet weak var txtBrokerAddress2: UITextField!
    @IBOutlet weak var txtBrokerCity: UITextField!
    @IBOutlet weak var txtBrokerZipCode: UITextField!
    @IBOutlet weak var txtBrokerState: UITextField!
    @IBOutlet weak var txtBrokerCountry: UITextField!
    @IBOutlet weak var txtBrokerAadhar: UITextField!
    @IBOutlet weak var txtBrokerPan: UITextField!
    
    @IBOutlet weak var brokerMaleButton: UIButton!
    @IBOutlet weak var brokerFemaleButton: UIButton!
    @IBOutlet weak var brokerOtherButton: UIButton!
    @IBOutlet weak var brokerOwnerButton: UIButton!
    @IBOutlet weak var brokerTenantButton: UIButton!
    @IBOutlet weak var brokerBothButton: UIButton!
    @IBOutlet weak var brokerAadharButton: UIButton!
    @IBOutlet weak var brokerPanButton: UIButton!
    
    var isBrokerProfileSelect = false
    var isBrokerAadharSelect = false
    var isBrokerPanSelect = false
    
    var brokerProfileImagePath = ""
    var brokerAadharImagePath = ""
    var brokerPanImagePath = ""
    
    var brokerTypeStr = "Owner"
    var brokerGender = "male"
    
    var brokerProfileImage : UIImage?
    var brokerAadharImage : UIImage?
    var brokerPanImage : UIImage?
    
    var isBrokerMobile = false
    
    // Manager Details
    @IBOutlet weak var managerProfileImgView: UIImageView!
    
    var managerProfileImage : UIImage?
    var managerAadharImage : UIImage?
    var managerPanImage : UIImage?
    
    @IBOutlet weak var managerMaleButton: UIButton!
    @IBOutlet weak var managerFemaleButton: UIButton!
    @IBOutlet weak var managerOtherButton: UIButton!
    @IBOutlet weak var managerAadharButton: UIButton!
    @IBOutlet weak var managerPanButton: UIButton!
    
    @IBOutlet weak var txtManagerMobile: UITextField!
    @IBOutlet weak var txtManagerFirstName: UITextField!
    @IBOutlet weak var txtManagerLastName: UITextField!
    @IBOutlet weak var txtManagerEmail: UITextField!
    @IBOutlet weak var txtManagerDOB: UITextField!
    @IBOutlet weak var txtManagerAadhar: UITextField!
    @IBOutlet weak var txtManagerPan: UITextField!
    
    var isManagerMobile = false
    
    var isManagerProfileSelect = false
    var isManagerAadharSelect = false
    var isManagerPanSelect = false
    
    var managerProfileImagePath = ""
    var managerAadharImagePath = ""
    var managerPanImagePath = ""
    var managerGender = "male"
    
    
    @IBOutlet weak var amenitiesTop: NSLayoutConstraint!
    @IBOutlet weak var amenitiesHeight: NSLayoutConstraint!
    @IBOutlet weak var txtOtherAmenities: UITextField!
    
    @IBOutlet weak var lblBedRoomTop: NSLayoutConstraint!
    @IBOutlet weak var lblBedRoomHeight: NSLayoutConstraint!
    @IBOutlet weak var bedRoomCollectionTop: NSLayoutConstraint!
    @IBOutlet weak var bedRoomCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblBathRoomTop: NSLayoutConstraint!
    @IBOutlet weak var lblBathRoomHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bathRoomCollectionTop: NSLayoutConstraint!
    @IBOutlet weak var bathRoomCollectionHeight: NSLayoutConstraint!
    
    var occupancyTypeStr = ""
    @IBOutlet weak var familyButton: UIButton!
    @IBOutlet weak var bachelorButton: UIButton!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var occupancyTypeView: UIView!
   
    
    @IBOutlet weak var lblLeaseAddressTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseAddressHeight: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseAddressTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseAddressHeight: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseContactTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseContactHeight: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseContactTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseContactHeight: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseDesignationTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseDesignationHeight: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseDesignationTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseDesignationHeight: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseContactNumTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseContactNumHeight: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseContactNumTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseContactNumHeight: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseEmailTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseEmailTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseEmailHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var createPropertyHeaderHeight: NSLayoutConstraint!
    @IBOutlet weak var propertyRentedLabel: UILabel!
    @IBOutlet weak var yesLabel: UILabel!
    @IBOutlet weak var switchObj: UISwitch!
    
    @IBOutlet weak var addPropertyBrokerRepresentingButton: UIButton!

    @IBOutlet weak var brokerRepresentingLabelTop: NSLayoutConstraint!
    @IBOutlet weak var brokerRepresentingLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var brokerRepresentingViewTop: NSLayoutConstraint!
    @IBOutlet weak var brokerRepresentingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addPropertyBrokerRepresentingView: UIView!
    
    @IBOutlet weak var wingLabel: UILabel!
    @IBOutlet weak var wingDropImg: UIImageView!
    
    
    @IBOutlet weak var uploadPhotosLabel: UILabel!
    @IBOutlet weak var uploadPhotosLabelWidth: NSLayoutConstraint!
    
    @IBOutlet weak var tenantCameraButton: UIButton!
    @IBOutlet weak var brokerCameraButton: UIButton!
    @IBOutlet weak var managerButton: UIButton!
    @IBOutlet weak var amenitiesNoteHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tenantAadharButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tenantAadharLabelHeight: NSLayoutConstraint!
   
    @IBOutlet weak var tenantPanButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tenantPanLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblApplyGST: UILabel!
    @IBOutlet weak var gstToggle: UISwitch!
    @IBOutlet weak var lblGSTNoteHeight: NSLayoutConstraint!
    
    @IBOutlet weak var brokerAadharButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var brokerAadharLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var brokerPanButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var brokerPanLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var managerAadharButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var managerAadharLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var managerPanButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var managerPanLabelHeight: NSLayoutConstraint!
    
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
                
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        
        self.view.backgroundColor = Constants.bgColor
        self.registrationButton.layer.masksToBounds = true
        self.lightBillButton.layer.masksToBounds = true
        self.ownerIdButton.layer.masksToBounds = true
        
        self.locationManager = CLLocationManager()
        self.locationManager.requestAlwaysAuthorization()
        //or use requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.delegate = self
        self.mapViewObj.isScrollEnabled = false
        
        
        self.propertyPhotosNameArray = NSMutableArray()
        self.propertyPhotosArray = NSMutableArray()
        self.propertyPhotoHeightConstraint.constant = 0
        
        // Add New Property
        self.txtPropertyName.delegate = self
        let txtPropertyNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtPropertyName.leftView = txtPropertyNameViewObj
        self.txtPropertyName.leftViewMode = .always
        
        self.txtBuiltUpArea.delegate = self
        let txtBuiltUpAreaViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBuiltUpArea.leftView = txtBuiltUpAreaViewObj
        self.txtBuiltUpArea.leftViewMode = .always
        
        self.txtCarpetArea.delegate = self
        let txtCarpetAreaViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCarpetArea.leftView = txtCarpetAreaViewObj
        self.txtCarpetArea.leftViewMode = .always
        
        // Property Location
        self.txtFlatNo.delegate = self
        let txtFlatNoViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFlatNo.leftView = txtFlatNoViewObj
        self.txtFlatNo.leftViewMode = .always
        
        self.txtBuildingName.delegate = self
        let txtBuildingNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBuildingName.leftView = txtBuildingNameViewObj
        self.txtBuildingName.leftViewMode = .always
        
        self.txtLocality.delegate = self
        let txtLocalityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLocality.leftView = txtLocalityViewObj
        self.txtLocality.leftViewMode = .always
        
        self.txtFloor.delegate = self
        let txtFloorViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFloor.leftView = txtFloorViewObj
        self.txtFloor.leftViewMode = .always
        
        self.txtWing.delegate = self
        let txtWingViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtWing.leftView = txtWingViewObj
        self.txtWing.leftViewMode = .always
        
        self.txtAddress1.delegate = self
        let txtAddress1ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress1.leftView = txtAddress1ViewObj
        self.txtAddress1.leftViewMode = .always
        
        self.txtAddress2.delegate = self
        let txtAddress2ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress2.leftView = txtAddress2ViewObj
        self.txtAddress2.leftViewMode = .always
        
        self.txtCity.delegate = self
        let txtCityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCity.leftView = txtCityViewObj
        self.txtCity.leftViewMode = .always
        
        self.txtZipCode.delegate = self
        let txtZipCodeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtZipCode.leftView = txtZipCodeViewObj
        self.txtZipCode.leftViewMode = .always
        
        self.txtState.delegate = self
        let txtStateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtState.leftView = txtStateViewObj
        self.txtState.leftViewMode = .always
        
        self.txtCountry.delegate = self
        let txtCountryViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCountry.leftView = txtCountryViewObj
        self.txtCountry.leftViewMode = .always
        
        // Add New Tenant
        self.txtTenantMobileNumber.delegate = self
        let txtTenantMobileNumberViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantMobileNumber.leftView = txtTenantMobileNumberViewObj
        self.txtTenantMobileNumber.leftViewMode = .always
        
        self.txtTenantFirstName.delegate = self
        let txtTenantFirstNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantFirstName.leftView = txtTenantFirstNameViewObj
        self.txtTenantFirstName.leftViewMode = .always
        
        self.txtTenantLastName.delegate = self
        let txtTenantLastNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantLastName.leftView = txtTenantLastNameViewObj
        self.txtTenantLastName.leftViewMode = .always
        
        self.txtTenantEmail.delegate = self
        let txtTenantEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantEmail.leftView = txtTenantEmailViewObj
        self.txtTenantEmail.leftViewMode = .always
        
        self.txtTenantDOB.delegate = self
        let txtTenantDOBViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantDOB.leftView = txtTenantDOBViewObj
        self.txtTenantDOB.leftViewMode = .always
        
        self.txtTenantAadhar.delegate = self
        let txtTenantAadharViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantAadhar.leftView = txtTenantAadharViewObj
        self.txtTenantAadhar.leftViewMode = .always
        
        self.txtTenantPan.delegate = self
        let txtTenantPanViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtTenantPan.leftView = txtTenantPanViewObj
        self.txtTenantPan.leftViewMode = .always
        
        
        // Lease Agreement Details
        self.txtLeaseCompanyName.delegate = self
        let txtLeaseCompanyNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseCompanyName.leftView = txtLeaseCompanyNameViewObj
        self.txtLeaseCompanyName.leftViewMode = .always
        
        self.txtLeaseAddress.delegate = self
        let txtLeaseAddressViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseAddress.leftView = txtLeaseAddressViewObj
        self.txtLeaseAddress.leftViewMode = .always
        
        self.txtLeaseContactPerson.delegate = self
        let txtLeaseContactPersonViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseContactPerson.leftView = txtLeaseContactPersonViewObj
        self.txtLeaseContactPerson.leftViewMode = .always
        
        self.txtLeaseDesignation.delegate = self
        let txtLeaseDesignationViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseDesignation.leftView = txtLeaseDesignationViewObj
        self.txtLeaseDesignation.leftViewMode = .always
        
        self.txtLeaseContactNumber.delegate = self
        let txtLeaseContactNumberViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseContactNumber.leftView = txtLeaseContactNumberViewObj
        self.txtLeaseContactNumber.leftViewMode = .always
        
        self.txtLeaseEmail.delegate = self
        let txtLeaseEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseEmail.leftView = txtLeaseEmailViewObj
        self.txtLeaseEmail.leftViewMode = .always
        
        self.txtLeaseFreePeriod.delegate = self
        let txtLeaseFreePeriodViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseFreePeriod.leftView = txtLeaseFreePeriodViewObj
        self.txtLeaseFreePeriod.leftViewMode = .always
        
        self.txtLeaseStartDate.delegate = self
        let txtLeaseStartDateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseStartDate.leftView = txtLeaseStartDateViewObj
        self.txtLeaseStartDate.leftViewMode = .always
        
        self.txtLeaseRentStartDate.delegate = self
        let txtLeaseRentStartDateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseRentStartDate.leftView = txtLeaseRentStartDateViewObj
        self.txtLeaseRentStartDate.leftViewMode = .always
        
        self.txtLeaseMonthlyRentAmt.delegate = self
        let txtLeaseMonthlyRentAmtViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseMonthlyRentAmt.leftView = txtLeaseMonthlyRentAmtViewObj
        self.txtLeaseMonthlyRentAmt.leftViewMode = .always
        
        self.txtLeaseSecurityDeposite.delegate = self
        let txtLeaseSecurityDepositeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseSecurityDeposite.leftView = txtLeaseSecurityDepositeViewObj
        self.txtLeaseSecurityDeposite.leftViewMode = .always
        
        
        // Broker Details
        self.txtBrokerMobileNumber.delegate = self
        let txtBrokerMobileNumberViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerMobileNumber.leftView = txtBrokerMobileNumberViewObj
        self.txtBrokerMobileNumber.leftViewMode = .always
        
        self.txtBrokerFirstName.delegate = self
        let txtBrokerFirstNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerFirstName.leftView = txtBrokerFirstNameViewObj
        self.txtBrokerFirstName.leftViewMode = .always
        
        self.txtBrokerLastName.delegate = self
        let txtBrokerLastNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerLastName.leftView = txtBrokerLastNameViewObj
        self.txtBrokerLastName.leftViewMode = .always
        
        self.txtBrokerEmail.delegate = self
        let txtBrokerEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerEmail.leftView = txtBrokerEmailViewObj
        self.txtBrokerEmail.leftViewMode = .always
        
        self.txtBrokerDOB.delegate = self
        let txtBrokerDOBViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerDOB.leftView = txtBrokerDOBViewObj
        self.txtBrokerDOB.leftViewMode = .always
        
        self.txtBrokerCompanyName.delegate = self
        let txtBrokerCompanyNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerCompanyName.leftView = txtBrokerCompanyNameViewObj
        self.txtBrokerCompanyName.leftViewMode = .always
        
        self.txtBrokerAddress1.delegate = self
        let txtBrokerAddress1ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerAddress1.leftView = txtBrokerAddress1ViewObj
        self.txtBrokerAddress1.leftViewMode = .always
        
        self.txtBrokerAddress2.delegate = self
        let txtBrokerAddress2ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerAddress2.leftView = txtBrokerAddress2ViewObj
        self.txtBrokerAddress2.leftViewMode = .always
        
        self.txtBrokerCity.delegate = self
        let txtBrokerCityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerCity.leftView = txtBrokerCityViewObj
        self.txtBrokerCity.leftViewMode = .always
        
        self.txtBrokerZipCode.delegate = self
        let txtBrokerZipCodeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerZipCode.leftView = txtBrokerZipCodeViewObj
        self.txtBrokerZipCode.leftViewMode = .always
        
        self.txtBrokerState.delegate = self
        let txtBrokerStateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerState.leftView = txtBrokerStateViewObj
        self.txtBrokerState.leftViewMode = .always
        
        self.txtBrokerCountry.delegate = self
        let txtBrokerCountryViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerCountry.leftView = txtBrokerCountryViewObj
        self.txtBrokerCountry.leftViewMode = .always
        
        self.txtBrokerAadhar.delegate = self
        let txtBrokerAadharViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerAadhar.leftView = txtBrokerAadharViewObj
        self.txtBrokerAadhar.leftViewMode = .always
        
        self.txtBrokerPan.delegate = self
        let txtBrokerPanViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBrokerPan.leftView = txtBrokerPanViewObj
        self.txtBrokerPan.leftViewMode = .always
        
        
        // Manager Details
        self.txtManagerMobile.delegate = self
        let txtManagerMobileViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerMobile.leftView = txtManagerMobileViewObj
        self.txtManagerMobile.leftViewMode = .always
        
        self.txtManagerFirstName.delegate = self
        let txtManagerFirstNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerFirstName.leftView = txtManagerFirstNameViewObj
        self.txtManagerFirstName.leftViewMode = .always
        
        self.txtManagerLastName.delegate = self
        let txtManagerLastNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerLastName.leftView = txtManagerLastNameViewObj
        self.txtManagerLastName.leftViewMode = .always
        
        self.txtManagerEmail.delegate = self
        let txtManagerEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerEmail.leftView = txtManagerEmailViewObj
        self.txtManagerEmail.leftViewMode = .always
        
        self.txtManagerDOB.delegate = self
        let txtManagerDOBViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerDOB.leftView = txtManagerDOBViewObj
        self.txtManagerDOB.leftViewMode = .always
        
        self.txtManagerAadhar.delegate = self
        let txtManagerAadharViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerAadhar.leftView = txtManagerAadharViewObj
        self.txtManagerAadhar.leftViewMode = .always
        
        self.txtManagerPan.delegate = self
        let txtManagerPanViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtManagerPan.leftView = txtManagerPanViewObj
        self.txtManagerPan.leftViewMode = .always
        
        self.txtOtherAmenities.delegate = self
        let txtOtherAmenitiesViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtOtherAmenities.leftView = txtOtherAmenitiesViewObj
        self.txtOtherAmenities.leftViewMode = .always
        
        self.newPropertyTypeArray = NSMutableArray()
        self.newPropertyTypeArray?.add("Flat")
        self.newPropertyTypeArray?.add("Duplex")
        self.newPropertyTypeArray?.add("Pent house")
        self.newPropertyTypeArray?.add("Row House")
        self.newPropertyTypeArray?.add("Bungalow")
        
        
        self.selectedNewPropertyTypeArray = NSMutableArray()
        for _ in 0..<self.newPropertyTypeArray!.count
        {
            self.selectedNewPropertyTypeArray?.add("0")
        }
        self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
        
        self.bedroomArray = NSMutableArray()
        self.bedroomArray?.add("0")
        self.bedroomArray?.add("1")
        self.bedroomArray?.add("2")
        self.bedroomArray?.add("3")
        self.bedroomArray?.add("4")
        self.bedroomArray?.add("5")
        self.bedroomArray?.add("6")
        self.bedroomArray?.add("7")
        self.bedroomArray?.add("8")
        self.bedroomArray?.add("9")
        self.bedroomArray?.add("10")
        self.bedroomArray?.add("10+")
        
        self.selectedBedroomArray = NSMutableArray()
        for _ in 0..<self.bedroomArray!.count
        {
            self.selectedBedroomArray?.add("0")
        }
        
        self.bathroomArray = NSMutableArray()
        self.bathroomArray?.add("0")
        self.bathroomArray?.add("1")
        self.bathroomArray?.add("2")
        self.bathroomArray?.add("3")
        self.bathroomArray?.add("4")
        self.bathroomArray?.add("5")
        self.bathroomArray?.add("6")
        self.bathroomArray?.add("7")
        self.bathroomArray?.add("8")
        self.bathroomArray?.add("9")
        self.bathroomArray?.add("10")
        self.bathroomArray?.add("10+")
        
        self.selectedBathroomArray = NSMutableArray()
        for _ in 0..<self.bathroomArray!.count
        {
            self.selectedBathroomArray?.add("0")
        }
        
        self.carParkArray = NSMutableArray()
        self.carParkArray?.add("0")
        self.carParkArray?.add("1")
        self.carParkArray?.add("2")
        self.carParkArray?.add("3")
        self.carParkArray?.add("4")
        self.carParkArray?.add("5")
        self.carParkArray?.add("6")
        self.carParkArray?.add("7")
        self.carParkArray?.add("8")
        self.carParkArray?.add("9")
        self.carParkArray?.add("10")
        self.carParkArray?.add("10+")
        
        self.selectedCarParkArray = NSMutableArray()
        for _ in 0..<self.carParkArray!.count
        {
            self.selectedCarParkArray?.add("0")
        }
        
        self.amenitiesArray = NSMutableArray()
        self.amenitiesArray?.add("Swimming pool")
        self.amenitiesArray?.add("fitness")
        self.amenitiesArray?.add("Badminton Court")
        self.amenitiesArray?.add("Free Wifi")
        self.amenitiesArray?.add("Other")
        
        self.amenitiesImageArray = NSMutableArray()
        self.amenitiesImageArray?.add("swimmingPool")
        self.amenitiesImageArray?.add("fitness")
        self.amenitiesImageArray?.add("badminton")
        self.amenitiesImageArray?.add("wifi")
        self.amenitiesImageArray?.add("roundPlus")
        
        
        self.selectedAmenitiesArray = NSMutableArray()
        for _ in 0..<self.amenitiesArray!.count
        {
            self.selectedAmenitiesArray?.add("0")
        }
        
        self.propertyDocArray = NSMutableArray()
        self.propertyDocArray?.add("Registration")
        self.propertyDocArray?.add("Light Bill")
        self.propertyDocArray?.add("Owner ID")
        
        self.propertyDocImageArray = NSMutableArray()
        self.propertyDocImageArray?.add(UIImage(named: "plus")!)
        self.propertyDocImageArray?.add(UIImage(named: "plus")!)
        self.propertyDocImageArray?.add(UIImage(named: "plus")!)
        
//        self.createHeaderView()
        
        self.residentailButton.layer.cornerRadius = 15
        self.residentailButton.backgroundColor = Constants.darkBlueColor
        self.residentailButton.layer.masksToBounds = true
        
        self.commercialButton.layer.cornerRadius = 15
        self.commercialButton.backgroundColor = .white
        self.commercialButton.layer.masksToBounds = true
        
        self.leaseCompanyButton.layer.cornerRadius = 15
        self.leaseCompanyButton.backgroundColor = Constants.darkBlueColor
        self.leaseCompanyButton.layer.masksToBounds = true
        
        self.leaseIndividualButton.layer.cornerRadius = 15
        self.leaseIndividualButton.backgroundColor = .white
        self.leaseIndividualButton.layer.masksToBounds = true
        
        self.propertyTypeCollection.dataSource = self
        self.propertyTypeCollection.delegate = self
        
        let propertyTypeCellNib = UINib(nibName: "NewPropertyTypeCollectionViewCell", bundle: nil)
        self.propertyTypeCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.propertyTypeCollection.showsHorizontalScrollIndicator = false
        
        self.bedroomCollection.dataSource = self
        self.bedroomCollection.delegate = self
        self.bedroomCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.bedroomCollection.showsHorizontalScrollIndicator = false
        
        self.bathroomCollection.dataSource = self
        self.bathroomCollection.delegate = self
        self.bathroomCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.bathroomCollection.showsHorizontalScrollIndicator = false
        
        self.carParkCollection.dataSource = self
        self.carParkCollection.delegate = self
        self.carParkCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.carParkCollection.showsHorizontalScrollIndicator = false
        
        self.amenitiesCollection.dataSource = self
        self.amenitiesCollection.delegate = self
        
        let amenitiesCollectionNib = UINib(nibName: "BoxCollectionViewCell", bundle: nil)
        self.amenitiesCollection.register(amenitiesCollectionNib, forCellWithReuseIdentifier: "boxCollectionCell")
        self.amenitiesCollection.showsHorizontalScrollIndicator = false
        
        
        self.propertyPhotosCollection.dataSource = self
        self.propertyPhotosCollection.delegate = self
        let propertyPhotoCollectionNib = UINib(nibName: "PropertyPhotosCollectionViewCell", bundle: nil)
        self.propertyPhotosCollection.register(propertyPhotoCollectionNib, forCellWithReuseIdentifier: "propertyPhotoCell")
        self.propertyPhotosCollection.showsHorizontalScrollIndicator = false
        
//        self.propertyDocumentCollection.dataSource = self
//        self.propertyDocumentCollection.delegate = self
//        self.propertyDocumentCollection.register(amenitiesCollectionNib, forCellWithReuseIdentifier: "propertyInfoCell")
//        self.propertyDocumentCollection.showsHorizontalScrollIndicator = false
        
//        self.leasePropertyDocCollectionView.dataSource = self
//        self.leasePropertyDocCollectionView.delegate = self
//        self.leasePropertyDocCollectionView.register(amenitiesCollectionNib, forCellWithReuseIdentifier: "propertyInfoCell")
//        self.leasePropertyDocCollectionView.showsHorizontalScrollIndicator = false
        
        self.headerView.layer.cornerRadius = 30
        self.headerView.layer.masksToBounds = true
        
        self.headerSubView.layer.cornerRadius = 30
        self.headerSubView.layer.masksToBounds = true
        
        self.newPropertyView.backgroundColor = .clear
        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
        self.mainScrollView.addSubview(self.newPropertyView)
        self.mainScrollView.contentSize.height = 1800
        self.newPropertyView.isHidden = false
        
        self.propertyLocationView.backgroundColor = .clear
        self.propertyLocationView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1200)
        self.mainScrollView.addSubview(self.propertyLocationView)
        self.propertyLocationView.isHidden = true
        
        self.addTenantView.backgroundColor = .clear
        self.addTenantView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1300)
        self.mainScrollView.addSubview(self.addTenantView)
        self.addTenantView.isHidden = true
        
        self.leaseView.backgroundColor = .clear
        self.leaseView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2350)
        self.mainScrollView.addSubview(self.leaseView)
        self.leaseView.isHidden = true
        
        self.brokerDetailView.backgroundColor = .clear
        self.brokerDetailView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
        self.mainScrollView.addSubview(self.brokerDetailView)
        self.brokerDetailView.isHidden = true
        
        self.propertyManagerView.backgroundColor = .clear
        self.propertyManagerView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1250)
        self.mainScrollView.addSubview(self.propertyManagerView)
        self.propertyManagerView.isHidden = true
        
        self.lblHeader.text = "Add new property (17% completed)"
        self.lblHeader.font = UIFont(name: "SofiaPro-Medium", size: 14)
        
        self.lblPageCount.text = "1/6"
        self.lblPageCount.font = UIFont(name: "SofiaPro-Bold", size: 14)
        
        self.skipButton.isHidden = true
        
        self.brokerRepresentingLabelTop.constant = 0
        self.brokerRepresentingLabelHeight.constant = 0
        
        self.brokerRepresentingViewTop.constant = 0
        self.brokerRepresentingViewHeight.constant = 0
        
        self.addPropertyBrokerRepresentingView.isHidden = true
        
        /////
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.skipButton.addBottomViewBorder(.white, height: 0.8)
        
//        self.headerView.layer.cornerRadius = 30
//        self.headerView.layer.masksToBounds = true
//
//        self.headerSubView.layer.cornerRadius = 30
//        self.headerSubView.layer.masksToBounds = true
//
//        self.homeView.layer.cornerRadius = 20
//        self.homeView.layer.masksToBounds = true
//
//        self.newPropertyView.backgroundColor = .clear
//        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1800)
//        self.mainScrollView.addSubview(self.newPropertyView)
//        self.mainScrollView.contentSize.height = 1800
//        self.newPropertyView.isHidden = false
//
//        self.propertyLocationView.backgroundColor = .clear
//        self.propertyLocationView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1200)
//        self.mainScrollView.addSubview(self.propertyLocationView)
//        self.propertyLocationView.isHidden = true
//
//        self.addTenantView.backgroundColor = .clear
//        self.addTenantView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1300)
//        self.mainScrollView.addSubview(self.addTenantView)
//        self.addTenantView.isHidden = true
//
//        self.leaseView.backgroundColor = .clear
//        self.leaseView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
//        self.mainScrollView.addSubview(self.leaseView)
//        self.leaseView.isHidden = true
    }
    
    func createHeaderView()
    {
//        // Add New Property
//        self.addNewPropertyViewObj = UIView(frame: CGRect(x: 25, y: 5, width: 70, height: 70))
//        self.addNewPropertyViewObj?.backgroundColor = Constants.darkBlueColor
//        self.addNewPropertyViewObj?.layer.cornerRadius = 15
//        self.headerScrollView.addSubview(self.addNewPropertyViewObj!)
//
//        self.addNewPropertyImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 40, height: 40))
//        self.addNewPropertyImgViewObj?.image = UIImage(named: "createPropertyGray")
//        self.addNewPropertyViewObj?.addSubview(self.addNewPropertyImgViewObj!)
        
        self.addNewPropertyViewObj = UIView(frame: CGRect(x: 25, y: 10, width: 60, height: 60))
        self.addNewPropertyViewObj?.backgroundColor = Constants.darkBlueColor
        self.addNewPropertyViewObj?.layer.cornerRadius = 15
        self.headerScrollView.addSubview(self.addNewPropertyViewObj!)
        
        self.addNewPropertyImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        self.addNewPropertyImgViewObj?.image = UIImage(named: "createPropertyGray")
        self.addNewPropertyViewObj?.addSubview(self.addNewPropertyImgViewObj!)
        
        self.addNewPropertyLabel = UILabel(frame: CGRect(x: -25, y: 30, width: 25, height: 1))
        self.addNewPropertyLabel?.backgroundColor = Constants.darkBlueColor
        self.addNewPropertyViewObj?.addSubview(self.addNewPropertyLabel!)
        
        // Property Location
        self.propertyLocationViewObj = UIView(frame: CGRect(x: (self.addNewPropertyViewObj?.frame.origin.x)! + (self.addNewPropertyViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60))
        self.propertyLocationViewObj?.backgroundColor = Constants.bgColor
        self.propertyLocationViewObj?.layer.cornerRadius = 15
        self.headerScrollView.addSubview(self.propertyLocationViewObj!)
        
        self.propertyLocationImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        self.propertyLocationImgViewObj?.image = UIImage(named: "propertyLocation")
        self.propertyLocationViewObj?.addSubview(self.propertyLocationImgViewObj!)
                
        self.propertyLocationLabel = UILabel(frame: CGRect(x: -40, y: 30, width: 40, height: 1))
        self.propertyLocationLabel?.backgroundColor = .lightGray
        self.propertyLocationViewObj?.addSubview(self.propertyLocationLabel!)
        
        // Add Tenant
        self.addTenantViewObj = UIView(frame: CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60))
        self.addTenantViewObj?.backgroundColor = Constants.bgColor
        self.addTenantViewObj?.layer.cornerRadius = 15
        self.headerScrollView.addSubview(self.addTenantViewObj!)
        
        self.addTenantImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        self.addTenantImgViewObj?.image = UIImage(named: "tenant")
        self.addTenantViewObj?.addSubview(self.addTenantImgViewObj!)
              
        self.addTenantLabel = UILabel(frame: CGRect(x: -40, y: 30, width: 40, height: 1))
        self.addTenantLabel?.backgroundColor = .lightGray
        self.addTenantViewObj?.addSubview(self.addTenantLabel!)
        
        
        // Lease Agreement
        self.leaseAgreementViewObj = UIView(frame: CGRect(x: (self.addTenantViewObj?.frame.origin.x)! + (self.addTenantViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60))
        self.leaseAgreementViewObj?.backgroundColor = Constants.bgColor
        self.leaseAgreementViewObj?.layer.cornerRadius = 15
        self.headerScrollView.addSubview(self.leaseAgreementViewObj!)
        
        self.leaseAgreementImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        self.leaseAgreementImgViewObj?.image = UIImage(named: "leaseAgreement")
        self.leaseAgreementViewObj?.addSubview(self.leaseAgreementImgViewObj!)
        
        self.leaseAgreementLabel = UILabel(frame: CGRect(x: -40, y: 30, width: 40, height: 1))
        self.leaseAgreementLabel?.backgroundColor = .lightGray
        self.leaseAgreementViewObj?.addSubview(self.leaseAgreementLabel!)
        
        // Broker Details
        self.brokerDetailsViewObj = UIView(frame: CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60))
        self.brokerDetailsViewObj?.backgroundColor = Constants.bgColor
        self.brokerDetailsViewObj?.layer.cornerRadius = 15
        self.headerScrollView.addSubview(self.brokerDetailsViewObj!)
        
        self.brokerDetailsImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        self.brokerDetailsImgViewObj?.image = UIImage(named: "broker")
        self.brokerDetailsViewObj?.addSubview(self.brokerDetailsImgViewObj!)
        
        self.brokerDetailsLabel = UILabel(frame: CGRect(x: -40, y: 30, width: 40, height: 1))
        self.brokerDetailsLabel?.backgroundColor = .lightGray
        self.brokerDetailsViewObj?.addSubview(self.brokerDetailsLabel!)
        
        // Property Manager Details
        self.propertyManagerDetailsViewObj = UIView(frame: CGRect(x: (self.brokerDetailsViewObj?.frame.origin.x)! + (self.brokerDetailsViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60))
        self.propertyManagerDetailsViewObj?.backgroundColor = Constants.bgColor
        self.propertyManagerDetailsViewObj?.layer.cornerRadius = 15
        self.headerScrollView.addSubview(self.propertyManagerDetailsViewObj!)
    
        self.propertyManagerDetailsImgViewObj = UIImageView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        self.propertyManagerDetailsImgViewObj?.image = UIImage(named: "broker")
        self.propertyManagerDetailsViewObj?.addSubview(self.propertyManagerDetailsImgViewObj!)
        
        self.propertyManagerDetailsLabel = UILabel(frame: CGRect(x: -40, y: 30, width: 40, height: 1))
        self.propertyManagerDetailsLabel?.backgroundColor = .lightGray
        self.propertyManagerDetailsViewObj?.addSubview(self.propertyManagerDetailsLabel!)
        
        
        self.headerScrollView.contentSize.width = (self.propertyManagerDetailsViewObj?.frame.origin.x)! + (self.propertyManagerDetailsViewObj?.frame.size.width)! + 30
        self.headerScrollView.showsVerticalScrollIndicator = false
        self.headerScrollView.showsHorizontalScrollIndicator = false
    }
    
    /*
    //MARK: - Keyboard Notifications
    @objc func keyboardWillShow(notification: NSNotification)
    {
        print("keyboardWillShow")
        
        self.createPropertyHeaderHeight.constant = 85
        self.headerView.layer.cornerRadius = 20
        self.propertyRentedLabel.isHidden = true
        self.yesLabel.isHidden = true
        self.switchObj.isHidden = true
        self.lblHeader.isHidden = true
//        self.skipButton.isHidden = true
        self.lblPageCount.isHidden = true
        self.headerSubView.isHidden = true
    }

    @objc func keyboardWillHide(notification: NSNotification)
    {
        print("keyboardWillHide")
        
        self.createPropertyHeaderHeight.constant = 320
        self.headerView.layer.cornerRadius = 30
        self.propertyRentedLabel.isHidden = false
        self.yesLabel.isHidden = false
        self.switchObj.isHidden = false
        self.lblHeader.isHidden = false
//        self.skipButton.isHidden = true
        self.lblPageCount.isHidden = false
        self.headerSubView.isHidden = false
    }*/
    
    //MARK: - Slider Value Change
    @IBAction func builtUpAreaValueChanged(_ sender: UISlider)
    {
        self.lblBuiltUpArea.text = "\(Int(sender.value)) sq.ft"
        self.builtUpAreaStr = "\(Int(sender.value))"
    }
    
    @IBAction func carpetAreaValueChanged(_ sender: UISlider)
    {
        self.lblCarpetArea.text = "\(Int(sender.value)) sq.ft"
        self.carpetAreaStr = "\(Int(sender.value))"
    }
    
    //MARK: - Switch Value Change
    @IBAction func leaseApplyGSTValueChanged(_ sender: UISwitch)
    {
        print("leaseApplyGSTValueChanged")
        if sender.isOn
        {
            self.applyGST = 1
        }
        else
        {
            self.applyGST = 0
        }
    }
    
    @IBAction func isPropertyRentedValueChanged(_ sender: UISwitch)
    {
        print("isPropertyRentedValueChanged")
        
        if sender.isOn
        {
            self.isPropertyRented = "1"
            
            DispatchQueue.main.async {
                self.yesLabel.text = "Yes"
            }
        }
        else
        {
            self.isPropertyRented = "0"
            
            DispatchQueue.main.async {
                self.yesLabel.text = "No"
            }
        }
        
        if self.isPropertyRented == "1"
        {
            if self.roleString == "Owner"
            {
                self.lblHeader.text = "Add new property (17% completed)"
                self.lblPageCount.text = "1/6"
            }
            else if self.roleString == "Broker"
            {
                self.lblHeader.text = "Add new property (20% completed)"
                self.lblPageCount.text = "1/5"
            }
            else if self.roleString == "Tenant"
            {
                self.lblHeader.text = "Add new property (20% completed)"
                self.lblPageCount.text = "1/5"
            }
            else if self.roleString == "Property Manager"
            {
                self.lblHeader.text = "Add new property (17% completed)"
                self.lblPageCount.text = "1/6"
            }
        }
        else
        {
            if self.roleString == "Owner"
            {
                self.lblHeader.text = "Add new property (25% completed)"
                self.lblPageCount.text = "1/4"
            }
            else if self.roleString == "Broker"
            {
                self.lblHeader.text = "Add new property (34% completed)"
                self.lblPageCount.text = "1/3"
            }
            else if self.roleString == "Tenant"
            {
                self.lblHeader.text = "Add new property (50% completed)"
                self.lblPageCount.text = "1/2"
            }
            else if self.roleString == "Property Manager"
            {
                self.lblHeader.text = "Add new property (25% completed)"
                self.lblPageCount.text = "1/4"
            }
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if self.isPropertyPhotoSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.uploadPhotosLabel.text = "Add another photos"
                self.uploadPhotosLabelWidth.constant = 195
                self.isPropertyPhotoSelect = false
                self.propertyPhotosNameArray?.add(Constants.propertyImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png")
                self.propertyPhotosArray?.add(pickedImage)
                
                DispatchQueue.main.async {
                    
                    if self.propertyPhotosArray!.count > 0
                    {
                        self.propertyPhotoHeightConstraint.constant = 120
                        self.mainScrollView.contentSize.height = 1800
                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                    }
                    self.view.layoutIfNeeded()
                    self.propertyPhotosCollection.reloadData()
                }
            }
        }
        else if self.isTenantProfileSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isTenantProfileSelect = false
                
                self.tenantProfileImageView.image = pickedImage
                
                self.tenantProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isTenantAadharSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isTenantAadharSelect = false
                
                self.tenantAadharImage = pickedImage
                
                self.tenantAadharButton.setImage(pickedImage, for: .normal)
                self.tenantAadharButton.imageView?.contentMode = .scaleAspectFill
                self.tenantAadharButton.layer.masksToBounds = true
                
                self.tenantAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isTenantPanSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isTenantPanSelect = false
                
                self.tenantPanImage = pickedImage
                
                self.tenantPanButton.setImage(pickedImage, for: .normal)
                self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
                self.tenantPanButton.layer.masksToBounds = true
                
                self.tenantPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isLeaseAgreementSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isLeaseAgreementSelect = false
                
                self.leaseAgreementImage = pickedImage
                
                self.leaseAgreementButton.setImage(pickedImage, for: .normal)
                self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
                self.leaseAgreementButton.layer.masksToBounds = true
                
                self.leaseAgreementImagePath = Constants.agreementFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isLeasePoliceVerificationSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isLeasePoliceVerificationSelect = false
                
                self.leasePoliceVerificationImage = pickedImage
                
                self.leasePoliceVerificationButton.setImage(pickedImage, for: .normal)
                self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
                self.leasePoliceVerificationButton.layer.masksToBounds = true
                
                self.leasePoliceVerificationImagePath = Constants.policeVerificationFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isLeaseNOCSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isLeaseNOCSelect = false
                
                self.leaseNOCImage = pickedImage
                
                self.leaseNOCButton.setImage(pickedImage, for: .normal)
                self.leaseNOCButton.imageView?.contentMode = .scaleAspectFill
                self.leaseNOCButton.layer.masksToBounds = true
                
                self.leaseNOCImagePath = Constants.nocFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isBrokerProfileSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isBrokerProfileSelect = false
                
                self.brokerProfilePic.image = pickedImage
                
                self.brokerProfileImage = pickedImage
                
                self.brokerProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isBrokerAadharSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isBrokerAadharSelect = false
                
                self.brokerAadharButton.setImage(pickedImage, for: .normal)
                self.brokerAadharButton.imageView?.contentMode = .scaleAspectFill
                self.brokerAadharButton.layer.masksToBounds = true
                
                self.brokerAadharImage = pickedImage
                
                self.brokerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isBrokerPanSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isBrokerPanSelect = false
                
                self.brokerPanButton.setImage(pickedImage, for: .normal)
                self.brokerPanButton.imageView?.contentMode = .scaleAspectFill
                self.brokerPanButton.layer.masksToBounds = true
                
                self.brokerPanImage = pickedImage
                
                self.brokerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isManagerProfileSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isManagerProfileSelect = false
                                
                self.managerProfileImgView.image = pickedImage
                
                self.managerProfileImage = pickedImage
                
                self.managerProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isManagerAadharSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isManagerAadharSelect = false
                         
                self.managerAadharImage = pickedImage
                
                self.managerAadharButton.setImage(pickedImage, for: .normal)
                self.managerAadharButton.imageView?.contentMode = .scaleAspectFill
                self.managerAadharButton.layer.masksToBounds = true
                
                self.managerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isManagerPanSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isManagerPanSelect = false
                                
                self.managerPanButton.setImage(pickedImage, for: .normal)
                self.managerPanButton.imageView?.contentMode = .scaleAspectFill
                self.managerPanButton.layer.masksToBounds = true
                
                self.managerPanImage = pickedImage
                
                self.managerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else
        {
            self.propertyDocImageArray?.removeAllObjects()
            
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                if self.isRegistrationSelect
                {
                    self.registrationImage = pickedImage
                    self.registrationButton.setImage(pickedImage, for: .normal)
                    self.registrationButton.imageView?.contentMode = .scaleAspectFill
                    
                    self.registrationButton.layer.masksToBounds = true
                    
                    self.registrationDocPath = Constants.registrationImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                }
                else if self.isLightBillSelect
                {
                    self.lightBillImage = pickedImage
                    self.lightBillButton.setImage(pickedImage, for: .normal)
                    self.lightBillButton.imageView?.contentMode = .scaleAspectFill
                    self.lightBillButton.layer.masksToBounds = true
                    
                    self.lightBillDocPath = Constants.lightBillImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                }
                else if self.isOwnerId
                {
                    self.ownerIdImage = pickedImage
                    self.ownerIdButton.setImage(pickedImage, for: .normal)
                    self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
                    self.ownerIdButton.layer.masksToBounds = true
                    
                    self.ownerIdDocPath = Constants.ownerIdImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                }
            }
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.isPropertyPhotoSelect = false
        
        self.isRegistrationSelect = false
        self.isLightBillSelect = false
        self.isOwnerId = false
        
        self.isTenantProfileSelect = false
        self.isTenantAadharSelect = false
        self.isTenantPanSelect = false
        
        self.isLeaseAgreementSelect = false
        self.isLeasePoliceVerificationSelect = false
        self.isLeaseNOCSelect = false
        
        self.isBrokerProfileSelect = false
        self.isBrokerAadharSelect = false
        self.isBrokerPanSelect = false
        
        self.isManagerProfileSelect = false
        self.isManagerAadharSelect = false
        self.isManagerPanSelect = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])
    {
        if urls.first != nil
        {
            if self.isRegistrationSelect
            {
                self.registrationImage = AppUtility.drawPDFfromURL(url: urls.first!)
                self.registrationButton.setImage(self.registrationImage, for: .normal)
                self.registrationButton.imageView?.contentMode = .scaleAspectFill
                self.registrationButton.layer.masksToBounds = true
                self.registrationDocPath = Constants.registrationImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLightBillSelect
            {
                self.lightBillImage = AppUtility.drawPDFfromURL(url: urls.first!)
                self.lightBillButton.setImage(self.lightBillImage, for: .normal)
                self.lightBillButton.imageView?.contentMode = .scaleAspectFill
                self.lightBillButton.layer.masksToBounds = true
                
                self.lightBillDocPath = Constants.lightBillImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isOwnerId
            {
                self.ownerIdImage = AppUtility.drawPDFfromURL(url: urls.first!)
                self.ownerIdButton.setImage(self.ownerIdImage, for: .normal)
                self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
                self.ownerIdButton.layer.masksToBounds = true
                
                self.ownerIdDocPath = Constants.ownerIdImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isPropertyPhotoSelect
            {
                self.uploadPhotosLabel.text = "Add another photos"
                self.uploadPhotosLabelWidth.constant = 195
                self.isPropertyPhotoSelect = false
                self.propertyPhotosNameArray?.add(Constants.propertyImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png")
                self.propertyPhotosArray?.add(AppUtility.drawPDFfromURL(url: urls.first!)!)
            
                
                DispatchQueue.main.async {
                    
                    if self.propertyPhotosArray!.count > 0
                    {
                        self.propertyPhotoHeightConstraint.constant = 120
                        self.mainScrollView.contentSize.height = 1800
                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                    }
                    
                    self.propertyPhotosCollection.reloadData()
                }
            }
            else if self.isTenantAadharSelect
            {
                self.isTenantAadharSelect = false
                
                self.tenantAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.tenantAadharButton.setImage(self.tenantAadharImage, for: .normal)
                self.tenantAadharButton.imageView?.contentMode = .scaleAspectFill
                self.tenantAadharButton.layer.masksToBounds = true
                
                self.tenantAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isTenantPanSelect
            {
                self.isTenantPanSelect = false
                
                self.tenantPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.tenantPanButton.setImage(self.tenantPanImage, for: .normal)
                self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
                self.tenantPanButton.layer.masksToBounds = true
                
                self.tenantPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLeaseAgreementSelect
            {
                self.isLeaseAgreementSelect = false
                
                self.leaseAgreementImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.leaseAgreementButton.setImage(self.leaseAgreementImage, for: .normal)
                self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
                self.leaseAgreementButton.layer.masksToBounds = true
                
                self.leaseAgreementImagePath = Constants.agreementFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLeasePoliceVerificationSelect
            {
                self.isLeasePoliceVerificationSelect = false
                
                self.leasePoliceVerificationImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.leasePoliceVerificationButton.setImage(self.leasePoliceVerificationImage, for: .normal)
                self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
                self.leasePoliceVerificationButton.layer.masksToBounds = true
                
                self.leasePoliceVerificationImagePath = Constants.policeVerificationFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLeaseNOCSelect
            {
                self.isLeaseNOCSelect = false
                
                self.leaseNOCImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.leaseNOCButton.setImage(self.leaseNOCImage, for: .normal)
                self.leaseNOCButton.imageView?.contentMode = .scaleAspectFill
                self.leaseNOCButton.layer.masksToBounds = true
                
                self.leaseNOCImagePath = Constants.nocFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isBrokerAadharSelect
            {
                self.isBrokerAadharSelect = false
                
                self.brokerAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.brokerAadharButton.setImage(self.brokerAadharImage, for: .normal)
                self.brokerAadharButton.imageView?.contentMode = .scaleAspectFill
                self.brokerAadharButton.layer.masksToBounds = true
                
                self.brokerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isBrokerPanSelect
            {
                self.isBrokerPanSelect = false
                
                self.brokerPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.brokerPanButton.setImage(self.brokerPanImage, for: .normal)
                self.brokerPanButton.imageView?.contentMode = .scaleAspectFill
                self.brokerPanButton.layer.masksToBounds = true
                
                self.brokerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isManagerAadharSelect
            {
                self.isManagerAadharSelect = false
                         
                self.managerAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.managerAadharButton.setImage(self.managerAadharImage, for: .normal)
                self.managerAadharButton.imageView?.contentMode = .scaleAspectFill
                self.managerAadharButton.layer.masksToBounds = true
                
                self.managerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isManagerPanSelect
            {
                self.isManagerPanSelect = false
                
                self.managerPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.managerPanButton.setImage(self.managerPanImage, for: .normal)
                self.managerPanButton.imageView?.contentMode = .scaleAspectFill
                self.managerPanButton.layer.masksToBounds = true
                
                self.managerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }

        }
    }
    
    //MARK: - TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.txtTenantDOB
        {
//            self.txtTenantDOB.isEnabled = false

            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

            datePicker.show("Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in

                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")

                    self.txtTenantDOB.text = "\(formatter.string(from: dt))"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtLeaseStartDate
        {
//            self.txtLeaseStartDate.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
           
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
            
            datePicker.show("Lease Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtLeaseStartDate.text = "\(formatter.string(from: dt))"
                    self.txtLeaseRentStartDate.text = "\(formatter.string(from: dt))"
                    
                    /////
                    self.selectedLeaseStartDate = dt
                    let diffInDays = Calendar.current.dateComponents([.day], from: dt, to: dt).day
                    self.txtLeaseFreePeriod.text = "\(diffInDays ?? 0)"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtLeaseRentStartDate
        {
//            self.txtLeaseRentStartDate.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
        
            
            datePicker.show("Lease Date Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtLeaseRentStartDate.text = "\(formatter.string(from: dt))"
                    
                    /////
                    self.selectedLeaseRentStartDate = dt
                    let diffInDays = Calendar.current.dateComponents([.day], from: self.selectedLeaseStartDate, to: self.selectedLeaseRentStartDate).day
                    
                    self.txtLeaseFreePeriod.text = "\(diffInDays ?? 0)"
                    /////
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtBrokerDOB
        {
//            self.txtBrokerDOB.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            datePicker.show("Broker Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtBrokerDOB.text = "\(formatter.string(from: dt))"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtManagerDOB
        {
//            self.txtManagerDOB.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            datePicker.show("Manager Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtManagerDOB.text = "\(formatter.string(from: dt))"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtFloor
        {
//            self.txtFloor.isEnabled = false
            
            let incidentArray = ["Ground-Foor", "1st Floor", "2nd Floor", "3rd Floor", "4th Floor", "5th Floor", "6th Floor", "7th Floor", "8th Floor", "9th Floor", "10th Floor", "11th Floor", "12th Floor", "13th Floor", "14th Floor", "15th Floor", "16th Floor", "17th Floor", "18th Floor", "19th Floor", "20th Floor", "21th Floor", "22nd Floor", "23rd Floor", "24th Floor", "25th Floor", "26th Floor", "27th Floor", "28th Floor", "29th Floor", "30th Floor", "31st Floor", "32th Floor", "33th Floor", "34th Floor", "35th Floor","36th Floor", "37th Floor", "38th Floor", "39th Floor", "40th Floor", "41th Floor", "42th Floor", "43th Floor", "44th Floor", "45th Floor", "46th Floor", "47th Floor", "48th Floor", "49th Floor", "50th Floor", "51th Floor", "52th Floor", "53th Floor", "54th Floor", "55th Floor", "56th Floor", "57th Floor", "58th Floor", "59th Floor", "60th Floor"]
            
            var selectedFloorArray = [String]()
            
            for i in 0...60 {
                selectedFloorArray.append("\(i)")
                
            }
            
            PickerView().showDoalog(title: "Select Floor", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "Owner") { (selectedString, selectedIndex) in

                self.txtFloor.text = "\(selectedString)"
                self.selectedFloor = "\(selectedFloorArray[selectedIndex])"
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtWing && self.propertySubType == "Row House"
        {
//            self.txtFloor.isEnabled = false
            
            let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",]
            
            PickerView().showDoalog(title: "Select No.of Basement", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "Owner") { (selectedString, selectedIndex) in

                self.txtWing.text = "\(selectedString)"
                self.selectedBasement = "\(selectedString)"
            }
            
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField)
//    {
//        if textField == self.txtTenantDOB
//        {
//            DispatchQueue.main.async {
//                self.txtTenantEmail.resignFirstResponder()
//                self.view.endEditing(false)
//                textField.resignFirstResponder()
//            }
//
//            self.txtTenantDOB.isEnabled = false
//
//            let datePicker = DatePickerDialog()
//            let currentDate = Date()
//            var dateComponents = DateComponents()
//            dateComponents.year = -100
//            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
//
//            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
//
//                if let dt = date
//                {
//                    let formatter = DateFormatter()
//    //                formatter.dateFormat = "dd MMM - yyyy"
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    print("Selected Date : \(formatter.string(from: dt))")
//
//                    self.txtTenantDOB.text = "\(formatter.string(from: dt))"
//                }
//            }
//        }
//        else if textField == self.txtLeaseStartDate
//        {
//            textField.resignFirstResponder()
//            self.txtLeaseStartDate.isEnabled = false
//
//            let datePicker = DatePickerDialog()
//            let currentDate = Date()
//
//            var dateComponents = DateComponents()
//            dateComponents.year = -100
//            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
//
//            var futureDateComponents = DateComponents()
//            futureDateComponents.year = 100
//            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
//
//            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
//
//                if let dt = date
//                {
//                    let formatter = DateFormatter()
//    //                formatter.dateFormat = "dd MMM - yyyy"
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    print("Selected Date : \(formatter.string(from: dt))")
//
//                    self.txtLeaseStartDate.text = "\(formatter.string(from: dt))"
//                }
//            }
//        }
//        else if textField == self.txtLeaseRentStartDate
//        {
//            textField.resignFirstResponder()
//            self.txtLeaseRentStartDate.isEnabled = false
//
//            let datePicker = DatePickerDialog()
//            let currentDate = Date()
//            var dateComponents = DateComponents()
//            dateComponents.year = -100
//            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
//
//            var futureDateComponents = DateComponents()
//            futureDateComponents.year = 100
//            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
//
//            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
//
//                if let dt = date
//                {
//                    let formatter = DateFormatter()
//    //                formatter.dateFormat = "dd MMM - yyyy"
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    print("Selected Date : \(formatter.string(from: dt))")
//
//                    self.txtLeaseRentStartDate.text = "\(formatter.string(from: dt))"
//                }
//            }
//        }
//        else if textField == self.txtBrokerDOB
//        {
//            textField.resignFirstResponder()
//            self.txtBrokerDOB.isEnabled = false
//
//            let datePicker = DatePickerDialog()
//            let currentDate = Date()
//            var dateComponents = DateComponents()
//            dateComponents.year = -100
//            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
//
//            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
//
//                if let dt = date
//                {
//                    let formatter = DateFormatter()
//    //                formatter.dateFormat = "dd MMM - yyyy"
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    print("Selected Date : \(formatter.string(from: dt))")
//
//                    self.txtBrokerDOB.text = "\(formatter.string(from: dt))"
//                }
//            }
//        }
//        else if textField == self.txtManagerDOB
//        {
//            textField.resignFirstResponder()
//            self.txtManagerDOB.isEnabled = false
//
//            let datePicker = DatePickerDialog()
//            let currentDate = Date()
//            var dateComponents = DateComponents()
//            dateComponents.year = -100
//            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
//
//            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
//
//                if let dt = date
//                {
//                    let formatter = DateFormatter()
//    //                formatter.dateFormat = "dd MMM - yyyy"
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    print("Selected Date : \(formatter.string(from: dt))")
//
//                    self.txtManagerDOB.text = "\(formatter.string(from: dt))"
//                }
//            }
//        }
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if self.txtTenantMobileNumber == textField
        {
            self.getUserProfile()
        }
        else if self.txtBrokerMobileNumber == textField
        {
            self.getBrokerProfile()
        }
        else if self.txtManagerMobile == textField
        {
            self.getPropertyManagerProfile()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.txtLeaseFreePeriod
        {
            if let char = string.cString(using: String.Encoding.utf8)
            {
                    
                let isBackSpace = strcmp(char, "\\b")
                
                if (isBackSpace == -92)
                {
//                    self.selectedLeaseFreePeriod = String(self.selectedLeaseFreePeriod.prefix(self.selectedLeaseFreePeriod.count - 1))
                    self.selectedLeaseFreePeriod = self.selectedLeaseFreePeriod.isEmpty == true ? "0" : String(self.selectedLeaseFreePeriod.prefix(self.selectedLeaseFreePeriod.count - 1))

                }
                else
                {
                    self.selectedLeaseFreePeriod = (textField.text ?? "") + string
                }
                
            }
            
            /// Code Here...
            if self.txtLeaseStartDate.text?.isEmpty != true
            {
                /////
                var rentStartDateComponents = DateComponents()
                rentStartDateComponents.day = Int("\(self.selectedLeaseFreePeriod.isEmpty == true ? "0" : self.selectedLeaseFreePeriod)")
                let rentStartDate = Calendar.current.date(byAdding: rentStartDateComponents, to: self.selectedLeaseStartDate)
                
                let rentStartDateformatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                rentStartDateformatter.dateFormat = "yyyy-MM-dd"
                print("Rent Start Date : \(rentStartDateformatter.string(from: rentStartDate!))")
                self.txtLeaseRentStartDate.text = "\(rentStartDateformatter.string(from: rentStartDate!))"
            }
            else
            {
                print("Please Select Start Date")
            }
        }
        else if textField == self.txtTenantMobileNumber || textField == self.txtBrokerMobileNumber || textField == self.txtManagerMobile || textField == self.txtTenantAadhar || textField == self.txtBrokerAadhar || textField == self.txtManagerAadhar
        {
            if string == ""
            {
                return true
            }
            else if (textField.text?.count)! > 11
            {
                return false
            }
            
            return true
        }
        else if textField == self.txtTenantPan || textField == self.txtBrokerPan || textField == self.txtManagerPan
        {
            if string == ""
            {
                return true
            }
            else if (textField.text?.count)! > 9
            {
                return false
            }
            
            return true
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        print("textFieldShouldReturn")
        
        return true
    }
    
    //MARK: - Phonebook Delegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact])
    {
        if contacts.count > 0
        {
            let selectedPhoneNumber = contacts[0].phoneNumbers[0].value.stringValue
            
            if self.isBrokerMobile == true
            {
                self.isBrokerMobile = false
                self.txtBrokerMobileNumber.text = "\(selectedPhoneNumber)"
                
                self.getBrokerProfile()
            }
            else if self.isManagerMobile == true
            {
                self.isManagerMobile = false
                self.txtManagerMobile.text = "\(selectedPhoneNumber)"
                
                self.getPropertyManagerProfile()
            }
            else
            {
                self.txtTenantMobileNumber.text = "\(selectedPhoneNumber)"
               
                self.getUserProfile()
            }
            
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func roleButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["Owner", "Broker", "Tenant", "Property Manager"]
        
        PickerView().showDoalog(title: "Select Your Role", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "Owner") { (selectedString, selectedIndex) in

            self.roleButton.setTitle(selectedString, for: .normal)
            self.roleString = "\(selectedString)"
            
            if self.isPropertyRented == "1"
            {
                if selectedString == "Owner"
                {
                    self.lblHeader.text = "Add new property (17% completed)"
                    self.lblPageCount.text = "1/6"
                }
                else if selectedString == "Broker"
                {
                    self.lblHeader.text = "Add new property (20% completed)"
                    self.lblPageCount.text = "1/5"
                }
                else if selectedString == "Tenant"
                {
                    self.lblHeader.text = "Add new property (20% completed)"
                    self.lblPageCount.text = "1/5"
                }
                else if selectedString == "Property Manager"
                {
                    self.lblHeader.text = "Add new property (17% completed)"
                    self.lblPageCount.text = "1/6"
                }
            }
            else
            {
                if selectedString == "Owner"
                {
                    self.lblHeader.text = "Add new property (25% completed)"
                    self.lblPageCount.text = "1/4"
                }
                else if selectedString == "Broker"
                {
                    self.lblHeader.text = "Add new property (34% completed)"
                    self.lblPageCount.text = "1/3"
                }
                else if selectedString == "Tenant"
                {
                    self.lblHeader.text = "Add new property (50% completed)"
                    self.lblPageCount.text = "1/2"
                }
                else if selectedString == "Property Manager"
                {
                    self.lblHeader.text = "Add new property (25% completed)"
                    self.lblPageCount.text = "1/4"
                }
            }
            
            if selectedString == "Broker"
            {
                self.brokerRepresentingLabelTop.constant = 15
                self.brokerRepresentingLabelHeight.constant = 35
                
                self.brokerRepresentingViewTop.constant = 7
                self.brokerRepresentingViewHeight.constant = 50
                
                self.addPropertyBrokerRepresentingView.isHidden = false
            }
            else
            {
                self.brokerRepresentingLabelTop.constant = 0
                self.brokerRepresentingLabelHeight.constant = 0
                
                self.brokerRepresentingViewTop.constant = 0
                self.brokerRepresentingViewHeight.constant = 0
                
                self.addPropertyBrokerRepresentingView.isHidden = true
                
                self.addPropertyBrokerRepresentingBy = ""
            }
        }
    }
    
    @IBAction func addPropertyBrokerRepresentngButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["Owner", "Tenant", "Both"]
        
        PickerView().showDoalog(title: "Broker Representng By", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "Owner") { (selectedString, selectedIndex) in

            self.addPropertyBrokerRepresentingButton.setTitle(selectedString, for: .normal)
            self.addPropertyBrokerRepresentingBy = "\(selectedString)"
        }
    }
    
    
    @IBAction func uploadPropertyPhotosButtonClicked(_ sender: UIButton)
    {
//        self.view.endEditing(true)
        self.isPropertyPhotoSelect = true
        
        let alert = UIAlertController(title: "Select Property Photos", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
        
    }
    
    @IBAction func skipButtonClicked(_ sender: UIButton)
    {
        if self.isPropertyRented == "1"
        {
            if self.roleString == "Owner"
            {
                if self.lblTopHeader.text == "Property Manager"
                {
                    self.navigationController?.popViewController(animated: true)
                }
                else if self.lblTopHeader.text == "Add Broker"
                {
                    print("Navigate To Property Manager")
                    
                    DispatchQueue.main.async {
                        self.lblTopHeader.text = "Property Manager"
                        self.lblHeader.text = "Property manager details (100% completed)"
                        self.lblPageCount.text = "6/6"
                        self.skipButton.isHidden = false
                        
                        self.propertyManagerDetailsViewObj?.frame = CGRect(x: (self.brokerDetailsViewObj?.frame.origin.x)! + (self.brokerDetailsViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                        self.propertyManagerDetailsViewObj?.backgroundColor = .orange
                        self.propertyManagerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                        
                        self.propertyManagerDetailsLabel?.backgroundColor = Constants.darkBlueColor
                        
//                        self.headerScrollView.contentSize.width = (self.propertyManagerDetailsViewObj?.frame.origin.x)! + (self.propertyManagerDetailsViewObj?.frame.size.width)! + 30
                    }
                    
                    self.newPropertyView.isHidden = true
                    self.leaseView.isHidden = true
                    self.propertyLocationView.isHidden = true
                    self.addTenantView.isHidden = true
                    self.brokerDetailView.isHidden = true
                    
                    self.propertyManagerView.isHidden = false
                    
                    self.mainScrollView.contentOffset.y = 0
                    self.mainScrollView.contentSize.height = 1400
                }
            }
            else if self.roleString == "Tenant"
            {
                if self.lblTopHeader.text == "Add Broker"
                {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if self.roleString == "Property Manager"
            {
                if self.lblTopHeader.text == "Add Broker"
                {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            if self.roleString == "Owner"
            {
                if self.lblTopHeader.text == "Add Broker"
                {
                    DispatchQueue.main.async {
                        self.lblTopHeader.text = "Property Manager"
                        self.lblHeader.text = "Property manager details (100% completed)"
                        self.lblPageCount.text = "4/4"
                        self.skipButton.isHidden = false
                        
                        self.propertyManagerDetailsViewObj?.frame = CGRect(x: (self.brokerDetailsViewObj?.frame.origin.x)! + (self.brokerDetailsViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                        self.propertyManagerDetailsViewObj?.backgroundColor = .orange
                        self.propertyManagerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                        
                        self.propertyManagerDetailsLabel?.backgroundColor = Constants.darkBlueColor
                        
//                        self.headerScrollView.contentSize.width = (self.propertyManagerDetailsViewObj?.frame.origin.x)! + (self.propertyManagerDetailsViewObj?.frame.size.width)! + 30
                    }
                    
                    self.newPropertyView.isHidden = true
                    self.leaseView.isHidden = true
                    self.propertyLocationView.isHidden = true
                    self.addTenantView.isHidden = true
                    self.brokerDetailView.isHidden = true
                    
                    self.propertyManagerView.isHidden = false
                    
                    self.mainScrollView.contentOffset.y = 0
                    self.mainScrollView.contentSize.height = 1400
                }
                else if self.lblTopHeader.text == "Property Manager"
                {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if self.roleString == "Broker"
            {
                if self.lblTopHeader.text == "Add Owner"
                {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else if self.roleString == "Property Manager"
            {
                if self.lblTopHeader.text == "Add Owner"
                {
                    DispatchQueue.main.async {
                        
                        self.isLeaseAgreementSelect = false
                        self.isLeasePoliceVerificationSelect = false
                        self.isLeaseNOCSelect = false
                        self.lblTopHeader.text = "Add Broker"
                        self.lblHeader.text = "Broker details (100% completed)"
                        self.lblPageCount.text = "4/4"
                        self.skipButton.isHidden = false
                        
                        self.brokerDetailsViewObj?.frame = CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                        self.brokerDetailsViewObj?.backgroundColor = .orange
                        self.brokerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                        
                        self.brokerDetailsLabel?.backgroundColor = Constants.darkBlueColor
                        
                        self.newPropertyView.isHidden = true
                        self.leaseView.isHidden = true
                        self.propertyLocationView.isHidden = true
                        self.addTenantView.isHidden = true
                        self.propertyManagerView.isHidden = true
                        
                        self.brokerDetailView.isHidden = false
                        
                        self.mainScrollView.contentOffset.y = 0
                        self.mainScrollView.contentSize.height = 2000
                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                    }
                }
                else if self.lblTopHeader.text == "Add Broker"
                {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
    }
    
    @IBAction func residentialButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
//            self.view.endEditing(true)
            self.lblBedRoomTop.constant = 15
            self.lblBedRoomHeight.constant = 35
            self.bedRoomCollectionTop.constant = 7
            self.bedRoomCollectionHeight.constant = 60
            
            self.lblBathRoomTop.constant = 15
            self.lblBathRoomHeight.constant = 35
            self.bathRoomCollectionTop.constant = 7
            self.bathRoomCollectionHeight.constant = 60
            
            self.propertyTypeStr = self.residentailButton.currentTitle ?? ""
            self.propertySubType = "Flat"
            
            self.residentailButton.layer.cornerRadius = 15
            self.residentailButton.backgroundColor = Constants.darkBlueColor
            self.residentailButton.setTitleColor(.white, for: .normal)
            
            self.commercialButton.backgroundColor = .white
            self.commercialButton.setTitleColor(.lightGray, for: .normal)
            
            self.newPropertyTypeArray?.removeAllObjects()

            self.newPropertyTypeArray?.add("Flat")
            self.newPropertyTypeArray?.add("Duplex")
            self.newPropertyTypeArray?.add("Pent house")
            self.newPropertyTypeArray?.add("Row House")
            self.newPropertyTypeArray?.add("Bungalow")
            
            self.selectedNewPropertyTypeArray?.removeAllObjects()
            for _ in 0..<self.newPropertyTypeArray!.count
            {
                self.selectedNewPropertyTypeArray?.add("0")
            }
            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
            self.propertyTypeCollection.reloadData()
            
            self.mainScrollView.contentSize.height = 1800
        }
    }
    
    @IBAction func commercialButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
//            self.view.endEditing(true)
            self.noOfRoomsStr = ""
            self.noOfBathroomsStr = ""
            
            self.lblBedRoomTop.constant = 0
            self.lblBedRoomHeight.constant = 0
            self.bedRoomCollectionTop.constant = 0
            self.bedRoomCollectionHeight.constant = 0
            
            self.lblBathRoomTop.constant = 0
            self.lblBathRoomHeight.constant = 0
            self.bathRoomCollectionTop.constant = 0
            self.bathRoomCollectionHeight.constant = 0
            
            self.propertyTypeStr = self.commercialButton.currentTitle ?? ""
            self.propertySubType = "Shop or Showroom"
            
            self.commercialButton.layer.cornerRadius = 15
            self.commercialButton.backgroundColor = Constants.darkBlueColor
            self.commercialButton.setTitleColor(.white, for: .normal)
            
            self.residentailButton.backgroundColor = .white
            self.residentailButton.setTitleColor(.lightGray, for: .normal)

            self.newPropertyTypeArray?.removeAllObjects()

            self.newPropertyTypeArray?.add("Shop or Showroom")
            self.newPropertyTypeArray?.add("Industrial")
            self.newPropertyTypeArray?.add("Office")
            self.newPropertyTypeArray?.add("IT")
            
            self.selectedNewPropertyTypeArray?.removeAllObjects()
            for _ in 0..<self.newPropertyTypeArray!.count
            {
                self.selectedNewPropertyTypeArray?.add("0")
            }
            
            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
            
            self.propertyTypeCollection.reloadData()
            
            self.mainScrollView.contentSize.height = 1600
        }
    }
    
    @objc func newPropertyTypeButtonClicked(sender : UIButton)
    {
        print("newPropertyTypeButtonClicked")
//        self.view.endEditing(true)
        self.propertySubType = (sender.currentTitle ?? "")
        
        self.selectedNewPropertyTypeArray?.removeAllObjects()
        for _ in 0..<self.newPropertyTypeArray!.count
        {
            self.selectedNewPropertyTypeArray?.add("0")
        }
        
        self.selectedNewPropertyTypeArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.propertyTypeCollection.reloadData()
        }
    }
    
    @objc func bedroomButtonClicked(sender : UIButton)
    {
        print("bedroomButtonClicked")
//        self.view.endEditing(true)
        self.noOfRoomsStr = (sender.currentTitle ?? "")
        
        self.selectedBedroomArray?.removeAllObjects()
        for _ in 0..<self.bedroomArray!.count
        {
            self.selectedBedroomArray?.add("0")
        }
        
        self.selectedBedroomArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.bedroomCollection.reloadData()
        }
    }
    
    @objc func bathroomButtonClicked(sender : UIButton)
    {
        print("bathroomButtonClicked")
        
//        self.view.endEditing(true)
        self.noOfBathroomsStr = (sender.currentTitle ?? "")
        
        self.selectedBathroomArray?.removeAllObjects()
        for _ in 0..<self.bathroomArray!.count
        {
            self.selectedBathroomArray?.add("0")
        }
        
        self.selectedBathroomArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.bathroomCollection.reloadData()
        }
    }
    
    @IBAction func furnishedButtonClicked(_ sender: UIButton)
    {
//        self.view.endEditing(true)
        self.furnishedTypeStr = (sender.currentTitle ?? "")
        
        self.furnishedButton.backgroundColor = Constants.darkBlueColor
        self.furnishedButton.setTitleColor(.white, for: .normal)
        
        self.nonFurnishedButton.backgroundColor = .white
        self.nonFurnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.partialButton.backgroundColor = .white
        self.partialButton.setTitleColor(.lightGray, for: .normal)
    }
    
    @IBAction func nonFurnishedButtonClicked(_ sender: UIButton)
    {
//        self.view.endEditing(true)
        self.furnishedTypeStr = (sender.currentTitle ?? "")
        
        self.furnishedButton.backgroundColor = .white
        self.furnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.nonFurnishedButton.backgroundColor = Constants.darkBlueColor
        self.nonFurnishedButton.setTitleColor(.white, for: .normal)
        
        self.partialButton.backgroundColor = .white
        self.partialButton.setTitleColor(.lightGray, for: .normal)
    }
    
    @IBAction func partialButtonClicked(_ sender: UIButton)
    {
//        self.view.endEditing(true)
        self.furnishedTypeStr = (sender.currentTitle ?? "")
        
        self.furnishedButton.backgroundColor = .white
        self.furnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.nonFurnishedButton.backgroundColor = .white
        self.nonFurnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.partialButton.backgroundColor = Constants.darkBlueColor
        self.partialButton.setTitleColor(.white, for: .normal)
    }
    
    @objc func carParkButtonClicked(sender : UIButton)
    {
        print("carParkButtonClicked")
//        self.view.endEditing(true)
        self.noOfCarParkStr = (sender.currentTitle ?? "")
        
        self.selectedCarParkArray?.removeAllObjects()
        for _ in 0..<self.carParkArray!.count
        {
            self.selectedCarParkArray?.add("0")
        }
        
        self.selectedCarParkArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.carParkCollection.reloadData()
        }
    }
    
    @objc func amenitiesButtonClicked(sender : UIButton)
    {
        print("amenitiesButtonClicked")
        
        if (sender.currentTitle ?? "") == "Other"
        {
            if "\(self.selectedAmenitiesArray?.object(at: sender.tag) ?? "")" == "1"
            {
                self.amenitiesTop.constant = 0
                self.amenitiesHeight.constant = 0
                self.amenitiesNoteHeight.constant = 0
                self.txtOtherAmenities.text = ""
            }
            else
            {
                self.amenitiesTop.constant = 10
                self.amenitiesHeight.constant = 50
                self.amenitiesNoteHeight.constant = 15
            }
            
        }
        
        self.selectedAmenitiesArray?.replaceObject(at: sender.tag, with: ("\(self.selectedAmenitiesArray?.object(at: sender.tag) ?? "")" == "1") ? "0" : "1")
        
        self.amenitiesStr = ""
        
        for (index, currentObj) in (self.selectedAmenitiesArray!.enumerated())
        {
            if "\(currentObj)" == "1"
            {
                if "\(self.amenitiesArray?.object(at: index) ?? "")" == "Other"
                {
                    self.amenitiesStr = (self.txtOtherAmenities.text ?? "").isEmpty ? "\(self.amenitiesStr)" : self.amenitiesStr+("\(self.txtOtherAmenities.text ?? "")")+","
                }
                else
                {
                    self.amenitiesStr = self.amenitiesStr+("\(self.amenitiesArray?.object(at: index) ?? "")")+","
                }
            }
        }
        
        DispatchQueue.main.async {
            self.amenitiesCollection.reloadData()
            self.view.setNeedsLayout()
        }

        /*
        if (sender.currentTitle ?? "") != "Other"
        {
            if !self.amenitiesStr.contains(sender.currentTitle ?? "")
            {
                self.amenitiesStr = self.amenitiesStr+(sender.currentTitle ?? "")+","
            }
        }
        
        
        if (sender.currentTitle ?? "") == "Other"
        {
            if "\(self.selectedAmenitiesArray?.object(at: sender.tag) ?? "")" == "1"
            {
                self.amenitiesTop.constant = 0
                self.amenitiesHeight.constant = 0
            }
            else
            {
                self.amenitiesTop.constant = 10
                self.amenitiesHeight.constant = 50
            }
            
        }
        
        self.selectedAmenitiesArray?.replaceObject(at: sender.tag, with: ("\(self.selectedAmenitiesArray?.object(at: sender.tag) ?? "")" == "1") ? "0" : "1")
        
        DispatchQueue.main.async {
            self.amenitiesCollection.reloadData()
        }
 */
    }
    
    @objc func propertyDocButtonClicked(sender : UIButton)
    {
//        self.view.endEditing(true)
        self.isPropertyPhotoSelect = false
        
        if sender.tag == 0
        {
            self.isRegistrationSelect = true
            self.isLightBillSelect = false
            self.isOwnerId = false
        }
        else if sender.tag == 1
        {
            self.isRegistrationSelect = false
            self.isLightBillSelect = true
            self.isOwnerId = false
        }
        else if sender.tag == 2
        {
            self.isRegistrationSelect = false
            self.isLightBillSelect = false
            self.isOwnerId = true
        }
        
        let alert = UIAlertController(title: "Select Document", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func addNewPropertySaveButtonClicked(_ sender: UIButton)
    {
//        self.view.endEditing(true)
        if self.roleButton.currentTitle?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Your Role", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtPropertyName.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Property Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertyTypeStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Property Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertySubType.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Property-Sub Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.noOfRoomsStr.isEmpty == true && self.propertyTypeStr != "Commercial"
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Selecte No.of Bedroom", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.noOfBathroomsStr.isEmpty == true && self.propertyTypeStr != "Commercial"
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Selecte No.of Bathrooms", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCarpetArea.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Carpet Area", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.furnishedTypeStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Property Status", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.noOfCarParkStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select No.of Car Parking", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.amenitiesStr.isEmpty == true && self.txtOtherAmenities.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Amenities", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if (self.txtBuiltUpArea.text ?? "").isEmpty != true
            {
                if (Int64(self.txtBuiltUpArea.text ?? "") ?? 0) > (Int64(self.txtCarpetArea.text ?? "") ?? 0)
                {
                    DispatchQueue.main.async {
                        self.lblTopHeader.text = "Property Location"
                        
                        if self.isPropertyRented == "1"
                        {
                            if self.roleString == "Owner"
                            {
                                self.lblHeader.text = "Property Location  (34% completed)"
                                self.lblPageCount.text = "2/6"
                            }
                            else if self.roleString == "Broker"
                            {
                                self.lblHeader.text = "Property Location  (40% completed)"
                                self.lblPageCount.text = "2/5"
                            }
                            else if self.roleString == "Tenant"
                            {
                                self.lblHeader.text = "Property Location  (40% completed)"
                                self.lblPageCount.text = "2/5"
                            }
                            else if self.roleString == "Property Manager"
                            {
                                self.lblHeader.text = "Property Location  (34% completed)"
                                self.lblPageCount.text = "2/6"
                            }
                        }
                        else
                        {
                            if self.roleString == "Owner"
                            {
                                self.lblHeader.text = "Property Location (50% completed)"
                                self.lblPageCount.text = "2/4"
                            }
                            else if self.roleString == "Broker"
                            {
                                self.lblHeader.text = "Property Location  (68% completed)"
                                self.lblPageCount.text = "2/3"
                            }
                            else if self.roleString == "Tenant"
                            {
                                self.lblHeader.text = "Property Location  (100% completed)"
                                self.lblPageCount.text = "2/2"
                            }
                            else if self.roleString == "Property Manager"
                            {
                                self.lblHeader.text = "Property Location  (50% completed)"
                                self.lblPageCount.text = "2/4"
                            }
                        }
                        
                        if self.propertySubType == "Row House"
                        {
                            self.wingLabel.text = "No.of Basement*"
                            self.txtWing.placeholder = "No.of Basements"
                            self.wingDropImg.isHidden = false
                        }
                        else
                        {
                            self.wingLabel.text = "Wing"
                            self.txtWing.placeholder = "Enter Wing"
                            self.wingDropImg.isHidden = true
                        }
                        
                        self.propertyLocationViewObj?.frame = CGRect(x: (self.addNewPropertyViewObj?.frame.origin.x)! + (self.addNewPropertyViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                        self.propertyLocationViewObj?.backgroundColor = .orange
                        self.propertyLocationImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                        self.propertyLocationLabel?.backgroundColor = Constants.darkBlueColor
                    }
                    
                    self.newPropertyView.isHidden = true
                    self.leaseView.isHidden = true
                    self.addTenantView.isHidden = true
                    self.propertyManagerView.isHidden = true
                    
                    self.propertyLocationView.isHidden = false
                    self.brokerDetailView.isHidden = true
                    
                    self.mainScrollView.contentOffset.y = 0
                    self.mainScrollView.contentSize.height = 1200
                }
                else
                {
                    let alertViewController = UIAlertController(title: "Error", message: "Built up area must me greater than carpet area", preferredStyle: .alert)
                    
                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                        
                    }))
                    
                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.lblTopHeader.text = "Property Location"
    //                self.lblHeader.text = "Property Location (34% completed)"
    //                self.lblPageCount.text = "2/6"
                    
                    if self.isPropertyRented == "1"
                    {
                        if self.roleString == "Owner"
                        {
                            self.lblHeader.text = "Property Location  (34% completed)"
                            self.lblPageCount.text = "2/6"
                        }
                        else if self.roleString == "Broker"
                        {
                            self.lblHeader.text = "Property Location  (40% completed)"
                            self.lblPageCount.text = "2/5"
                        }
                        else if self.roleString == "Tenant"
                        {
                            self.lblHeader.text = "Property Location  (40% completed)"
                            self.lblPageCount.text = "2/5"
                        }
                        else if self.roleString == "Property Manager"
                        {
                            self.lblHeader.text = "Property Location  (34% completed)"
                            self.lblPageCount.text = "2/6"
                        }
                    }
                    else
                    {
                        if self.roleString == "Owner"
                        {
                            self.lblHeader.text = "Property Location (50% completed)"
                            self.lblPageCount.text = "2/4"
                        }
                        else if self.roleString == "Broker"
                        {
                            self.lblHeader.text = "Property Location  (68% completed)"
                            self.lblPageCount.text = "2/3"
                        }
                        else if self.roleString == "Tenant"
                        {
                            self.lblHeader.text = "Property Location  (100% completed)"
                            self.lblPageCount.text = "2/2"
                        }
                        else if self.roleString == "Property Manager"
                        {
                            self.lblHeader.text = "Property Location  (50% completed)"
                            self.lblPageCount.text = "2/4"
                        }
                    }
                    
                    if self.propertySubType == "Row House"
                    {
                        self.wingLabel.text = "No.of Basement*"
                        self.txtWing.placeholder = "No.of Basements"
                        self.wingDropImg.isHidden = false
                    }
                    else
                    {
                        self.wingLabel.text = "Wing"
                        self.txtWing.placeholder = "Enter Wing"
                        self.wingDropImg.isHidden = true
                    }
                    
                    self.propertyLocationViewObj?.frame = CGRect(x: (self.addNewPropertyViewObj?.frame.origin.x)! + (self.addNewPropertyViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                    self.propertyLocationViewObj?.backgroundColor = .orange
                    self.propertyLocationImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                    self.propertyLocationLabel?.backgroundColor = Constants.darkBlueColor
                }
                
                self.newPropertyView.isHidden = true
                self.leaseView.isHidden = true
                self.addTenantView.isHidden = true
                self.propertyManagerView.isHidden = true
                
                self.propertyLocationView.isHidden = false
                self.brokerDetailView.isHidden = true
                
                self.mainScrollView.contentOffset.y = 0
                self.mainScrollView.contentSize.height = 1200
            }
        }
        
        
    }
    
    @IBAction func addLocationSaveButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtFlatNo.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Flat No.", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtFloor.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Floor No.", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBuildingName.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Building Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLocality.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Locality", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtAddress1.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Address Line 1", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCity.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter City", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtZipCode.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter ZipCode", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtState.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter State", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCountry.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Country", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            self.addNewPropertyResponse()
        }
    }
    
    @IBAction func addTenantViewButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertyIdStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Property Not Created", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtTenantMobileNumber.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Tenant's Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtTenantFirstName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Tenant's First Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtTenantLastName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Tenant's Last Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtTenantEmail.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Tenant's Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.tenantGenderString.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Tenant's Gender", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtTenantEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtTenantEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            
            if self.lblTopHeader.text == "Add Owner"
            {
                self.addTenantResponse(contactType: "Owner")
            }
            else
            {
                self.addTenantResponse(contactType: "Tenant")
            }
        }
        
    }
    
    @IBAction func addLeaseViewButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.leaseTypeStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Lease Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.leaseTypeStr == "Individual" && self.occupancyTypeStr.isEmpty
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Occupancy Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseCompanyName.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Company Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseAddress.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Address", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseContactPerson.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Contact Person Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseDesignation.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Designation", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseContactNumber.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Contact Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseEmail.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseStartDate.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Start Date", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseFreePeriod.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Rent Free Period", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseRentStartDate.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Rent Start Date", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseMonthlyRentAmt.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Rent Amount", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseSecurityDeposite.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Rent Security Deposite", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtLeaseEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtLeaseEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            self.addLeaseAgreementResponse()
        }
    }
    
    
    @IBAction func addBrokerViewButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerMobileNumber.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerFirstName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's First Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerLastName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Last Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerEmail.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.brokerGender.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Gender", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerAddress1.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Address 1", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerCity.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's City", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerZipCode.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's ZipCode", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerState.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's State", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBrokerCountry.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Broker's Country", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtBrokerEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtBrokerEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            
            self.addBrokerDetailsResponse()
        }
    }
    
    @IBAction func leaseCompanyButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
//            self.view.endEditing(true)
            
            self.occupancyTypeStr = ""
            self.companyNameLabel.text = "Company Name*"
            self.occupancyTypeView.isHidden = true
            self.txtLeaseCompanyName.isHidden = false
           
            self.lblLeaseAddressTop.constant = 15
            self.lblLeaseAddressHeight.constant = 35
            self.txtLeaseAddressTop.constant = 7
            self.txtLeaseAddressHeight.constant = 50
            
            self.lblLeaseContactTop.constant = 15
            self.lblLeaseContactHeight.constant = 35
            self.txtLeaseContactTop.constant = 7
            self.txtLeaseContactHeight.constant = 50
            
            self.lblLeaseDesignationTop.constant = 15
            self.lblLeaseDesignationHeight.constant = 35
            self.txtLeaseDesignationTop.constant = 7
            self.txtLeaseDesignationHeight.constant = 50
            
            self.lblLeaseContactNumTop.constant = 15
            self.lblLeaseContactNumHeight.constant = 35
            self.txtLeaseContactNumTop.constant = 7
            self.txtLeaseContactNumHeight.constant = 50
            
            self.lblLeaseEmailTop.constant = 15
            self.lblLeaseEmailHeight.constant = 35
            self.txtLeaseEmailTop.constant = 7
            self.txtLeaseEmailHeight.constant = 50
            
            self.leaseTypeStr = sender.currentTitle ?? ""
            
            self.leaseCompanyButton.layer.cornerRadius = 15
            self.leaseCompanyButton.backgroundColor = Constants.darkBlueColor
            self.leaseCompanyButton.setTitleColor(.white, for: .normal)
            
            self.leaseIndividualButton.backgroundColor = .white
            self.leaseIndividualButton.setTitleColor(.lightGray, for: .normal)
            
            self.mainScrollView.contentSize.height = 2300
        }
    }
    
    @IBAction func leaseIndividualButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
//            self.view.endEditing(true)
            
            self.companyNameLabel.text = "Occupancy Type*"
            self.occupancyTypeView.isHidden = false
            self.occupancyTypeView.backgroundColor = .clear
            self.txtLeaseCompanyName.isHidden = true
           
            self.lblLeaseAddressTop.constant = 0
            self.lblLeaseAddressHeight.constant = 0
            self.txtLeaseAddressTop.constant = 0
            self.txtLeaseAddressHeight.constant = 0
            self.lblLeaseContactTop.constant = 0
            self.lblLeaseContactHeight.constant = 0
            self.txtLeaseContactTop.constant = 0
            self.txtLeaseContactHeight.constant = 0
            self.lblLeaseDesignationTop.constant = 0
            self.lblLeaseDesignationHeight.constant = 0
            self.txtLeaseDesignationTop.constant = 0
            self.txtLeaseDesignationHeight.constant = 0
            self.lblLeaseContactNumTop.constant = 0
            self.lblLeaseContactNumHeight.constant = 0
            self.txtLeaseContactNumTop.constant = 0
            self.txtLeaseContactNumHeight.constant = 0
            self.lblLeaseEmailTop.constant = 0
            self.lblLeaseEmailHeight.constant = 0
            self.txtLeaseEmailTop.constant = 0
            self.txtLeaseEmailHeight.constant = 0
            
            
            self.leaseTypeStr = sender.currentTitle ?? ""
            self.leaseIndividualButton.layer.cornerRadius = 15
            self.leaseIndividualButton.backgroundColor = Constants.darkBlueColor
            self.leaseIndividualButton.setTitleColor(.white, for: .normal)
            
            self.leaseCompanyButton.backgroundColor = .white
            self.leaseCompanyButton.setTitleColor(.lightGray, for: .normal)
            
            self.mainScrollView.contentSize.height = 1800
        }
    }
    
    @IBAction func registrationButtonClicked(_ sender: UIButton)
    {
        self.isRegistrationSelect = true
        self.isLightBillSelect = false
        self.isOwnerId = false
        
        let alert = UIAlertController(title: "Select Registration Doc", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
                        
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func lightBillButtonClicked(_ sender: UIButton)
    {
        self.isRegistrationSelect = false
        self.isLightBillSelect = true
        self.isOwnerId = false
        
        let alert = UIAlertController(title: "Select Light Bill", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func owenerIdButtonClicked(_ sender: UIButton)
    {
        self.isRegistrationSelect = false
        self.isLightBillSelect = false
        self.isOwnerId = true
        
        let alert = UIAlertController(title: "Select Owenr Id", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func addTenantProfileButtonClicked(_ sender: UIButton)
    {
        self.isTenantProfileSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func sddTenantPhoneBookButtonClicked(_ sender: UIButton)
    {
        let peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        
        DispatchQueue.main.async {
            self.present(peoplePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func tenantMaleButtonClicked(_ sender: UIButton)
    {
        self.tenantFemaleButton.isSelected = false
        self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.tenantOtherButton.isSelected = false
        self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.tenantMaleButton.isSelected
        {
            self.tenantMaleButton.isSelected = false
            self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.tenantGenderString = ""
        }
        else
        {
            self.tenantMaleButton.isSelected = true
            self.tenantMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.tenantGenderString = "male"
        }
        
        //////
//        self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//
//        if sender.isSelected
//        {
//            sender.isSelected = false
//            self.tenantGenderString = ""
//            self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        }
//        else
//        {
//            self.tenantGenderString = "male"
//            sender.isSelected = true
//            self.tenantMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
//        }
    }
    
    @IBAction func tenantFemaleButtonClicked(_ sender: UIButton)
    {
        self.tenantMaleButton.isSelected = false
        self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.tenantOtherButton.isSelected = false
        self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.tenantFemaleButton.isSelected
        {
            self.tenantFemaleButton.isSelected = false
            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.tenantGenderString = ""
        }
        else
        {
            self.tenantFemaleButton.isSelected = true
            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.tenantGenderString = "female"
        }
        
        /////
//        self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//
//        if sender.isSelected
//        {
//            sender.isSelected = false
//            self.tenantGenderString = ""
//            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        }
//        else
//        {
//            sender.isSelected = true
//            self.tenantGenderString = "female"
//            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
//        }
    }
    
    @IBAction func tenantOtherButtonClicked(_ sender: UIButton)
    {
        self.tenantMaleButton.isSelected = false
        self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.tenantFemaleButton.isSelected = false
        self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.tenantOtherButton.isSelected
        {
            self.tenantOtherButton.isSelected = false
            self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.tenantGenderString = ""
        }
        else
        {
            self.tenantOtherButton.isSelected = true
            self.tenantOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.tenantGenderString = "other"
        }
        
        ///
//        self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//
//        if sender.isSelected
//        {
//            sender.isSelected = false
//            self.tenantGenderString = ""
//            self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
//        }
//        else
//        {
//            sender.isSelected = true
//            self.tenantGenderString = "other"
//            self.tenantOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
//        }
    }
    
    @IBAction func tenantAadharButtonClicked(_ sender: UIButton)
    {
        self.isTenantAadharSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func tenantPanButtonClicked(_ sender: UIButton)
    {
        self.isTenantPanSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func leaseYearButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
        
        PickerView().showDoalog(title: "Select Year", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "1") { (selectedString, selectedIndex) in

            self.leaseYearButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseMonthButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
        
        PickerView().showDoalog(title: "Select Months", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "1") { (selectedString, selectedIndex) in

            self.leaseMonthButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseRentDueDateButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
        
        PickerView().showDoalog(title: "Rent Due Date (every month)", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseRentDueDateStr = "\(selectedString)"
            
            if selectedString == "1"
            {
                self.leaseRentDueDate.setTitle("\(selectedString)", for: .normal)
            }
            else if selectedString == "2"
            {
                self.leaseRentDueDate.setTitle("\(selectedString)nd (of every month)", for: .normal)
            }
            else if selectedString == "3"
            {
                self.leaseRentDueDate.setTitle("\(selectedString)rd (of every month)", for: .normal)
            }
            else
            {
                self.leaseRentDueDate.setTitle("\(selectedString)th (of every month)", for: .normal)
            }
        }
    }
    
    @IBAction func leaseEscalationYearButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
        
        PickerView().showDoalog(title: "Escalation Year", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseEscalationYearButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseEscalationMonthButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
        
        PickerView().showDoalog(title: "Escalation Month", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseEscalationMonthButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseEscalationPercentageButtonClicked(_ sender: UIButton)
    {
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"]
        
        PickerView().showDoalog(title: "Escalation Percentage", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseEscalationPercentageButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseOwnerLockInButtonClicked(_ sender: UIButton)
    {
        var incidentArray = [String]()
        
        for i in 0...60 {
            incidentArray.append("\(i)")
            
        }
        
        PickerView().showDoalog(title: "Owner Lock In", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseOwnerLockInButton.setTitle("\(selectedString)", for: .normal)
            self.leaseTenantLockInButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseTenantLockInButtonClicked(_ sender: UIButton)
    {
        var incidentArray = [String]()
        
        for i in 0...60 {
            incidentArray.append("\(i)")
        }
        
        PickerView().showDoalog(title: "Tenant Lock In", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseTenantLockInButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseNoticeButtonClicked(_ sender: UIButton)
    {
        var incidentArray = [String]()
        
        for i in 0...60 {
            incidentArray.append("\(i)")
        }
        
        PickerView().showDoalog(title: "Notice Period", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseNoticeButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseAgreementButtonClicked(_ sender: UIButton)
    {
        self.isLeaseAgreementSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func leasePoliceVerificationButtonClicked(_ sender: UIButton)
    {
        self.isLeasePoliceVerificationSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func leaseNOCButtonClicked(_ sender: UIButton)
    {
        self.isLeaseNOCSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func brokerProfileButtonClicked(_ sender: UIButton)
    {
        self.isBrokerProfileSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func brokerPhoneBookButtonClicked(_ sender: UIButton)
    {
        self.isBrokerMobile = true
        
        let peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        
        DispatchQueue.main.async {
            self.present(peoplePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func brokerMaleButtonClicked(_ sender: UIButton)
    {
        self.brokerFemaleButton.isSelected = false
        self.brokerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.brokerOtherButton.isSelected = false
        self.brokerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.brokerMaleButton.isSelected
        {
            self.brokerMaleButton.isSelected = false
            self.brokerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerGender = ""
        }
        else
        {
            self.brokerMaleButton.isSelected = true
            self.brokerMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerGender = "male"
        }
    }
    
    @IBAction func brokerFemaleButtonClicked(_ sender: UIButton)
    {
        self.brokerMaleButton.isSelected = false
        self.brokerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.brokerOtherButton.isSelected = false
        self.brokerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.brokerFemaleButton.isSelected
        {
            self.brokerFemaleButton.isSelected = false
            self.brokerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerGender = ""
        }
        else
        {
            self.brokerFemaleButton.isSelected = true
            self.brokerFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerGender = "female"
        }
    }
    
    @IBAction func brokerOtherButtonClicked(_ sender: UIButton)
    {
        self.brokerMaleButton.isSelected = false
        self.brokerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.brokerFemaleButton.isSelected = false
        self.brokerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.brokerOtherButton.isSelected
        {
            self.brokerOtherButton.isSelected = false
            self.brokerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerGender = ""
        }
        else
        {
            self.brokerOtherButton.isSelected = true
            self.brokerOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerGender = "other"
        }
    }
    
    @IBAction func brokerOwnerButtonClicked(_ sender: UIButton)
    {
        self.brokerTenantButton.isSelected = false
        self.brokerTenantButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.brokerBothButton.isSelected = false
        self.brokerBothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.brokerOwnerButton.isSelected
        {
            self.brokerOwnerButton.isSelected = false
            self.brokerOwnerButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerTypeStr = ""
        }
        else
        {
            self.brokerOwnerButton.isSelected = true
            self.brokerOwnerButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerTypeStr = "Owner"
        }
    }
    
    @IBAction func brokerTenantButtonClicked(_ sender: UIButton)
    {
        self.brokerOwnerButton.isSelected = false
        self.brokerOwnerButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.brokerBothButton.isSelected = false
        self.brokerBothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.brokerTenantButton.isSelected
        {
            self.brokerTenantButton.isSelected = false
            self.brokerTenantButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerTypeStr = ""
        }
        else
        {
            self.brokerTenantButton.isSelected = true
            self.brokerTenantButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerTypeStr = "Tenant"
        }
    }
    
    @IBAction func brokerBothButtonClicked(_ sender: UIButton)
    {
        self.brokerOwnerButton.isSelected = false
        self.brokerOwnerButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.brokerTenantButton.isSelected = false
        self.brokerTenantButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.brokerBothButton.isSelected
        {
            self.brokerBothButton.isSelected = false
            self.brokerBothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.brokerTypeStr = ""
        }
        else
        {
            self.brokerBothButton.isSelected = true
            self.brokerBothButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.brokerTypeStr = "Both"
        }
    }
    
    @IBAction func brokerAadharButtonClicked(_ sender: UIButton)
    {
        self.isBrokerAadharSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func brokerPanButtonClicked(_ sender: UIButton)
    {
        self.isBrokerPanSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func managerProfileButtonClicked(_ sender: UIButton)
    {
        self.isManagerProfileSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func managerPhoneBookButtonClicked(_ sender: UIButton)
    {
        self.isManagerMobile = true
        
        let peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        
        DispatchQueue.main.async {
            self.present(peoplePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func managerMaleButtonClicked(_ sender: UIButton)
    {
        self.managerFemaleButton.isSelected = false
        self.managerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.managerOtherButton.isSelected = false
        self.managerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.managerMaleButton.isSelected
        {
            self.managerMaleButton.isSelected = false
            self.managerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.managerGender = ""
        }
        else
        {
            self.managerMaleButton.isSelected = true
            self.managerMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.managerGender = "male"
        }
    }
    
    @IBAction func managerFemaleButtonClicked(_ sender: UIButton)
    {
        self.managerMaleButton.isSelected = false
        self.managerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.managerOtherButton.isSelected = false
        self.managerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.managerFemaleButton.isSelected
        {
            self.managerFemaleButton.isSelected = false
            self.managerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.managerGender = ""
        }
        else
        {
            self.managerFemaleButton.isSelected = true
            self.managerFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.managerGender = "female"
        }
    }
    
    @IBAction func managerOtherButtonClicked(_ sender: UIButton)
    {
        self.managerMaleButton.isSelected = false
        self.managerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.managerFemaleButton.isSelected = false
        self.managerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.managerOtherButton.isSelected
        {
            self.managerOtherButton.isSelected = false
            self.managerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.managerGender = ""
        }
        else
        {
            self.managerOtherButton.isSelected = true
            self.managerOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.managerGender = "other"
        }
    }
    
    @IBAction func managerAadharButtonClicked(_ sender: UIButton)
    {
        self.isManagerAadharSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func managerPanButtonClicked(_ sender: UIButton)
    {
        self.isManagerPanSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func addPropertyManagerButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtManagerMobile.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtManagerFirstName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's First Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtManagerLastName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's Last Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtManagerEmail.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.managerGender.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Property Manager's Gender", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtManagerEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtManagerEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            
            self.addPropertyManagerDetailsResponse()
        }
 
    }
    
    @IBAction func selectLocationButtonClicked(_ sender: UIButton)
    {
        let filterSearchVC = SelectLocationViewController(nibName: "SelectLocationViewController", bundle: nil)
        filterSearchVC.areaDelegate = self
        self.navigationController?.pushViewController(filterSearchVC, animated: true)
    }
    
    @IBAction func leaseFamilyButtonClicked(_ sender: UIButton)
    {
        self.bachelorButton.isSelected = false
        self.bachelorButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.familyButton.isSelected
        {
            self.familyButton.isSelected = false
            self.familyButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.occupancyTypeStr = ""
        }
        else
        {
            self.familyButton.isSelected = true
            self.familyButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.occupancyTypeStr = "family"
        }
    }
    
    @IBAction func leaseBachelorButtonClicked(_ sender: UIButton)
    {
        self.familyButton.isSelected = false
        self.familyButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.bachelorButton.isSelected
        {
            self.bachelorButton.isSelected = false
            self.bachelorButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.occupancyTypeStr = ""
        }
        else
        {
            self.bachelorButton.isSelected = true
            self.bachelorButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.occupancyTypeStr = "bachelor"
        }
    }
    
    @objc func propertyPhotosCrossButtonClicked(sender : UIButton)
    {
        self.propertyPhotosArray?.removeObject(at: sender.tag)
        self.propertyPhotosNameArray?.removeObject(at: sender.tag)
        
        
        if self.propertyPhotosArray!.count > 0
        {
            self.propertyPhotoHeightConstraint.constant = 120
        }
        else
        {
            self.propertyPhotoHeightConstraint.constant = 0
            self.uploadPhotosLabel.text = "Upload"
            self.uploadPhotosLabelWidth.constant = 100
        }
        
        //self.propertyPhotoHeightConstraint.constant = 0
        DispatchQueue.main.async {
            self.propertyPhotosCollection.reloadData()
        }
    }
    
    @IBAction func addTenantDOBButtonClicked(_ sender: UIButton)
    {
        let datePicker = DatePickerDialog()
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

        datePicker.show("Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in

            if let dt = date
            {
                let formatter = DateFormatter()
//                formatter.dateFormat = "dd MMM - yyyy"
                formatter.dateFormat = "yyyy-MM-dd"
                print("Selected Date : \(formatter.string(from: dt))")

                self.txtTenantDOB.text = "\(formatter.string(from: dt))"
            }
        }
    }
    
    @IBAction func leaseStartDateButton(_ sender: UIButton)
    {
        print("leaseStartDateButton")
        
        let datePicker = DatePickerDialog()
        let currentDate = Date()
       
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        var futureDateComponents = DateComponents()
        futureDateComponents.year = 100
        let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
        
        datePicker.show("Lease Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
            
            if let dt = date
            {
                let formatter = DateFormatter()
//                formatter.dateFormat = "dd MMM - yyyy"
                formatter.dateFormat = "yyyy-MM-dd"
                print("Selected Date : \(formatter.string(from: dt))")
                
                self.txtLeaseStartDate.text = "\(formatter.string(from: dt))"
                self.txtLeaseRentStartDate.text = "\(formatter.string(from: dt))"
                
                /////
                self.selectedLeaseStartDate = dt
                let diffInDays = Calendar.current.dateComponents([.day], from: dt, to: dt).day
                self.txtLeaseFreePeriod.text = "\(diffInDays ?? 0)"
            }
        }
    }
    
    //MARK: - Add New Property Response
    func addNewPropertyResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
//            if self.txtOtherAmenities.text?.isEmpty != true
//            {
//                self.amenitiesStr = self.amenitiesStr.isEmpty ? "\(self.txtOtherAmenities.text ?? "")," : "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1)),\(self.txtOtherAmenities.text ?? ""),"
//            }
            
            if self.txtOtherAmenities.text?.isEmpty != true && self.amenitiesStr.isEmpty != true
            {
                self.amenitiesStr = (self.amenitiesStr + "\(self.txtOtherAmenities.text ?? "")")
            }
            else if self.txtOtherAmenities.text?.isEmpty != true
            {
                self.amenitiesStr = self.amenitiesStr.isEmpty ? "\(self.txtOtherAmenities.text ?? "")" : "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))"
            }
            else
            {
                if self.amenitiesStr.isEmpty != true
                {
                    self.amenitiesStr = "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))"
                }
            }
            
            if self.propertyPhotosNameArray!.count > 0
            {
                self.uploadPropertyImages()
            }
            
            if self.registrationDocPath.isEmpty != true
            {
                self.uploadRegistrationImages()
            }
            
            if self.lightBillDocPath.isEmpty != true
            {
                self.uploadLightBillImages()
            }
            
            if self.ownerIdDocPath.isEmpty != true
            {
                self.uploadOwnerIdImages()
            }
            
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": (Int64(AppUtility.getUserProfileMobileNumber()) ?? 0),
                             "created_by":"\(self.roleButton.currentTitle ?? "")",
                             "propertyName":"\(self.txtPropertyName.text ?? "")",
                             "property_type":"\(self.propertyTypeStr)-\(self.propertySubType)",
                             "flat_no":"\(self.txtFlatNo.text ?? "")",
                             "bldg_name":"\(self.txtBuildingName.text ?? "")",
                             "locality":"\(self.txtLocality.text ?? "")",
                             "floor_no":"\(self.selectedFloor)",
                             "wing":"\(self.txtWing.text ?? "")",
                             "address1":"\(self.txtAddress1.text ?? "")",
                             "address2":"\(self.txtAddress2.text ?? "")",
                             "city":"\(self.txtCity.text ?? "")",
                             "zipCode":(Int64(self.txtZipCode.text ?? "") ?? 0),
                             "state":"\(self.txtState.text ?? "")",
                             "country":"\(self.txtCountry.text ?? "")",
                             "bedrooms":"\(self.noOfRoomsStr)",
                             "bathrooms":"\(self.noOfBathroomsStr)",
                             "builtup_area":"\(self.txtBuiltUpArea.text ?? "")",
                             "carpet_area":"\(self.txtCarpetArea.text ?? "")",
                             "furnished_type":"\(self.furnishedTypeStr)",
                             "maintenance_charge":"0",
                             "parking":"\(self.noOfCarParkStr)",
//                             "Amenities":self.amenitiesStr.isEmpty ? "" : "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))",
                             "Amenities" : self.amenitiesStr.isEmpty ? "" : "\(self.amenitiesStr)",
                             "prop_photo":(self.propertyPhotosNameArray ?? [""]),
                             "reg_doc":"\(self.registrationDocPath)",
                             "electricity_bill":"\(self.lightBillDocPath)",
                             "owner_id":"\(self.ownerIdDocPath)",
                             "latitude":"\(self.selectedLatValue)",
                             "longitude":"\(self.selectedLonValue)",
                             "is_property_enable":String("1").utf8.map{UInt8($0)}[0],
//                             "is_property_enable":"\(self.isPropertyRented)",
                             "property_progress_state":0,
                             "broker_type":"\(self.addPropertyBrokerRepresentingBy)",
            ] as [String : Any]
            print(paramDict)
            
            DispatchQueue.global(qos: .background).async {

                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/createProperty", headerVal: "", parameters: paramDict) { (status, response) in

                    if status
                    {
                        self.propertyIdStr = "\((response as AnyObject).value(forKey: "property_id") ?? "")"

                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                            if self.isPropertyRented == "1"
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Add Tenant"
                                        self.lblHeader.text = "Add tenant (51% completed)"
                                        self.lblPageCount.text = "3/6"

                                        //                                    self.propertyIdStr = "\((response as AnyObject).value(forKey: "property_id") ?? "")"

                                        self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.addTenantViewObj?.backgroundColor = .orange
                                        self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.addTenantLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.addTenantView.isHidden = false
                                        self.brokerDetailView.isHidden = true

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 1300
                                    }
                                }
                                else if self.roleString == "Broker"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Add Owner"
                                        self.lblHeader.text = "Add Owner (60% completed)"
                                        self.lblPageCount.text = "3/5"

                                        self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.addTenantViewObj?.backgroundColor = .orange
                                        self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.addTenantLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.addTenantView.isHidden = false
                                        self.brokerDetailView.isHidden = true

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 1300
                                    }
                                }
                                else if self.roleString == "Tenant"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Add Owner"
                                        self.lblHeader.text = "Add Owner (60% completed)"
                                        self.lblPageCount.text = "3/6"

                                        self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.addTenantViewObj?.backgroundColor = .orange
                                        self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.addTenantLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.addTenantView.isHidden = false
                                        self.brokerDetailView.isHidden = true

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 1300
                                    }
                                }
                                else if self.roleString == "Property Manager"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Add Owner"
                                        self.lblHeader.text = "Add Owner (51% completed)"
                                        self.lblPageCount.text = "3/6"

                                        self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.addTenantViewObj?.backgroundColor = .orange
                                        self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.addTenantLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.addTenantView.isHidden = false
                                        self.brokerDetailView.isHidden = true

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 1300
                                    }
                                }


                            }
                            else
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {

                                        self.isLeaseAgreementSelect = false
                                        self.isLeasePoliceVerificationSelect = false
                                        self.isLeaseNOCSelect = false

                                        self.lblTopHeader.text = "Add Broker"
                                        self.lblHeader.text = "Broker details (75% completed)"
                                        self.lblPageCount.text = "3/4"
                                        self.skipButton.isHidden = false

                                        self.brokerDetailsViewObj?.frame = CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.brokerDetailsViewObj?.backgroundColor = .orange
                                        self.brokerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.brokerDetailsLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.brokerDetailView.isHidden = false

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2000
                                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                                    }
                                }
                                else if self.roleString == "Broker"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Add Owner"
                                        self.lblHeader.text = "Add Owner (100% completed)"
                                        self.lblPageCount.text = "3/3"
                                        self.skipButton.isHidden = false

                                        self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.addTenantViewObj?.backgroundColor = .orange
                                        self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.addTenantLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.addTenantView.isHidden = false
                                        self.brokerDetailView.isHidden = true

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 1300
                                    }
                                }
                                else if self.roleString == "Tenant"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                else if self.roleString == "Property Manager"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Add Owner"
                                        self.lblHeader.text = "Add Owner (75% completed)"
                                        self.lblPageCount.text = "3/4"
                                        self.skipButton.isHidden = false

                                        self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.addTenantViewObj?.backgroundColor = .orange
                                        self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.addTenantLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.addTenantView.isHidden = false
                                        self.brokerDetailView.isHidden = true

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 1300
                                    }
                                }
                            }

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }

                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Add New Tenant Response
    func addTenantResponse(contactType : String)
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.tenantProfileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.tenantProfileImageView.image!, imageName: self.tenantProfileImagePath)
            }
            
            if self.tenantAadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.tenantAadharImage!, imageName: self.tenantAadharImagePath)
            }
            
            if self.tenantPanImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.tenantPanImage!, imageName: self.tenantPanImagePath)
            }
            
            KRProgressHUD.show()
                        
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_type":"\(contactType)", //String
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno":(Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtTenantMobileNumber.text ?? ""))") ?? 0) as Any, //Long
                "firstName":"\(self.txtTenantFirstName.text ?? "")", //String
                "lastName":"\(self.txtTenantLastName.text ?? "")", //String
                "email":"\(self.txtTenantEmail.text ?? "")", //String
                "adhar_doc":"\(self.tenantAadharImagePath)", //String
                "pan_card_doc":"\(self.tenantPanImagePath)", //String
                "contact_photo":"\(self.tenantProfileImagePath)", //String
                //"companyinfo":self.companyInfoArray, //String Array
                "istenantbroker":false, //Boolean
                "iscompanyoccupant":false, //Boolean
                //"bank_info":self.bankInfoArray, //String Array
                "dob":"\(self.txtTenantDOB.text ?? "")", //String
                "pan_number":"\(self.txtTenantPan.text ?? "")", //String
                "adhar_number":"\(self.txtTenantAadhar.text ?? "")", //String
                "gender":"\(self.tenantGenderString)", //String
                "doa":"", //String
                "propertyName":"\(self.txtPropertyName.text ?? "")", //String
                "property_progress_state":Int(60), //Int
                "is_temporary":"\(self.isTenantTemporary)", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/addPropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            if self.isPropertyRented == "1"
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {
                                        
                                        self.lblTopHeader.text = "Lease Agreement"
                                        self.isTenantProfileSelect = false
                                        self.isTenantAadharSelect = false
                                        self.isTenantPanSelect = false
                                        
                                        self.lblHeader.text = "Lease agreement details (68% completed)"
                                        self.lblPageCount.text = "4/6"
                                        
                                        self.leaseAgreementViewObj?.frame = CGRect(x: (self.addTenantViewObj?.frame.origin.x)! + (self.addTenantViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.leaseAgreementViewObj?.backgroundColor = .orange
                                        self.leaseAgreementImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                        
                                        self.leaseAgreementLabel?.backgroundColor = Constants.darkBlueColor
                                        
                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = false
                                        self.propertyLocationView.isHidden = true
                                        self.brokerDetailView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true
                                        
                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2300
                                        
                                        if self.propertyTypeStr == "Commercial"
                                        {
                                            self.lblApplyGST.isHidden = false
                                            self.gstToggle.isHidden = false
                                            self.lblGSTNoteHeight.constant = 25
                                        }
                                        else
                                        {
                                            self.lblApplyGST.isHidden = true
                                            self.gstToggle.isHidden = true
                                            
                                            self.lblGSTNoteHeight.constant = 0
                                        }
                                    }
                                }
                                else if self.roleString == "Broker"
                                {
                                    if self.lblTopHeader.text == "Add Owner"
                                    {
                                        DispatchQueue.main.async {
                                            self.lblTopHeader.text = "Add Tenant"
                                            self.lblHeader.text = "Add tenant (80% completed)"
                                            self.lblPageCount.text = "4/5"
                                            

                                            self.txtTenantMobileNumber.text = ""
                                            self.txtTenantFirstName.text = ""
                                            self.txtTenantLastName.text = ""
                                            self.txtTenantEmail.text = ""
                                            self.txtTenantDOB.text = ""
                                            self.tenantGenderString = ""
                                            
                                            self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                                            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                                            self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                                            
                                            self.txtTenantAadhar.text = ""
                                            self.isTenantAadharSelect = false
                                            self.tenantAadharImagePath = ""
                                            self.tenantAadharButton.setBackgroundImage(UIImage(named: "plus"), for: .normal)
                                            
                                            self.tenantPanImagePath = ""
                                            self.txtTenantPan.text = ""
                                            self.isTenantPanSelect = false
                                            self.tenantPanButton.setBackgroundImage(UIImage(named: "plus"), for: .normal)
                                            
                                            self.tenantProfileImagePath = ""
                                            self.tenantProfileImageView.image = UIImage(named: "avatar")
                                            self.isTenantProfileSelect = false
                                            
                                            self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                            self.addTenantViewObj?.backgroundColor = .orange
                                            self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                            
                                            self.addTenantLabel?.backgroundColor = Constants.darkBlueColor
                                            
                                            self.newPropertyView.isHidden = true
                                            self.leaseView.isHidden = true
                                            self.propertyLocationView.isHidden = true
                                            self.propertyManagerView.isHidden = true
                                            
                                            self.addTenantView.isHidden = false
                                            self.brokerDetailView.isHidden = true
                                            
                                            self.mainScrollView.contentOffset.y = 0
                                            self.mainScrollView.contentSize.height = 1300
                                        }
                                    }
                                    else
                                    {
                                        DispatchQueue.main.async {
                                            
                                            self.lblTopHeader.text = "Lease Agreement"
                                            self.isTenantProfileSelect = false
                                            self.isTenantAadharSelect = false
                                            self.isTenantPanSelect = false
                                            
                                            self.lblHeader.text = "Lease agreement details (100% completed)"
                                            self.lblPageCount.text = "5/5"
                                            
                                            self.leaseAgreementViewObj?.frame = CGRect(x: (self.addTenantViewObj?.frame.origin.x)! + (self.addTenantViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                            self.leaseAgreementViewObj?.backgroundColor = .orange
                                            self.leaseAgreementImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                            
                                            self.leaseAgreementLabel?.backgroundColor = Constants.darkBlueColor
                                            
                                            self.newPropertyView.isHidden = true
                                            self.leaseView.isHidden = false
                                            self.propertyLocationView.isHidden = true
                                            self.brokerDetailView.isHidden = true
                                            self.addTenantView.isHidden = true
                                            self.propertyManagerView.isHidden = true
                                            
                                            self.mainScrollView.contentOffset.y = 0
                                            self.mainScrollView.contentSize.height = 2300
                                            
                                            if self.propertyTypeStr == "Commercial"
                                            {
                                                self.lblApplyGST.isHidden = false
                                                self.gstToggle.isHidden = false
                                                
                                                self.lblGSTNoteHeight.constant = 25
                                            }
                                            else
                                            {
                                                self.lblApplyGST.isHidden = true
                                                self.gstToggle.isHidden = true
                                                
                                                self.lblGSTNoteHeight.constant = 0
                                            }
                                        }
                                    }
                                    
                                }
                                else if self.roleString == "Property Manager"
                                {
                                    if self.lblTopHeader.text == "Add Owner"
                                    {
                                        DispatchQueue.main.async {
                                            self.lblTopHeader.text = "Add Tenant"
                                            self.lblHeader.text = "Add tenant (68% completed)"
                                            self.lblPageCount.text = "4/6"
                                            

                                            self.txtTenantMobileNumber.text = ""
                                            self.txtTenantFirstName.text = ""
                                            self.txtTenantLastName.text = ""
                                            self.txtTenantEmail.text = ""
                                            self.txtTenantDOB.text = ""
                                            self.tenantGenderString = ""
                                            
                                            self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                                            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                                            self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                                            
                                            self.txtTenantAadhar.text = ""
                                            self.isTenantAadharSelect = false
                                            self.tenantAadharImagePath = ""
                                            self.tenantAadharButton.setBackgroundImage(UIImage(named: "plus"), for: .normal)
                                            
                                            self.tenantPanImagePath = ""
                                            self.txtTenantPan.text = ""
                                            self.isTenantPanSelect = false
                                            self.tenantPanButton.setBackgroundImage(UIImage(named: "plus"), for: .normal)
                                            
                                            self.tenantProfileImagePath = ""
                                            self.tenantProfileImageView.image = UIImage(named: "avatar")
                                            self.isTenantProfileSelect = false
                                            
                                            self.addTenantViewObj?.frame = CGRect(x: (self.propertyLocationViewObj?.frame.origin.x)! + (self.propertyLocationViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                            self.addTenantViewObj?.backgroundColor = .orange
                                            self.addTenantImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                            
                                            self.addTenantLabel?.backgroundColor = Constants.darkBlueColor
                                            
                                            self.newPropertyView.isHidden = true
                                            self.leaseView.isHidden = true
                                            self.propertyLocationView.isHidden = true
                                            self.propertyManagerView.isHidden = true
                                            
                                            self.addTenantView.isHidden = false
                                            self.brokerDetailView.isHidden = true
                                            
                                            self.mainScrollView.contentOffset.y = 0
                                            self.mainScrollView.contentSize.height = 1300
                                        }
                                    }
                                    else
                                    {
                                        DispatchQueue.main.async {
                                            
                                            self.lblTopHeader.text = "Lease Agreement"
                                            self.isTenantProfileSelect = false
                                            self.isTenantAadharSelect = false
                                            self.isTenantPanSelect = false
                                            
                                            self.lblHeader.text = "Lease agreement details (85% completed)"
                                            self.lblPageCount.text = "5/6"
                                            
                                            self.leaseAgreementViewObj?.frame = CGRect(x: (self.addTenantViewObj?.frame.origin.x)! + (self.addTenantViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                            self.leaseAgreementViewObj?.backgroundColor = .orange
                                            self.leaseAgreementImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                            
                                            self.leaseAgreementLabel?.backgroundColor = Constants.darkBlueColor
                                            
                                            self.newPropertyView.isHidden = true
                                            self.leaseView.isHidden = false
                                            self.propertyLocationView.isHidden = true
                                            self.brokerDetailView.isHidden = true
                                            self.addTenantView.isHidden = true
                                            self.propertyManagerView.isHidden = true
                                            
                                            self.mainScrollView.contentOffset.y = 0
                                            self.mainScrollView.contentSize.height = 2300
                                            
                                            if self.propertyTypeStr == "Commercial"
                                            {
                                                self.lblApplyGST.isHidden = false
                                                self.gstToggle.isHidden = false
                                                
                                                self.lblGSTNoteHeight.constant = 25
                                            }
                                            else
                                            {
                                                self.lblApplyGST.isHidden = true
                                                self.gstToggle.isHidden = true
                                                
                                                self.lblGSTNoteHeight.constant = 0
                                            }
                                        }
                                    }
                                    
                                }
                                else if self.roleString == "Tenant"
                                {
                                    DispatchQueue.main.async {
                                        
                                        self.lblTopHeader.text = "Lease Agreement"
                                        self.isTenantProfileSelect = false
                                        self.isTenantAadharSelect = false
                                        self.isTenantPanSelect = false
                                        
                                        self.lblHeader.text = "Lease agreement details (80% completed)"
                                        self.lblPageCount.text = "4/6"
                                        
                                        self.leaseAgreementViewObj?.frame = CGRect(x: (self.addTenantViewObj?.frame.origin.x)! + (self.addTenantViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.leaseAgreementViewObj?.backgroundColor = .orange
                                        self.leaseAgreementImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                        
                                        self.leaseAgreementLabel?.backgroundColor = Constants.darkBlueColor
                                        
                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = false
                                        self.propertyLocationView.isHidden = true
                                        self.brokerDetailView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true
                                        
                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2300
                                        
                                        if self.propertyTypeStr == "Commercial"
                                        {
                                            self.lblApplyGST.isHidden = false
                                            self.gstToggle.isHidden = false
                                            
                                            self.lblGSTNoteHeight.constant = 25
                                        }
                                        else
                                        {
                                            self.lblApplyGST.isHidden = true
                                            self.gstToggle.isHidden = true
                                            
                                            self.lblGSTNoteHeight.constant = 0
                                        }
                                    }
                                }
                                
                            }
                            else
                            {
                                if self.roleString == "Broker"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                else if self.roleString == "Property Manager"
                                {
                                    DispatchQueue.main.async {
                                        
                                        self.isLeaseAgreementSelect = false
                                        self.isLeasePoliceVerificationSelect = false
                                        self.isLeaseNOCSelect = false
                                        self.lblTopHeader.text = "Add Broker"
                                        self.lblHeader.text = "Broker details (100% completed)"
                                        self.lblPageCount.text = "4/4"
                                        self.skipButton.isHidden = false
                                        
                                        self.brokerDetailsViewObj?.frame = CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.brokerDetailsViewObj?.backgroundColor = .orange
                                        self.brokerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                        
                                        self.brokerDetailsLabel?.backgroundColor = Constants.darkBlueColor
                                        
                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true
                                        
                                        self.brokerDetailView.isHidden = false
                                        
                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2000
                                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                                    }
                                }
                            }
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Add Broker Details
    func addBrokerDetailsResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.brokerProfileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerProfileImage!, imageName: "\(self.brokerProfileImagePath)")
            }
            
            if self.brokerAadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerAadharImage!, imageName: "\(self.brokerAadharImagePath)")
            }
            
            if self.brokerPanImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.brokerPanImage!, imageName: "\(self.brokerPanImagePath)")
            }
            
            KRProgressHUD.show()
            
            self.companyInfoArray.append("\(self.txtBrokerCompanyName.text ?? "")")
            self.companyInfoArray.append("\(self.txtBrokerAddress1.text ?? "")")
            self.companyInfoArray.append("\(self.txtBrokerAddress2.text ?? "")")
            self.companyInfoArray.append("\(self.txtBrokerCity.text ?? "")")
            self.companyInfoArray.append("\(self.txtBrokerZipCode.text ?? "")")
            self.companyInfoArray.append("\(self.txtBrokerState.text ?? "")")
            self.companyInfoArray.append("\(self.txtBrokerCountry.text ?? "")")
            
            self.bankInfoArray.append("")
            
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_type":"Broker", //String
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno":(Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtBrokerMobileNumber.text ?? ""))") ?? 0) as Any, //Long
                "firstName":"\(self.txtBrokerFirstName.text ?? "")", //String
                "lastName":"\(self.txtBrokerLastName.text ?? "")", //String
                "email":"\(self.txtBrokerEmail.text ?? "")", //String
                "adhar_doc":"\(self.brokerAadharImagePath)", //String
                "pan_card_doc":"\(self.brokerPanImagePath)", //String
                "contact_photo":"\(self.brokerProfileImagePath)", //String
                "companyinfo":self.companyInfoArray, //String Array
                "Nationality":"",
                "istenantbroker":false, //Boolean
                "iscompanyoccupant":false, //Boolean
                "bank_info":self.bankInfoArray, //String Array
                "dob":"\(self.txtBrokerDOB.text ?? "")", //String
                "pan_number":"\(self.txtBrokerPan.text ?? "")", //String
                "adhar_number":"\(self.txtBrokerAadhar.text ?? "")", //String
                "gender":"\(self.brokerGender)", //String
                "doa":"", //String
                "propertyName":"\(self.txtPropertyName.text ?? "")", //String
                "property_progress_state":Int(60), //Int
                "is_temporary":"\(self.isTenantTemporary)", //String
                "broker_type":"\(self.brokerTypeStr)"
            ] as [String : Any]
            
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/addPropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            if self.isPropertyRented == "1"
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Property Manager"
                                        self.lblHeader.text = "Property manager details (100% completed)"
                                        self.lblPageCount.text = "6/6"
                                        self.skipButton.isHidden = false
                                        
                                        self.propertyManagerDetailsViewObj?.frame = CGRect(x: (self.brokerDetailsViewObj?.frame.origin.x)! + (self.brokerDetailsViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.propertyManagerDetailsViewObj?.backgroundColor = .orange
                                        self.propertyManagerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                        
                                        self.propertyManagerDetailsLabel?.backgroundColor = Constants.darkBlueColor
                                        
//                                        self.headerScrollView.contentSize.width = (self.propertyManagerDetailsViewObj?.frame.origin.x)! + (self.propertyManagerDetailsViewObj?.frame.size.width)! + 30
                                    }
                                    
                                    self.newPropertyView.isHidden = true
                                    self.leaseView.isHidden = true
                                    self.propertyLocationView.isHidden = true
                                    self.addTenantView.isHidden = true
                                    self.brokerDetailView.isHidden = true
                                    
                                    self.propertyManagerView.isHidden = false
                                    
                                    self.mainScrollView.contentOffset.y = 0
                                    self.mainScrollView.contentSize.height = 1400
                                    
                                }
                                else if self.roleString == "Tenant" || self.roleString == "Property Manager"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                            else
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {
                                        self.lblTopHeader.text = "Property Manager"
                                        self.lblHeader.text = "Property manager details (100% completed)"
                                        self.lblPageCount.text = "4/4"
                                        self.skipButton.isHidden = false
                                        
                                        self.propertyManagerDetailsViewObj?.frame = CGRect(x: (self.brokerDetailsViewObj?.frame.origin.x)! + (self.brokerDetailsViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.propertyManagerDetailsViewObj?.backgroundColor = .orange
                                        self.propertyManagerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
                                        
                                        self.propertyManagerDetailsLabel?.backgroundColor = Constants.darkBlueColor
                                        
//                                        self.headerScrollView.contentSize.width = (self.propertyManagerDetailsViewObj?.frame.origin.x)! + (self.propertyManagerDetailsViewObj?.frame.size.width)! + 30
                                    }
                                    
                                    self.newPropertyView.isHidden = true
                                    self.leaseView.isHidden = true
                                    self.propertyLocationView.isHidden = true
                                    self.addTenantView.isHidden = true
                                    self.brokerDetailView.isHidden = true
                                    
                                    self.propertyManagerView.isHidden = false
                                    
                                    self.mainScrollView.contentOffset.y = 0
                                    self.mainScrollView.contentSize.height = 1400

                                }
                                else if self.roleString == "Property Manager"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                            
                            
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Add Lease Agreement Details
    func addLeaseAgreementResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.leaseAgreementImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.leaseAgreementImage!, imageName: "\(self.leaseAgreementImagePath)")
            }
            
            if self.leasePoliceVerificationImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.leasePoliceVerificationImage!, imageName: "\(self.leasePoliceVerificationImagePath)")
            }
            
            if self.leaseNOCImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.leaseNOCImage!, imageName: "\(self.leaseNOCImagePath)")
            }
            
            KRProgressHUD.show()
                
            self.leaseCompanyInfoArray.append("\(self.txtLeaseCompanyName.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtLeaseAddress.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtLeaseContactPerson.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtLeaseDesignation.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtLeaseContactNumber.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtLeaseEmail.text ?? "")")
            
            self.leaseDurationArray.append("\(self.leaseYearButton.currentTitle ?? ""),\(self.leaseMonthButton.currentTitle ?? "")")
            
            self.leaseEscalationArray.append("\(self.leaseEscalationYearButton.currentTitle ?? ""),\(self.leaseEscalationMonthButton.currentTitle ?? "")")
            
            let totalYears = Int(self.leaseYearButton.currentTitle ?? "")! * 365
            let totalMonths = Int(self.leaseMonthButton.currentTitle ?? "")! * 30
            let totalDays : Int = totalYears + totalMonths
            
            let escPerAmt = ((Int64(self.txtLeaseMonthlyRentAmt.text ?? "") ?? 0) * Int64(self.leaseEscalationYearButton.currentTitle ?? "")! )/100
            let escAmt : Int64 = ((Int64(self.txtLeaseMonthlyRentAmt.text ?? "") ?? 0) + escPerAmt)
            
            var finalAmount : Int64 = 0
            if self.gstToggle.isOn
            {
                let gstValue = ((Int64(self.txtLeaseMonthlyRentAmt.text ?? "") ?? 0) * 18)/100
                
                finalAmount = gstValue + (Int64(self.txtLeaseMonthlyRentAmt.text ?? "") ?? 0)
            }
            else
            {
                finalAmount = (Int64(self.txtLeaseMonthlyRentAmt.text ?? "") ?? 0)
            }
            
            let paramDict = [
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "lease_type":"\(self.leaseTypeStr)", //String
                "companyinfo":self.leaseCompanyInfoArray, //String Array
                "occupancy_type":"\(self.occupancyTypeStr)", //String
                "tenant_firstname":"", //String
                "tenant_lastname":"", //String
                "duration":self.leaseDurationArray, //String Array
                "start_date":"\(self.txtLeaseStartDate.text ?? "")", //String
                "rent_free_period":"\(self.txtLeaseFreePeriod.text ?? "")", //String
                "rent_start_date":"\(self.txtLeaseRentStartDate.text ?? "")", //String
//                "monthly_rent":(Int64(self.txtLeaseMonthlyRentAmt.text ?? "") ?? 0) as Any, //Long
                "monthly_rent":Int64(finalAmount) as Any, //Long
                "previous_rent":(Int64(0)) as Any, //Long
                "isRentEscalated":"0", //String
                "security_deposit_amt":(Int64(self.txtLeaseSecurityDeposite.text ?? "") ?? 0) as Any, //Long
                //"security_deposit_mnth":"", //Long
                "rent_due_date":(Int64(self.leaseRentDueDateStr) ?? 0) as Any, //Long
                "escalation_percent":(Int64(self.leaseEscalationPercentageButton.currentTitle ?? "") ?? 0) as Any, //Long
                "escalation_month":self.leaseEscalationArray, //String Array
                "owner_lockin":(Int64(self.leaseOwnerLockInButton.currentTitle ?? "") ?? 0) as Any, //Long
                "tenant_lockin":(Int64(self.leaseTenantLockInButton.currentTitle ?? "") ?? 0) as Any, //Long
                "notice_period":(Int64(self.leaseNoticeButton.currentTitle ?? "") ?? 0) as Any, //Long
                "rent_doc":"\(self.leaseRentImagePath)", //String
                "police_verf_doc":"\(self.leasePoliceImagePath)", //String
                "society_noc_doc":"\(self.leaseSocietyNOCImagePath)", //String
                "totalDays":totalDays, //Int
                "gst":self.applyGST, //Int
                "property_progress_state":80, //Int
                "escalationAmount":"\(escAmt)", //String
                "owner_lockin_end":"\(self.getLockinEndDate(dateStr: "\(self.txtLeaseStartDate.text ?? "")", noOfDays: "\(self.leaseOwnerLockInButton.currentTitle ?? "")"))", //String
                "tenant_lockin_end":"\(self.getLockinEndDate(dateStr: "\(self.txtLeaseStartDate.text ?? "")", noOfDays: "\(self.leaseTenantLockInButton.currentTitle ?? "")"))", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {

                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/addRentdetails", headerVal: "", parameters: paramDict) { (status, response) in

                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                            if self.isPropertyRented == "1"
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {

                                        self.isLeaseAgreementSelect = false
                                        self.isLeasePoliceVerificationSelect = false
                                        self.isLeaseNOCSelect = false

                                        self.lblTopHeader.text = "Add Broker"
                                        self.lblHeader.text = "Broker details (85% completed)"
                                        self.lblPageCount.text = "5/6"
                                        self.skipButton.isHidden = false

                                        self.brokerDetailsViewObj?.frame = CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.brokerDetailsViewObj?.backgroundColor = .orange
                                        self.brokerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.brokerDetailsLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.brokerDetailView.isHidden = false

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2000
                                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                                    }
                                }
                                else if self.roleString == "Broker"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                else if self.roleString == "Tenant"
                                {
                                    DispatchQueue.main.async {

                                        self.isLeaseAgreementSelect = false
                                        self.isLeasePoliceVerificationSelect = false
                                        self.isLeaseNOCSelect = false

                                        self.lblTopHeader.text = "Add Broker"
                                        self.lblHeader.text = "Broker details (100% Completed)"
                                        self.lblPageCount.text = "5/5"
                                        self.skipButton.isHidden = false

                                        self.brokerDetailsViewObj?.frame = CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.brokerDetailsViewObj?.backgroundColor = .orange
                                        self.brokerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.brokerDetailsLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.brokerDetailView.isHidden = false

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2000
                                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                                    }
                                }
                                else if self.roleString == "Property Manager"
                                {
                                    DispatchQueue.main.async {

                                        self.isLeaseAgreementSelect = false
                                        self.isLeasePoliceVerificationSelect = false
                                        self.isLeaseNOCSelect = false

                                        self.lblTopHeader.text = "Add Broker"
                                        self.lblHeader.text = "Broker details (100% completed)"
                                        self.lblPageCount.text = "6/6"
                                        self.skipButton.isHidden = false

                                        self.brokerDetailsViewObj?.frame = CGRect(x: (self.leaseAgreementViewObj?.frame.origin.x)! + (self.leaseAgreementViewObj?.frame.size.width)! + 40, y: 10, width: 60, height: 60)
                                        self.brokerDetailsViewObj?.backgroundColor = .orange
                                        self.brokerDetailsImgViewObj?.frame = CGRect(x: 15, y: 15, width: 30, height: 30)

                                        self.brokerDetailsLabel?.backgroundColor = Constants.darkBlueColor

                                        self.newPropertyView.isHidden = true
                                        self.leaseView.isHidden = true
                                        self.propertyLocationView.isHidden = true
                                        self.addTenantView.isHidden = true
                                        self.propertyManagerView.isHidden = true

                                        self.brokerDetailView.isHidden = false

                                        self.mainScrollView.contentOffset.y = 0
                                        self.mainScrollView.contentSize.height = 2000
                                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
                                    }
                                }

                            }
                            else
                            {

                            }

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }

                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    func getLockinEndDate(dateStr : String, noOfDays : String) -> String
    {
        if dateStr.isEmpty == true
        {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateObj : Date = formatter.date(from: dateStr)!
        
        var dateComponents = DateComponents()
        dateComponents.month = Int(noOfDays)
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: dateObj)
        
        print("Selected Date : \(formatter.string(from: threeMonthAgo!))")
        
        return "\(formatter.string(from: threeMonthAgo!))"
    }
    
    //MARK: - Add Property Manager Details
    func addPropertyManagerDetailsResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.managerProfileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.managerProfileImage!, imageName: "\(self.managerProfileImagePath)")
            }
            
            if self.managerAadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.managerAadharImage!, imageName: "\(self.managerAadharImagePath)")
            }
            
            if self.managerPanImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.managerPanImage!, imageName: "\(self.managerPanImagePath)")
            }
            
            KRProgressHUD.show()
                
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_type":"Property Manager", //String
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno":(Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtManagerMobile.text ?? ""))") ?? 0) as Any, //Long
                "firstName":"\(self.txtManagerFirstName.text ?? "")", //String
                "lastName":"\(self.txtManagerLastName.text ?? "")", //String
                "email":"\(self.txtManagerEmail.text ?? "")", //String
                "adhar_doc":"\(self.managerAadharImagePath)", //String
                "pan_card_doc":"\(self.managerPanImagePath)", //String
                "contact_photo":"\(self.managerProfileImagePath)", //String
                "Nationality":"",
                "istenantbroker":false, //Boolean
                "iscompanyoccupant":false, //Boolean
                "dob":"\(self.txtManagerDOB.text ?? "")", //String
                "pan_number":"\(self.txtManagerPan.text ?? "")", //String
                "adhar_number":"\(self.txtManagerAadhar.text ?? "")", //String
                "gender":"\(self.managerGender)", //String
                "doa":"", //String
                "propertyName":"\(self.txtPropertyName.text ?? "")", //String
                "property_progress_state":Int(60), //Int
                "is_temporary":"\(self.isTenantTemporary)", //String
//                "broker_type":"\(self.brokerTypeStr)"
            ] as [String : Any]
            
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/addPropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            if self.isPropertyRented == "1"
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                            else
                            {
                                if self.roleString == "Owner"
                                {
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                            
                            
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Get User Profile
    func getUserProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtTenantMobileNumber.text ?? ""))") ?? 0),
                             "searchResult":"",
                             "emailId":"",
                             "property_name":"",
                             "property_id":"",
                             "data":""
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        DispatchQueue.main.async {
                           
//                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")").isEmpty != true
//                            {
//                                self.tenantProfileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
//                                self.tenantProfileImageView.isUserInteractionEnabled = false
//                                self.tenantProfileImageView.clipsToBounds = true
//                            }
                            
                            self.tenantProfileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            self.tenantProfileImageView.isUserInteractionEnabled = false
                            self.tenantProfileImageView.clipsToBounds = true
                            self.tenantCameraButton.isEnabled = false
                            
                            self.tenantGenderString = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")"
                            
                            self.txtTenantFirstName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "firstName") ?? "")"
                            self.txtTenantFirstName.isEnabled = false
                            
                            self.txtTenantLastName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "lastName") ?? "")"
                            self.txtTenantLastName.isEnabled = false
                            
                            self.txtTenantEmail.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "email") ?? "")"
                            self.txtTenantEmail.isEnabled = false
                            
                            self.txtTenantDOB.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "dob") ?? "")"
                            self.txtTenantDOB.isEnabled = false
                            
                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "male"
                            {
                                self.tenantMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "female"
                            {
                                self.tenantFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else
                            {
                                self.tenantOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            
                            self.tenantMaleButton.isEnabled = false
                            self.tenantFemaleButton.isEnabled = false
                            self.tenantOtherButton.isEnabled = false
                            
                            self.txtTenantAadhar.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_no") ?? "")"
                            self.txtTenantAadhar.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")".isEmpty != true
                            {
                                self.tenantAadharButtonHeight.constant = 75
                                self.tenantAadharLabelHeight.constant = 75
                                
                                let aadharImgView = UIImageView()
                                aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")")")! as URL)) { (response, error, nil, url) in
                                    
                                    if error == nil
                                    {
                                        self.tenantAadharButton.setImage(response, for: .normal)
                                        self.tenantAadharButton.imageView?.contentMode = .scaleAspectFill
                                        self.tenantAadharButton.isUserInteractionEnabled = false
                                        self.tenantAadharButton.clipsToBounds = true
                                    }
                                }
                            }
                            else
                            {
                                self.tenantAadharButtonHeight.constant = 0
                                self.tenantAadharLabelHeight.constant = 0
                            }
                            
                            self.tenantAadharButton.isEnabled = false
                            self.tenantPanButton.isEnabled = false
                            
                            self.txtTenantPan.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_no") ?? "")"
                            self.txtTenantPan.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")".isEmpty != true
                            {
                                self.tenantPanButtonHeight.constant = 75
                                self.tenantPanLabelHeight.constant = 75
                                
                                let panImgView = UIImageView()
//                                panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
//                                self.tenantPanButton.setImage(panImgView.image, for: .normal)
//                                self.tenantPanButton.isEnabled = false
//                                self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
                                
                                panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")")")! as URL)) { (response, error, nil, url) in
                                    
                                    if error == nil
                                    {
                                        self.tenantPanButton.setImage(response, for: .normal)
                                        self.tenantPanButton.isUserInteractionEnabled = false
                                        self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
                                        self.tenantPanButton.clipsToBounds = true
                                    }
                                }
                            }
                            else
                            {
                                self.tenantPanButtonHeight.constant = 0
                                self.tenantPanLabelHeight.constant = 0
                            }
                            
                            self.isTenantTemporary = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "is_temporary") ?? "")"
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                           
                            self.tenantAadharButtonHeight.constant = 75
                            self.tenantAadharLabelHeight.constant = 75
                            
                            self.tenantPanButtonHeight.constant = 75
                            self.tenantPanLabelHeight.constant = 75
                            
                            self.tenantGenderString = "male"
                            self.tenantCameraButton.isEnabled = true
                            
                            self.tenantProfileImageView.image = UIImage(named: "avatar")
                            self.tenantProfileImageView.isUserInteractionEnabled = true
                            self.tenantProfileImageView.clipsToBounds = true
                            
                            self.txtTenantFirstName.text = ""
                            self.txtTenantFirstName.isEnabled = true
                            
                            self.txtTenantLastName.text = ""
                            self.txtTenantLastName.isEnabled = true
                            
                            self.txtTenantEmail.text = ""
                            self.txtTenantEmail.isEnabled = true
                            
                            self.txtTenantDOB.text = ""
                            self.txtTenantDOB.isEnabled = true
                            
                            self.tenantMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.tenantFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.tenantOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            
                            self.tenantMaleButton.isEnabled = true
                            self.tenantFemaleButton.isEnabled = true
                            self.tenantOtherButton.isEnabled = true

                            self.txtTenantAadhar.text = ""
                            self.txtTenantAadhar.isEnabled = true

                            self.tenantAadharButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.tenantAadharButton.imageView?.contentMode = .scaleAspectFill
                            self.tenantAadharButton.isUserInteractionEnabled = true
                            self.tenantAadharButton.clipsToBounds = true
                            
                            self.tenantAadharButton.isEnabled = true
                            self.tenantPanButton.isEnabled = true

                            self.txtTenantPan.text = ""
                            self.txtTenantPan.isEnabled = true

                            self.tenantPanButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.tenantPanButton.isUserInteractionEnabled = true
                            self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
                            self.tenantPanButton.clipsToBounds = true

                            self.isTenantTemporary = "0"
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Get Broker Profile
    func getBrokerProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtBrokerMobileNumber.text ?? ""))") ?? 0),
                             "searchResult":"",
                             "emailId":"",
                             "property_name":"",
                             "property_id":"",
                             "data":""
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        DispatchQueue.main.async {
                           
//                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")").isEmpty != true
//                            {
//                                self.brokerDetailsImgViewObj!.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
//                                self.brokerDetailsImgViewObj?.isUserInteractionEnabled = false
//                                self.brokerDetailsImgViewObj?.clipsToBounds = true
//                            }
                            
                            self.brokerCameraButton.isEnabled = false
                            
                            self.brokerProfilePic!.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            self.brokerProfilePic?.isUserInteractionEnabled = false
                            self.brokerProfilePic?.clipsToBounds = true
                            
                            self.txtBrokerFirstName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "firstName") ?? "")"
                            self.txtBrokerFirstName.isEnabled = false
                            
                            self.txtBrokerLastName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "lastName") ?? "")"
                            self.txtBrokerLastName.isEnabled = false
                            
                            self.txtBrokerEmail.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "email") ?? "")"
                            self.txtBrokerEmail.isEnabled = false
                            
                            self.txtBrokerDOB.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "dob") ?? "")"
                            self.txtBrokerDOB.isEnabled = false
                            
                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "male"
                            {
                                self.brokerMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "female"
                            {
                                self.brokerFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else
                            {
                                self.brokerOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            
                            self.brokerGender = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")"
                            
                            self.brokerTypeStr = ""
                            
                            self.brokerMaleButton.isEnabled = false
                            self.brokerFemaleButton.isEnabled = false
                            self.brokerOtherButton.isEnabled = false
                            
                            self.txtBrokerAadhar.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_no") ?? "")"
                            self.txtBrokerAadhar.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")".isEmpty == true
                            {
                                self.brokerAadharButtonHeight.constant = 0
                                self.brokerAadharLabelHeight.constant = 0
                            }
                            else
                            {
                                self.brokerAadharButtonHeight.constant = 75
                                self.brokerAadharLabelHeight.constant = 75
                            }
                            
                            let aadharImgView = UIImageView()
                            aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                           
                            self.brokerAadharButton.setImage(aadharImgView.image, for: .normal)
                            self.brokerAadharButton.imageView?.contentMode = .scaleAspectFill
                            self.brokerAadharButton.isUserInteractionEnabled = false
                            self.brokerAadharButton.clipsToBounds = true
                            
                            self.brokerAadharButton.isEnabled = false
                            self.brokerPanButton.isEnabled = false
                            
                            self.txtBrokerPan.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_no") ?? "")"
                            self.txtBrokerPan.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")".isEmpty == true
                            {
                                self.brokerPanButtonHeight.constant = 0
                                self.brokerPanLabelHeight.constant = 0
                            }
                            else
                            {
                                self.brokerPanButtonHeight.constant = 75
                                self.brokerPanLabelHeight.constant = 75
                            }
                            
                            let panImgView = UIImageView()
                            panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                           
                            self.brokerPanButton.setImage(panImgView.image, for: .normal)
                            self.brokerPanButton.isUserInteractionEnabled = false
                            self.brokerPanButton.imageView?.contentMode = .scaleAspectFill
                            self.brokerPanButton.clipsToBounds = true
                            
                            self.txtBrokerAddress1.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "address1") ?? "")"
                            self.txtBrokerAddress1.isEnabled = false
                            
                            self.txtBrokerAddress2.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "address2") ?? "")"
                            self.txtBrokerAddress2.isEnabled = false
                            
                            self.txtBrokerCity.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "city") ?? "")"
                            self.txtBrokerCity.isEnabled = false
                            
                            self.txtBrokerZipCode.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "zipCode") ?? "")"
                            self.txtBrokerZipCode.isEnabled = false
                            
                            self.txtBrokerState.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "state") ?? "")"
                            self.txtBrokerState.isEnabled = false
                                                        
                            self.isTenantTemporary = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "is_temporary") ?? "")"
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                           
                            self.brokerAadharButtonHeight.constant = 75
                            self.brokerAadharLabelHeight.constant = 75
                            
                            self.brokerPanButtonHeight.constant = 75
                            self.brokerPanLabelHeight.constant = 75
                            
                            self.brokerCameraButton.isEnabled = true
                        
                            self.brokerAadharButton.isEnabled = true
                            self.brokerPanButton.isEnabled = true
                            
                            self.brokerGender = "male"
                            self.brokerProfilePic?.image =  UIImage(named: "avatar")
                            self.brokerProfilePic?.isUserInteractionEnabled = true
                            self.brokerProfilePic?.clipsToBounds = true
                            
                            self.txtBrokerFirstName.text = ""
                            self.txtBrokerFirstName.isEnabled = true
                            
                            self.txtBrokerLastName.text = ""
                            self.txtBrokerLastName.isEnabled = true
                            
                            self.txtBrokerEmail.text = ""
                            self.txtBrokerEmail.isEnabled = true
                            
                            self.txtBrokerDOB.text = ""
                            self.txtBrokerDOB.isEnabled = true
                            
                            self.brokerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.brokerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.brokerBothButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            
                            self.brokerMaleButton.isEnabled = true
                            self.brokerFemaleButton.isEnabled = true
                            self.brokerOtherButton.isEnabled = true
                                                    
                            self.txtBrokerAadhar.text = ""
                            self.txtBrokerAadhar.isEnabled = true
                            
                            
                            self.brokerAadharButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.brokerAadharButton.imageView?.contentMode = .scaleAspectFill
                            self.brokerAadharButton.isUserInteractionEnabled = true
                            self.brokerAadharButton.clipsToBounds = true
                            
                            self.txtBrokerPan.text = ""
                            self.txtBrokerPan.isEnabled = true
                            
                            self.brokerPanButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.brokerPanButton.isUserInteractionEnabled = true
                            self.brokerPanButton.imageView?.contentMode = .scaleAspectFill
                            self.brokerPanButton.clipsToBounds = true
                                                        
                            self.txtBrokerAddress1.text = ""
                            self.txtBrokerAddress1.isEnabled = true
                            
                            self.txtBrokerAddress2.text = ""
                            self.txtBrokerAddress2.isEnabled = true
                            
                            self.txtBrokerCity.text = ""
                            self.txtBrokerCity.isEnabled = true
                            
                            self.txtBrokerZipCode.text = ""
                            self.txtBrokerZipCode.isEnabled = true
                            
                            self.txtBrokerState.text = ""
                            self.txtBrokerState.isEnabled = true
                                                        
                            self.isTenantTemporary = "0"
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Get Property Manager Profile
    func getPropertyManagerProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtManagerMobile.text ?? ""))") ?? 0),
                             "searchResult":"",
                             "emailId":"",
                             "property_name":"",
                             "property_id":"",
                             "data":""
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        DispatchQueue.main.async {
                           
                            self.managerButton.isEnabled = false
                            
                            self.managerProfileImgView!.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            self.managerProfileImgView.isUserInteractionEnabled = false
                            
                            self.txtManagerFirstName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "firstName") ?? "")"
                            self.txtManagerFirstName.isEnabled = false
                            
                            self.txtManagerLastName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "lastName") ?? "")"
                            self.txtManagerLastName.isEnabled = false
                            
                            self.txtManagerEmail.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "email") ?? "")"
                            self.txtManagerEmail.isEnabled = false
                            
                            self.txtManagerDOB.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "dob") ?? "")"
                            self.txtManagerDOB.isEnabled = false
                            
                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "male"
                            {
                                self.managerMaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "female"
                            {
                                self.managerFemaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else
                            {
                                self.managerOtherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            self.managerGender = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")"
                            self.managerMaleButton.isEnabled = false
                            self.managerFemaleButton.isEnabled = false
                            self.managerOtherButton.isEnabled = false
                            
                            self.txtManagerAadhar.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_no") ?? "")"
                            self.txtManagerAadhar.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")".isEmpty == true
                            {
                                self.managerAadharButtonHeight.constant = 0
                                self.managerAadharLabelHeight.constant = 0
                            }
                            else
                            {
                                self.managerAadharButtonHeight.constant = 75
                                self.managerAadharLabelHeight.constant = 75
                            }
                            
                            let aadharImgView = UIImageView()
                            aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                           
                            self.managerAadharButton.setImage(aadharImgView.image, for: .normal)
                            self.managerAadharButton.imageView?.contentMode = .scaleAspectFill
                            self.managerAadharButton.isUserInteractionEnabled = false
                            self.managerAadharButton.clipsToBounds = true
                            
                            self.managerAadharButton.isEnabled = false
                            self.managerPanButton.isEnabled = false
                            
                            self.txtManagerPan.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_no") ?? "")"
                            self.txtManagerPan.isEnabled = false
                            
                            if "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")".isEmpty == true
                            {
                                self.managerPanButtonHeight.constant = 0
                                self.managerPanLabelHeight.constant = 0
                            }
                            else
                            {
                                self.managerPanButtonHeight.constant = 75
                                self.managerPanLabelHeight.constant = 75
                            }
                            
                            let panImgView = UIImageView()
                            panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
                         
                            self.managerPanButton.setImage(panImgView.image, for: .normal)
                            self.managerPanButton.isUserInteractionEnabled = false
                            self.managerPanButton.imageView?.contentMode = .scaleAspectFill
                            self.managerPanButton.clipsToBounds = true
                            
                            self.isTenantTemporary = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "is_temporary") ?? "")"
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                           
                            self.managerAadharButtonHeight.constant = 75
                            self.managerAadharLabelHeight.constant = 75
                            
                            self.managerPanButtonHeight.constant = 75
                            self.managerPanLabelHeight.constant = 75
                            
                            self.managerButton.isEnabled = true
                            
                            self.managerAadharButton.isEnabled = true
                            self.managerPanButton.isEnabled = true
                            
                            self.managerGender = "male"
                            
                            self.managerProfileImgView.image = UIImage(named: "avatar")
                            self.managerProfileImgView.isUserInteractionEnabled = true
                            
                            self.txtManagerFirstName.text = ""
                            self.txtManagerFirstName.isEnabled = true
                            
                            self.txtManagerLastName.text = ""
                            self.txtManagerLastName.isEnabled = true
                            
                            self.txtManagerEmail.text = ""
                            self.txtManagerEmail.isEnabled = true
                            
                            self.txtManagerDOB.text = ""
                            self.txtManagerDOB.isEnabled = true
                            
                            self.managerMaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.managerFemaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.managerOtherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            
                            self.managerMaleButton.isEnabled = true
                            self.managerFemaleButton.isEnabled = true
                            self.managerOtherButton.isEnabled = true
                            
                            self.txtManagerAadhar.text = ""
                            self.txtManagerAadhar.isEnabled = true
                            
                            self.managerAadharButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.managerAadharButton.imageView?.contentMode = .scaleAspectFill
                            self.managerAadharButton.isUserInteractionEnabled = true
                            self.managerAadharButton.clipsToBounds = true
                            
                            self.txtManagerPan.text = ""
                            self.txtManagerPan.isEnabled = true
                            
                            self.managerPanButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.managerPanButton.isUserInteractionEnabled = true
                            self.managerPanButton.imageView?.contentMode = .scaleAspectFill
                            self.managerPanButton.clipsToBounds = true
                            
                            self.isTenantTemporary = "0"
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Upload Property Images
    func uploadPropertyImages()
    {
        for i in 0..<self.propertyPhotosArray!.count
        {
            KRProgressHUD.show()
            
            DispatchQueue.global(qos: .background).async {
                
                let req = NetworkManager.api.uploadImage(image: (self.propertyPhotosArray?.object(at: i) as! UIImage), imageName: "\(self.propertyPhotosNameArray?.object(at: i) ?? "")")

                req.done { (url) in
                    print("Property Success URL : \(url)")
                    
                }.catch { (error) in
                    print("Error : \(error.localizedDescription)")
                }
                
                KRProgressHUD.dismiss()
            }
        }
    }
    
    //MARK: - Upload Registration Images
    func uploadRegistrationImages()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.registrationImage!, imageName: "\(self.registrationDocPath)")
            
            req.done { (url) in
                print("Registration Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Upload LightBill Images
    func uploadLightBillImages()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.lightBillImage!, imageName: "\(self.lightBillDocPath)")
            
            req.done { (url) in
                print("Light Bill Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Upload Owner Id Images
    func uploadOwnerIdImages()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.ownerIdImage!, imageName: "\(self.ownerIdDocPath)")
            
            req.done { (url) in
                print("Owner Id Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Upload Registration Images
    func uploadImageToServer(uploadImg : UIImage, imageName : String)
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: uploadImg, imageName: "\(imageName)")
            
            req.done { (url) in
                print("Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Update Tenant Profile
    func updateTenantProfileResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": Int64(AppUtility.getUserProfileMobileNumber()) as Any,
                             "property_name":0,
                             "firstName":"\(self.txtTenantFirstName.text ?? "")",
                             "lastName":"\(self.txtTenantLastName.text ?? "")",
                             "dob":"\(self.txtTenantDOB.text ?? "")",
//                             "address1":"\(self.txtAddress1.text ?? "")",
//                             "address2":"\(self.txtAddress2.text ?? "")",
                             "email":"\(self.txtTenantEmail.text ?? "")",
//                             "city":"\(self.txtCity.text ?? "")",
//                             "zipCode": Int64(self.txtPostCode.text ?? "") as Any,
//                             "state":"\(self.txtState.text ?? "")",
                             "adhar_doc_path":"\(self.tenantAadharImagePath)",
                             "pan_doc_path":"\(self.tenantPanImagePath)",
                             "profile_path":"\(self.tenantProfileImagePath)",
                             "adhar_no":"\(self.txtTenantAadhar.text ?? "")",
                             "pan_no":"\(self.txtTenantPan.text ?? "")",
                             "gender":"\(self.tenantGenderString)",
                             "is_temporary":"0",
                             "broker_type":"0",
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updateUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if self.tenantProfileImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.tenantProfileImageView.image!, imageName: self.tenantProfileImagePath)
                        }
                        
                        if self.tenantAadharImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.tenantAadharImage!, imageName: self.tenantAadharImagePath)
                        }
                        
                        if self.tenantPanImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.tenantPanImage!, imageName: self.tenantPanImagePath)
                        }
                        
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Update Broker Profile
    func updateBrokerProfileResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": Int64(AppUtility.getUserProfileMobileNumber()) as Any,
                             "property_name":0,
                             "firstName":"\(self.txtBrokerFirstName.text ?? "")",
                             "lastName":"\(self.txtBrokerLastName.text ?? "")",
                             "dob":"\(self.txtBrokerDOB.text ?? "")",
                             "address1":"\(self.txtBrokerAddress1.text ?? "")",
                             "address2":"\(self.txtBrokerAddress2.text ?? "")",
                             "email":"\(self.txtBrokerEmail.text ?? "")",
                             "city":"\(self.txtBrokerCity.text ?? "")",
                             "zipCode": Int64(self.txtBrokerZipCode.text ?? "") as Any,
                             "state":"\(self.txtBrokerState.text ?? "")",
                             "adhar_doc_path":"\(self.brokerAadharImagePath)",
                             "pan_doc_path":"\(self.brokerPanImagePath)",
                             "profile_path":"\(self.brokerProfileImagePath)",
                             "adhar_no":"\(self.txtBrokerAadhar.text ?? "")",
                             "pan_no":"\(self.txtBrokerPan.text ?? "")",
                             "gender":"\(self.brokerGender)",
                             "is_temporary":"\(self.isTenantTemporary)",
                             "broker_type":"0",
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updateUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if self.brokerProfileImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.brokerProfileImage!, imageName: "\(self.brokerProfileImagePath)")
                        }
                        
                        if self.brokerAadharImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.brokerAadharImage!, imageName: "\(self.brokerAadharImagePath)")
                        }
                        
                        if self.brokerPanImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.brokerPanImage!, imageName: "\(self.brokerPanImagePath)")
                        }
                        
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Update Project Manager Profile
    func updateManagerProfileResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": Int64(AppUtility.getUserProfileMobileNumber()) as Any,
                             "property_name":0,
                             "firstName":"\(self.txtManagerFirstName.text ?? "")",
                             "lastName":"\(self.txtManagerLastName.text ?? "")",
                             "dob":"\(self.txtManagerDOB.text ?? "")",
//                             "address1":"\(self.txtBrokerAddress1.text ?? "")",
//                             "address2":"\(self.txtBrokerAddress2.text ?? "")",
                             "email":"\(self.txtManagerEmail.text ?? "")",
//                             "city":"\(self.txtBrokerCity.text ?? "")",
//                             "zipCode": Int64(self.txtBrokerZipCode.text ?? "") as Any,
//                             "state":"\(self.txtBrokerState.text ?? "")",
                             "adhar_doc_path":"\(self.managerAadharImagePath)",
                             "pan_doc_path":"\(self.managerPanImagePath)",
                             "profile_path":"\(self.managerProfileImagePath)",
                             "adhar_no":"\(self.txtManagerAadhar.text ?? "")",
                             "pan_no":"\(self.txtManagerPan.text ?? "")",
                             "gender":"\(self.managerGender)",
                             "is_temporary":"\(self.isTenantTemporary)",
                             "broker_type":"0",
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updateUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        if self.managerProfileImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.managerProfileImage!, imageName: "\(self.managerProfileImagePath)")
                        }
                        
                        if self.managerAadharImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.managerAadharImage!, imageName: "\(self.managerAadharImagePath)")
                        }
                        
                        if self.managerPanImagePath.isEmpty != true
                        {
                            self.uploadImageToServer(uploadImg: self.managerPanImage!, imageName: "\(self.managerPanImagePath)")
                        }
                        
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Filter Selected Area Delegate
    func sendSelectedArea(placesDict : PlaceDetails)
    {
        DispatchQueue.main.async {
            self.selectedLatValue = "\(placesDict.coordinate?.latitude ?? 0)"
            self.selectedLonValue = "\(placesDict.coordinate?.longitude ?? 0)"
            
            self.txtLocality.text = "\(placesDict.locality ?? "")"
            self.txtAddress1.text = "\(placesDict.name ?? "")"
            self.txtAddress2.text = "\(placesDict.subLocality ?? "")"
            
            self.txtCity.text = "\(placesDict.subAdministrativeArea ?? "")"
            self.txtZipCode.text = "\(placesDict.postalCode ?? "")"
            self.txtState.text = "\(placesDict.administrativeArea ?? "")"
            self.txtCountry.text = "\(placesDict.country ?? "")"
            
            let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
            let region = MKCoordinateRegion(center: placesDict.coordinate!, span: span)
            self.mapViewObj.setRegion(region, animated: true)
            
            if self.annotation == nil
            {
                self.annotation = MKPointAnnotation()
            }
            
            self.annotation!.coordinate = placesDict.coordinate!
            self.annotation!.title = "\(placesDict.name ?? "")"
            self.mapViewObj.addAnnotation(self.annotation!)
        }
    }
    
    //MARK: - Location Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        let currentLocation : CLLocation = locations.last!
        
        //        let latitude = String(format: "%f", currentLocation.coordinate.latitude)
        //        let longitude = String(format: "%f", currentLocation.coordinate.longitude)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: locValue, span: span)
        self.mapViewObj.setRegion(region, animated: true)
        
        if self.annotation == nil
        {
            self.annotation = MKPointAnnotation()
        }
        
        self.annotation!.coordinate = locValue
        self.annotation!.title = "My Location"
        self.mapViewObj.addAnnotation(self.annotation!)
        
        AppUtility.setUserData(keyString: "lat", valueString: "\(currentLocation.coordinate.latitude)")
        
        AppUtility.setUserData(keyString: "lon", valueString: "\(currentLocation.coordinate.longitude)")
        
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Location Error : \(error.localizedDescription)")
    }
}

extension CreatePropertyViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.propertyTypeCollection
        {
            if self.newPropertyTypeArray != nil
            {
                return self.newPropertyTypeArray!.count
            }
        }
//        else if collectionView == self.leasePropertyDocCollectionView
//        {
//            if self.propertyDocArray != nil
//            {
//                return self.propertyDocArray!.count
//            }
//        }
        else if collectionView == self.bedroomCollection
        {
            if self.bedroomArray != nil
            {
                return self.bedroomArray!.count
            }
        }
        else if collectionView == self.bathroomCollection
        {
            if self.bathroomArray != nil
            {
                return self.bathroomArray!.count
            }
        }
        else if collectionView == self.carParkCollection
        {
            if self.carParkArray != nil
            {
                return self.carParkArray!.count
            }
        }
        else if collectionView == self.amenitiesCollection
        {
            if self.amenitiesArray != nil
            {
                return self.amenitiesArray!.count
            }
        }
        else if collectionView == self.propertyPhotosCollection
        {
            if self.propertyPhotosArray != nil
            {
                if propertyPhotosArray!.count > 0
                {
                    return self.propertyPhotosArray!.count
                }
            }
            
            return 0
        }
//        else if collectionView == self.propertyDocumentCollection
//        {
//            return 3
//        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.propertyTypeCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
            
            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true
                
                if ("\(self.selectedNewPropertyTypeArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }
            
            myCell.titleButton.setTitle("\(self.newPropertyTypeArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(newPropertyTypeButtonClicked(sender:)), for: .touchUpInside)
            
            return myCell
        }
        else if collectionView == self.bedroomCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
            
            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true
                
                if ("\(self.selectedBedroomArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }
            
            myCell.titleButton.setTitle("\(self.bedroomArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(bedroomButtonClicked(sender:)), for: .touchUpInside)
            
            return myCell
        }
        else if collectionView == self.bathroomCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
            
            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true
                
                if ("\(self.selectedBathroomArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }
            
            myCell.titleButton.setTitle("\(self.bathroomArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(bathroomButtonClicked(sender:)), for: .touchUpInside)
            
            return myCell
        }
        else if collectionView == self.carParkCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
            
            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true
                
                if ("\(self.selectedCarParkArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }
            
            myCell.titleButton.setTitle("\(self.carParkArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(carParkButtonClicked(sender:)), for: .touchUpInside)
            
            return myCell
        }
        else if collectionView == self.amenitiesCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "boxCollectionCell", for: indexPath) as! BoxCollectionViewCell
            
            myCell.baseView.backgroundColor = .clear
            
            myCell.imageViewObj.layer.cornerRadius = 15
            myCell.imageViewObj.backgroundColor = .clear
            
            myCell.imageViewObj.image = UIImage(named: "\(self.amenitiesImageArray![indexPath.item])")
            myCell.lblTitle.text = "\(self.amenitiesArray![indexPath.item])"
            
            myCell.buttonObj.setTitleColor(.clear, for: .normal)
            myCell.buttonObj.setTitle("\(self.amenitiesArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.buttonObj.tag = indexPath.row
            myCell.buttonObj.addTarget(self, action: #selector(amenitiesButtonClicked(sender:)), for: .touchUpInside)
            
            DispatchQueue.main.async {
                myCell.baseView.layer.cornerRadius = 10
                
                if ("\(self.selectedAmenitiesArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.baseView.backgroundColor = .white
                }
                else
                {
                    myCell.baseView.backgroundColor = .clear
                }
            }
            return myCell
        }
        else if collectionView == self.propertyPhotosCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "propertyPhotoCell", for: indexPath) as! PropertyPhotosCollectionViewCell
            
            DispatchQueue.main.async {
                myCell.imgViewObj.layer.cornerRadius = 15
                myCell.imgViewObj.contentMode = .scaleAspectFill
            }
            
            myCell.imgViewObj.image = (self.propertyPhotosArray?.object(at: indexPath.item) as? UIImage)
            
            myCell.crossButton.tag = indexPath.item
            myCell.crossButton.addTarget(self, action: #selector(self.propertyPhotosCrossButtonClicked(sender:)), for: .touchUpInside)
        
            return myCell
        }
//        else if collectionView == self.propertyDocumentCollection
//        {
//            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "propertyInfoCell", for: indexPath) as! PropertyInfoCollectionViewCell
//
//            myCell.baseView.backgroundColor = .clear
//
//            myCell.imageViewObj.layer.cornerRadius = 15
//            myCell.imageViewObj.backgroundColor = .white
//
//            myCell.imageViewObj.image = (self.propertyDocImageArray?.object(at: indexPath.item) as! UIImage)
//            myCell.lblTitle.text = "\(self.propertyDocArray![indexPath.item])"
//
//            myCell.buttonObj.tag = indexPath.row
//            myCell.buttonObj.addTarget(self, action: #selector(self.propertyDocButtonClicked(sender:)), for: .touchUpInside)
//
//            return myCell
//        }
//        else if collectionView == self.leasePropertyDocCollectionView
//        {
//            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "propertyInfoCell", for: indexPath) as! PropertyInfoCollectionViewCell
//
//            myCell.baseView.backgroundColor = .clear
//
//            myCell.imageViewObj.layer.cornerRadius = 15
//            myCell.imageViewObj.backgroundColor = .white
//
//            myCell.imageViewObj.image = UIImage(named: "image1.jpeg")
//            myCell.lblTitle.text = "\(self.propertyDocArray![indexPath.item])"
//
//            return myCell
//        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == self.propertyTypeCollection
        {
            return CGSize(width: 120.0, height: 40.0)
        }
        else if collectionView == self.bedroomCollection
        {
            return CGSize(width: 48.0, height: 48.0)
        }
        else if collectionView == self.bathroomCollection || collectionView == self.carParkCollection
        {
            return CGSize(width: 48.0, height: 48.0)
        }
//        else if collectionView == self.amenitiesCollection || collectionView == self.propertyDocumentCollection
//        {
//            return CGSize(width: 100.0, height: 150.0)
//        }
        else if collectionView == self.propertyPhotosCollection
        {
            return CGSize(width: 150.0, height: 100.0)
        }
//        else if collectionView == self.leasePropertyDocCollectionView
//        {
//            return CGSize(width: 100.0, height: 150.0)
//        }
        
//        return CGSize(width: 100.0, height: 120.0)
        return CGSize(width: 80.0, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if collectionView == self.bedroomCollection ||
            collectionView == self.bathroomCollection ||
            collectionView == self.carParkCollection
        {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        }
        
        return UIEdgeInsets(top: 15, left: 0, bottom: 5, right: 15)
    }
}
