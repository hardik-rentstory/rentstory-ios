//
//  UserInfoViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 16/02/21.
//

import UIKit
import SDWebImage
import MessageUI

class UserInfoViewController: UIViewController, MFMessageComposeViewControllerDelegate
{
    var isHideCall = false
    var isHideMsg = false
    var isHideMail = false
    
    var detailDict : NSDictionary?
    
    @IBOutlet weak var topHeaderLabel: UILabel!
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var aadharLabel: UILabel!
    @IBOutlet weak var aadharImgView: UIImageView!
    
    @IBOutlet weak var panLabel: UILabel!
    @IBOutlet weak var panImgView: UIImageView!
    
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var msgButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        DispatchQueue.main.async {
            
            self.callButton.isHidden = self.isHideCall
            self.msgButton.isHidden = self.isHideMsg
            self.mailButton.isHidden = self.isHideMail
            
            self.topHeaderLabel.text = "\(self.detailDict?.value(forKey: "firstName") ?? "")'s Profile"
            
            self.nameLabel.text = "\(self.detailDict?.value(forKey: "firstName") ?? "") \(self.detailDict?.value(forKey: "lastName") ?? "")"
            self.fullNameLabel.text = "\(self.detailDict?.value(forKey: "firstName") ?? "") \(self.detailDict?.value(forKey: "lastName") ?? "")"
            self.emailLabel.text = "\(self.detailDict?.value(forKey: "email") ?? "")"
            self.contactNumberLabel.text = "\(self.detailDict?.value(forKey: "mobileno") ?? "")"
            self.dobLabel.text = "\(self.detailDict?.value(forKey: "dob") ?? "")"
            self.genderLabel.text = "\(self.detailDict?.value(forKey: "gender") ?? "")"
            self.addressLabel.text = "NA"
            self.aadharLabel.text = "\(self.detailDict?.value(forKey: "adhar_number") ?? "")"
            self.panLabel.text = "\(self.detailDict?.value(forKey: "pan_number") ?? "")"
            
            self.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.detailDict?.value(forKey: "contact_photo") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
          
            self.aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.detailDict?.value(forKey: "adhar_doc") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
            
            self.panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.baseView.layer.cornerRadius = 30
            
            self.profileImageView.layer.cornerRadius = 10
            self.aadharImgView.layer.cornerRadius = 10
            self.panImgView.layer.cornerRadius = 10
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callButtonClicked(_ sender: UIButton)
    {
        print("brokerPhoneButtonClicked")
        
        if self.contactNumberLabel.text!.isEmpty != true
        {
            let myUrl = "tel://\(self.contactNumberLabel.text ?? "")"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func msgButtonClicked(_ sender: UIButton)
    {
        print("ownerMsgButtonClicked")
        
        if self.contactNumberLabel.text?.isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\(self.contactNumberLabel.text ?? "")"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Owner's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func mailButtonClicked(_ sender: UIButton)
    {
        print("brokerMailButtonClicked")
        
        if self.emailLabel.text?.isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\(self.emailLabel.text ?? "")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's EmailId Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - Message Delegate
    func createEmailUrl(to: String, subject: String, body: String) -> URL?
    {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
//        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")

        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")

    
        if let gmailUrl = defaultUrl, UIApplication.shared.canOpenURL(defaultUrl!)
        {
            return gmailUrl
        }
        
        return URL(string: "")
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
