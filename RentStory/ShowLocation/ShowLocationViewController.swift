//
//  ShowLocationViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 08/02/21.
//

import UIKit
import CoreLocation
import MapKit
import AddressBook
import AddressBookUI

class ShowLocationViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate
{
    var propertyDict : NSDictionary?
    
    var annotation : MKPointAnnotation?
    var locationManager: CLLocationManager!
    
    
    @IBOutlet weak var mapViewObj: MKMapView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.locationManager = CLLocationManager()
        self.locationManager.requestAlwaysAuthorization()
        //or use requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        self.locationManager.startUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
//        self.locationManager.delegate = self
        
        
        let locValue = CLLocationCoordinate2DMake(("\(self.propertyDict?.value(forKey: "latitude") ?? 0)" as NSString).doubleValue, ("\(self.propertyDict?.value(forKey: "longitude") ?? 0)" as NSString).doubleValue)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: locValue, span: span)
        self.mapViewObj.setRegion(region, animated: true)
        
        if self.annotation == nil
        {
            self.annotation = MKPointAnnotation()
        }
        
        self.annotation!.coordinate = locValue
        self.annotation!.title = "\(self.propertyDict?.value(forKey: "propertyName") ?? "")"
        self.mapViewObj.addAnnotation(self.annotation!)
        
        /*
         let locValue:CLLocationCoordinate2D = manager.location!.coordinate
         
         let currentLocation : CLLocation = locations.last!
         
         //        let latitude = String(format: "%f", currentLocation.coordinate.latitude)
         //        let longitude = String(format: "%f", currentLocation.coordinate.longitude)
         
         let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
         let region = MKCoordinateRegion(center: locValue, span: span)
         self.mapViewObj.setRegion(region, animated: true)
         
         if self.annotation == nil
         {
             self.annotation = MKPointAnnotation()
         }
         
         self.annotation!.coordinate = locValue
         self.annotation!.title = "My Location"
         self.mapViewObj.addAnnotation(self.annotation!)
         */
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
