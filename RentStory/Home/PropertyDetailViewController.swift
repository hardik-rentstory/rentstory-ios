//
//  PropertyDetailViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 23/11/20.
//

import UIKit
import FSPagerView
import SDWebImage
import KRProgressHUD
import Firebase
import FirebaseAuth
import MessageUI

class PropertyDetailViewController: UIViewController, FSPagerViewDataSource,FSPagerViewDelegate, OTPVerificationDelegate, MFMessageComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate
{
    var tableViewObj : UITableView?
    
    var ownerDict = NSDictionary()
    var tenantDict = NSDictionary()
    var brokerDict = NSDictionary()
    var propertyManagerDict = NSDictionary()
    
    var ownerIsVerified = "0"
    var tenantIsVerified = "0"
    
    var owner_ContactId = ""
    var tenant_ContactId = ""
    var broker_ContactId = ""
    var propertyManager_ContactId = ""
    
    var verificationCode = ""
    var ownerPhoneNumber = ""
    var tenantPhoneNumber = ""
    var brokerPhoneNumber = ""
    var propertyManagerPhoneNumber = ""
    
    var peopleInvolvedArray : NSMutableArray?
    var leaseDict : NSDictionary?
    
    var detailDict : NSDictionary?
    
    var infoImgArray = [String]()
//    var infoImgArray : NSMutableArray?
    var infoTitleArray = [String]()
    
    var leaseImgArray = [String]()
    var leaseTitleArray = [String]()

    var addMoreTitleArray = NSMutableArray()
    
    var bottomView: UIView!
    
    var viewChat: UIView!
    var viewRentHistory: UIView!
    var viewNotifications: UIView!
    
    var btnChat: UIButton!
    var btnRentHistory: UIButton!
    var btnNotifications: UIButton!
    
    var addMoreLabel : UILabel?
    
    var lblChat: UILabel!
    var lblRentHistory: UILabel!
    var lblNotifications: UILabel!
    
    var imgViewChat: UIImageView!
    var imgViewRentHistory: UIImageView!
    var imgViewNotifications: UIImageView!
    
    fileprivate var imageNames = [String]()
   
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var buttonViewObj: UIView!
    @IBOutlet var propertyInfoView: UIView!
    
    @IBOutlet var peopleInvolvedView: UIView!
    
    @IBOutlet weak var propertyInfoContentView: UIView!
    @IBOutlet weak var propertyInfoSubView: UIView!
    
    @IBOutlet weak var propertyInfoArrowButton: UIButton!
    @IBOutlet weak var btnPropertyInfo: UIButton!
    @IBOutlet weak var btnPeopleInvolved: UIButton!
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var mainScrollView: UIScrollView!

    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var infoHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var infoCollectionView: UICollectionView!
    
    @IBOutlet weak var leaseView: UIView!
    @IBOutlet weak var leaseSubView: UIView!
    
    @IBOutlet weak var leaseViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnLeaseArrow: UIButton!
    
    @IBOutlet weak var infoDocView: UIView!
    @IBOutlet weak var infoDocArrowButton: UIButton!
    
    @IBOutlet weak var infoDocSubView: UIView!
    
//    @IBOutlet weak var leaseDocCollectionView: UICollectionView!
    @IBOutlet weak var infoDocHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    
    var addMorePeopleCollectionView: UICollectionView?
    
    @IBOutlet weak var ownerView: UIView!
    @IBOutlet weak var ownerSubView: UIView!
    @IBOutlet weak var ownerArrowButton: UIButton!
    @IBOutlet weak var ownerImgView: UIImageView!
//    @IBOutlet weak var ownerCallButton: UIButton!
//    @IBOutlet weak var ownerEmailButton: UIButton!
//    @IBOutlet weak var ownerMessageButton: UIButton!
    
    @IBOutlet weak var tenantView: UIView!
    @IBOutlet weak var tenantSubView: UIView!
    
    @IBOutlet weak var tenantArrowButton: UIButton!
    @IBOutlet weak var tenantHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tenantImgView: UIImageView!
//    @IBOutlet weak var tenantCallButton: UIButton!
//    @IBOutlet weak var tenantEmailButton: UIButton!
//    @IBOutlet weak var tenantMessageButton: UIButton!
    
    @IBOutlet weak var ownerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var brokerView: UIView!
    @IBOutlet weak var brokerSubView: UIView!
    @IBOutlet weak var brokerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var brokerImgView: UIImageView!
    @IBOutlet weak var brokerArrowButton: UIButton!
    
//    @IBOutlet weak var brokerCallButton: UIButton!
//    @IBOutlet weak var brokerEmailButton: UIButton!
//    @IBOutlet weak var brokerMessageButton: UIButton!
    
    @IBOutlet weak var propertyManagerView: UIView!
    @IBOutlet weak var propertyManagerSubView: UIView!
    
    @IBOutlet weak var propertyManagerArrowButton: UIButton!
    @IBOutlet weak var propertyManagerImgView: UIImageView!
    
//    @IBOutlet weak var propertyMgrCallButton: UIButton!
//    @IBOutlet weak var propertyMgrEmailButton: UIButton!
//    @IBOutlet weak var propertyMgrMessageButton: UIButton!
    
    @IBOutlet weak var propertyManagerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addTenantView: UIView!
    @IBOutlet weak var addBrokerView: UIView!
    @IBOutlet weak var addPropertyManagerView: UIView!
    
    
    @IBOutlet weak var lblPropertyType: UILabel!
    
    @IBOutlet weak var lblPropertyName: UILabel!
    
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var lblPeriod: UILabel!
    @IBOutlet weak var lblDeposite: UILabel!
    @IBOutlet weak var lblRent: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblNotice: UILabel!
    @IBOutlet weak var lblEscalation: UILabel!
    @IBOutlet weak var lblLockingPeriod: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    
    @IBOutlet weak var ownerViewTop: NSLayoutConstraint!
    @IBOutlet weak var tenantViewTop: NSLayoutConstraint!
    @IBOutlet weak var brokerViewTop: NSLayoutConstraint!
    @IBOutlet weak var projectManagerTop: NSLayoutConstraint!
    
    
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var ownerSubNameLabel: UILabel!
    
    @IBOutlet weak var ownerEmailLabel: UILabel!
    
    @IBOutlet weak var ownerPhoneLabel: UILabel!
    
    @IBOutlet weak var ownerVerifyImgView: UIImageView!
    @IBOutlet weak var ownerSubVerifyImgView: UIImageView!
    
    @IBOutlet weak var ownerVerifyLabel: UILabel!
    @IBOutlet weak var ownerSubVerifyLabel: UILabel!

    @IBOutlet weak var ownerVerifyButton: UIButton!
    @IBOutlet weak var ownerSubVerifyButton: UIButton!
    
    
    @IBOutlet weak var tenantNameLabel: UILabel!
    @IBOutlet weak var tenantSubNameLabel: UILabel!
    
    @IBOutlet weak var tenantVerifyImgView: UIImageView!
    @IBOutlet weak var tenantSubVerifyImgView: UIImageView!
    
    @IBOutlet weak var tenantVerifyLabel: UILabel!
    @IBOutlet weak var tenantSubVerifyLabel: UILabel!
    
    @IBOutlet weak var tenantVerifyButton: UIButton!
    @IBOutlet weak var tenantSubVerifyButton: UIButton!
    
    @IBOutlet weak var tenantEmailLabel: UILabel!
    @IBOutlet weak var tenantPhoneLabel: UILabel!
    
    
    @IBOutlet weak var brokerNameLabel: UILabel!
    @IBOutlet weak var brokerSubNameLabel: UILabel!
    
    @IBOutlet weak var brokerVerifyImgView: UIImageView!
    @IBOutlet weak var brokerSubVerifyImgView: UIImageView!
    
    @IBOutlet weak var brokerVerifyLabel: UILabel!
    @IBOutlet weak var brokerSubVerifyLabel: UILabel!
    
    @IBOutlet weak var brokerVerifyButton: UIButton!
    @IBOutlet weak var brokerSubVerifyButton: UIButton!
    
    @IBOutlet weak var brokerEmailLabel: UILabel!
    @IBOutlet weak var brokerPhoneLabel: UILabel!
    
    @IBOutlet weak var propertyManagerNameLabel: UILabel!
    @IBOutlet weak var propertyManagerVerifyImgView: UIImageView!
    @IBOutlet weak var propertyManagerVerifyLabel: UILabel!
    @IBOutlet weak var propertyManagerVerifyOtpButton: UIButton!
    @IBOutlet weak var propertyManagerEmailLabel: UILabel!
    @IBOutlet weak var propertyManagerPhoneLabel: UILabel!
    
    
    @IBOutlet weak var ownerPhoneButton: UIButton!
    @IBOutlet weak var ownerMsgButton: UIButton!
    @IBOutlet weak var ownerMailButton: UIButton!
//    @IBOutlet weak var ownerViewButton: UIButton!
    @IBOutlet weak var ownerEditButton: UIButton!
    @IBOutlet weak var ownerDeleteButton: UIButton!
    
    @IBOutlet weak var tenantPhoneButton: UIButton!
    @IBOutlet weak var tenantMsgButton: UIButton!
    @IBOutlet weak var tenantMailButton: UIButton!
    @IBOutlet weak var tenantViewButton: UIButton!
    @IBOutlet weak var tenantEditButton: UIButton!
    @IBOutlet weak var tenantDeleteButton: UIButton!
    
    @IBOutlet weak var brokerPhoneButton: UIButton!
    @IBOutlet weak var brokerMailButton: UIButton!
    @IBOutlet weak var brokerMsgButton: UIButton!
    @IBOutlet weak var brokerViewButton: UIButton!
    @IBOutlet weak var brokerEditButton: UIButton!
    @IBOutlet weak var brokerDeleteButton: UIButton!
    
    @IBOutlet weak var pmPhoneButton: UIButton!
    @IBOutlet weak var pmMailButton: UIButton!
    @IBOutlet weak var pmMsgButton: UIButton!
    @IBOutlet weak var pmViewButton: UIButton!
    @IBOutlet weak var pmEditButton: UIButton!
    @IBOutlet weak var pmDeleteButton: UIButton!
    
    
    @IBOutlet weak var baseViweHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet var peopleInvolvedViewObj: UIView!
    
//    @IBOutlet weak var peopleInvolvedTableView: UITableView!
    
    @IBOutlet weak var registrationDocImageView: UIImageView!
    @IBOutlet weak var lblRegistration: UILabel!
    @IBOutlet weak var electricityImageView: UIImageView!
    @IBOutlet weak var lblElectricity: UILabel!
    @IBOutlet weak var ownerIdImageView: UIImageView!
    @IBOutlet weak var lblOwnerId: UILabel!
    
    @IBOutlet weak var lblNoDoc: UILabel!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let imgObj = detailDict?.value(forKey: "prop_photo")
        {
            self.imageNames = (imgObj as? String ?? "").components(separatedBy: ",")
        }
        
        if "\(detailDict?.value(forKey: "Amenities") ?? "")".components(separatedBy: ",").count > 0
        {
            for currentObj in "\(detailDict?.value(forKey: "Amenities") ?? "")".components(separatedBy: ",")
            {
                print(currentObj)
                
                if currentObj.lowercased() == "swimming pool"
                {
                    self.infoTitleArray.append("\(currentObj)")
                    self.infoImgArray.append("swimmingPool")
                }
                else if currentObj.lowercased() == "fitness"
                {
                    self.infoTitleArray.append("\(currentObj)")
                    self.infoImgArray.append("fitness")
                }
                else if currentObj.lowercased() == "badminton court"
                {
                    self.infoTitleArray.append("\(currentObj)")
                    self.infoImgArray.append("badminton")
                }
                else if currentObj.lowercased() == "free wifi"
                {
                    self.infoTitleArray.append("\(currentObj)")
                    self.infoImgArray.append("wifi")
                }
                else
                {
                    self.infoTitleArray.append("\(currentObj)")
                    self.infoImgArray.append("wifi")
                }
            }
            
            self.infoTitleArray.append("area")
            self.infoImgArray.append("area")
            
            self.infoTitleArray.append("bathroom")
            self.infoImgArray.append("bathroom")
            
            self.infoTitleArray.append("beds")
            self.infoImgArray.append("beds")
        }
        
        self.leaseImgArray = ["\(self.detailDict?.value(forKey: "reg_doc") ?? "")".isEmpty == true ? "homeLogo" : "\(self.detailDict?.value(forKey: "reg_doc") ?? "")", "\(self.detailDict?.value(forKey: "electricity_bill") ?? "")".isEmpty == true ? "homeLogo" : "\(self.detailDict?.value(forKey: "electricity_bill") ?? "")","homeLogo", "homeLogo"]
        self.leaseTitleArray = ["Registered doc", "Electricity Bill", "Owner ID Proof"]
        
        self.addMoreTitleArray.add("Add Owner")
        self.addMoreTitleArray.add("Add Tenant")
        self.addMoreTitleArray.add("Add Broker")
        self.addMoreTitleArray.add("Add Property Manager")
        
        self.pagerView.delegate = self
        self.pagerView.dataSource = self
//        self.pagerView.transformer = FSPagerViewTransformer(type: .coverFlow)
        self.pagerView.transformer = FSPagerViewTransformer(type: .depth)
        self.pagerView.itemSize = FSPagerView.automaticSize
        self.pagerView.decelerationDistance = 1
        
        self.btnPropertyInfo.isSelected = true
        self.btnPropertyInfo.backgroundColor = Constants.darkBlueColor
        self.btnPropertyInfo.setTitleColor(.lightGray, for: .normal)
        
        self.btnPeopleInvolved.isSelected = false
        self.btnPeopleInvolved.backgroundColor = .white
        self.btnPeopleInvolved.setTitleColor(.lightGray, for: .normal)
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.lblHeader.font = UIFont(name: "SofiaPro-Medium", size: 28)
        }
        
//        self.mainScrollView.backgroundColor = .clear
        
        // Property Info View
        self.propertyInfoView.backgroundColor = .clear
        
        self.propertyInfoView.frame = CGRect(x: 0.0, y: 100, width: UIScreen.main.bounds.size.width, height: 920)
        self.baseView.addSubview(self.propertyInfoView)

//        self.mainScrollView.addSubview(self.propertyInfoView)
        
//        DispatchQueue.main.async {
//            self.viewHeight.constant = 1100
//            self.mainScrollView.contentSize.height = 1450
//        }
        
        self.propertyInfoArrowButton.isSelected = true
        self.propertyInfoView.isHidden = false
        
        // People Involved View
//        self.peopleInvolvedView.backgroundColor = .clear
//        self.peopleInvolvedView.frame = CGRect(x: 0.0, y: 120.0, width: UIScreen.main.bounds.size.width, height: 1550)
//        self.baseView.addSubview(self.peopleInvolvedView)
//        self.mainScrollView.addSubview(self.peopleInvolvedView)
        
        self.peopleInvolvedViewObj.backgroundColor = .clear
        self.peopleInvolvedViewObj.frame = CGRect(x: 0.0, y: 100.0, width: UIScreen.main.bounds.size.width, height: 1200)
        self.baseView.addSubview(self.peopleInvolvedViewObj)
        
//        self.peopleInvolvedTableView.delegate = self
//        self.peopleInvolvedTableView.dataSource = self
//        self.peopleInvolvedTableView.isScrollEnabled = false
//        self.peopleInvolvedTableView.separatorColor = .clear
//        self.peopleInvolvedTableView.backgroundColor = .clear
    
        let peopleInvolvedNib = UINib(nibName: "PeopleInvolvedTableViewCell", bundle: nil)
//        self.peopleInvolvedTableView.register(peopleInvolvedNib, forCellReuseIdentifier: "peopleInvolvedCell")
    
        self.tableViewObj = UITableView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 500), style: .plain)
        self.tableViewObj!.delegate = self
        self.tableViewObj!.dataSource = self
        self.tableViewObj!.isScrollEnabled = false
        self.tableViewObj!.separatorColor = .clear
        self.tableViewObj!.backgroundColor = .clear
        self.peopleInvolvedViewObj.addSubview(self.tableViewObj!)
        tableViewObj!.register(peopleInvolvedNib, forCellReuseIdentifier: "peopleInvolvedCell")
        
        self.addMoreLabel = UILabel(frame: CGRect(x: 15, y: Int(self.tableViewObj!.frame.origin.y + self.tableViewObj!.frame.height) + 20, width: Int(UIScreen.main.bounds.width - 30), height: 30))
        self.addMoreLabel?.text = "Add More People"
        self.addMoreLabel?.font = UIFont(name: "SofiaPro-Medium", size: 17)
        self.peopleInvolvedViewObj.addSubview(self.addMoreLabel!)
        
//        self.peopleInvolvedView.isHidden = true
        self.peopleInvolvedViewObj.isHidden = true
        self.ownerArrowButton.isSelected = true
        
        self.btnLocation.layer.cornerRadius = self.btnLocation.frame.height/2
        self.btnLocation.setRadiusWithShadow(10)
        
        self.infoCollectionView.dataSource = self
        self.infoCollectionView.delegate = self
        
//        self.leaseDocCollectionView.dataSource = self
//        self.leaseDocCollectionView.delegate = self
        
        let infoCollectionNib = UINib(nibName: "PropertyInfoCollectionViewCell", bundle: nil)
        let addMoreCellNib = UINib(nibName: "BoxCollectionViewCell", bundle: nil)
        
        self.infoCollectionView.register(infoCollectionNib, forCellWithReuseIdentifier: "propertyInfoCell")
        self.infoCollectionView.showsHorizontalScrollIndicator = false
        
//        self.leaseDocCollectionView.register(addMoreCellNib, forCellWithReuseIdentifier: "boxCollectionCell")
//        self.leaseDocCollectionView.showsHorizontalScrollIndicator = false
        
        //
        let layout : UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 0, bottom: 5, right: 15)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 15
        layout.itemSize = CGSize(width: 100.0, height: 120.0)
        
        self.addMorePeopleCollectionView = UICollectionView(frame: CGRect(x: 20.0, y: (self.addMoreLabel?.frame.origin.y)! + (self.addMoreLabel?.frame.height)! + 5, width: UIScreen.main.bounds.width - 40, height: 128), collectionViewLayout: layout)
        self.addMorePeopleCollectionView!.dataSource = self
        self.addMorePeopleCollectionView!.delegate = self
        self.addMorePeopleCollectionView!.backgroundColor = .clear
        self.peopleInvolvedViewObj.addSubview(self.addMorePeopleCollectionView!)
        
        
        self.addMorePeopleCollectionView!.register(addMoreCellNib, forCellWithReuseIdentifier: "boxCollectionCell")
        self.addMorePeopleCollectionView!.showsHorizontalScrollIndicator = false
        

        self.btnLeaseArrow.isSelected = true
        self.infoDocArrowButton.isSelected = true
        
        self.propertyInfoContentView.backgroundColor = .white
        self.leaseView.backgroundColor = .white
        self.infoDocView.backgroundColor = .white

        self.tenantArrowButton.isSelected = true
        self.brokerArrowButton.isSelected = true
        self.propertyManagerArrowButton.isSelected = true
        
        self.lblPropertyType.text = "\(self.detailDict?.value(forKey: "property_type") ?? "")"
        self.lblPropertyName.text = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"

        self.lblLocation.text = "\((self.detailDict?.value(forKey: "wing") ?? "")), \((self.detailDict?.value(forKey: "flat_no") ?? "")), \((self.detailDict?.value(forKey: "bldg_name") ?? "")), \((self.detailDict?.value(forKey: "address1") ?? "")), \((self.detailDict?.value(forKey: "address2") ?? ""))"
        
        self.lblRent.text = "₹\(self.detailDict?.value(forKey: "monthly_rent") ?? "")" == "<null>" ? "NA" : "\(self.detailDict?.value(forKey: "monthly_rent") ?? "")/m"
        
//        self.getPropertyDetail()
//        self.getRentalInfo()
        
        if "\(detailDict?.value(forKey: "reg_doc") ?? "")".isEmpty && "\(detailDict?.value(forKey: "electricity_bill") ?? "")".isEmpty && "\(detailDict?.value(forKey: "owner_id") ?? "")".isEmpty
        {
            self.registrationDocImageView.isHidden = true
            self.lblRegistration.isHidden = true
            
            self.electricityImageView.isHidden = true
            self.lblElectricity.isHidden = true
            
            self.ownerIdImageView.isHidden = true
            self.lblOwnerId.isHidden = true
            
            self.lblNoDoc.isHidden = false
        }
        else
        {
            self.registrationDocImageView.isHidden = false
            self.lblRegistration.isHidden = false
            self.registrationDocImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "reg_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
            
            self.electricityImageView.isHidden = false
            self.lblElectricity.isHidden = false
            self.electricityImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "electricity_bill") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
            
            self.ownerIdImageView.isHidden = false
            self.lblOwnerId.isHidden = false
            self.ownerIdImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "owner_id") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
            
            self.lblNoDoc.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.getPropertyDetail()
        self.getRentalInfo()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.pagerView.layer.cornerRadius = 30
            self.pagerView.layer.masksToBounds = true
            
            self.baseView.layer.cornerRadius = 35
            self.baseView.backgroundColor = Constants.bgColor
            
            self.buttonViewObj.layer.cornerRadius = 25
            self.btnPropertyInfo.layer.cornerRadius = 15
            self.btnPeopleInvolved.layer.cornerRadius = 15
            
            self.propertyInfoContentView.layer.cornerRadius = 30
            self.propertyInfoContentView.layer.masksToBounds = true
            
            self.leaseView.layer.cornerRadius = 30
            self.leaseView.layer.masksToBounds = true
            
            self.infoDocView.layer.cornerRadius = 30
            self.infoDocView.layer.masksToBounds = true
            
//            self.peopleInvolvedView.layer.cornerRadius = 30
//            self.peopleInvolvedView.layer.masksToBounds = true

            self.peopleInvolvedViewObj.layer.cornerRadius = 30
            self.peopleInvolvedViewObj.layer.masksToBounds = true

            
            if self.propertyInfoView.isHidden != true
            {
                self.viewHeight.constant = 1050
                self.mainScrollView.contentSize.height = 1450
                self.baseView.backgroundColor = Constants.bgColor
            }
            else
            {
//                self.viewHeight.constant = 1500
                self.baseView.backgroundColor = Constants.bgColor
                
                var heightVal = 0
                if self.peopleInvolvedArray != nil
                {
                    if self.peopleInvolvedArray!.count > 0
                    {
                        if self.peopleInvolvedArray!.count == 1
                        {
                            heightVal = 600
                        }
                        else if self.peopleInvolvedArray!.count == 2
                        {
                            heightVal = 450 * self.peopleInvolvedArray!.count
                        }
                        else
                        {
                            heightVal = 300 * self.peopleInvolvedArray!.count
                        }
                    }
                }
            
                self.peopleInvolvedViewObj.frame = CGRect(x: self.peopleInvolvedViewObj.frame.origin.x, y: self.peopleInvolvedViewObj.frame.origin.y, width: self.peopleInvolvedViewObj.frame.width, height: CGFloat(heightVal))
                
                self.tableViewObj!.frame = CGRect(x: self.tableViewObj!.frame.origin.x, y: self.tableViewObj!.frame.origin.y, width: self.tableViewObj!.frame.width, height: CGFloat(155 * self.peopleInvolvedArray!.count))
                
                self.viewHeight.constant = self.tableViewObj!.frame.height + 300
                
                if self.addMoreTitleArray.count > 0
                {
                    self.addMoreLabel?.isHidden = false
                    
                    self.addMoreLabel?.frame = CGRect(x: 15, y: Int(self.tableViewObj!.frame.origin.y + self.tableViewObj!.frame.height) + 20, width: Int(UIScreen.main.bounds.width - 30), height: 30)
                }
                else
                {
                    self.addMoreLabel?.isHidden = true
                }
                
                
                self.addMorePeopleCollectionView?.frame = CGRect(x: 20.0, y: (self.addMoreLabel?.frame.origin.y)! + ((self.addMoreLabel?.frame.height)!) + 5, width: UIScreen.main.bounds.width - 40, height: 128)
                
                self.mainScrollView.contentSize.height = self.peopleInvolvedViewObj.frame.height + 200
                   
                DispatchQueue.main.async {
                    self.tableViewObj?.reloadData()
                }
            }
            
            self.ownerView.layer.cornerRadius = 30
            self.ownerView.layer.masksToBounds = true
            
            self.ownerImgView.layer.cornerRadius = 10
//            self.ownerCallButton.layer.cornerRadius = 10
//            self.ownerEmailButton.layer.cornerRadius = 10
//            self.ownerMessageButton.layer.cornerRadius = 10
            
            self.tenantView.layer.cornerRadius = 30
            self.tenantView.layer.masksToBounds = true
            
            self.tenantImgView.layer.cornerRadius = 10
//            self.tenantCallButton.layer.cornerRadius = 10
//            self.tenantEmailButton.layer.cornerRadius = 10
//            self.tenantMessageButton.layer.cornerRadius = 10

            self.brokerView.layer.cornerRadius = 30
            self.brokerView.layer.masksToBounds = true
            
            self.brokerImgView.layer.cornerRadius = 10
//            self.brokerCallButton.layer.cornerRadius = 10
//            self.brokerEmailButton.layer.cornerRadius = 10
//            self.brokerMessageButton.layer.cornerRadius = 10
            
            self.propertyManagerView.layer.cornerRadius = 30
            self.propertyManagerView.layer.masksToBounds = true
            
            self.propertyManagerImgView.layer.cornerRadius = 10
//            self.propertyMgrCallButton.layer.cornerRadius = 10
//            self.propertyMgrEmailButton.layer.cornerRadius = 10
//            self.propertyMgrMessageButton.layer.cornerRadius = 10
            
            self.addTenantView.layer.cornerRadius = 10
            self.addBrokerView.layer.cornerRadius = 10
            self.addPropertyManagerView.layer.cornerRadius = 10
        }
        
        self.bottomView = UIView(frame: CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 100, width: UIScreen.main.bounds.size.width, height: 100))
        self.bottomView.backgroundColor = Constants.darkBlueColor
        self.bottomView.layer.cornerRadius = 23
        self.view.addSubview(self.bottomView)
        
        
        // Middle View : Add Property
        self.viewChat = UIView(frame: CGRect(x: UIScreen.main.bounds.size.width/2, y: 15.0, width: 80.0, height: 60.0))
        self.viewChat.backgroundColor = Constants.darkGrayColor
        self.viewChat.layer.cornerRadius = 15
        self.bottomView.addSubview(self.viewChat)
        
        self.imgViewChat = UIImageView(frame: CGRect(x: 30.0, y: 0.0, width: 25.0, height: 60.0))
        self.imgViewChat.image = UIImage(named: "chatGray")
        self.imgViewChat.contentMode = .scaleAspectFit
        self.viewChat.addSubview(self.imgViewChat)
        
        self.lblChat = UILabel(frame: CGRect(x: self.imgViewChat.frame.origin.x + self.imgViewChat.frame.size.width + 5, y: 0, width: 0, height: 60.0))
        self.lblChat.text = "Chat"
        self.lblChat.font = UIFont(name: "SofiaPro", size: 15)
        self.lblChat.textAlignment = .left
        self.viewChat.addSubview(self.lblChat)
        
        self.btnChat = UIButton(type: .custom)
        self.btnChat.frame = CGRect(x: 0.0, y: 0.0, width: self.viewChat.frame.width, height: self.viewChat.frame.height)
        self.btnChat.backgroundColor = .clear
        self.btnChat.addTarget(self, action: #selector(chatButtonClicked(sender:)), for: .touchUpInside)
        self.viewChat.addSubview(self.btnChat)
        
        
        // First View : Discover
        self.viewRentHistory = UIView(frame: CGRect(x: 15, y: 15.0, width: self.viewChat.frame.origin.x - 30, height: 60.0))
        self.viewRentHistory.backgroundColor = .white
        self.viewRentHistory.layer.cornerRadius = 15
        self.bottomView.addSubview(self.viewRentHistory)
        
        self.imgViewRentHistory = UIImageView(frame: CGRect(x: 30.0, y: 0.0, width: 25.0, height: 60.0))
        self.imgViewRentHistory.image = UIImage(named: "rentHistoryBlue")
        self.imgViewRentHistory.contentMode = .scaleAspectFit
        self.viewRentHistory.addSubview(self.imgViewRentHistory)
        
        self.lblRentHistory = UILabel(frame: CGRect(x: self.imgViewRentHistory.frame.origin.x + self.imgViewRentHistory.frame.size.width + 5, y: 7, width: 100, height: 50.0))
        self.lblRentHistory.text = "Rent History"
        self.lblRentHistory.font = UIFont(name: "SofiaPro", size: 16)
        self.lblRentHistory.textAlignment = .left
        self.lblRentHistory.textColor = Constants.darkBlueColor
        self.viewRentHistory.addSubview(self.lblRentHistory)
        
        self.btnRentHistory = UIButton(type: .custom)
        self.btnRentHistory.frame = CGRect(x: 0.0, y: 0.0, width: self.viewRentHistory.frame.width, height: self.viewRentHistory.frame.height)
        self.btnRentHistory.backgroundColor = .clear
        self.btnRentHistory.addTarget(self, action: #selector(rentHistoryButtonClicked(sender:)), for: .touchUpInside)
        self.viewRentHistory.addSubview(self.btnRentHistory)
        
        
        // Third View : Notifications
//        self.viewNotifications = UIView(frame: CGRect(x: self.viewAddProperty.frame.origin.x + self.viewAddProperty.frame.size.width + 15, y: 15.0, width: 80.0, height: 60.0))
        self.viewNotifications = UIView(frame: CGRect(x: self.bottomView.frame.width - 80 - 15, y: 15.0, width: 80.0, height: 60.0))
        self.viewNotifications.backgroundColor = Constants.darkGrayColor
        self.viewNotifications.layer.cornerRadius = 15
        self.bottomView.addSubview(self.viewNotifications)
        
        self.imgViewNotifications = UIImageView(frame: CGRect(x: 30.0, y: 0.0, width: 25.0, height: 60.0))
        self.imgViewNotifications.image = UIImage(named: "notificationGray")
        self.imgViewNotifications.contentMode = .scaleAspectFit
        self.viewNotifications.addSubview(self.imgViewNotifications)
        
        self.btnNotifications = UIButton(type: .custom)
        self.btnNotifications.frame = CGRect(x: 0.0, y: 0.0, width: self.viewNotifications.frame.width, height: self.viewNotifications.frame.height)
        self.btnNotifications.backgroundColor = .clear
        self.btnNotifications.addTarget(self, action: #selector(self.notificationButtonClicked(sender:)), for: .touchUpInside)
        self.viewNotifications.addSubview(self.btnNotifications)
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func propertyInfoButtonClicked(_ sender: UIButton)
    {
        self.btnPropertyInfo.isSelected = true
        self.btnPropertyInfo.setTitleColor(.white, for: .normal)
        self.btnPropertyInfo.backgroundColor = Constants.darkBlueColor
        
        self.btnPeopleInvolved.isSelected = false
        self.btnPeopleInvolved.setTitleColor(.lightGray, for: .normal)
        self.btnPeopleInvolved.backgroundColor = .white
        
        self.propertyInfoView.isHidden = false
//        self.peopleInvolvedView.isHidden = true
        self.peopleInvolvedViewObj.isHidden = true
        
//        DispatchQueue.main.async {
//            self.viewHeight.constant = 1100
//            self.mainScrollView.contentSize.height = 1450
//        }
    }
    
    @IBAction func peopleInvolvedButtonClicked(_ sender: UIButton)
    {
        self.btnPeopleInvolved.isSelected = true
        self.btnPeopleInvolved.setTitleColor(.white, for: .normal)
        self.btnPeopleInvolved.backgroundColor = Constants.darkBlueColor
        
        self.btnPropertyInfo.isSelected = false
        self.btnPropertyInfo.setTitleColor(.lightGray, for: .normal)
        self.btnPropertyInfo.backgroundColor = .white
        
        self.propertyInfoView.isHidden = true
        self.peopleInvolvedViewObj.isHidden = false
        
        DispatchQueue.main.async {
            self.tableViewObj!.reloadData()
            self.view.setNeedsLayout()
        }
        
//        DispatchQueue.main.async {
//            self.viewHeight.constant = 210 * 3
//            self.mainScrollView.contentSize.height = 2000
//        }
        
        
//        self.peopleInvolvedView.isHidden = false
        
//        self.ownerViewTop.constant = 0
//        self.ownerHeightConstraint.constant = 0
//        self.ownerView.isHidden = true
//
//        self.brokerViewTop.constant = 0
//        self.brokerHeightConstraint.constant = 0
//        self.brokerView.isHidden = true
//
//        self.tenantViewTop.constant = 0
//        self.tenantHeightConstraint.constant = 0
//        self.tenantView.isHidden = true
//
//        self.projectManagerTop.constant = 0
//        self.propertyManagerHeightConstraint.constant = 0
//        self.propertyManagerView.isHidden = true
//
//        if self.peopleInvolvedArray != nil
//        {
//            if self.peopleInvolvedArray!.count > 0
//            {
//                for currentObj in self.peopleInvolvedArray!
//                {
//                    print(currentObj)
//
//                    if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "owner"
//                    {
//                        self.addMoreTitleArray.remove("Add Owner")
//
//                        self.ownerDict = (currentObj as! NSDictionary)
//
//                        self.owner_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
//
//                        self.ownerPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//
//                        if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
//                        {
//                            self.ownerImgView.image = UIImage(named: "avatar")
//                        }
//                        else
//                        {
//                            self.ownerImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
//                        }
//
//                        self.ownerViewTop.constant = 15
//                        self.ownerHeightConstraint.constant = 220
//                        self.ownerView.isHidden = false
//
//                        self.ownerNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//                        self.ownerSubNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//
//                        self.ownerEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
//                        self.ownerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
//                        {
//                            self.ownerIsVerified = "1"
//
//                            self.ownerVerifyImgView.isHidden = false
//                            self.ownerVerifyLabel.isHidden = false
//                            self.ownerVerifyButton.isHidden = true
//
//                            self.ownerSubVerifyImgView.isHidden = false
//                            self.ownerSubVerifyLabel.isHidden = false
//                            self.ownerSubVerifyButton.isHidden = true
//                        }
//                        else
//                        {
//                            self.ownerIsVerified = "0"
//
//                            self.ownerVerifyImgView.isHidden = true
//                            self.ownerVerifyLabel.isHidden = true
//                            self.ownerVerifyButton.isHidden = false
//
//                            self.ownerSubVerifyImgView.isHidden = true
//                            self.ownerSubVerifyLabel.isHidden = true
//                            self.ownerSubVerifyButton.isHidden = false
//                        }
//
////                        self.mainScrollView.contentSize.height = self.mainScrollView.contentSize.height + 200
//                    }
//                    else if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "tenant"
//                    {
//                        self.addMoreTitleArray.remove("Add Tenant")
//
//                        self.tenantDict = (currentObj as! NSDictionary)
//
//                        self.tenant_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
//
//                        self.tenantPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        self.tenantViewTop.constant = 15
//                        self.tenantHeightConstraint.constant = 220
//                        self.tenantView.isHidden = false
//
////                        if self.tenantPhoneNumber == AppUtility.getUserData(keyVal: "mobileNo")
////                        {
////                            self.tenantPhoneButton.isHidden = true
////                            self.tenantMailButton.isHidden = true
////                            self.tenantMsgButton.isHidden = true
////                            self.tenantViewButton.isHidden = true
////                            self.tenantEditButton.isHidden = true
////                            self.tenantDeleteButton.isHidden = true
////                        }
//
//                        if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
//                        {
//                            self.tenantImgView.image = UIImage(named: "avatar")
//                        }
//                        else
//                        {
//                            self.tenantImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
//                        }
//
//                        self.tenantNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//                        self.tenantSubNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//
//                        self.tenantEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
//                        self.tenantPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
//                        {
//                            self.tenantIsVerified = "1"
//
//                            self.tenantVerifyImgView.isHidden = false
//                            self.tenantVerifyLabel.isHidden = false
//                            self.tenantVerifyButton.isHidden = true
//
//                            self.tenantSubVerifyImgView.isHidden = false
//                            self.tenantSubVerifyLabel.isHidden = false
//                            self.tenantSubVerifyButton.isHidden = true
//                        }
//                        else
//                        {
//                            self.tenantIsVerified = "0"
//
//                            self.tenantVerifyImgView.isHidden = true
//                            self.tenantVerifyLabel.isHidden = true
//                            self.tenantVerifyButton.isHidden = false
//
//                            self.tenantSubVerifyImgView.isHidden = true
//                            self.tenantSubVerifyLabel.isHidden = true
//                            self.tenantSubVerifyButton.isHidden = false
//                        }
//
//
////                        self.mainScrollView.contentSize.height = self.mainScrollView.contentSize.height + 200
//                    }
//                    else if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "broker"
//                    {
//                        self.brokerDict = (currentObj as! NSDictionary)
//
//                        self.broker_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
//
//                        self.brokerPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        self.brokerViewTop.constant = 15
//                        self.brokerHeightConstraint.constant = 220
//                        self.brokerView.isHidden = false
//
////                        if self.brokerPhoneNumber == AppUtility.getUserData(keyVal: "mobileNo")
////                        {
////                            self.brokerPhoneButton.isHidden = true
////                            self.brokerMailButton.isHidden = true
////                            self.brokerMsgButton.isHidden = true
////                            self.brokerViewButton.isHidden = true
////                            self.brokerEditButton.isHidden = true
////                            self.brokerDeleteButton.isHidden = true
////                        }
//
//                        if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
//                        {
//                            self.brokerImgView.image = UIImage(named: "avatar")
//                        }
//                        else
//                        {
//                            self.brokerImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
//                        }
//
//                        self.brokerNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//                        self.brokerSubNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//
//                        self.brokerEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
//                        self.brokerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
//                        {
//                            self.brokerVerifyImgView.isHidden = false
//                            self.brokerVerifyLabel.isHidden = false
//                            self.brokerVerifyButton.isHidden = true
//
//                            self.brokerSubVerifyImgView.isHidden = false
//                            self.brokerSubVerifyLabel.isHidden = false
//                            self.brokerSubVerifyButton.isHidden = true
//                        }
//                        else
//                        {
//                            self.brokerVerifyImgView.isHidden = true
//                            self.brokerVerifyLabel.isHidden = true
//                            self.brokerVerifyButton.isHidden = false
//
//                            self.brokerSubVerifyImgView.isHidden = true
//                            self.brokerSubVerifyLabel.isHidden = true
//                            self.tenantSubVerifyButton.isHidden = false
//                        }
//
////                        self.mainScrollView.contentSize.height = self.mainScrollView.contentSize.height + 200
//                    }
//                    else if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "property manager"
//                    {
//                        self.addMoreTitleArray.remove("Add Property Manager")
//
//                        self.propertyManagerDict = (currentObj as! NSDictionary)
//
//                        self.propertyManager_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
//
//                        self.propertyManagerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        self.propertyManagerPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        self.projectManagerTop.constant = 15
//                        self.propertyManagerHeightConstraint.constant = 220
//                        self.propertyManagerView.isHidden = false
//
////                        if self.propertyManagerPhoneNumber == AppUtility.getUserData(keyVal: "mobileNo")
////                        {
////                            self.pmPhoneButton.isHidden = true
////                            self.pmMailButton.isHidden = true
////                            self.pmMsgButton.isHidden = true
////                            self.pmViewButton.isHidden = true
////                            self.pmEditButton.isHidden = true
////                            self.pmDeleteButton.isHidden = true
////                        }
//
//                        if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
//                        {
//                            self.propertyManagerImgView.image = UIImage(named: "avatar")
//                        }
//                        else
//                        {
//                            self.propertyManagerImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
//                        }
//
//                        self.propertyManagerNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
//
//                        self.propertyManagerEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
//                        self.propertyManagerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
//
//                        if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
//                        {
//                            self.propertyManagerVerifyImgView.isHidden = false
//                            self.propertyManagerVerifyLabel.isHidden = false
//                            self.propertyManagerVerifyOtpButton.isHidden = true
//                        }
//                        else
//                        {
//                            self.propertyManagerVerifyImgView.isHidden = true
//                            self.propertyManagerVerifyLabel.isHidden = true
//                            self.propertyManagerVerifyOtpButton.isHidden = false
//                        }
//
////                        self.mainScrollView.contentSize.height = self.mainScrollView.contentSize.height + 200
//                    }
//                }
//
//                // To Show/Hide Action Buttons (i.e Call, Msg, Mail, Edit, Delete)
//
//                if "\(self.detailDict?.value(forKey: "mobileno") ?? "")" == AppUtility.getUserData(keyVal: "mobileNo")
//                {
//                    self.ownerPhoneButton.isHidden = true
//                    self.ownerMailButton.isHidden = true
//                    self.ownerMsgButton.isHidden = true
//                    self.ownerEditButton.isHidden = true
//                    self.ownerDeleteButton.isHidden = true
//                    self.ownerHeightConstraint.constant = 165
//
//                    self.tenantPhoneButton.isHidden = false
//                    self.tenantMailButton.isHidden = false
//                    self.tenantMsgButton.isHidden = false
//                    self.tenantEditButton.isHidden = false
//                    self.tenantDeleteButton.isHidden = false
//
//                    self.brokerPhoneButton.isHidden = false
//                    self.brokerMailButton.isHidden = false
//                    self.brokerMsgButton.isHidden = false
//                    self.brokerEditButton.isHidden = false
//                    self.brokerDeleteButton.isHidden = false
//
//                    self.pmPhoneButton.isHidden = false
//                    self.pmMailButton.isHidden = false
//                    self.pmMsgButton.isHidden = false
//                    self.pmEditButton.isHidden = false
//                    self.pmDeleteButton.isHidden = false
//                }
//                else if self.ownerIsVerified == "1" && "\(self.detailDict?.value(forKey: "ownerNumber") ?? "")" == AppUtility.getUserData(keyVal: "mobileNo")
//                {
//                    self.ownerPhoneButton.isHidden = false
//                    self.ownerMailButton.isHidden = false
//                    self.ownerMsgButton.isHidden = false
//                    self.ownerEditButton.isHidden = false
//                    self.ownerDeleteButton.isHidden = false
//
//                    self.tenantPhoneButton.isHidden = false
//                    self.tenantMailButton.isHidden = false
//                    self.tenantMsgButton.isHidden = false
//                    self.tenantEditButton.isHidden = false
//                    self.tenantDeleteButton.isHidden = false
//
//                    self.brokerPhoneButton.isHidden = false
//                    self.brokerMailButton.isHidden = false
//                    self.brokerMsgButton.isHidden = false
//                    self.brokerEditButton.isHidden = false
//                    self.brokerDeleteButton.isHidden = false
//
//                    self.pmPhoneButton.isHidden = false
//                    self.pmMailButton.isHidden = false
//                    self.pmMsgButton.isHidden = false
//                    self.pmEditButton.isHidden = false
//                    self.pmDeleteButton.isHidden = false
//                }
//                else if self.ownerIsVerified == "0" && ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Owner")
//                {
//                    self.ownerPhoneButton.isHidden = false
//                    self.ownerMailButton.isHidden = false
//                    self.ownerMsgButton.isHidden = false
//                    self.ownerEditButton.isHidden = false
//                    self.ownerDeleteButton.isHidden = false
//
//                    self.tenantPhoneButton.isHidden = true
//                    self.tenantMailButton.isHidden = true
//                    self.tenantMsgButton.isHidden = true
//                    self.tenantEditButton.isHidden = true
//                    self.tenantDeleteButton.isHidden = true
//                    self.tenantHeightConstraint.constant = 165
//
//                    self.brokerPhoneButton.isHidden = true
//                    self.brokerMailButton.isHidden = true
//                    self.brokerMsgButton.isHidden = true
//                    self.brokerEditButton.isHidden = true
//                    self.brokerDeleteButton.isHidden = true
//                    self.brokerHeightConstraint.constant = 165
//
//                    self.pmPhoneButton.isHidden = true
//                    self.pmMailButton.isHidden = true
//                    self.pmMsgButton.isHidden = true
//                    self.pmEditButton.isHidden = true
//                    self.pmDeleteButton.isHidden = true
//                    self.propertyManagerHeightConstraint.constant = 165
//                }
//                else if self.ownerIsVerified == "0" && ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant")
//                {
//                    self.ownerPhoneButton.isHidden = true
//                    self.ownerMailButton.isHidden = true
//                    self.ownerMsgButton.isHidden = true
//                    self.ownerEditButton.isHidden = true
//                    self.ownerDeleteButton.isHidden = true
//                    self.ownerHeightConstraint.constant = 165
//
//                    self.tenantPhoneButton.isHidden = false
//                    self.tenantMailButton.isHidden = false
//                    self.tenantMsgButton.isHidden = false
//                    self.tenantEditButton.isHidden = false
//                    self.tenantDeleteButton.isHidden = false
//
//                    self.brokerPhoneButton.isHidden = true
//                    self.brokerMailButton.isHidden = true
//                    self.brokerMsgButton.isHidden = true
//                    self.brokerEditButton.isHidden = true
//                    self.brokerDeleteButton.isHidden = true
//                    self.brokerHeightConstraint.constant = 165
//
//                    self.pmPhoneButton.isHidden = true
//                    self.pmMailButton.isHidden = true
//                    self.pmMsgButton.isHidden = true
//                    self.pmEditButton.isHidden = true
//                    self.pmDeleteButton.isHidden = true
//                    self.propertyManagerHeightConstraint.constant = 165
//                }
//                else if self.ownerIsVerified == "0" && ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker")
//                {
//                    self.ownerPhoneButton.isHidden = true
//                    self.ownerMailButton.isHidden = true
//                    self.ownerMsgButton.isHidden = true
//                    self.ownerEditButton.isHidden = true
//                    self.ownerDeleteButton.isHidden = true
//                    self.ownerHeightConstraint.constant = 165
//
//                    self.tenantPhoneButton.isHidden = true
//                    self.tenantMailButton.isHidden = true
//                    self.tenantMsgButton.isHidden = true
//                    self.tenantEditButton.isHidden = true
//                    self.tenantDeleteButton.isHidden = true
//                    self.tenantHeightConstraint.constant = 165
//
//                    self.brokerPhoneButton.isHidden = false
//                    self.brokerMailButton.isHidden = false
//                    self.brokerMsgButton.isHidden = false
//                    self.brokerEditButton.isHidden = false
//                    self.brokerDeleteButton.isHidden = false
//
//                    self.pmPhoneButton.isHidden = true
//                    self.pmMailButton.isHidden = true
//                    self.pmMsgButton.isHidden = true
//                    self.pmEditButton.isHidden = true
//                    self.pmDeleteButton.isHidden = true
//                    self.propertyManagerHeightConstraint.constant = 165
//                }
//                else if self.ownerIsVerified == "0" && ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager")
//                {
//                    self.ownerPhoneButton.isHidden = true
//                    self.ownerMailButton.isHidden = true
//                    self.ownerMsgButton.isHidden = true
//                    self.ownerEditButton.isHidden = true
//                    self.ownerDeleteButton.isHidden = true
//                    self.ownerHeightConstraint.constant = 165
//
//                    self.tenantPhoneButton.isHidden = true
//                    self.tenantMailButton.isHidden = true
//                    self.tenantMsgButton.isHidden = true
//                    self.tenantEditButton.isHidden = true
//                    self.tenantDeleteButton.isHidden = true
//                    self.tenantHeightConstraint.constant = 165
//
//                    self.brokerPhoneButton.isHidden = true
//                    self.brokerMailButton.isHidden = true
//                    self.brokerMsgButton.isHidden = true
//                    self.brokerEditButton.isHidden = true
//                    self.brokerDeleteButton.isHidden = true
//                    self.brokerHeightConstraint.constant = 165
//
//                    self.pmPhoneButton.isHidden = false
//                    self.pmMailButton.isHidden = false
//                    self.pmMsgButton.isHidden = false
//                    self.pmEditButton.isHidden = false
//                    self.pmDeleteButton.isHidden = false
//                }
//                else
//                {
//                    self.ownerPhoneButton.isHidden = false
//                    self.ownerMailButton.isHidden = false
//                    self.ownerMsgButton.isHidden = false
//                    self.ownerEditButton.isHidden = true
//                    self.ownerDeleteButton.isHidden = true
//
//                    self.tenantPhoneButton.isHidden = false
//                    self.tenantMailButton.isHidden = false
//                    self.tenantMsgButton.isHidden = false
//                    self.tenantEditButton.isHidden = true
//                    self.tenantDeleteButton.isHidden = true
//
//                    self.brokerPhoneButton.isHidden = false
//                    self.brokerMailButton.isHidden = false
//                    self.brokerMsgButton.isHidden = false
//                    self.brokerEditButton.isHidden = true
//                    self.brokerDeleteButton.isHidden = true
//
//                    self.pmPhoneButton.isHidden = false
//                    self.pmMailButton.isHidden = false
//                    self.pmMsgButton.isHidden = false
//                    self.pmEditButton.isHidden = true
//                    self.pmDeleteButton.isHidden = true
//                }
//
//                // Show/Hide Add More People
//                if self.ownerIsVerified == "0"
//                {
//                    if ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant") ||
//                        ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker") ||
//                        ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager")
//                    {
//                        self.addMoreTitleArray.remove("Add Property Manager")
//                    }
//                    else
//                    {
//                        self.addMoreTitleArray.removeAllObjects()
//                    }
//                }
//                else
//                {
//                    if ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant") || ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker") || ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager")
//                    {
//                        self.addMoreTitleArray.removeAllObjects()
//                    }
//                }
//
//                DispatchQueue.main.async {
//                    self.addMorePeopleCollectionView.reloadData()
//                }
//            }
//        }
        
//        DispatchQueue.main.async {
//            self.viewHeight.constant = 1200
//            self.mainScrollView.contentSize.height = 2000
//        }
    }
    
    @objc func rentHistoryButtonClicked(sender : UIButton)
    {
        print("rentHistoryButtonClicked")
        
        UIView.animate(withDuration: 0.7) {
            
            self.viewChat.frame = CGRect(x: UIScreen.main.bounds.size.width/2, y: 15.0, width: 80.0, height: 60.0)
            self.viewChat.backgroundColor = Constants.darkGrayColor
            self.lblChat.frame.size.width = 0
            self.btnChat.frame = CGRect(x: 0.0, y: 0.0, width: self.viewChat.frame.width, height: self.viewChat.frame.height)
            self.imgViewChat.image = UIImage(named: "chatGray")
            
            
            self.viewRentHistory.backgroundColor = .white
            self.viewRentHistory.frame = CGRect(x: 15, y: 15, width: self.viewChat.frame.origin.x - 30, height: 60.0)
            self.lblRentHistory.frame = CGRect(x: self.imgViewRentHistory.frame.origin.x + self.imgViewRentHistory.frame.size.width + 5, y: 7, width: 100, height: 50.0)
            self.lblRentHistory.textColor = Constants.darkBlueColor
            self.btnRentHistory.frame = CGRect(x: 0.0, y: 0.0, width: self.viewRentHistory.frame.width, height: self.viewRentHistory.frame.height)
            self.imgViewRentHistory.image = UIImage(named: "rentHistoryBlue")
            
            

        } completion: { (_) in
            let rentHistoryVC = RentHistoryViewController(nibName: "RentHistoryViewController", bundle: nil)
            rentHistoryVC.propertyID = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            rentHistoryVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            rentHistoryVC.createByName = "\(self.detailDict?.value(forKey: "ownerFirstName") ?? "")" == "<null>" ? "NA" :  "\(self.detailDict?.value(forKey: "ownerFirstName") ?? "") \(self.detailDict?.value(forKey: "ownerLastName") ?? "")"
            self.navigationController?.pushViewController(rentHistoryVC, animated: true)
        }
    }
    
    @objc func chatButtonClicked(sender : UIButton)
    {
        print("chatButtonClicked")
        
        UIView.animate(withDuration: 0.7) {
            self.lblRentHistory.frame.size.width = 0
            self.viewRentHistory.backgroundColor = Constants.darkGrayColor
            self.viewRentHistory.frame = CGRect(x: 15, y: self.viewRentHistory.frame.origin.y, width: 80, height: 60)
            self.imgViewRentHistory.frame = CGRect(x: 27.5, y: 0, width: 25, height: 60)
            self.imgViewRentHistory.image = UIImage(named: "rentHistoryGray")
            self.btnRentHistory.frame = CGRect(x: 0.0, y: 0.0, width: self.viewRentHistory.frame.width, height: self.viewRentHistory.frame.height)
            
            
            self.viewChat.backgroundColor = .white//Constants.darkBlueColor
            self.viewChat.frame = CGRect(x: 15 + 80 + 15, y: self.viewChat.frame.origin.y, width: self.viewNotifications.frame.origin.x - (15 + 15 + 15 + 80), height: 60)
            self.lblChat.frame = CGRect(x: self.imgViewChat.frame.origin.x + self.imgViewChat.frame.size.width + 5, y: 7, width: 100, height: 50.0)
            self.lblChat.textColor = Constants.darkBlueColor
            self.btnChat.frame = CGRect(x: 0.0, y: 0.0, width: self.viewChat.frame.width, height: self.viewChat.frame.height)
            self.imgViewChat.image = UIImage(named: "chatBlue")
            
        } completion: { (_) in
            let chatListVC = ChatListViewController(nibName: "ChatListViewController", bundle: nil)
            chatListVC.peopleInvolvedArray = self.peopleInvolvedArray
            chatListVC.propertyPic = "\(self.imageNames[0])"
            self.navigationController?.pushViewController(chatListVC, animated: true)
        }

    }
    
    @objc func notificationButtonClicked(sender : UIButton)
    {
        print("notificationButtonClicked")
        
        let activityVC = ActivityViewController(nibName: "ActivityViewController", bundle: nil)
        activityVC.propertyId = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        activityVC.contactType = "\(self.detailDict?.value(forKey: "contact_type") ?? "")"
        self.navigationController?.pushViewController(activityVC, animated: true)
    }
    
    @IBAction func propertyInfoArrowButtonClicked(_ sender: UIButton)
    {
        print("propertyInfoArrowButtonClicked")
        
        let editPropertyDetailVC = EditPropertyDetailViewController(nibName: "EditPropertyDetailViewController", bundle: nil)
        editPropertyDetailVC.detailDict = self.detailDict
        self.navigationController?.pushViewController(editPropertyDetailVC, animated: true)
        
//        if self.propertyInfoArrowButton.isSelected
//        {
//            self.propertyInfoArrowButton.isSelected = false
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.propertyInfoArrowButton.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
//                    self.infoHeightConstraint.constant = 75
//                    self.propertyInfoSubView.isHidden = true
//
////                    self.view.setNeedsLayout()
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
//        else
//        {
//            self.propertyInfoArrowButton.isSelected = true
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.propertyInfoArrowButton.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
//                    self.infoHeightConstraint.constant = 350
//                    self.propertyInfoSubView.isHidden = false
//
////                    self.view.setNeedsLayout()
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
    }
    
    @IBAction func leaseButtonClicked(_ sender: UIButton)
    {
        print("leaseButtonClicked")
        let editLeaseAgreementVc = EditLeaseAgreementViewController(nibName: "EditLeaseAgreementViewController", bundle: nil)
        editLeaseAgreementVc.leaseDict = self.leaseDict
        self.navigationController?.pushViewController(editLeaseAgreementVc, animated: true)
        
        
//        if self.btnLeaseArrow.isSelected
//        {
//            self.btnLeaseArrow.isSelected = false
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.btnLeaseArrow.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
//                    self.leaseViewHeightConstraint.constant = 75
//                    self.leaseSubView.isHidden = true
//
////                    self.view.setNeedsLayout()
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
//        else
//        {
//            self.btnLeaseArrow.isSelected = true
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.btnLeaseArrow.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
//                    self.leaseViewHeightConstraint.constant = 330
//                    self.leaseSubView.isHidden = false
//
////                    self.view.setNeedsLayout()
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
    }
    
    @IBAction func infoDocArrowButtonClicked(_ sender: UIButton)
    {
        if self.infoDocArrowButton.isSelected
        {
            self.infoDocArrowButton.isSelected = false
            
//            UIView.animate(withDuration: 0.7) {
                DispatchQueue.main.async {
                    self.infoDocArrowButton.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
                    self.infoDocHeightConstraint.constant = 75
                    self.infoDocSubView.isHidden = true
                    
//                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }
                
//            } completion: { (_) in
//
//            }
        }
        else
        {
            self.infoDocArrowButton.isSelected = true
            
//            UIView.animate(withDuration: 0.7) {
                DispatchQueue.main.async {
                    self.infoDocArrowButton.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
                    self.infoDocHeightConstraint.constant = 250
                    self.infoDocSubView.isHidden = false
                    
//                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }
                
//            } completion: { (_) in
//
//            }
        }
    }
    
    @IBAction func ownerButtonClicked(_ sender: UIButton)
    {
//        if self.ownerArrowButton.isSelected
//        {
//            self.ownerArrowButton.isSelected = false
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.ownerArrowButton.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
//                    self.ownerHeightConstraint.constant = 80
//                    self.ownerSubView.isHidden = true
//
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
//        else
//        {
//            self.ownerArrowButton.isSelected = true
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.ownerArrowButton.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
//                    self.ownerHeightConstraint.constant = 280
//                    self.ownerSubView.isHidden = false
//
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
    }
    
    @IBAction func tenantButtonClicked(_ sender: UIButton)
    {
//        if self.tenantArrowButton.isSelected
//        {
//            self.tenantArrowButton.isSelected = false
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.tenantArrowButton.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
//                    self.tenantHeightConstraint.constant = 80
//                    self.tenantSubView.isHidden = true
//
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
//        else
//        {
//            self.tenantArrowButton.isSelected = true
//
////            UIView.animate(withDuration: 0.7) {
//                DispatchQueue.main.async {
//                    self.tenantArrowButton.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
//                    self.tenantHeightConstraint.constant = 280
//                    self.tenantSubView.isHidden = false
//
//                    self.view.layoutIfNeeded()
//                }
//
////            } completion: { (_) in
////
////            }
//        }
    }
    
    @IBAction func brokerButtonClicked(_ sender: UIButton)
    {
        if self.brokerArrowButton.isSelected
        {
            self.brokerArrowButton.isSelected = false
            
//            UIView.animate(withDuration: 0.7) {
                DispatchQueue.main.async {
                    self.brokerArrowButton.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
                    self.brokerHeightConstraint.constant = 80
                    self.brokerSubView.isHidden = true
                    
                    self.view.layoutIfNeeded()
                }
                
//            } completion: { (_) in
//
//            }
        }
        else
        {
            self.brokerArrowButton.isSelected = true
            
//            UIView.animate(withDuration: 0.7) {
                DispatchQueue.main.async {
                    self.brokerArrowButton.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
                    self.brokerHeightConstraint.constant = 280
                    self.brokerSubView.isHidden = false
                    
                    self.view.layoutIfNeeded()
                }
                
//            } completion: { (_) in
//
//            }
        }
    }
    
    @IBAction func propertyManagerButtonClicked(_ sender: UIButton)
    {
        if self.propertyManagerArrowButton.isSelected
        {
            self.propertyManagerArrowButton.isSelected = false
            
//            UIView.animate(withDuration: 0.7) {
                DispatchQueue.main.async {
                    self.propertyManagerArrowButton.setBackgroundImage(UIImage(named: "rightIcon"), for: .normal)
                    self.propertyManagerHeightConstraint.constant = 80
                    self.propertyManagerSubView.isHidden = true
                    
                    self.view.layoutIfNeeded()
                }
                
//            } completion: { (_) in
//
//            }
        }
        else
        {
            self.propertyManagerArrowButton.isSelected = true
            
//            UIView.animate(withDuration: 0.7) {
                DispatchQueue.main.async {
                    self.propertyManagerArrowButton.setBackgroundImage(UIImage(named: "downArrow"), for: .normal)
                    self.propertyManagerHeightConstraint.constant = 280
                    self.propertyManagerSubView.isHidden = false
                    
                    self.view.layoutIfNeeded()
                }
                
//            } completion: { (_) in
//
//            }
        }
    }
    
    @IBAction func ownerVerifyButtonClicked(_ sender: UIButton)
    {
        print("ownerVerifyButtonClicked")
       
        if self.ownerEmailLabel.text!.isEmpty != true
        {
            // Sign in using the verificationID and the code sent to the user
            let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
            verifyVC.otpVerificationDelegate = self
            verifyVC.verificationIdStr = self.verificationCode
            verifyVC.phoneNumberStr = (self.ownerPhoneLabel.text ?? "")
            verifyVC.propertyName = "\(detailDict?.value(forKey: "propertyName") ?? "")"
            verifyVC.contactId = "\(self.owner_ContactId)"
            verifyVC.contactType = "\(detailDict?.value(forKey: "contact_type") ?? "")"
            verifyVC.modalPresentationStyle = .overCurrentContext
            verifyVC.modalTransitionStyle = .flipHorizontal
            self.present(verifyVC, animated: true, completion: nil)
            
//            PhoneAuthProvider.provider().verifyPhoneNumber("\((self.tenantPhoneNumber.count == 10) ? "+\(AppUtility.getUserData(keyVal: "countryCode"))\(self.tenantPhoneNumber)" : "+\(self.tenantPhoneNumber)")", uiDelegate: nil) { (verificationID, error) in
//
//                if let error = error
//                {
//                    print(error.localizedDescription)
//                    return
//                }
//
//                self.verificationCode = "\(verificationID ?? "")"
//
//                // Sign in using the verificationID and the code sent to the user
//                let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
//                verifyVC.verificationIdStr = self.verificationCode
//                verifyVC.phoneNumberStr = self.tenantEmailLabel.text
//                verifyVC.modalPresentationStyle = .overCurrentContext
//                verifyVC.modalTransitionStyle = .flipHorizontal
//                self.present(verifyVC, animated: true, completion: nil)
//            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Phone Number Not Available", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        
//        if self.ownerPhoneNumber.isEmpty != true
//        {
//            PhoneAuthProvider.provider().verifyPhoneNumber("\((self.ownerPhoneNumber.count == 10) ? "+\(AppUtility.getUserData(keyVal: "countryCode"))\(self.ownerPhoneNumber)" : "+\(self.ownerPhoneNumber)")", uiDelegate: nil) { (verificationID, error) in
//
//                if let error = error
//                {
//                    print(error.localizedDescription)
//                    return
//                }
//
//                self.verificationCode = "\(verificationID ?? "")"
//
//                // Sign in using the verificationID and the code sent to the user
//                let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
//                verifyVC.verificationIdStr = self.verificationCode
//                verifyVC.phoneNumberStr = self.ownerPhoneNumber
//                verifyVC.modalPresentationStyle = .overCurrentContext
//                verifyVC.modalTransitionStyle = .flipHorizontal
//                self.present(verifyVC, animated: true, completion: nil)
//            }
//        }
//        else
//        {
//            let alertViewController = UIAlertController(title: "Error", message: "Phone Number Not Available", preferredStyle: .alert)
//
//            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//            }))
//
//            DispatchQueue.main.async {
//                self .present(alertViewController, animated: true, completion: nil)
//            }
//        }
    }
    
    @IBAction func brokerVerifyButtonClicked(_ sender: UIButton)
    {
        print("brokerVerifyButtonClicked")
        
        if self.brokerEmailLabel.text!.isEmpty != true
        {
            // Sign in using the verificationID and the code sent to the user
            let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
            verifyVC.otpVerificationDelegate = self
            verifyVC.verificationIdStr = self.verificationCode
            verifyVC.phoneNumberStr = (self.brokerEmailLabel.text ?? "")
            verifyVC.propertyName = "\(detailDict?.value(forKey: "propertyName") ?? "")"
            verifyVC.contactId = "\(self.broker_ContactId)"
            verifyVC.contactType = "\(detailDict?.value(forKey: "contact_type") ?? "")"
            verifyVC.modalPresentationStyle = .overCurrentContext
            verifyVC.modalTransitionStyle = .flipHorizontal
            self.present(verifyVC, animated: true, completion: nil)
            
//            PhoneAuthProvider.provider().verifyPhoneNumber("\((self.tenantPhoneNumber.count == 10) ? "+\(AppUtility.getUserData(keyVal: "countryCode"))\(self.tenantPhoneNumber)" : "+\(self.tenantPhoneNumber)")", uiDelegate: nil) { (verificationID, error) in
//
//                if let error = error
//                {
//                    print(error.localizedDescription)
//                    return
//                }
//
//                self.verificationCode = "\(verificationID ?? "")"
//
//                // Sign in using the verificationID and the code sent to the user
//                let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
//                verifyVC.verificationIdStr = self.verificationCode
//                verifyVC.phoneNumberStr = self.tenantEmailLabel.text
//                verifyVC.modalPresentationStyle = .overCurrentContext
//                verifyVC.modalTransitionStyle = .flipHorizontal
//                self.present(verifyVC, animated: true, completion: nil)
//            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Phone Number Not Available", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
       
//        if self.brokerPhoneNumber.isEmpty != true
//        {
//            PhoneAuthProvider.provider().verifyPhoneNumber("\((self.brokerPhoneNumber.count == 10) ? "+\(AppUtility.getUserData(keyVal: "countryCode"))\(self.brokerPhoneNumber)" : "+\(self.brokerPhoneNumber)")", uiDelegate: nil) { (verificationID, error) in
//
//                if let error = error
//                {
//                    print(error.localizedDescription)
//                    return
//                }
//
//                self.verificationCode = "\(verificationID ?? "")"
//
//                // Sign in using the verificationID and the code sent to the user
//                let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
//                verifyVC.verificationIdStr = self.verificationCode
//                verifyVC.phoneNumberStr = self.brokerPhoneNumber
//                verifyVC.modalPresentationStyle = .overCurrentContext
//                verifyVC.modalTransitionStyle = .flipHorizontal
//                self.present(verifyVC, animated: true, completion: nil)
//            }
//        }
//        else
//        {
//            let alertViewController = UIAlertController(title: "Error", message: "Phone Number Not Available", preferredStyle: .alert)
//
//            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//            }))
//
//            DispatchQueue.main.async {
//                self .present(alertViewController, animated: true, completion: nil)
//            }
//        }
    }
    
    @IBAction func tenantVerifyButtonClicked(_ sender: UIButton)
    {
        print("tenantVerifyButtonClicked")
       
        if self.tenantEmailLabel.text!.isEmpty != true
        {
            // Sign in using the verificationID and the code sent to the user
            let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
            verifyVC.otpVerificationDelegate = self
            verifyVC.verificationIdStr = self.verificationCode
            verifyVC.phoneNumberStr = (self.tenantPhoneLabel.text ?? "")
            verifyVC.propertyName = "\(detailDict?.value(forKey: "propertyName") ?? "")"
            verifyVC.contactId = "\(self.tenant_ContactId)"
            verifyVC.contactType = "\(detailDict?.value(forKey: "contact_type") ?? "")"
            verifyVC.modalPresentationStyle = .overCurrentContext
            verifyVC.modalTransitionStyle = .flipHorizontal
            self.present(verifyVC, animated: true, completion: nil)
            
//            PhoneAuthProvider.provider().verifyPhoneNumber("\((self.tenantPhoneNumber.count == 10) ? "+\(AppUtility.getUserData(keyVal: "countryCode"))\(self.tenantPhoneNumber)" : "+\(self.tenantPhoneNumber)")", uiDelegate: nil) { (verificationID, error) in
//
//                if let error = error
//                {
//                    print(error.localizedDescription)
//                    return
//                }
//
//                self.verificationCode = "\(verificationID ?? "")"
//
//                // Sign in using the verificationID and the code sent to the user
//                let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
//                verifyVC.verificationIdStr = self.verificationCode
//                verifyVC.phoneNumberStr = self.tenantEmailLabel.text
//                verifyVC.modalPresentationStyle = .overCurrentContext
//                verifyVC.modalTransitionStyle = .flipHorizontal
//                self.present(verifyVC, animated: true, completion: nil)
//            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Phone Number Not Available", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func propertyManagerVerifyButtonClicked(_ sender: UIButton)
    {
        if self.propertyManagerEmailLabel.text!.isEmpty != true
        {
            // Sign in using the verificationID and the code sent to the user
            let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
            verifyVC.otpVerificationDelegate = self
            verifyVC.verificationIdStr = self.verificationCode
            verifyVC.phoneNumberStr = (self.propertyManagerEmailLabel.text ?? "")
            verifyVC.propertyName = "\(detailDict?.value(forKey: "propertyName") ?? "")"
            verifyVC.contactId = "\(self.propertyManager_ContactId)"
            verifyVC.contactType = "\(detailDict?.value(forKey: "contact_type") ?? "")"
            verifyVC.modalPresentationStyle = .overCurrentContext
            verifyVC.modalTransitionStyle = .flipHorizontal
            self.present(verifyVC, animated: true, completion: nil)
            
//            PhoneAuthProvider.provider().verifyPhoneNumber("\((self.tenantPhoneNumber.count == 10) ? "+\(AppUtility.getUserData(keyVal: "countryCode"))\(self.tenantPhoneNumber)" : "+\(self.tenantPhoneNumber)")", uiDelegate: nil) { (verificationID, error) in
//
//                if let error = error
//                {
//                    print(error.localizedDescription)
//                    return
//                }
//
//                self.verificationCode = "\(verificationID ?? "")"
//
//                // Sign in using the verificationID and the code sent to the user
//                let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
//                verifyVC.verificationIdStr = self.verificationCode
//                verifyVC.phoneNumberStr = self.tenantEmailLabel.text
//                verifyVC.modalPresentationStyle = .overCurrentContext
//                verifyVC.modalTransitionStyle = .flipHorizontal
//                self.present(verifyVC, animated: true, completion: nil)
//            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Phone Number Not Available", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func addTenantButtonClicked(_ sender: UIButton)
    {
        print("addTenantButtonClicked")
        
        let addTenantVC = AddTenantViewController(nibName: "AddTenantViewController", bundle: nil)
        
        if sender.tag == 1
        {
            addTenantVC.contactTypeStr = "Owner"
        }
        else
        {
            addTenantVC.contactTypeStr = "Tenant"
        }
        
        addTenantVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        addTenantVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
        self.navigationController?.pushViewController(addTenantVC, animated: true)
    }
    
    @IBAction func addBrokerButtonClicked(_ sender: UIButton)
    {
        print("addBrokerButtonClicked")
        
        let addBrokerVC = AddBrokerViewController(nibName: "AddBrokerViewController", bundle: nil)
        addBrokerVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        addBrokerVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
        self.navigationController?.pushViewController(addBrokerVC, animated: true)
    }
    
    @IBAction func addPropertyManagerButtonClicked(_ sender: UIButton)
    {
        print("addPropertyManagerButtonClicked")
        
        let addPropertyVC = AddPropertyManagerViewController(nibName: "AddPropertyManagerViewController", bundle: nil)
        addPropertyVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        addPropertyVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
        self.navigationController?.pushViewController(addPropertyVC, animated: true)
    }
    
    @IBAction func leaseDocButtonClicked(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            // Registration Doc
            if "\(detailDict?.value(forKey: "reg_doc") ?? "")".isEmpty != true
            {
                let showPhotoVC = ShowPhotoViewController(nibName: "ShowPhotoViewController", bundle: nil)
                showPhotoVC.photoURL = "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "reg_doc") ?? "")")"
                showPhotoVC.modalPresentationStyle = .overCurrentContext
                showPhotoVC.modalTransitionStyle = .flipHorizontal
                self.present(showPhotoVC, animated: true, completion: nil)
            }
        }
        else if sender.tag == 2
        {
            // Electricity Bill
            if "\(detailDict?.value(forKey: "electricity_bill") ?? "")".isEmpty != true
            {
                let showPhotoVC = ShowPhotoViewController(nibName: "ShowPhotoViewController", bundle: nil)
                showPhotoVC.photoURL = "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "electricity_bill") ?? "")")"
                showPhotoVC.modalPresentationStyle = .overCurrentContext
                showPhotoVC.modalTransitionStyle = .flipHorizontal
                self.present(showPhotoVC, animated: true, completion: nil)
            }
        }
        else if sender.tag == 3
        {
            // Owner ID Proof
            if "\(detailDict?.value(forKey: "owner_id") ?? "")".isEmpty != true
            {
                let showPhotoVC = ShowPhotoViewController(nibName: "ShowPhotoViewController", bundle: nil)
                showPhotoVC.photoURL = "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "owner_id") ?? "")")"
                showPhotoVC.modalPresentationStyle = .overCurrentContext
                showPhotoVC.modalTransitionStyle = .flipHorizontal
                self.present(showPhotoVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func documentAttachedButtonClicked(sender : UIButton)
    {
        if sender.tag == 1
        {
            // Registration Doc
            if "\(detailDict?.value(forKey: "reg_doc") ?? "")".isEmpty != true
            {
                let showPhotoVC = ShowPhotoViewController(nibName: "ShowPhotoViewController", bundle: nil)
                showPhotoVC.photoURL = "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "reg_doc") ?? "")")"
                showPhotoVC.modalPresentationStyle = .overCurrentContext
                showPhotoVC.modalTransitionStyle = .flipHorizontal
                self.present(showPhotoVC, animated: true, completion: nil)
            }
        }
        else if sender.tag == 2
        {
            // Electricity Bill
            if "\(detailDict?.value(forKey: "electricity_bill") ?? "")".isEmpty != true
            {
                let showPhotoVC = ShowPhotoViewController(nibName: "ShowPhotoViewController", bundle: nil)
                showPhotoVC.photoURL = "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "electricity_bill") ?? "")")"
                showPhotoVC.modalPresentationStyle = .overCurrentContext
                showPhotoVC.modalTransitionStyle = .flipHorizontal
                self.present(showPhotoVC, animated: true, completion: nil)
            }
        }
        else if sender.tag == 3
        {
            // Owner ID Proof
            if "\(detailDict?.value(forKey: "owner_id") ?? "")".isEmpty != true
            {
                let showPhotoVC = ShowPhotoViewController(nibName: "ShowPhotoViewController", bundle: nil)
                showPhotoVC.photoURL = "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "owner_id") ?? "")")"
                showPhotoVC.modalPresentationStyle = .overCurrentContext
                showPhotoVC.modalTransitionStyle = .flipHorizontal
                self.present(showPhotoVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func showLocationButtonClicked(_ sender: UIButton)
    {
        let showLocaitonVC = ShowLocationViewController(nibName: "ShowLocationViewController", bundle: nil)
        showLocaitonVC.propertyDict = self.detailDict
        self.navigationController?.pushViewController(showLocaitonVC, animated: true)
    }
    
    @IBAction func ownerPhoneButtonClicked(_ sender: UIButton)
    {
        print("ownerPhoneButtonClicked")
        
        if self.ownerPhoneNumber.isEmpty != true
        {
            let myUrl = "tel://\(self.ownerPhoneNumber)"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Owner's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func ownerMailButtonClicked(_ sender: UIButton)
    {
        print("ownerMailButtonClicked")
        
        if self.ownerEmailLabel.text?.isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\(self.ownerEmailLabel.text ?? "")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Owner's EmailId Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func ownerMsgButtonClicked(_ sender: UIButton)
    {
        print("ownerMsgButtonClicked")
        
        if self.ownerPhoneNumber.isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\(self.ownerPhoneNumber)"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Owner's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
//    @IBAction func ownerViewButtonClicked(_ sender: UIButton)
//    {
//        print("ownerViewButtonClicked")
//    }
//
    @IBAction func ownerEditButtonClicked(_ sender: UIButton)
    {
        print("ownerEditButtonClicked")
    }
    
    @IBAction func ownerDeleteButtonClicked(_ sender: UIButton)
    {
        print("ownerDeleteButtonClicked")
    }
    
    @IBAction func tenantPhoneButtonClicked(_ sender: UIButton)
    {
        print("tenantPhoneButtonClicked")
        
        if self.tenantPhoneNumber.isEmpty != true
        {
            let myUrl = "tel://\(self.tenantPhoneNumber)"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Tenant's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func tenantMailButtonClicked(_ sender: UIButton)
    {
        print("tenantMailButtonClicked")
        
        if self.tenantEmailLabel.text?.isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\(self.tenantEmailLabel.text ?? "")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Tenant's EmailId Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func tenantMsgButtonClicked(_ sender: UIButton)
    {
        print("tenantMsgButtonClicked")
        
        if self.tenantPhoneNumber.isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\(self.tenantPhoneNumber)"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Tenant's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func tenantViewButtonClicked(_ sender: UIButton)
    {
        print("tenantViewButtonClicked")
    }
    
    @IBAction func tenantEditButtonClicked(_ sender: UIButton)
    {
        print("tenantEditButtonClicked")
        
        let editTenantVC = AddTenantViewController(nibName: "AddTenantViewController", bundle: nil)
        editTenantVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        editTenantVC.contactTypeStr = "Tenant"
        editTenantVC.detailDict = self.tenantDict
        editTenantVC.isEditTenant = true
        self.navigationController?.pushViewController(editTenantVC, animated: true)
    }
    
    @IBAction func tenantDeleteButtonClicked(_ sender: UIButton)
    {
        print("tenantDeleteButtonClicked")
    }
    
    @IBAction func brokerPhoneButtonClicked(_ sender: UIButton)
    {
        print("brokerPhoneButtonClicked")
        
        if self.brokerPhoneNumber.isEmpty != true
        {
            let myUrl = "tel://\(self.brokerPhoneNumber)"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func brokerMailButtonClicked(_ sender: UIButton)
    {
        print("brokerMailButtonClicked")
        
        if self.brokerEmailLabel.text?.isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\(self.brokerEmailLabel.text ?? "")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's EmailId Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func brokerMsgButtonClicked(_ sender: UIButton)
    {
        print("brokerMsgButtonClicked")
        
        if self.brokerPhoneNumber.isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\(self.brokerPhoneNumber)"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func brokerViewButtonClicked(_ sender: UIButton)
    {
        print("brokerViewButtonClicked")
    }
    
    @IBAction func brokerEditButtonClicked(_ sender: UIButton)
    {
        print("brokerEditButtonClicked")
        
        let editBrokerVC = AddBrokerViewController(nibName: "AddBrokerViewController", bundle: nil)
        editBrokerVC.detailDict = self.brokerDict
        editBrokerVC.isEditBroker = true
        editBrokerVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        editBrokerVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
        self.navigationController?.pushViewController(editBrokerVC, animated: true)
    }
    
    @IBAction func brokerDeleteButtonClicked(_ sender: UIButton)
    {
        print("brokerDeleteButtonClicked")
    }
    
    @IBAction func pmPhoneButtonClicked(_ sender: UIButton)
    {
        print("pmPhoneButtonClicked")
        
        if self.self.propertyManagerPhoneNumber.isEmpty != true
        {
            let myUrl = "tel://\(self.self.propertyManagerPhoneNumber)"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Property Manager's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func pmMailButtonClicked(_ sender: UIButton)
    {
        print("pmMailButtonClicked")
        
        if self.propertyManagerEmailLabel.text?.isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\(self.propertyManagerEmailLabel.text ?? "")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Property Manager's EmailId Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func pmMsgButtonClicked(_ sender: UIButton)
    {
        print("pmMsgButtonClicked")
        
        if self.propertyManagerPhoneNumber.isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\(self.propertyManagerPhoneNumber)"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Property Manager's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func pmViewButtonClicked(_ sender: UIButton)
    {
        print("pmViewButtonClicked")
    }
    
    @IBAction func pmEditButtonClicked(_ sender: UIButton)
    {
        print("pmEditButtonClicked")
        
        let addPropertyVC = AddPropertyManagerViewController(nibName: "AddPropertyManagerViewController", bundle: nil)
        addPropertyVC.isEditPM = true
        addPropertyVC.detailDict = self.propertyManagerDict
        addPropertyVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
        addPropertyVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
        self.navigationController?.pushViewController(addPropertyVC, animated: true)
    }
    
    @IBAction func pmDeleteButtonClicked(_ sender: UIButton)
    {
        print("pmDeleteButtonClicked")
    }
    
    @IBAction func ownerDetailButtonClicked(_ sender: UIButton)
    {
        let userInfoVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
        userInfoVC.detailDict = self.ownerDict
        userInfoVC.isHideCall = self.ownerPhoneButton.isHidden
        userInfoVC.isHideMsg = self.ownerMsgButton.isHidden
        userInfoVC.isHideMail = self.ownerMailButton.isHidden
        self.navigationController?.pushViewController(userInfoVC, animated: true)
    }
    
    @IBAction func tenantDetailButtonClicked(_ sender: UIButton)
    {
        let userInfoVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
        userInfoVC.detailDict = self.tenantDict
        userInfoVC.isHideCall = self.tenantPhoneButton.isHidden
        userInfoVC.isHideMsg = self.tenantMsgButton.isHidden
        userInfoVC.isHideMail = self.tenantMailButton.isHidden
        self.navigationController?.pushViewController(userInfoVC, animated: true)
    }
    
    @IBAction func brokerDetailButtonClicked(_ sender: UIButton)
    {
        let userInfoVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
        userInfoVC.detailDict = self.brokerDict
        userInfoVC.isHideCall = self.brokerPhoneButton.isHidden
        userInfoVC.isHideMsg = self.brokerMsgButton.isHidden
        userInfoVC.isHideMail = self.brokerMailButton.isHidden
        self.navigationController?.pushViewController(userInfoVC, animated: true)
    }
    
    @IBAction func pmDetailButtonClicked(_ sender: UIButton)
    {
        let userInfoVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
        userInfoVC.detailDict = self.propertyManagerDict
        userInfoVC.isHideCall = self.pmPhoneButton.isHidden
        userInfoVC.isHideMsg = self.pmMsgButton.isHidden
        userInfoVC.isHideMail = self.pmMailButton.isHidden
        self.navigationController?.pushViewController(userInfoVC, animated: true)
    }
    
    @objc func addMorePeopleButtonClicked(sender : UIButton)
    {
        if sender.tag == 1
        {
            print("Add Owner")
            self.addTenantButtonClicked(sender)
        }
        else if sender.tag == 2
        {
            print("Add Tenant")
            
            self.addTenantButtonClicked(sender)
        }
        else if sender.tag == 3
        {
            print("Add Broker")
            self.addBrokerButtonClicked(sender)
        }
        else if sender.tag == 4
        {
            print("Add Property Manager")
            self.addPropertyManagerButtonClicked(sender)
        }
    }
    
    @objc func verifyViaOTPButtonClicked(sender : UIButton)
    {
        print("verifyViaOTPButtonClicked : Tag - \(sender.tag)")
        
        // Sign in using the verificationID and the code sent to the user
        let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
        verifyVC.otpVerificationDelegate = self
        verifyVC.verificationIdStr = self.verificationCode
        verifyVC.phoneNumberStr = "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")"
        verifyVC.propertyName = "\(detailDict?.value(forKey: "propertyName") ?? "")"
        verifyVC.contactId = "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_id") ?? "")"
        verifyVC.contactType = "\(detailDict?.value(forKey: "contact_type") ?? "")"
        verifyVC.modalPresentationStyle = .overCurrentContext
        verifyVC.modalTransitionStyle = .flipHorizontal
        self.present(verifyVC, animated: true, completion: nil)
    }
    
    @objc func callButtonClicked(sender : UIButton)
    {
        print("callButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")".isEmpty != true
        {
            let myUrl = "tel://\("\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")")"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Mobile Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func emailButtonClicked(sender : UIButton)
    {
        print("emailButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "email") ?? "")".isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\("\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "email") ?? "")")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Email Id Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func msgButtonClicked(sender : UIButton)
    {
        print("msgButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")".isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\("\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")")"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func editButtonClicked(sender : UIButton)
    {
        print("editButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
        {
            let editTenantVC = AddTenantViewController(nibName: "AddTenantViewController", bundle: nil)
            editTenantVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            editTenantVC.contactTypeStr = "Owner"
            editTenantVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            editTenantVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            editTenantVC.isEditTenant = true
            self.navigationController?.pushViewController(editTenantVC, animated: true)
        }
        else if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Broker"
        {
            let editBrokerVC = AddBrokerViewController(nibName: "AddBrokerViewController", bundle: nil)
            editBrokerVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            editBrokerVC.isEditBroker = true
            editBrokerVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            editBrokerVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            self.navigationController?.pushViewController(editBrokerVC, animated: true)
        }
        else if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
        {
            let editTenantVC = AddTenantViewController(nibName: "AddTenantViewController", bundle: nil)
            editTenantVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            editTenantVC.contactTypeStr = "Tenant"
            editTenantVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            editTenantVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            editTenantVC.isEditTenant = true
            self.navigationController?.pushViewController(editTenantVC, animated: true)
        }
        else if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Property Manager"
        {
            let addPropertyVC = AddPropertyManagerViewController(nibName: "AddPropertyManagerViewController", bundle: nil)
            addPropertyVC.isEditPM = true
            addPropertyVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            addPropertyVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            addPropertyVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            self.navigationController?.pushViewController(addPropertyVC, animated: true)
        }
        
        
    }
    
    @objc func deleteButtonClicked(sender : UIButton)
    {
        print("deleteButtonClicked")
        
        let alertViewController = UIAlertController(title: "Delete", message: "Do You Want To Delete \((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "firstName") ?? "")", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            
            print("Delete  -- Yes")
            
            self.removePeopleInvolved(mobileno: "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")", contact_id: "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_id") ?? "")", contact_type: "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")")
        }))
        
        alertViewController.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (alert: UIAlertAction!) in
            print("Delete  -- No")
        }))
        
        DispatchQueue.main.async {
            self .present(alertViewController, animated: true, completion: nil)
        }
    }
    
    @objc func detailButtonClicked(sender : UIButton)
    {
        print("detailButtonClicked")
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewObj)
        let indexPath = self.tableViewObj!.indexPathForRow(at: buttonPosition)
        
        if indexPath != nil
        {
            let myCell = (self.tableViewObj!.cellForRow(at: indexPath!) as? PeopleInvolvedTableViewCell)
            
            let userInfoVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
            userInfoVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            userInfoVC.isHideCall = (myCell?.callButton.isHidden ?? false)
            userInfoVC.isHideMsg = (myCell?.msgButton.isHidden ?? false)
            userInfoVC.isHideMail = (myCell?.emailButton.isHidden ?? false)
            self.navigationController?.pushViewController(userInfoVC, animated: true)
        }
        
    }
    
    @IBAction func propertyActionButtonClicked(_ sender: UIButton)
    {
        let propertyActionVC = PropertyDetailActionsViewController(nibName: "PropertyDetailActionsViewController", bundle: nil)
        propertyActionVC.leaseDict = self.leaseDict
        propertyActionVC.detailDict = self.detailDict
        propertyActionVC.peopleInvolvedArray = self.peopleInvolvedArray
        propertyActionVC.ownerIsVerified = self.ownerIsVerified
        self.navigationController?.pushViewController(propertyActionVC, animated: true)
    }

    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.peopleInvolvedArray != nil
        {
            if self.peopleInvolvedArray!.count > 0
            {
                return self.peopleInvolvedArray!.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "mobileno") ?? "")" == "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        {
            self.view.setNeedsLayout()
            return 130
        }
        else if self.ownerIsVerified == "1" && "\(self.detailDict?.value(forKey: "ownerNumber") ?? "")" == "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        {
            self.view.setNeedsLayout()
            return 180
        }
        else if self.ownerIsVerified == "0" && (("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Owner" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager"))
        {
            self.view.setNeedsLayout()
            return 180
        }
        else
        {
            self.view.setNeedsLayout()
            return 130
        }
        
//        return 180
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "peopleInvolvedCell", for: indexPath) as! PeopleInvolvedTableViewCell
        
        DispatchQueue.main.async {
            myCell.selectionStyle = .none
            myCell.backgroundColor = Constants.bgColor
            myCell.mainView.backgroundColor = .white
            myCell.mainView.layer.cornerRadius = 15
            
            myCell.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
            myCell.profileImageView.contentMode = .scaleAspectFill
            myCell.profileImageView.clipsToBounds = true
            myCell.profileImageView.layer.cornerRadius = 8
            myCell.profileImageView.layer.masksToBounds = true
            
            myCell.contactTypeLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")"
            
            myCell.nameLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "firstName") ?? "") \((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "lastName") ?? "")"
            
            myCell.mobileLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "mobileno") ?? "")"
            
            myCell.emailLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "email") ?? "")"
            
            if "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
            {
                myCell.verifyButton.isHidden = true
                myCell.verifiedImageView.isHidden = false
                myCell.verifiedLabel.isHidden = false
            }
            else
            {
                myCell.verifyButton.isHidden = false
                myCell.verifiedImageView.isHidden = true
                myCell.verifiedLabel.isHidden = true
            }
            
            myCell.verifyButton.tag = indexPath.row
            myCell.verifyButton.addTarget(self, action: #selector(self.verifyViaOTPButtonClicked(sender:)), for: .touchUpInside)
        }
        
        if "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "mobileno") ?? "")" == "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        {
            myCell.callButton.isHidden = true
            myCell.msgButton.isHidden = true
            myCell.emailButton.isHidden = true
            myCell.editButton.isHidden = true
            myCell.deleteButton.isHidden = true
        }
        else if self.ownerIsVerified == "1" && "\(self.detailDict?.value(forKey: "ownerNumber") ?? "")" == "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        {
            myCell.callButton.isHidden = false
            myCell.msgButton.isHidden = false
            myCell.emailButton.isHidden = false
            myCell.editButton.isHidden = false
            myCell.deleteButton.isHidden = false
        }
        else if self.ownerIsVerified == "0" && (("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Owner" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager"))
        {
            myCell.callButton.isHidden = false
            myCell.msgButton.isHidden = false
            myCell.emailButton.isHidden = false
            myCell.editButton.isHidden = false
            myCell.deleteButton.isHidden = false
        }
        else
        {
            myCell.callButton.isHidden = false
            myCell.msgButton.isHidden = false
            myCell.emailButton.isHidden = false
            myCell.editButton.isHidden = true
            myCell.deleteButton.isHidden = true
        }
        
        myCell.callButton.tag = indexPath.row
        myCell.callButton.addTarget(self, action: #selector(callButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.msgButton.tag = indexPath.row
        myCell.msgButton.addTarget(self, action: #selector(msgButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.emailButton.tag = indexPath.row
        myCell.emailButton.addTarget(self, action: #selector(emailButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.editButton.tag = indexPath.row
        myCell.editButton.addTarget(self, action: #selector(editButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.deleteButton.tag = indexPath.row
        myCell.deleteButton.addTarget(self, action: #selector(deleteButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.detailButton.tag = indexPath.row
        myCell.detailButton.addTarget(self, action: #selector(detailButtonClicked(sender:)), for: .touchUpInside)
        
        return myCell
    }
    
    //MARK: - Message Delegate
    func createEmailUrl(to: String, subject: String, body: String) -> URL?
    {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
//        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")

        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")

    
        if let gmailUrl = defaultUrl, UIApplication.shared.canOpenURL(defaultUrl!)
        {
            return gmailUrl
        }
        
        return URL(string: "")
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - OTP Verification Delegate
    func otpVerification()
    {
        self.refreshPropertyDetailResponse()
    }
    
    
    // MARK: - FSPagerViewDataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int
    {
        return imageNames.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell
    {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.imageNames[index])")! as URL), placeholderImage: UIImage(named: "signupHomeLogo"))
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    //MARK: - Property Detail Info
    func getPropertyDetail()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0)
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getPropertycontacts", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Property Info Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableArray)
                        {
                            self.peopleInvolvedArray = NSMutableArray()
                            self.peopleInvolvedArray = responseObj
                            
                            for currentObj in self.peopleInvolvedArray!
                            {
                                if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner" && "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                {
                                    self.ownerIsVerified = "1"
                                }
                                
                                if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
                                {
                                    self.addMoreTitleArray.remove("Add Owner")
                                }
                                else if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
                                {
                                    self.addMoreTitleArray.remove("Add Tenant")
                                }
                                else if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Property Manager"
                                {
                                    self.addMoreTitleArray.remove("Add Property Manager")
                                }
                                
                            }
                        
                            // Show/Hide Add More People
                            if self.ownerIsVerified == "0"
                            {
                                if ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant") ||
                                    ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker") ||
                                    ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager")
                                {
                                    self.addMoreTitleArray.remove("Add Property Manager")
                                }
                                else
                                {
                                    self.addMoreTitleArray.removeAllObjects()
                                }
                            }
                            else
                            {
                                if ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant") || ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker") || ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager")
                                {
                                    self.addMoreTitleArray.removeAllObjects()
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.tableViewObj?.reloadData()
                                self.addMorePeopleCollectionView?.reloadData()
                                self.view.setNeedsLayout()
                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    func refreshPropertyDetailResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0)
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getPropertycontacts", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Refresh Property Info Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableArray)
                        {
                            self.peopleInvolvedArray = NSMutableArray()
                            self.peopleInvolvedArray = responseObj
                            
                            for currentObj in self.peopleInvolvedArray!
                            {
                                if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner" && "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                {
                                    self.ownerIsVerified = "1"
                                }
                                
                                if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
                                {
                                    self.addMoreTitleArray.remove("Add Owner")
                                }
                                else if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
                                {
                                    self.addMoreTitleArray.remove("Add Tenant")
                                }
                                else if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Property Manager"
                                {
                                    self.addMoreTitleArray.remove("Add Property Manager")
                                }
                                
                            }
                        
                            // Show/Hide Add More People
                            if self.ownerIsVerified == "0"
                            {
                                if ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant") ||
                                    ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker") ||
                                    ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager")
                                {
                                    self.addMoreTitleArray.remove("Add Property Manager")
                                }
                                else
                                {
                                    self.addMoreTitleArray.removeAllObjects()
                                }
                            }
                            else
                            {
                                if ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant") || ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker") || ("\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager")
                                {
                                    self.addMoreTitleArray.removeAllObjects()
                                }
                            }
                            
                            DispatchQueue.main.async {
                                self.addMorePeopleCollectionView?.reloadData()
                            }
                            
                            DispatchQueue.main.async {
                                if self.peopleInvolvedArray != nil
                                {
                                    if self.peopleInvolvedArray!.count > 0
                                    {
                                        for currentObj in self.peopleInvolvedArray!
                                        {
                                            print(currentObj)
                                            
                                            if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "owner"
                                            {
                                                self.owner_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
                                                
                                                self.ownerPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
                                                {
                                                    self.ownerImgView.image = UIImage(named: "")
                                                }
                                                else
                                                {
                                                    self.ownerImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                                                }
                                                
                                                
                                                
                                                self.ownerNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                self.ownerSubNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                
                                                self.ownerEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
                                                self.ownerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                                {
                                                    self.ownerVerifyImgView.isHidden = false
                                                    self.ownerVerifyLabel.isHidden = false
                                                    self.ownerVerifyButton.isHidden = true
                                                    
                                                    self.ownerSubVerifyImgView.isHidden = false
                                                    self.ownerSubVerifyLabel.isHidden = false
                                                    self.ownerSubVerifyButton.isHidden = true
                                                }
                                                else
                                                {
                                                    self.ownerVerifyImgView.isHidden = true
                                                    self.ownerVerifyLabel.isHidden = true
                                                    self.ownerVerifyButton.isHidden = false
                                                    
                                                    self.ownerSubVerifyImgView.isHidden = true
                                                    self.ownerSubVerifyLabel.isHidden = true
                                                    self.ownerSubVerifyButton.isHidden = false
                                                }
                                                
                                                
                                            }
                                            else if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "tenant"
                                            {
                                                self.tenant_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
                                                
                                                self.tenantPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                               
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
                                                {
                                                    self.tenantImgView.image = UIImage(named: "")
                                                }
                                                else
                                                {
                                                    self.tenantImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                                                }
                                                
                                                self.tenantNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                self.tenantSubNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                
                                                self.tenantEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
                                                self.tenantPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                                {
                                                    self.tenantVerifyImgView.isHidden = false
                                                    self.tenantVerifyLabel.isHidden = false
                                                    self.tenantVerifyButton.isHidden = true
                                                    
                                                    self.tenantSubVerifyImgView.isHidden = false
                                                    self.tenantSubVerifyLabel.isHidden = false
                                                    self.tenantSubVerifyButton.isHidden = true
                                                }
                                                else
                                                {
                                                    self.tenantVerifyImgView.isHidden = true
                                                    self.tenantVerifyLabel.isHidden = true
                                                    self.tenantVerifyButton.isHidden = false
                                                    
                                                    self.tenantSubVerifyImgView.isHidden = true
                                                    self.tenantSubVerifyLabel.isHidden = true
                                                    self.tenantSubVerifyButton.isHidden = false
                                                }
                                                
                                                
                                                
                                            }
                                            else if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "broker"
                                            {
                                                self.broker_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
                                                
                                                self.brokerPhoneNumber = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
                                                {
                                                    self.brokerImgView.image = UIImage(named: "")
                                                }
                                                else
                                                {
                                                    self.brokerImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                                                }
                                                
                                                self.brokerNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                self.brokerSubNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                
                                                self.brokerEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
                                                self.brokerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                                {
                                                    self.brokerVerifyImgView.isHidden = false
                                                    self.brokerVerifyLabel.isHidden = false
                                                    self.brokerVerifyButton.isHidden = true
                                                    
                                                    self.brokerSubVerifyImgView.isHidden = false
                                                    self.brokerSubVerifyLabel.isHidden = false
                                                    self.brokerSubVerifyButton.isHidden = true
                                                }
                                                else
                                                {
                                                    self.brokerVerifyImgView.isHidden = true
                                                    self.brokerVerifyLabel.isHidden = true
                                                    self.brokerVerifyButton.isHidden = false
                                                    
                                                    self.brokerSubVerifyImgView.isHidden = true
                                                    self.brokerSubVerifyLabel.isHidden = true
                                                    self.tenantSubVerifyButton.isHidden = false
                                                }
                                                
                                            }
                                            else if ("\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")").lowercased() == "property manager"
                                            {
                                                self.propertyManager_ContactId = "\((currentObj as AnyObject).value(forKey: "contact_id") ?? "")"
                                                
                                                self.propertyManagerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")" == "undefined"
                                                {
                                                    self.propertyManagerImgView.image = UIImage(named: "")
                                                }
                                                else
                                                {
                                                    self.propertyManagerImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((currentObj as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                                                }
                                                
                                                self.propertyManagerNameLabel.text = "\((currentObj as AnyObject).value(forKey: "firstName") ?? "") \((currentObj as AnyObject).value(forKey: "lastName") ?? "")"
                                                
                                                self.propertyManagerEmailLabel.text = "\((currentObj as AnyObject).value(forKey: "email") ?? "")"
                                                self.propertyManagerPhoneLabel.text = "\((currentObj as AnyObject).value(forKey: "mobileno") ?? "")"
                                                
                                                if "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                                {
                                                    self.propertyManagerVerifyImgView.isHidden = false
                                                    self.propertyManagerVerifyLabel.isHidden = false
                                                    self.propertyManagerVerifyOtpButton.isHidden = true
                                                }
                                                else
                                                {
                                                    self.propertyManagerVerifyImgView.isHidden = true
                                                    self.propertyManagerVerifyLabel.isHidden = true
                                                    self.propertyManagerVerifyOtpButton.isHidden = false
                                                }
                                            }
                                        }
                                        
//                                        self.tableViewObj!.reloadData()
                                        self.tableViewObj?.reloadData()
                                        self.addMorePeopleCollectionView?.reloadData()
                                    }
                                }
                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Rental Info
    func getRentalInfo()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0), //Long
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //(Int64("\(AppUtility.getUserData(keyVal: "mobileNo"))") ?? 0), //Long
                "firstName" : "", //String
                "lastName" : "", //String
                "propertyName" : "\(detailDict?.value(forKey: "propertyName") ?? "")", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getRentalInfo", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Rental Info Response : \(response)")
                        
                        if let responseObj = ((((response as AnyObject).value(forKey: "data")) as AnyObject).object(at: 0) as? NSDictionary)
                        {
                            self.leaseDict = NSDictionary()
                            self.leaseDict = responseObj
                            
                            DispatchQueue.main.async {
                         
//                                self.lblPeriod.text = "\(responseObj.value(forKey: "duration") ?? "") Years"
                                
                                self.lblPeriod.text = "\("\(responseObj.value(forKey: "duration") ?? "")".components(separatedBy: ",").first ?? "") Years"
                                self.lblDeposite.text = "₹\(responseObj.value(forKey: "security_deposit_amt") ?? "")"
                                self.lblStartDate.text = "\("\(responseObj.value(forKey: "rent_start_date") ?? "")".components(separatedBy: "T").first ?? "")"
                                self.lblNotice.text = "\(responseObj.value(forKey: "notice_period") ?? "") Month"
//                                self.lblEscalation.text = "\(responseObj.value(forKey: "escalationAmount") ?? "")% (1 Year)"
                                self.lblEscalation.text = "\(responseObj.value(forKey: "escalation_percent") ?? "")% (\("\(responseObj.value(forKey: "escalation_month") ?? "")".components(separatedBy: ",").first ?? "") Years, \("\(responseObj.value(forKey: "escalation_month") ?? "")".components(separatedBy: ",").last ?? "") Months)"
                                self.lblLockingPeriod.text = "\(responseObj.value(forKey: "owner_lockin") ?? "") Month"
                                self.lblExpDate.text = "\("\(responseObj.value(forKey: "end_date") ?? "")".components(separatedBy: "T").first ?? "")"
                                
                                self.lblRent.text = "₹\(responseObj.value(forKey: "monthly_rent") ?? "")" == "<null>" ? "NA" : "\(responseObj.value(forKey: "monthly_rent") ?? "")/m"
                                
//                                self.leaseDocCollectionView.reloadData()
                                
                                // Hardik..
                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Remove People Involved
    func removePeopleInvolved(mobileno : String, contact_id : String, contact_type : String)
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()

            
            let paramDict = [
                "mobileno":(Int64(mobileno) ?? 0), //Long
                "contact_id":(Int64(contact_id) ?? 0), //Long
                "contact_type":(Int64(contact_type) ?? 0), //Long
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/removePropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            self.getPropertyDetail()
                            self.getRentalInfo()
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
}

extension PropertyDetailViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.infoCollectionView
        {
            return self.infoTitleArray.count
        }
        else if collectionView == self.addMorePeopleCollectionView
        {
            return self.addMoreTitleArray.count
        }
        
        return self.leaseTitleArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.infoCollectionView
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "propertyInfoCell", for: indexPath) as! PropertyInfoCollectionViewCell
            
            myCell.baseView.backgroundColor = Constants.bgColor
            myCell.baseView.layer.cornerRadius = 15
            
            if "\(self.infoTitleArray[indexPath.item])" == "area"
            {
                myCell.imageViewObj.image = UIImage(named: "area")
                myCell.lblTitle.text = "\(self.detailDict?.value(forKey: "carpet_area") ?? "") sq.ft"
            }
            else if "\(self.infoTitleArray[indexPath.item])" == "bathroom"
            {
                myCell.imageViewObj.image = UIImage(named: "bathroom")
                myCell.lblTitle.text = "\(self.detailDict?.value(forKey: "bathroom") ?? "") Bathroom"
            }
            else if "\(self.infoTitleArray[indexPath.item])" == "beds"
            {
                myCell.imageViewObj.image = UIImage(named: "beds")
                myCell.lblTitle.text = "\(self.detailDict?.value(forKey: "bedroom") ?? "") Beds"
            }
            else
            {
                myCell.imageViewObj.image = UIImage(named: "\(self.infoImgArray[indexPath.item])")
                myCell.lblTitle.text = "\(self.infoTitleArray[indexPath.item])"
            }
            
            return myCell
        }
        else if collectionView == self.addMorePeopleCollectionView
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "boxCollectionCell", for: indexPath) as! BoxCollectionViewCell
            
            myCell.baseView.backgroundColor = Constants.bgColor
            myCell.baseView.layer.cornerRadius = 15
            
            myCell.baseView.backgroundColor = .white
            
            if "\(self.addMoreTitleArray[indexPath.item])" == "Add Tenant"
            {
                myCell.imageViewObj.image = UIImage(named: "tenant")
            }
            else
            {
                myCell.imageViewObj.image = UIImage(named: "broker")
            }

            if "\(self.addMoreTitleArray[indexPath.item])" == "Add Owner"
            {
                myCell.buttonObj.tag = 1
            }
            else if "\(self.addMoreTitleArray[indexPath.item])" == "Add Tenant"
            {
                myCell.buttonObj.tag = 2
            }
            else if "\(self.addMoreTitleArray[indexPath.item])" == "Add Broker"
            {
                myCell.buttonObj.tag = 3
            }
            else if "\(self.addMoreTitleArray[indexPath.item])" == "Add Property Manager"
            {
                myCell.buttonObj.tag = 4
            }
            
            myCell.lblTitle.text = "\(self.addMoreTitleArray[indexPath.item])"
            
            myCell.buttonObj.addTarget(self, action: #selector(self.addMorePeopleButtonClicked(sender:)), for: .touchUpInside)
            
            return myCell
        }
        else
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "boxCollectionCell", for: indexPath) as! BoxCollectionViewCell
            
            myCell.baseView.backgroundColor = Constants.bgColor
            myCell.baseView.layer.cornerRadius = 15
            
//            myCell.imageViewObj.image = UIImage(named: "\(self.leaseImgArray[indexPath.item])")
            
            if "\(self.leaseTitleArray[indexPath.item])" == "Registered doc"
            {
                myCell.buttonObj.tag = 1
                myCell.imageViewObj.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "reg_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
            }
            else if "\(self.leaseTitleArray[indexPath.item])" == "Electricity Bill"
            {
                myCell.buttonObj.tag = 2
                myCell.imageViewObj.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "electricity_bill") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
            }
            else if "\(self.leaseTitleArray[indexPath.item])" == "Owner ID Proof"
            {
                myCell.buttonObj.tag = 3
                myCell.imageViewObj.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(detailDict?.value(forKey: "owner_id") ?? "")")")! as URL), placeholderImage: UIImage(named: "homeLogo"))
            }
            
            myCell.imageLeading.constant = 0
            myCell.imageTop.constant = 0
            myCell.imageTrailing.constant = 0
            myCell.imageBottom.constant = 0
            
            myCell.imageViewObj.contentMode = .scaleAspectFill
            myCell.imageViewObj.clipsToBounds = true
//            myCell.imageViewObj.layer.masksToBounds = true
            
            
            
            myCell.buttonObj.addTarget(self, action: #selector(self.documentAttachedButtonClicked(sender:)), for: .touchUpInside)
            
            myCell.lblTitle.text = "\(self.leaseTitleArray[indexPath.item])"
            
            return myCell
        }
        
//        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == self.infoCollectionView
        {
            return CGSize(width: 100.0, height: 100.0)
        }
        
        return CGSize(width: 100.0, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 15, left: 0, bottom: 5, right: 15)
    }
}
