//
//  EditLeaseAgreementViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 24/02/21.
//

import UIKit
import DatePickerDialog
import SDWebImage
import KRProgressHUD
import AWSS3
import PromiseKit
import AddressBook
import AddressBookUI
import Contacts
import ContactsUI
import DatePickerDialog
import MobileCoreServices

class EditLeaseAgreementViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate
{
    var applyGST : Int = 0
    
    var leaseTypeStr = "Company"
    var occupancyTypeStr = ""
    var leaseRentDueDateStr = ""
    
    var leaseCompanyInfoArray = [String]()
    var leaseDurationArray = [String]()
    var leaseEscalationArray = [String]()
    
    var leaseAgreementImage : UIImage?
    var leasePoliceVerificationImage : UIImage?
    var leaseNOCImage : UIImage?
    
    var isLeaseAgreementSelect = false
    var isLeasePoliceVerificationSelect = false
    var isLeaseNOCSelect = false
    
    var leaseAgreementImagePath = ""
    var leasePoliceVerificationImagePath = ""
    var leaseNOCImagePath = ""
    
    var selectedLeaseStartDate = Date()
    var selectedLeaseRentStartDate = Date()
    
    var leaseDict : NSDictionary?
    
    @IBOutlet weak var updateButton:
        UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet var leaseView: UIView!
    
    
    @IBOutlet weak var leaseCompanyButton: UIButton!
    @IBOutlet weak var leaseIndividualButton: UIButton!
    
    @IBOutlet weak var occupancyTypeView: UIView!
    
    @IBOutlet weak var familyButton: UIButton!
    @IBOutlet weak var bachelorButton: UIButton!
    
    @IBOutlet weak var txtLeaseCompanyName: UITextField!
    
    @IBOutlet weak var companyNameLabel: UILabel!
    
    
    @IBOutlet weak var lblLeaseAddressTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseAddressHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtLeaseAddressTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseAddressHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblLeaseContactTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseContactHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtLeaseContactTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseContactHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblLeaseDesignationTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseDesignationHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtLeaseDesignationTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseDesignationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblLeaseContactNumTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseContactNumHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtLeaseContactNumTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseContactNumHeight: NSLayoutConstraint!
  
    @IBOutlet weak var lblLeaseEmailTop: NSLayoutConstraint!
    @IBOutlet weak var lblLeaseEmailHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtLeaseEmailTop: NSLayoutConstraint!
    @IBOutlet weak var txtLeaseEmailHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtContactPerson: UITextField!
    @IBOutlet weak var txtDesignation: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var leaseYearButton: UIButton!
    @IBOutlet weak var leaseMonthButton: UIButton!
    
    @IBOutlet weak var txtLeaseStartDate: UITextField!
    @IBOutlet weak var txtRentFreePeriod: UITextField!
    @IBOutlet weak var txtRentStartDate: UITextField!
    @IBOutlet weak var txtMonthlyRentAmt: UITextField!
    @IBOutlet weak var txtSecurityDeposite: UITextField!
    
    @IBOutlet weak var leaseRentDueDateButton: UIButton!
    
    @IBOutlet weak var leaseEscalationYearButton: UIButton!
    @IBOutlet weak var leaseEscalationMonthButton: UIButton!
    @IBOutlet weak var leaseEscalationPercentageButton: UIButton!
    
    @IBOutlet weak var leaseOwnerLockinButton: UIButton!
    @IBOutlet weak var leaseTenantLockinButton: UIButton!
    @IBOutlet weak var leaseNoticePeriodButton: UIButton!
    
    
    @IBOutlet weak var leaseAgreementButton: UIButton!
    @IBOutlet weak var leasePoliceVerificationButton: UIButton!
    @IBOutlet weak var leaseSocietyNocButton: UIButton!
    
    @IBOutlet weak var lblApplyGST: UILabel!
    @IBOutlet weak var gstToggle: UISwitch!
    @IBOutlet weak var lblGSTNoteHeight: NSLayoutConstraint!
    
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.leaseView.backgroundColor = .clear
        self.leaseView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2350)
        self.mainScrollView.addSubview(self.leaseView)
        
        
        self.leaseCompanyButton.layer.cornerRadius = 15
        self.leaseCompanyButton.backgroundColor = Constants.darkBlueColor
        self.leaseCompanyButton.layer.masksToBounds = true
        
        self.leaseIndividualButton.layer.cornerRadius = 15
        self.leaseIndividualButton.backgroundColor = .white
        self.leaseIndividualButton.layer.masksToBounds = true
        
        self.txtLeaseCompanyName.delegate = self
        let txtLeaseCompanyNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseCompanyName.leftView = txtLeaseCompanyNameViewObj
        self.txtLeaseCompanyName.leftViewMode = .always
        
        self.txtAddress.delegate = self
        let txtAddressViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress.leftView = txtAddressViewObj
        self.txtAddress.leftViewMode = .always
        
        self.txtContactPerson.delegate = self
        let txtContactPersonViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtContactPerson.leftView = txtContactPersonViewObj
        self.txtContactPerson.leftViewMode = .always
        
        self.txtDesignation.delegate = self
        let txtDesignationViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtDesignation.leftView = txtDesignationViewObj
        self.txtDesignation.leftViewMode = .always
        
        self.txtContactNumber.delegate = self
        let txtContactNumberViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtContactNumber.leftView = txtContactNumberViewObj
        self.txtContactNumber.leftViewMode = .always
        
        self.txtEmail.delegate = self
        let txtEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtEmail.leftView = txtEmailViewObj
        self.txtEmail.leftViewMode = .always
        
        self.txtRentStartDate.delegate = self
        let txtRentStartDateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtRentStartDate.leftView = txtRentStartDateViewObj
        self.txtRentStartDate.leftViewMode = .always
        
        self.txtRentFreePeriod.delegate = self
        let txtRentFreePeriodViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtRentFreePeriod.leftView = txtRentFreePeriodViewObj
        self.txtRentFreePeriod.leftViewMode = .always
        
        self.txtLeaseStartDate.delegate = self
        let txtLeaseStartDateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLeaseStartDate.leftView = txtLeaseStartDateViewObj
        self.txtLeaseStartDate.leftViewMode = .always
        
        self.txtMonthlyRentAmt.delegate = self
        let txtMonthlyRentAmtViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtMonthlyRentAmt.leftView = txtMonthlyRentAmtViewObj
        self.txtMonthlyRentAmt.leftViewMode = .always
        
        self.txtSecurityDeposite.delegate = self
        let txtSecurityDepositeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtSecurityDeposite.leftView = txtSecurityDepositeViewObj
        self.txtSecurityDeposite.leftViewMode = .always
        
        
        if self.leaseDict != nil
        {
            self.displayLeaseInfo()
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.leaseView.frame = CGRect(x: self.leaseView.frame.origin.x, y: self.leaseView.frame.origin.y, width: UIScreen.main.bounds.size.width, height: self.updateButton.frame.origin.y + self.updateButton.frame.height + 50)
            
            self.mainScrollView.contentSize.height = self.leaseView.frame.height
        }
    }
    
    func displayLeaseInfo()
    {
        print("displayLeaseInfo")
        
        if "\(self.leaseDict?.value(forKey: "leasetype") ?? "")" == "Individual"
        {
            self.companyNameLabel.text = "Occupancy Type"
            self.occupancyTypeView.isHidden = false
            self.occupancyTypeView.backgroundColor = .clear
            self.txtLeaseCompanyName.isHidden = true
           
            self.lblLeaseAddressTop.constant = 0
            self.lblLeaseAddressHeight.constant = 0
            self.txtLeaseAddressTop.constant = 0
            self.txtLeaseAddressHeight.constant = 0
            self.lblLeaseContactTop.constant = 0
            self.lblLeaseContactHeight.constant = 0
            self.txtLeaseContactTop.constant = 0
            self.txtLeaseContactHeight.constant = 0
            self.lblLeaseDesignationTop.constant = 0
            self.lblLeaseDesignationHeight.constant = 0
            self.txtLeaseDesignationTop.constant = 0
            self.txtLeaseDesignationHeight.constant = 0
            self.lblLeaseContactNumTop.constant = 0
            self.lblLeaseContactNumHeight.constant = 0
            self.txtLeaseContactNumTop.constant = 0
            self.txtLeaseContactNumHeight.constant = 0
            self.lblLeaseEmailTop.constant = 0
            self.lblLeaseEmailHeight.constant = 0
            self.txtLeaseEmailTop.constant = 0
            self.txtLeaseEmailHeight.constant = 0
            
            self.leaseTypeStr = "Individual"
            self.leaseIndividualButton.layer.cornerRadius = 15
            self.leaseIndividualButton.backgroundColor = Constants.darkBlueColor
            self.leaseIndividualButton.setTitleColor(.white, for: .normal)
            
            self.leaseCompanyButton.backgroundColor = .white
            self.leaseCompanyButton.setTitleColor(.lightGray, for: .normal)
            
            self.view.setNeedsLayout()
        }
        else
        {
            self.occupancyTypeStr = ""
            self.companyNameLabel.text = "Company Name*"
            self.occupancyTypeView.isHidden = true
            self.txtLeaseCompanyName.isHidden = false
           
            self.lblLeaseAddressTop.constant = 15
            self.lblLeaseAddressHeight.constant = 35
            self.txtLeaseAddressTop.constant = 7
            self.txtLeaseAddressHeight.constant = 50
            
            self.lblLeaseContactTop.constant = 15
            self.lblLeaseContactHeight.constant = 35
            self.txtLeaseContactTop.constant = 7
            self.txtLeaseContactHeight.constant = 50
            
            self.lblLeaseDesignationTop.constant = 15
            self.lblLeaseDesignationHeight.constant = 35
            self.txtLeaseDesignationTop.constant = 7
            self.txtLeaseDesignationHeight.constant = 50
            
            self.lblLeaseContactNumTop.constant = 15
            self.lblLeaseContactNumHeight.constant = 35
            self.txtLeaseContactNumTop.constant = 7
            self.txtLeaseContactNumHeight.constant = 50
            
            self.lblLeaseEmailTop.constant = 15
            self.lblLeaseEmailHeight.constant = 35
            self.txtLeaseEmailTop.constant = 7
            self.txtLeaseEmailHeight.constant = 50
            
            self.leaseTypeStr = "Company"
            
            self.leaseCompanyButton.layer.cornerRadius = 15
            self.leaseCompanyButton.backgroundColor = Constants.darkBlueColor
            self.leaseCompanyButton.setTitleColor(.white, for: .normal)
            
            self.leaseIndividualButton.backgroundColor = .white
            self.leaseIndividualButton.setTitleColor(.lightGray, for: .normal)
            
            if "\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",").count > 0
            {
                self.txtLeaseCompanyName.text = "\("\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[0])"
                self.txtAddress.text = "\("\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[1])"
                self.txtContactPerson.text = "\("\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[2])"
                self.txtDesignation.text = "\("\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[3])"
                self.txtContactNumber.text = "\("\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[4])"
                self.txtEmail.text = "\("\(self.leaseDict?.value(forKey: "companyinfo") ?? "")".components(separatedBy: ",")[5])"
            }
            
            self.view.setNeedsLayout()
        }
        
        if "\(self.leaseDict?.value(forKey: "duration") ?? "")".components(separatedBy: ",").count > 0
        {
            self.leaseYearButton.setTitle("\(self.leaseDict?.value(forKey: "duration") ?? "")".components(separatedBy: ",")[0], for: .normal)
            self.leaseMonthButton.setTitle("\(self.leaseDict?.value(forKey: "duration") ?? "")".components(separatedBy: ",")[1], for: .normal)
        }
        
        if "\(self.leaseDict?.value(forKey: "start_date") ?? "")".components(separatedBy: "T").count > 0
        {
            self.txtLeaseStartDate.text = "\(self.leaseDict?.value(forKey: "start_date") ?? "")".components(separatedBy: "T")[0]
        }
        
        self.txtRentFreePeriod.text = "\(self.leaseDict?.value(forKey: "rent_free_period") ?? "")"
        
        if "\(self.leaseDict?.value(forKey: "rent_start_date") ?? "")".components(separatedBy: "T").count > 0
        {
            self.txtRentStartDate.text = "\(self.leaseDict?.value(forKey: "rent_start_date") ?? "")".components(separatedBy: "T")[0]
        }
        
        self.txtMonthlyRentAmt.text = "\(self.leaseDict?.value(forKey: "monthly_rent") ?? "")"
        self.txtSecurityDeposite.text = "\(self.leaseDict?.value(forKey: "security_deposit_amt") ?? "")"
        self.leaseRentDueDateButton.setTitle("\(self.leaseDict?.value(forKey: "rent_due_date") ?? "")", for: .normal)
        
        if "\(self.leaseDict?.value(forKey: "escalation_month") ?? "")".components(separatedBy: ",").count > 0
        {
            self.leaseEscalationYearButton.setTitle("\("\(self.leaseDict?.value(forKey: "escalation_month") ?? "")".components(separatedBy: ",")[0])", for: .normal)
            self.leaseEscalationMonthButton.setTitle("\("\(self.leaseDict?.value(forKey: "escalation_month") ?? "")".components(separatedBy: ",")[1])", for: .normal)
        }
        
        self.leaseEscalationPercentageButton.setTitle("\(self.leaseDict?.value(forKey: "escalation_percent") ?? "")", for: .normal)
        
        self.leaseOwnerLockinButton.setTitle("\(self.leaseDict?.value(forKey: "owner_lockin") ?? "")", for: .normal)
        self.leaseTenantLockinButton.setTitle("\(self.leaseDict?.value(forKey: "tenant_lockin") ?? "")", for: .normal)
        self.leaseNoticePeriodButton.setTitle("\(self.leaseDict?.value(forKey: "notice_period") ?? "")", for: .normal)
        
        if "\(self.leaseDict?.value(forKey: "rent_doc") ?? "")".isEmpty != true
        {
            let registrationImgView = UIImageView()
            registrationImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.leaseDict?.value(forKey: "rent_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.leaseAgreementButton.setImage(registrationImgView.image, for: .normal)
            self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
            self.leaseAgreementButton.clipsToBounds = true
        }
        
        if "\(self.leaseDict?.value(forKey: "police_verf_doc") ?? "")".isEmpty != true
        {
            let registrationImgView = UIImageView()
            registrationImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.leaseDict?.value(forKey: "police_verf_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.leasePoliceVerificationButton.setImage(registrationImgView.image, for: .normal)
            self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
            self.leasePoliceVerificationButton.clipsToBounds = true
        }
        
        if "\(self.leaseDict?.value(forKey: "society_noc_doc") ?? "")".isEmpty != true
        {
            let registrationImgView = UIImageView()
            registrationImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.leaseDict?.value(forKey: "society_noc_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.leaseSocietyNocButton.setImage(registrationImgView.image, for: .normal)
            self.leaseSocietyNocButton.imageView?.contentMode = .scaleAspectFill
            self.leaseSocietyNocButton.clipsToBounds = true
        }
        
        if "\(self.leaseDict?.value(forKey: "occupancy_type") ?? "")" == "Family"
        {
            self.familyButton.isSelected = true
            self.familyButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.occupancyTypeStr = "family"
        }
        else if "\(self.leaseDict?.value(forKey: "occupancy_type") ?? "")" == "Bachelor"
        {
            self.bachelorButton.isSelected = true
            self.bachelorButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.occupancyTypeStr = "bachelor"
        }
        
        if "\(self.leaseDict?.value(forKey: "gst") ?? "")" == "1"
        {
            self.lblApplyGST.isHidden = false
            self.gstToggle.isHidden = false
            self.lblGSTNoteHeight.constant = 25
            self.gstToggle.isOn = true
        }
        else
        {
            self.gstToggle.isOn = false
            self.lblApplyGST.isHidden = true
            self.gstToggle.isHidden = true
            
            self.lblGSTNoteHeight.constant = 0
        }
        
    }
    
    //MARK: - Toggle Value Changed
    @IBAction func leaseApplyGSTValueChanged(_ sender: UISwitch)
    {
        print("leaseApplyGSTValueChanged")
        
        if sender.isOn
        {
            self.applyGST = 1
        }
        else
        {
            self.applyGST = 0
        }
    }
    
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func leaseCompanyButtonClicked(_ sender: UIButton)
    {
        print("leaseCompanyButtonClicked")
        
        DispatchQueue.main.async {
//            self.view.endEditing(true)
            
            self.occupancyTypeStr = ""
            self.companyNameLabel.text = "Company Name*"
            self.occupancyTypeView.isHidden = true
            self.txtLeaseCompanyName.isHidden = false
           
            self.lblLeaseAddressTop.constant = 15
            self.lblLeaseAddressHeight.constant = 35
            self.txtLeaseAddressTop.constant = 7
            self.txtLeaseAddressHeight.constant = 50
            
            self.lblLeaseContactTop.constant = 15
            self.lblLeaseContactHeight.constant = 35
            self.txtLeaseContactTop.constant = 7
            self.txtLeaseContactHeight.constant = 50
            
            self.lblLeaseDesignationTop.constant = 15
            self.lblLeaseDesignationHeight.constant = 35
            self.txtLeaseDesignationTop.constant = 7
            self.txtLeaseDesignationHeight.constant = 50
            
            self.lblLeaseContactNumTop.constant = 15
            self.lblLeaseContactNumHeight.constant = 35
            self.txtLeaseContactNumTop.constant = 7
            self.txtLeaseContactNumHeight.constant = 50
            
            self.lblLeaseEmailTop.constant = 15
            self.lblLeaseEmailHeight.constant = 35
            self.txtLeaseEmailTop.constant = 7
            self.txtLeaseEmailHeight.constant = 50
            
            self.leaseTypeStr = sender.currentTitle ?? ""
            
            self.leaseCompanyButton.layer.cornerRadius = 15
            self.leaseCompanyButton.backgroundColor = Constants.darkBlueColor
            self.leaseCompanyButton.setTitleColor(.white, for: .normal)
            
            self.leaseIndividualButton.backgroundColor = .white
            self.leaseIndividualButton.setTitleColor(.lightGray, for: .normal)
            
            self.view.setNeedsLayout()
        }
    }
    
    @IBAction func leaseIndividualButtonClicked(_ sender: UIButton)
    {
        print("leaseIndividualButtonClicked")
        
        DispatchQueue.main.async {
//            self.view.endEditing(true)
            
            self.companyNameLabel.text = "Occupancy Type*"
            self.occupancyTypeView.isHidden = false
            self.occupancyTypeView.backgroundColor = .clear
            self.txtLeaseCompanyName.isHidden = true
           
            self.lblLeaseAddressTop.constant = 0
            self.lblLeaseAddressHeight.constant = 0
            self.txtLeaseAddressTop.constant = 0
            self.txtLeaseAddressHeight.constant = 0
            self.lblLeaseContactTop.constant = 0
            self.lblLeaseContactHeight.constant = 0
            self.txtLeaseContactTop.constant = 0
            self.txtLeaseContactHeight.constant = 0
            self.lblLeaseDesignationTop.constant = 0
            self.lblLeaseDesignationHeight.constant = 0
            self.txtLeaseDesignationTop.constant = 0
            self.txtLeaseDesignationHeight.constant = 0
            self.lblLeaseContactNumTop.constant = 0
            self.lblLeaseContactNumHeight.constant = 0
            self.txtLeaseContactNumTop.constant = 0
            self.txtLeaseContactNumHeight.constant = 0
            self.lblLeaseEmailTop.constant = 0
            self.lblLeaseEmailHeight.constant = 0
            self.txtLeaseEmailTop.constant = 0
            self.txtLeaseEmailHeight.constant = 0
            
            self.leaseTypeStr = sender.currentTitle ?? ""
            self.leaseIndividualButton.layer.cornerRadius = 15
            self.leaseIndividualButton.backgroundColor = Constants.darkBlueColor
            self.leaseIndividualButton.setTitleColor(.white, for: .normal)
            
            self.leaseCompanyButton.backgroundColor = .white
            self.leaseCompanyButton.setTitleColor(.lightGray, for: .normal)
            
            self.view.setNeedsLayout()
        }
    }
    
    @IBAction func familyButtonClicked(_ sender: UIButton)
    {
        print("familyButtonClicked")
        
        self.bachelorButton.isSelected = false
        self.bachelorButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.familyButton.isSelected
        {
            self.familyButton.isSelected = false
            self.familyButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.occupancyTypeStr = ""
        }
        else
        {
            self.familyButton.isSelected = true
            self.familyButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.occupancyTypeStr = "family"
        }
    }
    
    @IBAction func bachelorButtonClicked(_ sender: UIButton)
    {
        print("bachelorButtonClicked")
        
        self.familyButton.isSelected = false
        self.familyButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.bachelorButton.isSelected
        {
            self.bachelorButton.isSelected = false
            self.bachelorButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.occupancyTypeStr = ""
        }
        else
        {
            self.bachelorButton.isSelected = true
            self.bachelorButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.occupancyTypeStr = "bachelor"
        }
    }
    
    @IBAction func leaseYearButtonClicked(_ sender: UIButton)
    {
        print("leaseYearButtonClicked")
        
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
        
        PickerView().showDoalog(title: "Select Year", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "1") { (selectedString, selectedIndex) in

            self.leaseYearButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseMonthButtonClicked(_ sender: UIButton)
    {
        print("leaseYearButtonClicked")
        
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
        
        PickerView().showDoalog(title: "Select Year", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "1") { (selectedString, selectedIndex) in

            self.leaseYearButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseRentDueDateButtonClicked(_ sender: UIButton)
    {
        print("leaseRentDueDateButtonClicked")
        
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
        
        PickerView().showDoalog(title: "Rent Due Date (every month)", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "1") { (selectedString, selectedIndex) in

            self.leaseRentDueDateStr = "\(selectedString)"
            
            if selectedString == "1"
            {
                self.leaseRentDueDateButton.setTitle("\(selectedString)", for: .normal)
            }
            else if selectedString == "2"
            {
                self.leaseRentDueDateButton.setTitle("\(selectedString)nd (of every month)", for: .normal)
            }
            else if selectedString == "3"
            {
                self.leaseRentDueDateButton.setTitle("\(selectedString)rd (of every month)", for: .normal)
            }
            else
            {
                self.leaseRentDueDateButton.setTitle("\(selectedString)th (of every month)", for: .normal)
            }
        }
    }
    
    @IBAction func leaseEscalationYearButtonClicked(_ sender: UIButton)
    {
        print("leaseEscalationYearButtonClicked")
        
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
        
        PickerView().showDoalog(title: "Escalation Year", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseEscalationYearButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseEscalationMonthButtonClicked(_ sender: UIButton)
    {
        print("leaseEscalationMonthButtonClicked")
        
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
        
        PickerView().showDoalog(title: "Escalation Month", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseEscalationMonthButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseEscalationPercentageButtonClicked(_ sender: UIButton)
    {
        print("leaseEscalationPercentageButtonClicked")
        
        let incidentArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"]
        
        PickerView().showDoalog(title: "Escalation Percentage", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseEscalationPercentageButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseOwnerLockinButtonClicked(_ sender: UIButton)
    {
        print("leaseOwnerLockinButtonClicked")
        
        var incidentArray = [String]()
        
        for i in 0...60 {
            incidentArray.append("\(i)")
            
        }
        
        PickerView().showDoalog(title: "Owner Lock In", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseOwnerLockinButton.setTitle("\(selectedString)", for: .normal)
            self.leaseTenantLockinButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseTenantLockinButtonClicked(_ sender: UIButton)
    {
        print("leaseTenantLockinButtonClicked")
        
        var incidentArray = [String]()
        
        for i in 0...60 {
            incidentArray.append("\(i)")
        }
        
        PickerView().showDoalog(title: "Tenant Lock In", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseTenantLockinButton.setTitle("\(selectedString)", for: .normal)
        }
    }
   
    @IBAction func leaseNoticePeriodButtonClicked(_ sender: UIButton)
    {
        print("leaseNoticePeriodButtonClicked")
        
        var incidentArray = [String]()
        
        for i in 0...60 {
            incidentArray.append("\(i)")
        }
        
        PickerView().showDoalog(title: "Notice Period", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "0") { (selectedString, selectedIndex) in

            self.leaseNoticePeriodButton.setTitle("\(selectedString)", for: .normal)
        }
    }
    
    @IBAction func leaseAgreementButtonClicked(_ sender: UIButton)
    {
        print("leaseAgreementButtonClicked")
        
        self.isLeaseAgreementSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func leasePoliceVerificationButtonClicked(_ sender: UIButton)
    {
        print("leasePoliceVerificationButtonClicked")
        
        self.isLeasePoliceVerificationSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func leaseSocietyNocButtonClicked(_ sender: UIButton)
    {
        print("leaseSocietyNocButtonClicked")
        
        self.isLeaseNOCSelect = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func updateButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.leaseTypeStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Lease Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.leaseTypeStr == "Individual" && self.occupancyTypeStr.isEmpty
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Occupancy Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseCompanyName.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Company Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtAddress.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Address", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtContactPerson.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Contact Person Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtDesignation.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Designation", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtContactNumber.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Contact Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtEmail.text!.isEmpty == true && self.leaseTypeStr == "Company"
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLeaseStartDate.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Start Date", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtRentFreePeriod.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Rent Free Period", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtRentStartDate.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Rent Start Date", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtMonthlyRentAmt.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Rent Amount", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtSecurityDeposite.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Rent Security Deposite", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            
            self.updateLeaseAgreement()
        }
    }
    
    @IBAction func startDateButtonClicked(_ sender: UIButton)
    {
        let datePicker = DatePickerDialog()
        let currentDate = Date()
       
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        var futureDateComponents = DateComponents()
        futureDateComponents.year = 100
        let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
        
        datePicker.show("Lease Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
            
            if let dt = date
            {
                let formatter = DateFormatter()
//                formatter.dateFormat = "dd MMM - yyyy"
                formatter.dateFormat = "yyyy-MM-dd"
                print("Selected Date : \(formatter.string(from: dt))")
                
                self.txtLeaseStartDate.text = "\(formatter.string(from: dt))"
                self.txtRentStartDate.text = "\(formatter.string(from: dt))"
                
                /////
                self.selectedLeaseStartDate = dt
                let diffInDays = Calendar.current.dateComponents([.day], from: dt, to: dt).day
                self.txtRentFreePeriod.text = "\(diffInDays ?? 0)"
            }
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if self.isLeaseAgreementSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isLeaseAgreementSelect = false
                
                self.leaseAgreementImage = pickedImage
                
                self.leaseAgreementButton.setImage(pickedImage, for: .normal)
                self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
                self.leaseAgreementButton.layer.masksToBounds = true
                
                self.leaseAgreementImagePath = Constants.agreementFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isLeasePoliceVerificationSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isLeasePoliceVerificationSelect = false
                
                self.leasePoliceVerificationImage = pickedImage
                
                self.leasePoliceVerificationButton.setImage(pickedImage, for: .normal)
                self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
                self.leasePoliceVerificationButton.layer.masksToBounds = true
                
                self.leasePoliceVerificationImagePath = Constants.policeVerificationFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isLeaseNOCSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isLeaseNOCSelect = false
                
                self.leaseNOCImage = pickedImage
                
                self.leaseSocietyNocButton.setImage(pickedImage, for: .normal)
                self.leaseSocietyNocButton.imageView?.contentMode = .scaleAspectFill
                self.leaseSocietyNocButton.layer.masksToBounds = true
                
                self.leaseNOCImagePath = Constants.nocFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        
        //        if self.isPropertyPhotoSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.uploadPhotosLabel.text = "Add another photos"
        //                self.uploadPhotosLabelWidth.constant = 195
        //                self.isPropertyPhotoSelect = false
        //                self.propertyPhotosNameArray?.add(Constants.propertyImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png")
        //                self.propertyPhotosArray?.add(pickedImage)
        //
        //                DispatchQueue.main.async {
        //
        //                    if self.propertyPhotosArray!.count > 0
        //                    {
        //                        self.propertyPhotoHeightConstraint.constant = 120
        //                        self.mainScrollView.contentSize.height = 2000
        //                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
        //                    }
        //                    self.view.layoutIfNeeded()
        //                    self.propertyPhotosCollection.reloadData()
        //                }
        //            }
        //        }
        //        else if self.isTenantProfileSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isTenantProfileSelect = false
        //
        //                self.tenantProfileImageView.image = pickedImage
        //
        //                self.tenantProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isTenantAadharSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isTenantAadharSelect = false
        //
        //                self.tenantAadharImage = pickedImage
        //
        //                self.tenantAadharButton.setImage(pickedImage, for: .normal)
        //                self.tenantAadharButton.imageView?.contentMode = .scaleAspectFill
        //                self.tenantAadharButton.layer.masksToBounds = true
        //
        //                self.tenantAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isTenantPanSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isTenantPanSelect = false
        //
        //                self.tenantPanImage = pickedImage
        //
        //                self.tenantPanButton.setImage(pickedImage, for: .normal)
        //                self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
        //                self.tenantPanButton.layer.masksToBounds = true
        //
        //                self.tenantPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isLeaseAgreementSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isLeaseAgreementSelect = false
        //
        //                self.leaseAgreementImage = pickedImage
        //
        //                self.leaseAgreementButton.setImage(pickedImage, for: .normal)
        //                self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
        //                self.leaseAgreementButton.layer.masksToBounds = true
        //
        //                self.leaseAgreementImagePath = Constants.agreementFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isLeasePoliceVerificationSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isLeasePoliceVerificationSelect = false
        //
        //                self.leasePoliceVerificationImage = pickedImage
        //
        //                self.leasePoliceVerificationButton.setImage(pickedImage, for: .normal)
        //                self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
        //                self.leasePoliceVerificationButton.layer.masksToBounds = true
        //
        //                self.leasePoliceVerificationImagePath = Constants.policeVerificationFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isLeaseNOCSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isLeaseNOCSelect = false
        //
        //                self.leaseNOCImage = pickedImage
        //
        //                self.leaseNOCButton.setImage(pickedImage, for: .normal)
        //                self.leaseNOCButton.imageView?.contentMode = .scaleAspectFill
        //                self.leaseNOCButton.layer.masksToBounds = true
        //
        //                self.leaseNOCImagePath = Constants.nocFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isBrokerProfileSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isBrokerProfileSelect = false
        //
        //                self.brokerProfilePic.image = pickedImage
        //
        //                self.brokerProfileImage = pickedImage
        //
        //                self.brokerProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isBrokerAadharSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isBrokerAadharSelect = false
        //
        //                self.brokerAadharButton.setImage(pickedImage, for: .normal)
        //                self.brokerAadharButton.imageView?.contentMode = .scaleAspectFill
        //                self.brokerAadharButton.layer.masksToBounds = true
        //
        //                self.brokerAadharImage = pickedImage
        //
        //                self.brokerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isBrokerPanSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isBrokerPanSelect = false
        //
        //                self.brokerPanButton.setImage(pickedImage, for: .normal)
        //                self.brokerPanButton.imageView?.contentMode = .scaleAspectFill
        //                self.brokerPanButton.layer.masksToBounds = true
        //
        //                self.brokerPanImage = pickedImage
        //
        //                self.brokerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isManagerProfileSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isManagerProfileSelect = false
        //
        //                self.managerProfileImgView.image = pickedImage
        //
        //                self.managerProfileImage = pickedImage
        //
        //                self.managerProfileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isManagerAadharSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isManagerAadharSelect = false
        //
        //                self.managerAadharImage = pickedImage
        //
        //                self.managerAadharButton.setImage(pickedImage, for: .normal)
        //                self.managerAadharButton.imageView?.contentMode = .scaleAspectFill
        //                self.managerAadharButton.layer.masksToBounds = true
        //
        //                self.managerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else if self.isManagerPanSelect
        //        {
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                self.isManagerPanSelect = false
        //
        //                self.managerPanButton.setImage(pickedImage, for: .normal)
        //                self.managerPanButton.imageView?.contentMode = .scaleAspectFill
        //                self.managerPanButton.layer.masksToBounds = true
        //
        //                self.managerPanImage = pickedImage
        //
        //                self.managerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //            }
        //        }
        //        else
        //        {
        //            self.propertyDocImageArray?.removeAllObjects()
        //
        //            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //            {
        //                if self.isRegistrationSelect
        //                {
        //                    self.registrationImage = pickedImage
        //                    self.registrationButton.setImage(pickedImage, for: .normal)
        //                    self.registrationButton.imageView?.contentMode = .scaleAspectFill
        //
        //                    self.registrationButton.layer.masksToBounds = true
        //
        //                    self.registrationDocPath = Constants.registrationImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //                }
        //                else if self.isLightBillSelect
        //                {
        //                    self.lightBillImage = pickedImage
        //                    self.lightBillButton.setImage(pickedImage, for: .normal)
        //                    self.lightBillButton.imageView?.contentMode = .scaleAspectFill
        //                    self.lightBillButton.layer.masksToBounds = true
        //
        //                    self.lightBillDocPath = Constants.lightBillImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //                }
        //                else if self.isOwnerId
        //                {
        //                    self.ownerIdImage = pickedImage
        //                    self.ownerIdButton.setImage(pickedImage, for: .normal)
        //                    self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
        //                    self.ownerIdButton.layer.masksToBounds = true
        //
        //                    self.ownerIdDocPath = Constants.ownerIdImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
        //                }
        //            }
        //        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
//        self.isPropertyPhotoSelect = false
        
//        self.isRegistrationSelect = false
//        self.isLightBillSelect = false
//        self.isOwnerId = false
        
//        self.isTenantProfileSelect = false
//        self.isTenantAadharSelect = false
//        self.isTenantPanSelect = false
        
        self.isLeaseAgreementSelect = false
        self.isLeasePoliceVerificationSelect = false
        self.isLeaseNOCSelect = false
        
//        self.isBrokerProfileSelect = false
//        self.isBrokerAadharSelect = false
//        self.isBrokerPanSelect = false
        
//        self.isManagerProfileSelect = false
//        self.isManagerAadharSelect = false
//        self.isManagerPanSelect = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])
    {
        if urls.first != nil
        {
            if self.isLeaseAgreementSelect
            {
                self.isLeaseAgreementSelect = false
                
                self.leaseAgreementImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.leaseAgreementButton.setImage(self.leaseAgreementImage, for: .normal)
                self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
                self.leaseAgreementButton.layer.masksToBounds = true
                
                self.leaseAgreementImagePath = Constants.agreementFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLeasePoliceVerificationSelect
            {
                self.isLeasePoliceVerificationSelect = false
                
                self.leasePoliceVerificationImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.leasePoliceVerificationButton.setImage(self.leasePoliceVerificationImage, for: .normal)
                self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
                self.leasePoliceVerificationButton.layer.masksToBounds = true
                
                self.leasePoliceVerificationImagePath = Constants.policeVerificationFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLeaseNOCSelect
            {
                self.isLeaseNOCSelect = false
                
                self.leaseNOCImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.leaseSocietyNocButton.setImage(self.leaseNOCImage, for: .normal)
                self.leaseSocietyNocButton.imageView?.contentMode = .scaleAspectFill
                self.leaseSocietyNocButton.layer.masksToBounds = true
                
                self.leaseNOCImagePath = Constants.nocFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            
            //            if self.isRegistrationSelect
            //            {
            //                self.registrationImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //                self.registrationButton.setImage(self.registrationImage, for: .normal)
            //                self.registrationButton.imageView?.contentMode = .scaleAspectFill
            //                self.registrationButton.layer.masksToBounds = true
            //                self.registrationDocPath = Constants.registrationImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isLightBillSelect
            //            {
            //                self.lightBillImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //                self.lightBillButton.setImage(self.lightBillImage, for: .normal)
            //                self.lightBillButton.imageView?.contentMode = .scaleAspectFill
            //                self.lightBillButton.layer.masksToBounds = true
            //
            //                self.lightBillDocPath = Constants.lightBillImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isOwnerId
            //            {
            //                self.ownerIdImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //                self.ownerIdButton.setImage(self.ownerIdImage, for: .normal)
            //                self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
            //                self.ownerIdButton.layer.masksToBounds = true
            //
            //                self.ownerIdDocPath = Constants.ownerIdImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isPropertyPhotoSelect
            //            {
            //                self.uploadPhotosLabel.text = "Add another photos"
            //                self.uploadPhotosLabelWidth.constant = 195
            //                self.isPropertyPhotoSelect = false
            //                self.propertyPhotosNameArray?.add(Constants.propertyImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png")
            //                self.propertyPhotosArray?.add(AppUtility.drawPDFfromURL(url: urls.first!)!)
            //
            //
            //                DispatchQueue.main.async {
            //
            //                    if self.propertyPhotosArray!.count > 0
            //                    {
            //                        self.propertyPhotoHeightConstraint.constant = 120
            //                        self.mainScrollView.contentSize.height = 2000
            //                        self.newPropertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 2000)
            //                    }
            //
            //                    self.propertyPhotosCollection.reloadData()
            //                }
            //            }
            //            else if self.isTenantAadharSelect
            //            {
            //                self.isTenantAadharSelect = false
            //
            //                self.tenantAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.tenantAadharButton.setImage(self.tenantAadharImage, for: .normal)
            //                self.tenantAadharButton.imageView?.contentMode = .scaleAspectFill
            //                self.tenantAadharButton.layer.masksToBounds = true
            //
            //                self.tenantAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isTenantPanSelect
            //            {
            //                self.isTenantPanSelect = false
            //
            //                self.tenantPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.tenantPanButton.setImage(self.tenantPanImage, for: .normal)
            //                self.tenantPanButton.imageView?.contentMode = .scaleAspectFill
            //                self.tenantPanButton.layer.masksToBounds = true
            //
            //                self.tenantPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isLeaseAgreementSelect
            //            {
            //                self.isLeaseAgreementSelect = false
            //
            //                self.leaseAgreementImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.leaseAgreementButton.setImage(self.leaseAgreementImage, for: .normal)
            //                self.leaseAgreementButton.imageView?.contentMode = .scaleAspectFill
            //                self.leaseAgreementButton.layer.masksToBounds = true
            //
            //                self.leaseAgreementImagePath = Constants.agreementFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isLeasePoliceVerificationSelect
            //            {
            //                self.isLeasePoliceVerificationSelect = false
            //
            //                self.leasePoliceVerificationImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.leasePoliceVerificationButton.setImage(self.leasePoliceVerificationImage, for: .normal)
            //                self.leasePoliceVerificationButton.imageView?.contentMode = .scaleAspectFill
            //                self.leasePoliceVerificationButton.layer.masksToBounds = true
            //
            //                self.leasePoliceVerificationImagePath = Constants.policeVerificationFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isLeaseNOCSelect
            //            {
            //                self.isLeaseNOCSelect = false
            //
            //                self.leaseNOCImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.leaseNOCButton.setImage(self.leaseNOCImage, for: .normal)
            //                self.leaseNOCButton.imageView?.contentMode = .scaleAspectFill
            //                self.leaseNOCButton.layer.masksToBounds = true
            //
            //                self.leaseNOCImagePath = Constants.nocFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isBrokerAadharSelect
            //            {
            //                self.isBrokerAadharSelect = false
            //
            //                self.brokerAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.brokerAadharButton.setImage(self.brokerAadharImage, for: .normal)
            //                self.brokerAadharButton.imageView?.contentMode = .scaleAspectFill
            //                self.brokerAadharButton.layer.masksToBounds = true
            //
            //                self.brokerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isBrokerPanSelect
            //            {
            //                self.isBrokerPanSelect = false
            //
            //                self.brokerPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.brokerPanButton.setImage(self.brokerPanImage, for: .normal)
            //                self.brokerPanButton.imageView?.contentMode = .scaleAspectFill
            //                self.brokerPanButton.layer.masksToBounds = true
            //
            //                self.brokerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isManagerAadharSelect
            //            {
            //                self.isManagerAadharSelect = false
            //
            //                self.managerAadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.managerAadharButton.setImage(self.managerAadharImage, for: .normal)
            //                self.managerAadharButton.imageView?.contentMode = .scaleAspectFill
            //                self.managerAadharButton.layer.masksToBounds = true
            //
            //                self.managerAadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            //            else if self.isManagerPanSelect
            //            {
            //                self.isManagerPanSelect = false
            //
            //                self.managerPanImage = AppUtility.drawPDFfromURL(url: urls.first!)
            //
            //                self.managerPanButton.setImage(self.managerPanImage, for: .normal)
            //                self.managerPanButton.imageView?.contentMode = .scaleAspectFill
            //                self.managerPanButton.layer.masksToBounds = true
            //
            //                self.managerPanImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            //            }
            
        }
    }
    
    //MARK: - TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.txtLeaseStartDate
        {
//            self.txtLeaseStartDate.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
           
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
            
            datePicker.show("Lease Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtLeaseStartDate.text = "\(formatter.string(from: dt))"
                    self.txtRentStartDate.text = "\(formatter.string(from: dt))"
                    
                    /////
                    self.selectedLeaseStartDate = dt
                    let diffInDays = Calendar.current.dateComponents([.day], from: dt, to: dt).day
                    self.txtRentFreePeriod.text = "\(diffInDays ?? 0)"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        else if textField == self.txtRentStartDate
        {
//            self.txtLeaseRentStartDate.isEnabled = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            var futureDateComponents = DateComponents()
            futureDateComponents.year = 100
            let futureDates = Calendar.current.date(byAdding: futureDateComponents, to: currentDate)
        
            
            datePicker.show("Lease Date Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: futureDates, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtRentStartDate.text = "\(formatter.string(from: dt))"
                    
                    self.selectedLeaseRentStartDate = dt
                    let diffInDays = Calendar.current.dateComponents([.day], from: self.selectedLeaseStartDate, to: self.selectedLeaseRentStartDate).day
                    
                    self.txtRentFreePeriod.text = "\(diffInDays ?? 0)"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        return true
    }
    
    //MARK: - Upload Registration Images
    func uploadImageToServer(uploadImg : UIImage, imageName : String)
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: uploadImg, imageName: "\(imageName)")
            
            req.done { (url) in
                print("Success URL - \(imageName) : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    func getLockinEndDate(dateStr : String, noOfDays : String) -> String
    {
        if dateStr.isEmpty == true
        {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateObj : Date = formatter.date(from: dateStr)!
        
        var dateComponents = DateComponents()
        dateComponents.month = Int(noOfDays)
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: dateObj)
        
        print("Selected Date : \(formatter.string(from: threeMonthAgo!))")
        
        return "\(formatter.string(from: threeMonthAgo!))"
    }
    
    //MARK: - Update Lease Agreement
    func updateLeaseAgreement()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.leaseAgreementImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.leaseAgreementImage!, imageName: "\(self.leaseAgreementImagePath)")
            }

            if self.leasePoliceVerificationImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.leasePoliceVerificationImage!, imageName: "\(self.leasePoliceVerificationImagePath)")
            }

            if self.leaseNOCImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.leaseNOCImage!, imageName: "\(self.leaseNOCImagePath)")
            }
            
            KRProgressHUD.show()
                
            self.leaseCompanyInfoArray.append("\(self.txtLeaseCompanyName.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtAddress.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtContactPerson.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtDesignation.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtContactNumber.text ?? "")")
            self.leaseCompanyInfoArray.append("\(self.txtEmail.text ?? "")")
            
            self.leaseDurationArray.append("\(self.leaseYearButton.currentTitle ?? ""),\(self.leaseMonthButton.currentTitle ?? "")")
            
            self.leaseEscalationArray.append("\(self.leaseEscalationYearButton.currentTitle ?? ""),\(self.leaseEscalationMonthButton.currentTitle ?? "")")
            
            let totalYears = Int(self.leaseYearButton.currentTitle ?? "")! * 365
            let totalMonths = Int(self.leaseMonthButton.currentTitle ?? "")! * 30
            let totalDays : Int = totalYears + totalMonths
            
            let escPerAmt = ((Int64(self.txtMonthlyRentAmt.text ?? "") ?? 0) * Int64(self.leaseEscalationYearButton.currentTitle ?? "")! )/100
            let escAmt : Int64 = ((Int64(self.txtMonthlyRentAmt.text ?? "") ?? 0) + escPerAmt)
            
            var finalAmount : Int64 = 0
            if self.gstToggle.isOn
            {
                let gstValue = ((Int64(self.txtMonthlyRentAmt.text ?? "") ?? 0) * 18)/100
                
                finalAmount = gstValue + (Int64(self.txtMonthlyRentAmt.text ?? "") ?? 0)
            }
            else
            {
                finalAmount = (Int64(self.txtMonthlyRentAmt.text ?? "") ?? 0)
            }
            
            let paramDict = [
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "property_id":(Int64("\(self.leaseDict?.value(forKey: "property_id") ?? "")")) as Any,//(Int64(self.propertyIdStr) ?? 0), //Long ADD DATA
                "lease_type":"\(self.leaseTypeStr)", //String
                "companyinfo":self.leaseCompanyInfoArray, //String Array
                "occupancy_type":"\(self.occupancyTypeStr)", //String
                "tenant_firstname":"", //String
                "tenant_lastname":"", //String
                "duration":self.leaseDurationArray, //String Array
                "start_date":"\(self.txtLeaseStartDate.text ?? "")", //String
                "rent_free_period":"\(self.txtRentFreePeriod.text ?? "")", //String
                "rent_start_date":"\(self.txtRentStartDate.text ?? "")", //String
                "monthly_rent":Int64(finalAmount) as Any, //Long
                "previous_rent":(Int64(0)) as Any, //Long
                "isRentEscalated":"0", //String
                "security_deposit_amt":(Int64(self.txtSecurityDeposite.text ?? "") ?? 0) as Any, //Long
                //"security_deposit_mnth":"", //Long
                "rent_due_date":(Int64(self.leaseRentDueDateStr) ?? 0) as Any, //Long
                "escalation_percent":(Int64(self.leaseEscalationPercentageButton.currentTitle ?? "") ?? 0) as Any, //Long
                "escalation_month":self.leaseEscalationArray, //String Array
                "owner_lockin":(Int64(self.leaseOwnerLockinButton.currentTitle ?? "") ?? 0) as Any, //Long
                "tenant_lockin":(Int64(self.leaseTenantLockinButton.currentTitle ?? "") ?? 0) as Any, //Long
                "notice_period":(Int64(self.leaseNoticePeriodButton.currentTitle ?? "") ?? 0) as Any, //Long
                "rent_doc":"\(self.leaseAgreementImagePath)", //String
                "police_verf_doc":"\(self.leasePoliceVerificationImagePath)", //String
                "society_noc_doc":"\(self.leaseNOCImagePath)", //String
                "totalDays":totalDays, //Int
                "gst":self.applyGST, //Int
                "property_progress_state":80, //Int
                "escalationAmount":"\(escAmt)", //String
                "owner_lockin_end":"\(self.getLockinEndDate(dateStr: "\(self.txtLeaseStartDate.text ?? "")", noOfDays: "\(self.leaseOwnerLockinButton.currentTitle ?? "")"))", //String
                "tenant_lockin_end":"\(self.getLockinEndDate(dateStr: "\(self.txtLeaseStartDate.text ?? "")", noOfDays: "\(self.leaseTenantLockinButton.currentTitle ?? "")"))", //String
            ] as [String : Any]
            
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updateRentContract", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
}
