//
//  PeopleInvolvedViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 25/02/21.
//

import UIKit
import SDWebImage
import KRProgressHUD
import MessageUI

class PeopleInvolvedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, OTPVerificationDelegate, MFMessageComposeViewControllerDelegate
{
    var ownerIsVerified = "0"
    
    var peopleInvolvedArray : NSMutableArray?
    var detailDict : NSDictionary?
    
    @IBOutlet weak var tableViewObj: UITableView!
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        let peopleInvolvedNib = UINib(nibName: "PeopleInvolvedTableViewCell", bundle: nil)
    
    
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.separatorColor = .clear
        self.tableViewObj.backgroundColor = .clear
        
        self.tableViewObj!.register(peopleInvolvedNib, forCellReuseIdentifier: "peopleInvolvedCell")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.getPropertyDetail()
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func verifyViaOTPButtonClicked(sender : UIButton)
    {
        print("verifyViaOTPButtonClicked : Tag - \(sender.tag)")
        
        // Sign in using the verificationID and the code sent to the user
        let verifyVC = VerifyPhoneViewController(nibName: "VerifyPhoneViewController", bundle: nil)
        verifyVC.otpVerificationDelegate = self
        verifyVC.verificationIdStr = ""
        verifyVC.phoneNumberStr = "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")"
        verifyVC.propertyName = "\(detailDict?.value(forKey: "propertyName") ?? "")"
        verifyVC.contactId = "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_id") ?? "")"
        verifyVC.contactType = "\(detailDict?.value(forKey: "contact_type") ?? "")"
        verifyVC.modalPresentationStyle = .overCurrentContext
        verifyVC.modalTransitionStyle = .flipHorizontal
        self.present(verifyVC, animated: true, completion: nil)
    }
    
    @objc func callButtonClicked(sender : UIButton)
    {
        print("callButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")".isEmpty != true
        {
            let myUrl = "tel://\("\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")")"
            
            if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty
            {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Mobile Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func emailButtonClicked(sender : UIButton)
    {
        print("emailButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "email") ?? "")".isEmpty != true
        {
            if let emailUrl = self.createEmailUrl(to: "\("\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "email") ?? "")")", subject: "", body: "")
            {
                UIApplication.shared.open(emailUrl)
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Email Id Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func msgButtonClicked(sender : UIButton)
    {
        print("msgButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")".isEmpty != true
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                DispatchQueue.main.async {
                    let recipients:[String] = ["\("\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "mobileno") ?? "")")"]
                    let messageController = MFMessageComposeViewController()
                    messageController.messageComposeDelegate  = self
                    messageController.recipients = recipients
                    messageController.body = ""
                    self.present(messageController, animated: true, completion: nil)
                }
            }
            else
            {
                print("handle text messaging not available")
                //handle text messaging not available
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "Error", message: "Broker's Phone Number Not Available", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func editButtonClicked(sender : UIButton)
    {
        print("editButtonClicked")
        
        if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
        {
            let editTenantVC = AddTenantViewController(nibName: "AddTenantViewController", bundle: nil)
            editTenantVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            editTenantVC.contactTypeStr = "Owner"
            editTenantVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            editTenantVC.isEditTenant = true
            self.navigationController?.pushViewController(editTenantVC, animated: true)
        }
        else if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Broker"
        {
            let editBrokerVC = AddBrokerViewController(nibName: "AddBrokerViewController", bundle: nil)
            editBrokerVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            editBrokerVC.isEditBroker = true
            editBrokerVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            editBrokerVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            self.navigationController?.pushViewController(editBrokerVC, animated: true)
        }
        else if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
        {
            let editTenantVC = AddTenantViewController(nibName: "AddTenantViewController", bundle: nil)
            editTenantVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            editTenantVC.contactTypeStr = "Tenant"
            editTenantVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            editTenantVC.isEditTenant = true
            self.navigationController?.pushViewController(editTenantVC, animated: true)
        }
        else if "\((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "contact_type") ?? "")" == "Property Manager"
        {
            let addPropertyVC = AddPropertyManagerViewController(nibName: "AddPropertyManagerViewController", bundle: nil)
            addPropertyVC.isEditPM = true
            addPropertyVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            addPropertyVC.propertyIdStr = "\(self.detailDict?.value(forKey: "property_id") ?? "")"
            addPropertyVC.propertyName = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            self.navigationController?.pushViewController(addPropertyVC, animated: true)
        }
        
        
    }
    
    @objc func deleteButtonClicked(sender : UIButton)
    {
        print("deleteButtonClicked")
        
        let alertViewController = UIAlertController(title: "Delete", message: "Do You Want To Delete \((self.peopleInvolvedArray?.object(at: sender.tag) as AnyObject).value(forKey: "firstName") ?? "")", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            
            print("Delete  -- Yes")
        }))
        
        alertViewController.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (alert: UIAlertAction!) in
            print("Delete  -- No")
        }))
        
        DispatchQueue.main.async {
            self .present(alertViewController, animated: true, completion: nil)
        }
    }
    
    @objc func detailButtonClicked(sender : UIButton)
    {
        print("detailButtonClicked")
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewObj)
        let indexPath = self.tableViewObj!.indexPathForRow(at: buttonPosition)
        
        if indexPath != nil
        {
            let myCell = (self.tableViewObj!.cellForRow(at: indexPath!) as? PeopleInvolvedTableViewCell)
            
            let userInfoVC = UserInfoViewController(nibName: "UserInfoViewController", bundle: nil)
            userInfoVC.detailDict = (self.peopleInvolvedArray?.object(at: sender.tag) as! NSDictionary)
            userInfoVC.isHideCall = (myCell?.callButton.isHidden ?? false)
            userInfoVC.isHideMsg = (myCell?.msgButton.isHidden ?? false)
            userInfoVC.isHideMail = (myCell?.emailButton.isHidden ?? false)
            self.navigationController?.pushViewController(userInfoVC, animated: true)
        }
        
    }
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.peopleInvolvedArray != nil
        {
            if self.peopleInvolvedArray!.count > 0
            {
                return self.peopleInvolvedArray!.count
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 180
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "peopleInvolvedCell", for: indexPath) as! PeopleInvolvedTableViewCell
        
        DispatchQueue.main.async {
            myCell.selectionStyle = .none
            myCell.backgroundColor = Constants.bgColor
            myCell.mainView.backgroundColor = .white
            myCell.mainView.layer.cornerRadius = 15
            
            myCell.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath+"/"+"\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
            myCell.profileImageView.contentMode = .scaleAspectFill
            myCell.profileImageView.clipsToBounds = true
            myCell.profileImageView.layer.cornerRadius = 8
            myCell.profileImageView.layer.masksToBounds = true
            
            myCell.contactTypeLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")"
            
            myCell.nameLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "firstName") ?? "") \((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "lastName") ?? "")"
            
            myCell.mobileLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "mobileno") ?? "")"
            
            myCell.emailLabel.text = "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "email") ?? "")"
            
            if "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
            {
                myCell.verifyButton.isHidden = true
                myCell.verifiedImageView.isHidden = false
                myCell.verifiedLabel.isHidden = false
            }
            else
            {
                myCell.verifyButton.isHidden = false
                myCell.verifiedImageView.isHidden = true
                myCell.verifiedLabel.isHidden = true
            }
            
            myCell.verifyButton.tag = indexPath.row
            myCell.verifyButton.addTarget(self, action: #selector(self.verifyViaOTPButtonClicked(sender:)), for: .touchUpInside)
        }
        
        if "\((self.peopleInvolvedArray?.object(at: indexPath.row) as AnyObject).value(forKey: "mobileno") ?? "")" == "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        {
            myCell.callButton.isHidden = true
            myCell.msgButton.isHidden = true
            myCell.emailButton.isHidden = true
            myCell.editButton.isHidden = true
            myCell.deleteButton.isHidden = true
        }
        else if self.ownerIsVerified == "1" && "\(self.detailDict?.value(forKey: "ownerNumber") ?? "")" == "\(AppUtility.getUserData(keyVal: "mobileNo"))"
        {
            myCell.callButton.isHidden = false
            myCell.msgButton.isHidden = false
            myCell.emailButton.isHidden = false
            myCell.editButton.isHidden = false
            myCell.deleteButton.isHidden = false
        }
        else if self.ownerIsVerified == "0" && (("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Owner" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Owner") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Broker" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Broker") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Tenant" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Tenant") || ("\(self.detailDict?.value(forKey: "created_by") ?? "")" == "Property Manager" && "\(self.detailDict?.value(forKey: "contact_type") ?? "")" == "Property Manager"))
        {
            myCell.callButton.isHidden = false
            myCell.msgButton.isHidden = false
            myCell.emailButton.isHidden = false
            myCell.editButton.isHidden = false
            myCell.deleteButton.isHidden = false
        }
        else
        {
            myCell.callButton.isHidden = false
            myCell.msgButton.isHidden = false
            myCell.emailButton.isHidden = false
            myCell.editButton.isHidden = true
            myCell.deleteButton.isHidden = true
        }
        
        myCell.callButton.tag = indexPath.row
        myCell.callButton.addTarget(self, action: #selector(self.callButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.msgButton.tag = indexPath.row
        myCell.msgButton.addTarget(self, action: #selector(self.msgButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.emailButton.tag = indexPath.row
        myCell.emailButton.addTarget(self, action: #selector(self.emailButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.editButton.tag = indexPath.row
        myCell.editButton.addTarget(self, action: #selector(self.editButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.deleteButton.tag = indexPath.row
        myCell.deleteButton.addTarget(self, action: #selector(self.deleteButtonClicked(sender:)), for: .touchUpInside)
        
        myCell.detailButton.tag = indexPath.row
        myCell.detailButton.addTarget(self, action: #selector(self.detailButtonClicked(sender:)), for: .touchUpInside)
        
        return myCell
    }
    
    //MARK: - Message Delegate
    func createEmailUrl(to: String, subject: String, body: String) -> URL?
    {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
//        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")

        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")

    
        if let gmailUrl = defaultUrl, UIApplication.shared.canOpenURL(defaultUrl!)
        {
            return gmailUrl
        }
        
        return URL(string: "")
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - OTP Verification Delegate
    func otpVerification()
    {
        print("otpVerification")
//        self.refreshPropertyDetailResponse()
    }
    
    //MARK: - Property Detail Info
    func getPropertyDetail()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0)
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getPropertycontacts", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Property Info Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableArray)
                        {
                            self.peopleInvolvedArray = NSMutableArray()
                            self.peopleInvolvedArray = responseObj
                            
                            for currentObj in self.peopleInvolvedArray!
                            {
                                if "\((currentObj as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner" && "\((currentObj as AnyObject).value(forKey: "is_verified") ?? "")" == "1"
                                {
                                    self.ownerIsVerified = "1"
                                }
                            }
                        
                            DispatchQueue.main.async {
                                self.tableViewObj?.reloadData()
                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
}
