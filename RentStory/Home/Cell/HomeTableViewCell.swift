//
//  HomeTableViewCell.swift
//  RentStory
//
//  Created by Hardik Pithadia on 05/11/20.
//

import UIKit

class HomeTableViewCell: UITableViewCell
{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblContactType: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var btnShare: UIButton!
   
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblTitleBrokerName: UILabel!
    @IBOutlet weak var lblBrokerName: UILabel!
    @IBOutlet weak var lblTitleTenantName: UILabel!
    
    @IBOutlet weak var lblTenantName: UILabel!
    
    @IBOutlet weak var lblTitleRentDueDate: UILabel!
    @IBOutlet weak var lblRentDueDate: UILabel!
    
    @IBOutlet weak var lblTitleRentAmount: UILabel!
    @IBOutlet weak var lblRentAmount: UILabel!
    
    @IBOutlet weak var lblGreen: UILabel!
    @IBOutlet weak var lblAvailableForRent: UILabel!
    
    @IBOutlet weak var lblGreenTop: NSLayoutConstraint!
    @IBOutlet weak var lblGreenHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAvailableTop: NSLayoutConstraint!
    @IBOutlet weak var lblAvailableHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var baseViewObj: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
