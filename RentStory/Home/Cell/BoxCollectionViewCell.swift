//
//  BoxCollectionViewCell.swift
//  RentStory
//
//  Created by Hardik Pithadia on 16/04/21.
//

import UIKit

class BoxCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageViewObj: UIImageView!
    @IBOutlet weak var baseView: UIView!
 
    @IBOutlet weak var buttonObj: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imageLeading: NSLayoutConstraint!
    @IBOutlet weak var imageTop: NSLayoutConstraint!
    @IBOutlet weak var imageBottom: NSLayoutConstraint!
    @IBOutlet weak var imageTrailing: NSLayoutConstraint!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
}
