//
//  PropertyInfoCollectionViewCell.swift
//  RentStory
//
//  Created by Hardik Pithadia on 24/11/20.
//

import UIKit

class PropertyInfoCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var imageViewObj: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var buttonObj: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

}
