//
//  EditPropertyDetailViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 18/02/21.
//

import UIKit
import SDWebImage
import KRProgressHUD
import CoreLocation
import MapKit
import AddressBook
import AddressBookUI
import GooglePlacesSearchController
import MobileCoreServices

class EditPropertyDetailViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate, SelectedAreaDelegate, UIDocumentPickerDelegate
{
    var propertyTypeStr = "Residential"
    var propertySubType = "Flat"
    
    var selectedLatValue = ""
    var selectedLonValue = ""
    var noOfRoomsStr = ""
    var noOfBathroomsStr = ""
    var builtUpAreaStr = ""
    var carpetAreaStr = ""
    var furnishedTypeStr = "Furnished"
    var noOfCarParkStr = ""
    var amenitiesStr = ""
    var selectedFloor = "0"
    
    var registrationImage : UIImage?
    var registrationDocPath = ""
    
    var lightBillImage : UIImage?
    var lightBillDocPath = ""
    
    var ownerIdImage : UIImage?
    var ownerIdDocPath = ""
    
    var isRegistrationSelect = false
    var isLightBillSelect = false
    var isOwnerId = false
    
    var isPropertyPhotoSelect = false
    
    var annotation : MKPointAnnotation?
    var locationManager: CLLocationManager!
    
    var propertyImageNames = NSMutableArray()
    
    var newPropertyTypeArray : NSMutableArray?
    var selectedNewPropertyTypeArray : NSMutableArray?
    
    var bedroomArray : NSMutableArray?
    var selectedBedroomArray : NSMutableArray?
    
    var bathroomArray : NSMutableArray?
    var selectedBathroomArray : NSMutableArray?
    
    var carParkArray : NSMutableArray?
    var selectedCarParkArray : NSMutableArray?
    
    var amenitiesArray : NSMutableArray?
    var amenitiesImageArray : NSMutableArray?
    var selectedAmenitiesArray : NSMutableArray?
    
    var propertyDocArray : NSMutableArray?
    var propertyDocImageArray : NSMutableArray?
    var propertyPhotosArray : NSMutableArray?
    var propertyPhotosNameArray : NSMutableArray?
    
    var detailDict : NSDictionary?
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    @IBOutlet var locationView: UIView!
    @IBOutlet var propertyView: UIView!
    
    @IBOutlet weak var txtPropertyName: UITextField!
    
    @IBOutlet weak var residentialButton: UIButton!
    
    @IBOutlet weak var commercialButton: UIButton!
    
    
    @IBOutlet weak var propertyTypeCollection: UICollectionView!
    @IBOutlet weak var bedroomCollection: UICollectionView!
    
    @IBOutlet weak var lblBedRoomHeight: NSLayoutConstraint!
    @IBOutlet weak var lblBedRoomTop: NSLayoutConstraint!
    @IBOutlet weak var bedroomCollectionTop: NSLayoutConstraint!
    
    @IBOutlet weak var bedroomCollectionHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var bathRoomCollection: UICollectionView!
    @IBOutlet weak var lblBathRoomTop: NSLayoutConstraint!
    @IBOutlet weak var lblBathRoomHeight: NSLayoutConstraint!
    @IBOutlet weak var bathRoomCollectionTop: NSLayoutConstraint!
    @IBOutlet weak var bathRoomCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtBuiltUpArea: UITextField!
    @IBOutlet weak var txtCarpetArea: UITextField!
    
    @IBOutlet weak var furnishedButton: UIButton!
    @IBOutlet weak var nonFurnishedButton: UIButton!
    @IBOutlet weak var partialButton: UIButton!
    
    @IBOutlet weak var carParkCollection: UICollectionView!
    
    @IBOutlet weak var amenitiesCollection: UICollectionView!
    
    @IBOutlet weak var txtOtherAmenities: UITextField!
    @IBOutlet weak var amenitiesTop: NSLayoutConstraint!
    
    @IBOutlet weak var amenitiesHeight: NSLayoutConstraint!
    
    @IBOutlet weak var propertyPhotoHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtFlatNo: UITextField!
    
    @IBOutlet weak var txtFloor: UITextField!
    
    @IBOutlet weak var txtWing: UITextField!
    
    @IBOutlet weak var txtBuildingName: UITextField!
    
    @IBOutlet weak var txtLocality: UITextField!
    
    @IBOutlet weak var txtAddress1: UITextField!
    
    
    @IBOutlet weak var txtAddress2: UITextField!
    
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var txtZipCode: UITextField!
    
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    
    @IBOutlet weak var propertyPhotosCollection: UICollectionView!
    
    @IBOutlet weak var uploadPhotosLabel: UILabel!
    
    @IBOutlet weak var uploadPhotosLabelWidth: NSLayoutConstraint!
    
    
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var lightBillButton: UIButton!
    @IBOutlet weak var ownerIdButton: UIButton!
    
    @IBOutlet weak var mapViewObj: MKMapView!
    
    @IBOutlet weak var propertyDocView: UIView!
    
    @IBOutlet weak var amenitiesNoteHeight: NSLayoutConstraint!
    
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.propertyView.backgroundColor = .clear
        self.propertyView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1450)
        self.mainScrollView.addSubview(self.propertyView)
        
        self.locationView.backgroundColor = .clear
        self.locationView.frame = CGRect(x: 0.0, y: self.propertyView.frame.origin.y + self.propertyView.frame .height, width: UIScreen.main.bounds.size.width, height: 1200)
        self.mainScrollView.addSubview(self.locationView)
        
        self.mainScrollView.contentSize.height = self.locationView.frame.origin.y + self.locationView.frame.height + 150
        
        
        self.txtPropertyName.delegate = self
        let txtPropertyNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtPropertyName.leftView = txtPropertyNameViewObj
        self.txtPropertyName.leftViewMode = .always
        
        self.txtBuiltUpArea.delegate = self
        let txtBuiltUpAreaViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBuiltUpArea.leftView = txtBuiltUpAreaViewObj
        self.txtBuiltUpArea.leftViewMode = .always
        
        self.txtCarpetArea.delegate = self
        let txtCarpetAreaViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCarpetArea.leftView = txtCarpetAreaViewObj
        self.txtCarpetArea.leftViewMode = .always
        
        self.txtOtherAmenities.delegate = self
        let txtOtherAmenitiesViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtOtherAmenities.leftView = txtOtherAmenitiesViewObj
        self.txtOtherAmenities.leftViewMode = .always
        
        self.txtFlatNo.delegate = self
        let txtFlatNoViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFlatNo.leftView = txtFlatNoViewObj
        self.txtFlatNo.leftViewMode = .always
        
        self.txtFloor.delegate = self
        let txtFloorViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFloor.leftView = txtFloorViewObj
        self.txtFloor.leftViewMode = .always
        
        self.txtWing.delegate = self
        let txtWingViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtWing.leftView = txtWingViewObj
        self.txtWing.leftViewMode = .always
        
        self.txtBuildingName.delegate = self
        let txtBuildingNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtBuildingName.leftView = txtBuildingNameViewObj
        self.txtBuildingName.leftViewMode = .always
        
        self.txtLocality.delegate = self
        let txtLocalityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLocality.leftView = txtLocalityViewObj
        self.txtLocality.leftViewMode = .always
        
        self.txtAddress1.delegate = self
        let txtAddress1ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress1.leftView = txtAddress1ViewObj
        self.txtAddress1.leftViewMode = .always
        
        self.txtAddress2.delegate = self
        let txtAddress2ViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAddress2.leftView = txtAddress2ViewObj
        self.txtAddress2.leftViewMode = .always
        
        self.txtCity.delegate = self
        let txtCityViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCity.leftView = txtCityViewObj
        self.txtCity.leftViewMode = .always
        
        self.txtZipCode.delegate = self
        let txtZipCodeViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtZipCode.leftView = txtZipCodeViewObj
        self.txtZipCode.leftViewMode = .always
        
        self.txtState.delegate = self
        let txtStateViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtState.leftView = txtStateViewObj
        self.txtState.leftViewMode = .always
        
        self.txtCountry.delegate = self
        let txtCountryViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtCountry.leftView = txtCountryViewObj
        self.txtCountry.leftViewMode = .always
        
        
        self.residentialButton.layer.cornerRadius = 15
        self.residentialButton.backgroundColor = Constants.darkBlueColor
        self.residentialButton.layer.masksToBounds = true
        
        self.commercialButton.layer.cornerRadius = 15
        self.commercialButton.backgroundColor = .white
        self.commercialButton.layer.masksToBounds = true
        
        self.newPropertyTypeArray = NSMutableArray()
        self.newPropertyTypeArray?.add("Flat")
        self.newPropertyTypeArray?.add("Duplex")
        self.newPropertyTypeArray?.add("Bunglow")
        self.newPropertyTypeArray?.add("Row House")
        
        self.selectedNewPropertyTypeArray = NSMutableArray()
        for _ in 0..<self.newPropertyTypeArray!.count
        {
            self.selectedNewPropertyTypeArray?.add("0")
        }
        self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
        
        self.propertyTypeCollection.dataSource = self
        self.propertyTypeCollection.delegate = self
        
        let propertyTypeCellNib = UINib(nibName: "NewPropertyTypeCollectionViewCell", bundle: nil)
        self.propertyTypeCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.propertyTypeCollection.showsHorizontalScrollIndicator = false
        
        self.bedroomArray = NSMutableArray()
        self.bedroomArray?.add("0")
        self.bedroomArray?.add("1")
        self.bedroomArray?.add("2")
        self.bedroomArray?.add("3")
        self.bedroomArray?.add("4")
        self.bedroomArray?.add("5")
        self.bedroomArray?.add("6")
        self.bedroomArray?.add("7")
        self.bedroomArray?.add("8")
        self.bedroomArray?.add("9")
        self.bedroomArray?.add("10")
        self.bedroomArray?.add("10+")
        
        self.selectedBedroomArray = NSMutableArray()
        for _ in 0..<self.bedroomArray!.count
        {
            self.selectedBedroomArray?.add("0")
        }
        
        self.bathroomArray = NSMutableArray()
        self.bathroomArray?.add("0")
        self.bathroomArray?.add("1")
        self.bathroomArray?.add("2")
        self.bathroomArray?.add("3")
        self.bathroomArray?.add("4")
        self.bathroomArray?.add("5")
        self.bathroomArray?.add("6")
        self.bathroomArray?.add("7")
        self.bathroomArray?.add("8")
        self.bathroomArray?.add("9")
        self.bathroomArray?.add("10")
        self.bathroomArray?.add("10+")
        
        self.selectedBathroomArray = NSMutableArray()
        for _ in 0..<self.bathroomArray!.count
        {
            self.selectedBathroomArray?.add("0")
        }
        
        self.carParkArray = NSMutableArray()
        self.carParkArray?.add("0")
        self.carParkArray?.add("1")
        self.carParkArray?.add("2")
        self.carParkArray?.add("3")
        self.carParkArray?.add("4")
        self.carParkArray?.add("5")
        self.carParkArray?.add("6")
        self.carParkArray?.add("7")
        self.carParkArray?.add("8")
        self.carParkArray?.add("9")
        self.carParkArray?.add("10")
        self.carParkArray?.add("10+")
        
        self.selectedCarParkArray = NSMutableArray()
        for _ in 0..<self.carParkArray!.count
        {
            self.selectedCarParkArray?.add("0")
        }
        
        self.amenitiesArray = NSMutableArray()
        self.amenitiesArray?.add("Swimming pool")
        self.amenitiesArray?.add("fitness")
        self.amenitiesArray?.add("Badminton Court")
        self.amenitiesArray?.add("Free Wifi")
        self.amenitiesArray?.add("Other")
        
        self.amenitiesImageArray = NSMutableArray()
        self.amenitiesImageArray?.add("swimmingPool")
        self.amenitiesImageArray?.add("fitness")
        self.amenitiesImageArray?.add("badminton")
        self.amenitiesImageArray?.add("wifi")
        self.amenitiesImageArray?.add("roundPlus")
        
        
        self.selectedAmenitiesArray = NSMutableArray()
        for _ in 0..<self.amenitiesArray!.count
        {
            self.selectedAmenitiesArray?.add("0")
        }
        
        self.bedroomCollection.dataSource = self
        self.bedroomCollection.delegate = self
        self.bedroomCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.bedroomCollection.showsHorizontalScrollIndicator = false
        
        self.bathRoomCollection.dataSource = self
        self.bathRoomCollection.delegate = self
        self.bathRoomCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.bathRoomCollection.showsHorizontalScrollIndicator = false
        
        
        self.carParkCollection.dataSource = self
        self.carParkCollection.delegate = self
        self.carParkCollection.register(propertyTypeCellNib, forCellWithReuseIdentifier: "newPropertyTypeCell")
        self.carParkCollection.showsHorizontalScrollIndicator = false
        
        self.amenitiesCollection.dataSource = self
        self.amenitiesCollection.delegate = self
        
        let amenitiesCollectionNib = UINib(nibName: "BoxCollectionViewCell", bundle: nil)
        self.amenitiesCollection.register(amenitiesCollectionNib, forCellWithReuseIdentifier: "boxCollectionCell")
        self.amenitiesCollection.showsHorizontalScrollIndicator = false
        
        self.propertyPhotosNameArray = NSMutableArray()
        self.propertyPhotosArray = NSMutableArray()
        self.propertyPhotoHeight.constant = 0
        
        self.propertyPhotosCollection.dataSource = self
        self.propertyPhotosCollection.delegate = self
        let propertyPhotoCollectionNib = UINib(nibName: "PropertyPhotosCollectionViewCell", bundle: nil)
        self.propertyPhotosCollection.register(propertyPhotoCollectionNib, forCellWithReuseIdentifier: "propertyPhotoCell")
        self.propertyPhotosCollection.showsHorizontalScrollIndicator = false
        
        self.displayPropertyDetails()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        DispatchQueue.main.async {
            self.propertyView.frame = CGRect(x: self.propertyView.frame.origin.x, y: self.propertyView.frame.origin.y, width: self.propertyView.frame.width, height: self.propertyDocView.frame.origin.y + self.propertyDocView.frame.height)
            
            self.locationView.frame = CGRect(x: 0.0, y: self.propertyView.frame.origin.y + self.propertyView.frame .height, width: UIScreen.main.bounds.size.width, height: self.locationView.frame.height)
            
            self.mainScrollView.contentSize.height = (self.locationView.frame.origin.y + self.locationView.frame.height)
        }
    }
    
    func displayPropertyDetails()
    {
        if self.detailDict != nil
        {
            self.txtPropertyName.text = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
            self.txtBuiltUpArea.text = "\(self.detailDict?.value(forKey: "builtup_area") ?? "")"
            self.txtCarpetArea.text = "\(self.detailDict?.value(forKey: "carpet_area") ?? "")"
            
            if "\(self.detailDict?.value(forKey: "property_type") ?? "")".isEmpty != true
            {
                if "\(self.detailDict?.value(forKey: "property_type") ?? "")".components(separatedBy: "-").count > 0
                {
                    if "\(self.detailDict?.value(forKey: "property_type") ?? "")".components(separatedBy: "-").first?.lowercased() == "residential"
                    {
                        self.lblBedRoomTop.constant = 15
                        self.lblBedRoomHeight.constant = 35
                        self.bedroomCollectionTop.constant = 7
                        self.bedroomCollectionHeight.constant = 60
            //
                        self.lblBathRoomTop.constant = 15
                        self.lblBathRoomHeight.constant = 35
                        self.bathRoomCollectionTop.constant = 7
                        self.bathRoomCollectionHeight.constant = 60
                        
                        self.propertyTypeStr = "Residential"
                        
                        self.propertySubType = ("\(self.detailDict?.value(forKey: "property_type") ?? "")".components(separatedBy: "-").last ?? "")
                        
                        self.residentialButton.backgroundColor = Constants.darkBlueColor
                        self.commercialButton.backgroundColor = .white
                        
                        self.newPropertyTypeArray?.removeAllObjects()

                        self.newPropertyTypeArray?.add("Flat")
                        self.newPropertyTypeArray?.add("Duplex")
                        self.newPropertyTypeArray?.add("Bungalow")
                        self.newPropertyTypeArray?.add("Row House")
                        self.newPropertyTypeArray?.add("Pent House")
                        
                        self.selectedNewPropertyTypeArray?.removeAllObjects()
                     
                        for _ in 0..<self.newPropertyTypeArray!.count
                        {
                            self.selectedNewPropertyTypeArray?.add("0")
                        }
                        
                        if self.propertySubType == "Flat"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
                        }
                        else if self.propertySubType == "Duplex"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 1, with: "1")
                        }
                        else if self.propertySubType == "Bungalow"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 2, with: "1")
                        }
                        else if self.propertySubType == "Row House"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 3, with: "3")
                        }
                        else if self.propertySubType == "Pent House"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 4, with: "1")
                        }
                        
                        DispatchQueue.main.async {
                            self.propertyTypeCollection.reloadData()
                        }
                    }
                    else
                    {
                        self.noOfRoomsStr = ""
                        self.noOfBathroomsStr = ""
            //
                        self.lblBedRoomTop.constant = 0
                        self.lblBedRoomHeight.constant = 0
                        self.bedroomCollectionTop.constant = 0
                        self.bedroomCollectionHeight.constant = 0

                        self.lblBathRoomTop.constant = 0
                        self.lblBathRoomHeight.constant = 0
                        self.bathRoomCollectionTop.constant = 0
                        self.bathRoomCollectionHeight.constant = 0
                        
                        self.propertyTypeStr = "Commercial"
                        self.propertySubType = ("\(self.detailDict?.value(forKey: "property_type") ?? "")".components(separatedBy: "-").last ?? "")
                        
                        self.residentialButton.backgroundColor = .white
                        self.residentialButton.setTitleColor(.lightGray, for: .normal)
                        
                        self.commercialButton.backgroundColor = Constants.darkBlueColor
                        
                        self.newPropertyTypeArray?.removeAllObjects()

                        self.newPropertyTypeArray?.add("Shop or Showroom")
                        self.newPropertyTypeArray?.add("Industrial")
                        self.newPropertyTypeArray?.add("Office")
                        self.newPropertyTypeArray?.add("IT")
                        
                        self.selectedNewPropertyTypeArray?.removeAllObjects()
                        for _ in 0..<self.newPropertyTypeArray!.count
                        {
                            self.selectedNewPropertyTypeArray?.add("0")
                        }
                        
                        if self.propertySubType == "Shop/Showroom" || self.propertySubType == "Shop or Showroom"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
                        }
                        else if self.propertySubType == "Industrial"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 1, with: "1")
                        }
                        else if self.propertySubType == "Office"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 2, with: "1")
                        }
                        else if self.propertySubType == "IT"
                        {
                            self.selectedNewPropertyTypeArray?.replaceObject(at: 3, with: "1")
                        }
                        
                        DispatchQueue.main.async {
                            self.propertyTypeCollection.reloadData()
                        }
                    }
                    
                }
            }
            
            for i in 0..<self.bedroomArray!.count
            {
                if "\(i)" == "\(self.detailDict?.value(forKey: "bedroom") ?? "")"
                {
                    self.selectedBedroomArray?.replaceObject(at: i, with: "1")
                    break
                }
            }
            DispatchQueue.main.async {
                self.bedroomCollection.reloadData()
            }
            
            self.noOfRoomsStr = "\(self.detailDict?.value(forKey: "bedroom") ?? "")"
            
            for i in 0..<self.bathroomArray!.count
            {
                if "\(i)" == "\(self.detailDict?.value(forKey: "bathroom") ?? "")"
                {
                    self.selectedBathroomArray?.replaceObject(at: i, with: "1")
                    break
                }
            }
            DispatchQueue.main.async {
                self.bathRoomCollection.reloadData()
            }
            
            self.noOfBathroomsStr = "\(self.detailDict?.value(forKey: "bathroom") ?? "")"
            
            if "\(self.detailDict?.value(forKey: "furnished_type") ?? "")" == "Furnished"
            {
                self.furnishedTypeStr = "\(self.detailDict?.value(forKey: "furnished_type") ?? "")"
                
                self.furnishedButton.backgroundColor = Constants.darkBlueColor
                self.furnishedButton.setTitleColor(.white, for: .normal)
                
                self.nonFurnishedButton.backgroundColor = .white
                self.nonFurnishedButton.setTitleColor(.lightGray, for: .normal)
                
                self.partialButton.backgroundColor = .white
                self.partialButton.setTitleColor(.lightGray, for: .normal)
            }
            else if "\(self.detailDict?.value(forKey: "furnished_type") ?? "")" == "Non Furnished"
            {
                self.furnishedTypeStr = "\(self.detailDict?.value(forKey: "furnished_type") ?? "")"
                
                self.furnishedButton.backgroundColor = .white
                self.furnishedButton.setTitleColor(.lightGray, for: .normal)
                
                self.nonFurnishedButton.backgroundColor = Constants.darkBlueColor
                self.nonFurnishedButton.setTitleColor(.white, for: .normal)
                
                self.partialButton.backgroundColor = .white
                self.partialButton.setTitleColor(.lightGray, for: .normal)
            }
            else
            {
                self.furnishedTypeStr = "\(self.detailDict?.value(forKey: "furnished_type") ?? "")"
                
                self.furnishedButton.backgroundColor = .white
                self.furnishedButton.setTitleColor(.lightGray, for: .normal)
                
                self.nonFurnishedButton.backgroundColor = .white
                self.nonFurnishedButton.setTitleColor(.lightGray, for: .normal)
                
                self.partialButton.backgroundColor = Constants.darkBlueColor
                self.partialButton.setTitleColor(.white, for: .normal)
            }
            
            for i in 0..<self.carParkArray!.count
            {
                if "\(i)" == "\(self.detailDict?.value(forKey: "parking") ?? "")"
                {
                    self.selectedCarParkArray?.replaceObject(at: i, with: "1")
                    break
                }
            }
            DispatchQueue.main.async {
                self.carParkCollection.reloadData()
            }
            
            self.noOfCarParkStr = "\(self.detailDict?.value(forKey: "parking") ?? "")"
            
            var tempAmenitiesStr = ""
            
            for currentObj in "\(self.detailDict?.value(forKey: "Amenities") ?? "")".components(separatedBy: ",")
            {
                print(currentObj)
                
                if "\(currentObj)".lowercased() == "swimming pool"
                {
                    self.selectedAmenitiesArray?.replaceObject(at: 0, with: "1")
                }
                else if "\(currentObj)".lowercased() == "fitness"
                {
                    self.selectedAmenitiesArray?.replaceObject(at: 1, with: "1")
                }
                else if "\(currentObj)".lowercased() == "badminton court"
                {
                    self.selectedAmenitiesArray?.replaceObject(at: 2, with: "1")
                }
                else if "\(currentObj)".lowercased() == "free wifi"
                {
                    self.selectedAmenitiesArray?.replaceObject(at: 3, with: "1")
                }
                else //if "\(currentObj)".lowercased() == "other"
                {
                    self.amenitiesTop.constant = 10
                    self.amenitiesHeight.constant = 50
                    self.amenitiesNoteHeight.constant = 15
                    
                    tempAmenitiesStr = tempAmenitiesStr + "\(currentObj),"
                    
                    if tempAmenitiesStr.isEmpty != true
                    {
                        self.txtOtherAmenities.text = String(tempAmenitiesStr.prefix(tempAmenitiesStr.count-1))
                    }
                    
                    
//                    self.txtOtherAmenities.text = (self.txtOtherAmenities.text?.isEmpty == true) ? "\(currentObj)" : (self.txtOtherAmenities.text! + ",\(currentObj)")
                    
                    self.selectedAmenitiesArray?.replaceObject(at: 4, with: "1")
                }
            }
            
            
            self.amenitiesStr = "\(self.detailDict?.value(forKey: "Amenities") ?? "")"
            
            DispatchQueue.main.async {
                self.amenitiesCollection.reloadData()
            }
            
            if "\(self.detailDict?.value(forKey: "prop_photo") ?? "")".isEmpty != true
            {
                if "\(self.detailDict?.value(forKey: "prop_photo") ?? "")".components(separatedBy: ",").count > 0
                {
                    self.propertyPhotosNameArray = (("\(self.detailDict?.value(forKey: "prop_photo") ?? "")".components(separatedBy: ",") as NSArray).mutableCopy() as! NSMutableArray)
                                        
                    if self.propertyPhotosNameArray!.count > 0
                    {
                        for currentImgObj in self.propertyPhotosNameArray!
                        {
                            let imageViewObj = UIImageView()
                            
                            imageViewObj.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(currentImgObj)")! as URL), placeholderImage: UIImage(named: "avatar"))
                            
                            self.propertyPhotosArray?.add(imageViewObj.image!)
                        }
                        
                        self.propertyPhotoHeight.constant = 120
                       
//                        self.propertyView.frame = CGRect(x: self.propertyView.frame.origin.x, y: self.propertyView.frame.origin.y, width: self.propertyView.frame.width, height: self.propertyView.frame.height)
//
//                        self.locationView.frame = CGRect(x: 0.0, y: self.propertyView.frame.origin.y + self.propertyView.frame .height, width: UIScreen.main.bounds.size.width, height: 1200)
                        
                        self.uploadPhotosLabel.text = "Add another photos"
                        self.uploadPhotosLabelWidth.constant = 195
                    }
                    else
                    {
//                        self.propertyView.frame = CGRect(x: self.propertyView.frame.origin.x, y: self.propertyView.frame.origin.y, width: self.propertyView.frame.width, height: self.propertyView.frame.height - 120)
//
//                        self.locationView.frame = CGRect(x: 0.0, y: self.propertyView.frame.origin.y + self.propertyView.frame .height, width: UIScreen.main.bounds.size.width, height: 1200)
                        
                        self.propertyPhotoHeight.constant = 0
                        self.uploadPhotosLabel.text = "Upload"
                        self.uploadPhotosLabelWidth.constant = 100
                    }
                    
                    DispatchQueue.main.async {
                        self.propertyPhotosCollection.reloadData()
                    }
                }
                
            }
            
            if "\(self.detailDict?.value(forKey: "reg_doc") ?? "")".isEmpty != true
            {
                let registrationImgView = UIImageView()
                registrationImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "reg_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                self.registrationButton.setImage(registrationImgView.image, for: .normal)
                self.registrationButton.imageView?.contentMode = .scaleAspectFill
                self.registrationButton.clipsToBounds = true
            }
            
            if "\(self.detailDict?.value(forKey: "electricity_bill") ?? "")".isEmpty != true
            {
                let lightBillImgView = UIImageView()
                lightBillImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "electricity_bill") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                self.lightBillButton.setImage(lightBillImgView.image, for: .normal)
                self.lightBillButton.imageView?.contentMode = .scaleAspectFill
                self.lightBillButton.clipsToBounds = true
            }
            
            if "\(self.detailDict?.value(forKey: "owner_id") ?? "")".isEmpty != true
            {
                let ownerIdImgView = UIImageView()
                ownerIdImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "owner_id") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                self.ownerIdButton.setImage(ownerIdImgView.image, for: .normal)
                self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
                self.ownerIdButton.clipsToBounds = true
            }
            
            DispatchQueue.main.async {
                self.locationManager = CLLocationManager()
                self.locationManager.requestAlwaysAuthorization()
                //or use requestWhenInUseAuthorization()
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.allowsBackgroundLocationUpdates = true
                self.locationManager.pausesLocationUpdatesAutomatically = false
                
                let locValue = CLLocationCoordinate2DMake(("\(self.detailDict?.value(forKey: "latitude") ?? "")" as NSString).doubleValue, ("\(self.detailDict?.value(forKey: "longitude") ?? "")" as NSString).doubleValue)
                
                let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
                let region = MKCoordinateRegion(center: locValue, span: span)
                self.mapViewObj.setRegion(region, animated: true)
                
                self.mapViewObj.isScrollEnabled = false
                
                if self.annotation == nil
                {
                    self.annotation = MKPointAnnotation()
                }
                
                self.annotation!.coordinate = locValue
                self.annotation!.title = "\(self.detailDict?.value(forKey: "propertyName") ?? "")"
                self.mapViewObj.addAnnotation(self.annotation!)
            }
            
            self.txtFlatNo.text = "\(self.detailDict?.value(forKey: "flat_no") ?? "")"
            self.txtFloor.text = "\(self.detailDict?.value(forKey: "floor") ?? "") Floor"
            self.txtWing.text = "\(self.detailDict?.value(forKey: "wing") ?? "")"
            self.txtBuildingName.text = "\(self.detailDict?.value(forKey: "bldg_name") ?? "")"
            self.txtLocality.text = "\(self.detailDict?.value(forKey: "locality") ?? "")"
            self.txtAddress1.text = "\(self.detailDict?.value(forKey: "address1") ?? "")"
            self.txtAddress2.text = "\(self.detailDict?.value(forKey: "address2") ?? "")"
            self.txtCity.text = "\(self.detailDict?.value(forKey: "city") ?? "")"
            self.txtZipCode.text = "\(self.detailDict?.value(forKey: "zipCode") ?? "")"
            self.txtState.text = "\(self.detailDict?.value(forKey: "state") ?? "")"
            self.txtCountry.text = "\(self.detailDict?.value(forKey: "country") ?? "")"
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func residentialButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.lblBedRoomTop.constant = 15
            self.lblBedRoomHeight.constant = 35
            self.bedroomCollectionTop.constant = 7
            self.bedroomCollectionHeight.constant = 60
//
            self.lblBathRoomTop.constant = 15
            self.lblBathRoomHeight.constant = 35
            self.bathRoomCollectionTop.constant = 7
            self.bathRoomCollectionHeight.constant = 60
            
            self.propertyTypeStr = self.residentialButton.currentTitle ?? ""
            self.propertySubType = "Flat"
            
            self.residentialButton.layer.cornerRadius = 15
            self.residentialButton.backgroundColor = Constants.darkBlueColor
            self.residentialButton.setTitleColor(.white, for: .normal)
            
            self.commercialButton.backgroundColor = .white
            self.commercialButton.setTitleColor(.lightGray, for: .normal)
            
            self.newPropertyTypeArray?.removeAllObjects()

            self.newPropertyTypeArray?.add("Flat")
            self.newPropertyTypeArray?.add("Duplex")
            self.newPropertyTypeArray?.add("Bungalow")
            self.newPropertyTypeArray?.add("Row House")
            self.newPropertyTypeArray?.add("Pent House")
            
            self.selectedNewPropertyTypeArray?.removeAllObjects()
            for _ in 0..<self.newPropertyTypeArray!.count
            {
                self.selectedNewPropertyTypeArray?.add("0")
            }
            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
            
            DispatchQueue.main.async {
                self.propertyTypeCollection.reloadData()
            }
            
            
//            self.mainScrollView.contentSize.height = 1800
        }
    }
    
    @IBAction func commercialButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.noOfRoomsStr = ""
            self.noOfBathroomsStr = ""
//
            self.lblBedRoomTop.constant = 0
            self.lblBedRoomHeight.constant = 0
            self.bedroomCollectionTop.constant = 0
            self.bedroomCollectionHeight.constant = 0

            self.lblBathRoomTop.constant = 0
            self.lblBathRoomHeight.constant = 0
            self.bathRoomCollectionTop.constant = 0
            self.bathRoomCollectionHeight.constant = 0
            
            self.propertyTypeStr = self.commercialButton.currentTitle ?? ""
            self.propertySubType = "Shop or Showroom"
            
            self.commercialButton.layer.cornerRadius = 15
            self.commercialButton.backgroundColor = Constants.darkBlueColor
            self.commercialButton.setTitleColor(.white, for: .normal)
            
            self.residentialButton.backgroundColor = .white
            self.residentialButton.setTitleColor(.lightGray, for: .normal)

            self.newPropertyTypeArray?.removeAllObjects()

            self.newPropertyTypeArray?.add("Shop or Showroom")
            self.newPropertyTypeArray?.add("Industrial")
            self.newPropertyTypeArray?.add("Office")
            self.newPropertyTypeArray?.add("IT")
            
            self.selectedNewPropertyTypeArray?.removeAllObjects()
            for _ in 0..<self.newPropertyTypeArray!.count
            {
                self.selectedNewPropertyTypeArray?.add("0")
            }
            
            self.selectedNewPropertyTypeArray?.replaceObject(at: 0, with: "1")
            
            DispatchQueue.main.async {
                self.propertyTypeCollection.reloadData()
            }
            
//            self.mainScrollView.contentSize.height = 1600
        }
    }
    
    @objc func newPropertyTypeButtonClicked(sender : UIButton)
    {
        print("newPropertyTypeButtonClicked")
     
        self.view.endEditing(true)
        self.propertySubType = (sender.currentTitle ?? "")
        
        self.selectedNewPropertyTypeArray?.removeAllObjects()
        for _ in 0..<self.newPropertyTypeArray!.count
        {
            self.selectedNewPropertyTypeArray?.add("0")
        }
        
        self.selectedNewPropertyTypeArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.propertyTypeCollection.reloadData()
        }
    }
    
    @objc func bedroomButtonClicked(sender : UIButton)
    {
        print("bedroomButtonClicked")
        self.view.endEditing(true)
        self.noOfRoomsStr = (sender.currentTitle ?? "")
        
        self.selectedBedroomArray?.removeAllObjects()
        for _ in 0..<self.bedroomArray!.count
        {
            self.selectedBedroomArray?.add("0")
        }
        
        self.selectedBedroomArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.bedroomCollection.reloadData()
        }
    }
    
    @objc func bathroomButtonClicked(sender : UIButton)
    {
        print("bathroomButtonClicked")
        
        self.view.endEditing(true)
        self.noOfBathroomsStr = (sender.currentTitle ?? "")
        
        self.selectedBathroomArray?.removeAllObjects()
        for _ in 0..<self.bathroomArray!.count
        {
            self.selectedBathroomArray?.add("0")
        }
        
        self.selectedBathroomArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.bathRoomCollection.reloadData()
        }
    }
    
    @IBAction func furnishedButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.furnishedTypeStr = (sender.currentTitle ?? "")
        
        self.furnishedButton.backgroundColor = Constants.darkBlueColor
        self.furnishedButton.setTitleColor(.white, for: .normal)
        
        self.nonFurnishedButton.backgroundColor = .white
        self.nonFurnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.partialButton.backgroundColor = .white
        self.partialButton.setTitleColor(.lightGray, for: .normal)
    }
    
    @IBAction func nonFurnishedButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.furnishedTypeStr = (sender.currentTitle ?? "")
        
        self.furnishedButton.backgroundColor = .white
        self.furnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.nonFurnishedButton.backgroundColor = Constants.darkBlueColor
        self.nonFurnishedButton.setTitleColor(.white, for: .normal)
        
        self.partialButton.backgroundColor = .white
        self.partialButton.setTitleColor(.lightGray, for: .normal)
    }
    
    @IBAction func partialButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.furnishedTypeStr = (sender.currentTitle ?? "")
        
        self.furnishedButton.backgroundColor = .white
        self.furnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.nonFurnishedButton.backgroundColor = .white
        self.nonFurnishedButton.setTitleColor(.lightGray, for: .normal)
        
        self.partialButton.backgroundColor = Constants.darkBlueColor
        self.partialButton.setTitleColor(.white, for: .normal)
    }
    
    @objc func carParkButtonClicked(sender : UIButton)
    {
        print("carParkButtonClicked")
        
        self.view.endEditing(true)
        self.noOfCarParkStr = (sender.currentTitle ?? "")
        
        self.selectedCarParkArray?.removeAllObjects()
        for _ in 0..<self.carParkArray!.count
        {
            self.selectedCarParkArray?.add("0")
        }
        
        self.selectedCarParkArray?.replaceObject(at: sender.tag, with: "1")
        
        DispatchQueue.main.async {
            self.carParkCollection.reloadData()
        }
    }
    
    @objc func amenitiesButtonClicked(sender : UIButton)
    {
        print("amenitiesButtonClicked")
        
        self.view.endEditing(true)
        
//        if (sender.currentTitle ?? "") != "Other"
//        {
//            if !self.amenitiesStr.contains((sender.currentTitle ?? "").lowercased())
//            {
//                if !self.amenitiesStr.isEmpty
//                {
//                    self.amenitiesStr = self.amenitiesStr+","+(sender.currentTitle ?? "")+","
//                }
//                else
//                {
//                    self.amenitiesStr = self.amenitiesStr+(sender.currentTitle ?? "")+","
//                }
//            }
//        }
        
        
        if (sender.currentTitle ?? "") == "Other"
        {
            if "\(self.selectedAmenitiesArray?.object(at: sender.tag) ?? "")" == "1"
            {
                self.amenitiesTop.constant = 0
                self.amenitiesHeight.constant = 0
                self.amenitiesNoteHeight.constant = 0
                self.txtOtherAmenities.text = ""
            }
            else
            {
                self.amenitiesTop.constant = 10
                self.amenitiesHeight.constant = 50
                self.amenitiesNoteHeight.constant = 15
            }
            
        }
        
        self.selectedAmenitiesArray?.replaceObject(at: sender.tag, with: ("\(self.selectedAmenitiesArray?.object(at: sender.tag) ?? "")" == "1") ? "0" : "1")
        
        self.amenitiesStr = ""
        
        for (index, currentObj) in (self.selectedAmenitiesArray!.enumerated())
        {
            if "\(currentObj)" == "1"
            {
                if "\(self.amenitiesArray?.object(at: index) ?? "")" == "Other"
                {
                    self.amenitiesStr = (self.txtOtherAmenities.text ?? "").isEmpty ? "\(self.amenitiesStr)" : self.amenitiesStr+("\(self.txtOtherAmenities.text ?? "")")+","
                }
                else
                {
                    self.amenitiesStr = self.amenitiesStr+("\(self.amenitiesArray?.object(at: index) ?? "")")+","
                }
            }
        }
        
        DispatchQueue.main.async {
            self.amenitiesCollection.reloadData()
            self.view.setNeedsLayout()
        }
    }
    
    @objc func propertyPhotosCrossButtonClicked(sender : UIButton)
    {
        if self.propertyPhotosArray!.count > 0
        {
            self.propertyPhotosArray?.removeObject(at: sender.tag)
        }
        
        if self.propertyPhotosNameArray!.count > 0
        {
            self.propertyPhotosNameArray?.removeObject(at: sender.tag)
        }
        
        if self.propertyPhotosNameArray!.count > 0
        {
            self.propertyPhotoHeight.constant = 120
           
//            self.propertyView.frame = CGRect(x: self.propertyView.frame.origin.x, y: self.propertyView.frame.origin.y, width: self.propertyView.frame.width, height: self.propertyView.frame.height + 120)
//
//            self.locationView.frame = CGRect(x: 0.0, y: self.propertyView.frame.origin.y + self.propertyView.frame .height, width: UIScreen.main.bounds.size.width, height: 1200)
        }
        else
        {
            self.propertyPhotoHeight.constant = 0
            
//            self.propertyView.frame = CGRect(x: self.propertyView.frame.origin.x, y: self.propertyView.frame.origin.y, width: self.propertyView.frame.width, height: self.propertyView.frame.height - 120)
//
//            self.locationView.frame = CGRect(x: 0.0, y: self.propertyView.frame.origin.y + self.propertyView.frame .height, width: UIScreen.main.bounds.size.width, height: 1200)
            
            self.uploadPhotosLabel.text = "Upload"
            self.uploadPhotosLabelWidth.constant = 100
        }
        
        DispatchQueue.main.async {
            self.propertyPhotosCollection.reloadData()
        }
    }
    
    @IBAction func uploadPropertyPhotosButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.isPropertyPhotoSelect = true
        
        let alert = UIAlertController(title: "Select Property Photos", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
        
    }
    
    @IBAction func addLocationButtonClicked(_ sender: UIButton)
    {
        let filterSearchVC = SelectLocationViewController(nibName: "SelectLocationViewController", bundle: nil)
        filterSearchVC.areaDelegate = self
        self.navigationController?.pushViewController(filterSearchVC, animated: true)
        
//        let placesVc = PlaceAutocompleteViewController(nibName: "PlaceAutocompleteViewController", bundle: nil)
//        placesVc.selectedAddressDelegate = self
//        self.navigationController?.pushViewController(placesVc, animated: true)
    }
    
    
    @IBAction func updateButtonClicked(_ sender: UIButton)
    {
        print("updateButtonClicked")
        
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtPropertyName.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Property Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertyTypeStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Property Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertySubType.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Property-Sub Type", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.noOfRoomsStr.isEmpty == true && self.propertyTypeStr != "Commercial"
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Selecte No.of Bedroom", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.noOfBathroomsStr.isEmpty == true && self.propertyTypeStr != "Commercial"
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Selecte No.of Bathrooms", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCarpetArea.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Carpet Area", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.furnishedTypeStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Property Status", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.noOfCarParkStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select No.of Car Parking", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.amenitiesStr.isEmpty == true && self.txtOtherAmenities.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Error", message: "Please Select Amenities", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtFlatNo.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Flat No.", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtFloor.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Floor No.", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtBuildingName.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Building Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLocality.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Locality", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtAddress1.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Address Line 1", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCity.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter City", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtZipCode.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter ZipCode", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtState.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter State", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtCountry.text?.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Country", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            self.updatePropertyDetails()
        }
    }

    @IBAction func registrationButtonClicked(_ sender: UIButton)
    {
        print("registrationButtonClicked")
        
        self.isRegistrationSelect = true
        self.isLightBillSelect = false
        self.isOwnerId = false
        
        let alert = UIAlertController(title: "Select Registration Doc", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
                        
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }

    @IBAction func lightBillButtonClicked(_ sender: UIButton)
    {
        print("lightBillButtonClicked")
        
        self.isRegistrationSelect = false
        self.isLightBillSelect = true
        self.isOwnerId = false
        
        let alert = UIAlertController(title: "Select Light Bill", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func ownerIdButtonClicked(_ sender: UIButton)
    {
        print("ownerIdButtonClicked")
        
        self.isRegistrationSelect = false
        self.isLightBillSelect = false
        self.isOwnerId = true
        
        let alert = UIAlertController(title: "Select Owenr Id", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    
    //MARK: - TextField Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.txtFloor
        {
//            self.txtFloor.isEnabled = false
            
            let incidentArray = ["Ground-Foor", "1st Floor", "2nd Floor", "3rd Floor", "4th Floor", "5th Floor", "6th Floor", "7th Floor", "8th Floor", "9th Floor", "10th Floor", "11th Floor", "12th Floor", "13th Floor", "14th Floor", "15th Floor", "16th Floor", "17th Floor", "18th Floor", "19th Floor", "20th Floor", "21th Floor", "22nd Floor", "23rd Floor", "24th Floor", "25th Floor", "26th Floor", "27th Floor", "28th Floor", "29th Floor", "30th Floor", "31st Floor", "32th Floor", "33th Floor", "34th Floor", "35th Floor","36th Floor", "37th Floor", "38th Floor", "39th Floor", "40th Floor", "41th Floor", "42th Floor", "43th Floor", "44th Floor", "45th Floor", "46th Floor", "47th Floor", "48th Floor", "49th Floor", "50th Floor", "51th Floor", "52th Floor", "53th Floor", "54th Floor", "55th Floor", "56th Floor", "57th Floor", "58th Floor", "59th Floor", "60th Floor"]
            
            var selectedFloorArray = [String]()
            
            for i in 0...60 {
                selectedFloorArray.append("\(i)")
                
            }
            
            PickerView().showDoalog(title: "Select Floor", options: incidentArray as NSArray, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", selected: "Owner") { (selectedString, selectedIndex) in

                self.txtFloor.text = "\(selectedString)"
                self.selectedFloor = "\(selectedFloorArray[selectedIndex])"
            }
            
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    
    //MARK: - Selected Address Delegate
    func sendSelectedArea(placesDict : PlaceDetails)
    {
        DispatchQueue.main.async {
            self.selectedLatValue = "\(placesDict.coordinate?.latitude ?? 0)"
            self.selectedLonValue = "\(placesDict.coordinate?.longitude ?? 0)"
            
            self.txtLocality.text = "\(placesDict.locality ?? "")"
            self.txtAddress1.text = "\(placesDict.name ?? "")"
            self.txtAddress2.text = "\(placesDict.subLocality ?? "")"
            
            self.txtCity.text = "\(placesDict.subAdministrativeArea ?? "")"
            self.txtZipCode.text = "\(placesDict.postalCode ?? "")"
            self.txtState.text = "\(placesDict.administrativeArea ?? "")"
            self.txtCountry.text = "\(placesDict.country ?? "")"
            
            let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
            let region = MKCoordinateRegion(center: placesDict.coordinate!, span: span)
            self.mapViewObj.setRegion(region, animated: true)
            
            if self.annotation == nil
            {
                self.annotation = MKPointAnnotation()
            }
            
            self.annotation!.coordinate = placesDict.coordinate!
            self.annotation!.title = "\(placesDict.name ?? "")"
            self.mapViewObj.addAnnotation(self.annotation!)
        }
    }
    
    //MARK: - Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])
    {
        if urls.first != nil
        {
            if self.isPropertyPhotoSelect
            {
                self.uploadPhotosLabel.text = "Add another photos"
                
                self.uploadPhotosLabelWidth.constant = 195
                
                self.isPropertyPhotoSelect = false
                
                self.propertyPhotosNameArray?.add(Constants.propertyImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png")
                
                self.propertyPhotosArray?.add(AppUtility.drawPDFfromURL(url: urls.first!)!)
                
                DispatchQueue.main.async {
                    
                    if self.propertyPhotosArray!.count > 0
                    {
                        self.propertyPhotoHeight.constant = 120
                    }
                    self.propertyPhotosCollection.reloadData()
                    self.propertyPhotosCollection.layoutSubviews()
                }
            }
            else if self.isRegistrationSelect
            {
                self.registrationImage = (AppUtility.drawPDFfromURL(url: urls.first!)!)
                self.registrationButton.setImage(self.registrationImage, for: .normal)
                self.registrationButton.imageView?.contentMode = .scaleAspectFill

                self.registrationButton.layer.masksToBounds = true

                self.registrationDocPath = Constants.registrationImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isLightBillSelect
            {
                self.lightBillImage = (AppUtility.drawPDFfromURL(url: urls.first!)!)
                self.lightBillButton.setImage(self.lightBillImage, for: .normal)
                self.lightBillButton.imageView?.contentMode = .scaleAspectFill
                self.lightBillButton.layer.masksToBounds = true

                self.lightBillDocPath = Constants.lightBillImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isOwnerId
            {
                self.ownerIdImage = (AppUtility.drawPDFfromURL(url: urls.first!)!)
                self.ownerIdButton.setImage(self.ownerIdImage, for: .normal)
                self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
                self.ownerIdButton.layer.masksToBounds = true

                self.ownerIdDocPath = Constants.ownerIdImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
    }

    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if self.isPropertyPhotoSelect
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.uploadPhotosLabel.text = "Add another photos"
               
                self.uploadPhotosLabelWidth.constant = 195
                
                self.isPropertyPhotoSelect = false
                
                self.propertyPhotosNameArray?.add(Constants.propertyImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png")
                
                self.propertyPhotosArray?.add(pickedImage)
                
                DispatchQueue.main.async {
                    
                    if self.propertyPhotosArray!.count > 0
                    {
                        self.propertyPhotoHeight.constant = 120
                    }
                    self.view.layoutIfNeeded()
                    self.propertyPhotosCollection.reloadData()
                }
            }
        }
        else
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                if self.isRegistrationSelect
                {
                    self.registrationImage = pickedImage
                    self.registrationButton.setImage(pickedImage, for: .normal)
                    self.registrationButton.imageView?.contentMode = .scaleAspectFill

                    self.registrationButton.layer.masksToBounds = true

                    self.registrationDocPath = Constants.registrationImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                }
                else if self.isLightBillSelect
                {
                    self.lightBillImage = pickedImage
                    self.lightBillButton.setImage(pickedImage, for: .normal)
                    self.lightBillButton.imageView?.contentMode = .scaleAspectFill
                    self.lightBillButton.layer.masksToBounds = true

                    self.lightBillDocPath = Constants.lightBillImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                }
                else if self.isOwnerId
                {
                    self.ownerIdImage = pickedImage
                    self.ownerIdButton.setImage(pickedImage, for: .normal)
                    self.ownerIdButton.imageView?.contentMode = .scaleAspectFill
                    self.ownerIdButton.layer.masksToBounds = true

                    self.ownerIdDocPath = Constants.ownerIdImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
                }
            }
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.isPropertyPhotoSelect = false
        
        self.isRegistrationSelect = false
        self.isLightBillSelect = false
        self.isOwnerId = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Update Property Details
    func updatePropertyDetails()
    {
        print("updatePropertyDetails")
        
        if Reachability.isConnectedToNetwork()
        {
            self.amenitiesStr = ""
            
            for (index, currentObj) in (self.selectedAmenitiesArray!.enumerated())
            {
                if "\(currentObj)" == "1"
                {
                    if "\(self.amenitiesArray?.object(at: index) ?? "")" == "Other"
                    {
                        self.amenitiesStr = (self.txtOtherAmenities.text ?? "").isEmpty ? "\(self.amenitiesStr)" : self.amenitiesStr+("\(self.txtOtherAmenities.text ?? "")")+","
                    }
                    else
                    {
                        self.amenitiesStr = self.amenitiesStr+("\(self.amenitiesArray?.object(at: index) ?? "")")+","
                    }
                }
            }
            
            self.amenitiesStr = "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))"
//
//            if self.txtOtherAmenities.text?.isEmpty != true && self.amenitiesStr.isEmpty != true
//            {
//                self.amenitiesStr = (self.amenitiesStr + "\(self.txtOtherAmenities.text ?? "")")
//            }
//            else if self.txtOtherAmenities.text?.isEmpty != true
//            {
//                self.amenitiesStr = self.amenitiesStr.isEmpty ? "\(self.txtOtherAmenities.text ?? "")" : "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))"
//            }
//            else
//            {
//                if self.amenitiesStr.isEmpty != true
//                {
//                    self.amenitiesStr = "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))"
//                }
//            }
            
            if self.propertyPhotosNameArray!.count > 0
            {
                self.uploadPropertyImages()
            }
            
            if self.registrationDocPath.isEmpty != true
            {
                self.uploadRegistrationImages()
            }
            
            if self.lightBillDocPath.isEmpty != true
            {
                self.uploadLightBillImages()
            }
            
            if self.ownerIdDocPath.isEmpty != true
            {
                self.uploadOwnerIdImages()
            }
            
//            KRProgressHUD.show()
            
            let paramDict = ["mobileno" : (Int64(AppUtility.getUserProfileMobileNumber()) ?? 0), //Long
                             "property_id" : (Int64("\(self.detailDict?.value(forKey: "property_id") ?? "")") ?? 0), //Long
                             "propertyName" : "\(self.txtPropertyName.text ?? "")", //String
                             "floor_no" : "\(self.selectedFloor)", // String
                             "parking" : "\(self.noOfCarParkStr)", // String
                             "wing" : "\(self.txtWing.text ?? "")", // String
                             "carpet_area" : "\(self.txtCarpetArea.text ?? "")", // String
                             "furnished_type" : "\(self.furnishedTypeStr)", // String
                             "state" : "\(self.txtState.text ?? "")", // String
                             "address1" : "\(self.txtAddress1.text ?? "")", // String
                             "address2" : "\(self.txtAddress2.text ?? "")", // String
                             "prop_photo" : (self.propertyPhotosNameArray ?? [""]), // String Array
                             "bedrooms" : "\(self.noOfRoomsStr)", // String
                             "bathrooms" : "\(self.noOfBathroomsStr)", // String
                             "country" : "\(self.txtCountry.text ?? "")", // String
                             "city" : "\(self.txtCity.text ?? "")", // String
                             "property_type" : "\(self.propertyTypeStr)-\(self.propertySubType)", // String
                             "builtup_area" : "\(self.txtBuiltUpArea.text ?? "")", // String
                             "reg_doc" : "\(self.registrationDocPath)", // String
//                             "Amenities" : self.amenitiesStr.isEmpty ? "" : "\(self.amenitiesStr.prefix(self.amenitiesStr.count-1))", // String
                             "Amenities" : self.amenitiesStr.isEmpty ? "" : "\(self.amenitiesStr)",
                             "zipCode" : (Int64(self.txtZipCode.text ?? "") ?? 0), // Long
                             "owner_id" : "\(self.ownerIdDocPath)", // String
                             "locality" : "\(self.txtLocality.text ?? "")", // String
                             "longitude" : "\(self.selectedLonValue)", // String
                             "latitude" : "\(self.selectedLatValue)", // String
                             "electricity_bill" : "\(self.lightBillDocPath)", // String
                             "maintenance_charge" : "0", // String
                             "flat_no" : "\(self.txtFlatNo.text ?? "")", // String
                             "bldg_name" : "\(self.txtBuildingName.text ?? "")", // String
                             "is_property_enable" : String("1").utf8.map{UInt8($0)}[0], // Boolean
                             "property_progress_state" : 0, //Int
                             
            ] as [String : Any]
            
            print(paramDict)
            
            DispatchQueue.global(qos: .background).async {

                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updateProperty", headerVal: "", parameters: paramDict) { (status, response) in

                    if status
                    {
                        if response.count > 0
                        {
                            let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)

                            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                                DispatchQueue.main.async {
                                    self.navigationController?.popToRootViewController(animated: true)
                                }

                            }))

                            DispatchQueue.main.async {
                                self .present(alertViewController, animated: true, completion: nil)
                            }
                        }

                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }

                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Upload Property Images
    func uploadPropertyImages()
    {
        for i in 0..<self.propertyPhotosArray!.count
        {
            KRProgressHUD.show()
            
            DispatchQueue.global(qos: .background).async {
                
                let req = NetworkManager.api.uploadImage(image: (self.propertyPhotosArray?.object(at: i) as! UIImage), imageName: "\(self.propertyPhotosNameArray?.object(at: i) ?? "")")

                req.done { (url) in
                    print("Property Success URL : \(url)")
                    
                }.catch { (error) in
                    print("Error : \(error.localizedDescription)")
                }
                
                KRProgressHUD.dismiss()
            }
        }
    }
    
    //MARK: - Upload Registration Images
    func uploadRegistrationImages()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.registrationImage!, imageName: "\(self.registrationDocPath)")
            
            req.done { (url) in
                print("Registration Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Upload LightBill Images
    func uploadLightBillImages()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.lightBillImage!, imageName: "\(self.lightBillDocPath)")
            
            req.done { (url) in
                print("Light Bill Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Upload Owner Id Images
    func uploadOwnerIdImages()
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: self.ownerIdImage!, imageName: "\(self.ownerIdDocPath)")
            
            req.done { (url) in
                print("Owner Id Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
}

extension EditPropertyDetailViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == self.propertyTypeCollection
        {
            if self.newPropertyTypeArray != nil
            {
                return self.newPropertyTypeArray!.count
            }
        }
        else if collectionView == self.bedroomCollection
        {
            if self.bedroomArray != nil
            {
                return self.bedroomArray!.count
            }
        }
        else if collectionView == self.bathRoomCollection
        {
            if self.bathroomArray != nil
            {
                return self.bathroomArray!.count
            }
        }
        else if collectionView == self.carParkCollection
        {
            if self.carParkArray != nil
            {
                return self.carParkArray!.count
            }
        }
        else if collectionView == self.amenitiesCollection
        {
            if self.amenitiesArray != nil
            {
                return self.amenitiesArray!.count
            }
        }
        else if collectionView == self.propertyPhotosCollection
        {
            if self.propertyPhotosArray != nil
            {
                if self.propertyPhotosArray!.count > 0
                {
                    return self.propertyPhotosArray!.count
                }
            }

//            return 0
        }
//        else if collectionView == self.propertyDocumentCollection
//        {
//            return 3
//        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.propertyTypeCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell
            
            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true
                
                if ("\(self.selectedNewPropertyTypeArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }
            
            myCell.titleButton.setTitle("\(self.newPropertyTypeArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(newPropertyTypeButtonClicked(sender:)), for: .touchUpInside)
            
            return myCell
        }
        else if collectionView == self.bedroomCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell

            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true

                if ("\(self.selectedBedroomArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }

            myCell.titleButton.setTitle("\(self.bedroomArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(bedroomButtonClicked(sender:)), for: .touchUpInside)

            return myCell
        }
        else if collectionView == self.bathRoomCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell

            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true

                if ("\(self.selectedBathroomArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }

            myCell.titleButton.setTitle("\(self.bathroomArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(bathroomButtonClicked(sender:)), for: .touchUpInside)

            return myCell
        }
        else if collectionView == self.carParkCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newPropertyTypeCell", for: indexPath) as! NewPropertyTypeCollectionViewCell

            DispatchQueue.main.async {
                myCell.titleButton.layer.cornerRadius = 12
                myCell.titleButton.layer.masksToBounds = true

                if ("\(self.selectedCarParkArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
                    myCell.titleButton.backgroundColor = Constants.darkBlueColor
                    myCell.titleButton.setTitleColor(.white, for: .normal)
                }
                else
                {
                    myCell.titleButton.backgroundColor = .white
                    myCell.titleButton.setTitleColor(.lightGray, for: .normal)
                }
            }

            myCell.titleButton.setTitle("\(self.carParkArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.titleButton.tag = indexPath.item
            myCell.titleButton.addTarget(self, action: #selector(carParkButtonClicked(sender:)), for: .touchUpInside)

            return myCell
        }
        else if collectionView == self.amenitiesCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "boxCollectionCell", for: indexPath) as! BoxCollectionViewCell

            myCell.baseView.backgroundColor = .clear

            myCell.imageViewObj.layer.cornerRadius = 15
            myCell.imageViewObj.backgroundColor = .clear

            myCell.imageViewObj.image = UIImage(named: "\(self.amenitiesImageArray![indexPath.item])")
            myCell.lblTitle.text = "\(self.amenitiesArray![indexPath.item])"

            myCell.buttonObj.setTitleColor(.clear, for: .normal)
            myCell.buttonObj.setTitle("\(self.amenitiesArray?.object(at: indexPath.item) as? String ?? "")", for: .normal)
            myCell.buttonObj.tag = indexPath.row
            myCell.buttonObj.addTarget(self, action: #selector(amenitiesButtonClicked(sender:)), for: .touchUpInside)

            DispatchQueue.main.async {
                myCell.baseView.layer.cornerRadius = 8
                
                if ("\(self.selectedAmenitiesArray?.object(at: indexPath.item) as? String ?? "")" == "1")
                {
//                    myCell.imageViewObj.backgroundColor = .orange
                    myCell.baseView.backgroundColor = .white
                }
                else
                {
//                    myCell.imageViewObj.backgroundColor = .white
                    myCell.baseView.backgroundColor = .clear
                }
            }
            return myCell
        }
        else if collectionView == self.propertyPhotosCollection
        {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "propertyPhotoCell", for: indexPath) as! PropertyPhotosCollectionViewCell

            DispatchQueue.main.async {
                myCell.imgViewObj.layer.cornerRadius = 15
                myCell.imgViewObj.contentMode = .scaleAspectFill
            }
            
//            myCell.imgViewObj.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.propertyPhotosNameArray?.object(at: indexPath.item) ?? "")")")! as URL), placeholderImage: UIImage(named: "signupHomeLogo"))
            
            myCell.imgViewObj.image = (self.propertyPhotosArray?.object(at: indexPath.item) as? UIImage)
            
            myCell.crossButton.tag = indexPath.item
            myCell.crossButton.addTarget(self, action: #selector(self.propertyPhotosCrossButtonClicked(sender:)), for: .touchUpInside)

            return myCell
        }

        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == self.propertyTypeCollection
        {
            return CGSize(width: 120.0, height: 40.0)
        }
        else if collectionView == self.bedroomCollection
        {
            return CGSize(width: 40.0, height: 40.0)
        }
        else if collectionView == self.bathRoomCollection || collectionView == self.carParkCollection
        {
            return CGSize(width: 40.0, height: 40.0)
        }
        else if collectionView == self.propertyPhotosCollection
        {
            return CGSize(width: 150.0, height: 100.0)
        }

        return CGSize(width: 80.0, height: 120.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 15, left: 0, bottom: 5, right: 15)
    }
}
