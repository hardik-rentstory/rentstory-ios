//
//  VerifyPhoneViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 20/01/21.
//

import UIKit
import SVPinView
import Firebase
import FirebaseAuth
import KRProgressHUD
import SRCountdownTimer

protocol OTPVerificationDelegate
{
    func otpVerification()
}

class VerifyPhoneViewController: UIViewController, SRCountdownTimerDelegate
{
    @IBOutlet weak var countdownTimer: SRCountdownTimer!
    
    @IBOutlet weak var resendButton: UIButton!
    
    var otpVerificationDelegate : OTPVerificationDelegate?
    
    var enteredPinStr = ""
    
    var verificationIdStr = ""
    var countryCodeStr = ""
    var phoneNumberStr = ""
    var contactId = ""
    var propertyName = ""
    var contactType = ""

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var pinView: SVPinView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear.withAlphaComponent(0.8)
        
        self.resendButton.isEnabled = false
        
        self.countdownTimer.labelFont = UIFont(name: "SofiaPro", size: 35)!
        self.countdownTimer.labelTextColor = Constants.darkBlueColor
        self.countdownTimer.timerFinishingText = "End"
        self.countdownTimer.delegate = self
        self.countdownTimer.lineWidth = 6
        self.countdownTimer.lineColor = Constants.darkBlueColor
        self.countdownTimer.start(beginingValue: 59, interval: 1)
        
        self.headerLabel.text = "Please type the verification otp sent to your Mobile.No \(self.phoneNumberStr)"
        
        self.configurePinView()
        
        self.getOTPResponse()
    }
    
    //MARK: - Counter Delegate
    func timerDidEnd(sender: SRCountdownTimer, elapsedTime: TimeInterval)
    {
        self.countdownTimer.isHidden = true
        self.resendButton.isEnabled = true
    }
    
    //MARK: - Configure PinView
    func configurePinView()
    {
        pinView.pinLength = 4
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 10
        pinView.textColor = UIColor.darkGray
        pinView.borderLineColor = UIColor.white
        pinView.activeBorderLineColor = UIColor.white
        pinView.borderLineThickness = 1
        pinView.shouldSecureText = true
        pinView.allowsWhitespaces = false
        pinView.style = .none
        pinView.fieldBackgroundColor = Constants.bgColor//UIColor.white.withAlphaComponent(0.3)
        pinView.activeFieldBackgroundColor = Constants.bgColor //UIColor.white.withAlphaComponent(0.5)
        pinView.fieldCornerRadius = 15
        pinView.activeFieldCornerRadius = 15
        pinView.placeholder = "******"
        pinView.deleteButtonAction = .deleteCurrentAndMoveToPrevious
        pinView.keyboardAppearance = .default
        pinView.tintColor = .darkGray
        pinView.becomeFirstResponderAtIndex = 0
        pinView.shouldDismissKeyboardOnEmptyFirstField = false
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
        }
    }
    
    func didFinishEnteringPin(pin:String)
    {
        self.enteredPinStr = pin
    }
    
    @objc func dismissKeyboard()
    {
        self.view.endEditing(false)
    }
    
    //MARK: - Helper Functions
    func showAlert(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    //MARK: - Button Actions
    @IBAction func crossButtonClicked(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func verifyButtonClicked(_ sender: UIButton)
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["contact_id": (Int64(self.contactId) ?? 0), //Long
                             "mobileno":(Int64(AppUtility.getMobileNumber(mobileNumber: self.phoneNumberStr)) ?? 0), //Long
                             "otp":(Int64(self.enteredPinStr) ?? 0), //Long
                             "contact_type":"\(self.contactType)", //String
//                             "otptype":"",
                             "property_name":"\(self.propertyName)", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/verifyContact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Verify Contact Response : \(response)")
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                            if self.otpVerificationDelegate != nil
                            {
                                self.otpVerificationDelegate?.otpVerification()
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
        
//        if !self.enteredPinStr.isEmpty
//        {
//            KRProgressHUD.show()
//
//            let credential = PhoneAuthProvider.provider().credential(
//                withVerificationID: self.verificationIdStr,
//                verificationCode: "\(self.enteredPinStr)")
//
//            Auth.auth().signIn(with: credential) { (authResult, error) in
//
//                if let error = error
//                {
//                    print((error.localizedDescription))
//                }
//                else
//                {
//                    print("Success")
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//                KRProgressHUD.dismiss()
//            }
//        }
//        else
//        {
//            let alertViewController = UIAlertController(title: "Error", message: "Please Enter Pin", preferredStyle: .alert)
//
//            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//            }))
//
//            DispatchQueue.main.async {
//                self .present(alertViewController, animated: true, completion: nil)
//            }
//        }
    }
    
    @IBAction func resentOTPButtonClicked(_ sender: UIButton)
    {
        print("resentOTPButtonClicked")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["contact_id": (Int64(self.contactId) ?? 0), //Long
                             "mobileno":(Int64(AppUtility.getMobileNumber(mobileNumber: self.phoneNumberStr)) ?? 0), //Long
                             "otptype":Int64(1),
                             "propertyName":"\(self.propertyName)", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/resendOTP", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Resend OTP Response : \(response)")
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Get OTP Response
    func getOTPResponse()
    {
        print("resentOTPButtonClicked")
        
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
//            let paramDict = [
//                "mobileno":(Int64(AppUtility.getMobileNumber(mobileNumber: self.phoneNumberStr)) ?? 0), //Long
//                "searchResult":"",
//
//            ] as [String : Any]
            
            let paramDict = ["mobileno":(Int64(AppUtility.getMobileNumber(mobileNumber: self.phoneNumberStr)) ?? 0),
                             "searchResult" : "",
                             "emailId" : "",
                             "property_name" : "",
                             "property_id" : "",
                             "data" : "",
                             
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getOTP", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Resend OTP Response : \(response)")
                        
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") ?? "")", preferredStyle: .alert)

                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                        }))

                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
}
