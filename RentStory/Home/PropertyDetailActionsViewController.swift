//
//  PropertyDetailActionsViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 24/02/21.
//

import UIKit
import KRProgressHUD


class PropertyDetailActionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var ownerIsVerified = "0"
    
    var leaseDict : NSDictionary?
    var detailDict : NSDictionary?
    
    var peopleInvolvedArray : NSMutableArray?
    
    
//    let tableArray = ["Edit Property Details", "Edit lease agreement", "Take actions", "View previous tenants"]
    
    let tableArray = ["Edit Property Details", "Edit lease agreement", "Take actions"]
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        self.baseView.backgroundColor = Constants.bgColor
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.backgroundColor = .clear
        self.tableViewObj.separatorColor = .white
        
        let myNib = UINib(nibName: "UserProfileTableViewCell", bundle: nil)
        self.tableViewObj.register(myNib, forCellReuseIdentifier: "userProfileCell")
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.baseView.layer.cornerRadius = 40
        self.baseView.layer.masksToBounds = true
        
        self.tableViewObj.layer.cornerRadius = 40
        self.tableViewObj.layer.masksToBounds = true
        
    }
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteButtonClicked(_ sender: UIButton)
    {
        let alertViewController = UIAlertController(title: "Delete", message: "Do You Want To Delete : \(self.detailDict?.value(forKey: "propertyName") ?? "")", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction!) in
            
            print("Delete  -- Yes")
            self.deletePropertyCardResponse()
           
        }))
        
        alertViewController.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (alert: UIAlertAction!) in
            print("Delete  -- No")
        }))
        
        DispatchQueue.main.async {
            self .present(alertViewController, animated: true, completion: nil)
        }
        
        
    }
    
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.tableArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "userProfileCell", for: indexPath) as! UserProfileTableViewCell
        
        myCell.selectionStyle = .none
        
        myCell.nameLabel.text = "\(self.tableArray[indexPath.row])"
        
        DispatchQueue.main.async {
            myCell.mainView.layer.cornerRadius = 20
            myCell.mainView.backgroundColor = .white
        }
        
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if "\(self.tableArray[indexPath.row])" == "Edit Property Details"
        {
            let editPropertyDetailVC = EditPropertyDetailViewController(nibName: "EditPropertyDetailViewController", bundle: nil)
            editPropertyDetailVC.detailDict = self.detailDict
            self.navigationController?.pushViewController(editPropertyDetailVC, animated: true)
        }
        else if "\(self.tableArray[indexPath.row])" == "Edit lease agreement"
        {
            let editLeaseAgreementVc = EditLeaseAgreementViewController(nibName: "EditLeaseAgreementViewController", bundle: nil)
            editLeaseAgreementVc.leaseDict = self.leaseDict
            self.navigationController?.pushViewController(editLeaseAgreementVc, animated: true)
        }
        else if "\(self.tableArray[indexPath.row])" == "Edit involved peoples"
        {
            let peopleInvolvedVC = PeopleInvolvedViewController(nibName: "PeopleInvolvedViewController", bundle: nil)
            peopleInvolvedVC.ownerIsVerified = self.ownerIsVerified
//            peopleInvolvedVC.peopleInvolvedArray = self.peopleInvolvedArray
            peopleInvolvedVC.detailDict = self.detailDict
            self.navigationController?.pushViewController(peopleInvolvedVC, animated: true)
        }
        else if "\(self.tableArray[indexPath.row])" == "Take actions"
        {
            let takeActionsVC = TakeActionsViewController(nibName: "TakeActionsViewController", bundle: nil)
            takeActionsVC.detailDict = self.detailDict
            takeActionsVC.contactListArray = self.peopleInvolvedArray
            takeActionsVC.endDateStr = ("\(self.leaseDict?.value(forKey: "end_date") ?? "")".components(separatedBy: "T").first ?? "")
            self.navigationController?.pushViewController(takeActionsVC, animated: true)
        }
    }
    
    //MARK: - Delete Property Card
    func deletePropertyCardResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno" : (Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0), //Long
                "property_id":(Int64("\(detailDict?.value(forKey: "property_id") ?? "")") ?? 0),
                "propertyName" : "\(self.detailDict?.value(forKey: "propertyName") ?? "")", //String
                "deleterName": "\(AppUtility.getUserData(keyVal: "name"))", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/deleteProperty", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Delete Property Response : \(response)")
                        
                        if response.count > 0
                        {
                            let alertViewController = UIAlertController(title: "Success", message: "\("\((response as AnyObject).value(forKey: "message") ?? "")")", preferredStyle: .alert)
                            
                            alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert: UIAlertAction!) in
                                
                                DispatchQueue.main.async {
                                    self.navigationController?.popToRootViewController(animated: true)
                                }
                               
                            }))
                            
                            
                            DispatchQueue.main.async {
                                self .present(alertViewController, animated: true, completion: nil)
                            }
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
}
