//
//  HomeViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 05/11/20.
//

import UIKit
import KRProgressHUD
import SDWebImage
import CoreLocation

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate
{
    @IBOutlet weak var searchBarObj: UISearchBar!
    
    var refreshControl : UIRefreshControl?
    
    var isSearch = false
    var filterArray = NSArray()
    
    var responseArray : NSMutableArray?
    var profileDict : NSMutableDictionary?
    
    var locationManager: CLLocationManager!

    var bottomView: UIView!
    
    var viewAddProperty: UIView!
    var viewDiscover: UIView!
    var viewNotifications: UIView!
    
    var btnAddProperty: UIButton!
    var btnDiscover: UIButton!
    var btnNotifications: UIButton!
    
    var lblAddProperty: UILabel!
    var lblDiscover: UILabel!
    var lblNotifications: UILabel!
    
    var imgViewAddProperty: UIImageView!
    var imgViewDiscover: UIImageView!
    var imgViewNotifications: UIImageView!
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnFilter: UIButton!
    
    @IBOutlet weak var profileImgView: UIImageView!
    
    @IBOutlet weak var lblProfileName: UILabel!
    
    @IBOutlet weak var tableViewObj: UITableView!
    
    @IBOutlet weak var lblNoData: UILabel!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        DispatchQueue.main.async {
//            self.locationManager = CLLocationManager()
//            self.locationManager.delegate = self
//            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            self.locationManager.requestAlwaysAuthorization()
//            self.locationManager.requestLocation()
//            self.locationManager.startUpdatingLocation()
//        }
        
        self.refreshControl = UIRefreshControl()
       
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *)
        {
            self.tableViewObj.refreshControl = refreshControl
        }
        else
        {
            self.tableViewObj.addSubview(self.refreshControl!)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUpdateProfileNotification(notification:)), name: Notification.Name("UpdateProfilePicNotification"), object: nil)
        
        self.lblNoData.isHidden = true
        
        self.baseView.layer.cornerRadius = 20

        self.baseView.backgroundColor = Constants.darkBlueColor
        self.view.backgroundColor = Constants.darkBlueColor
        
        self.mainView.backgroundColor = Constants.bgColor
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.showsHorizontalScrollIndicator = false
        self.tableViewObj.showsVerticalScrollIndicator = false
        
        // Configure Refresh Control
        self.refreshControl!.addTarget(self, action: #selector(self.pullToRefreshData(_:)), for: .valueChanged)
        
        let homeNib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        self.tableViewObj.register(homeNib, forCellReuseIdentifier: "homeCell")
        self.tableViewObj.separatorColor = .clear
        self.tableViewObj.backgroundColor = .clear
        self.tableViewObj.contentInset = UIEdgeInsets(top: -10.0, left: 0.0, bottom: 100, right: 0)
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.lblProfileName.font = UIFont(name: "SofiaPro-Light", size: 18)
        }
        
        self.searchBarObj.backgroundImage = UIImage()
        self.searchBarObj.searchTextField.backgroundColor = .white
        self.searchBarObj.delegate = self
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.profileImgView.layer.cornerRadius = self.profileImgView.frame.size.height/2
        self.profileImgView.clipsToBounds = true
        
        self.btnFilter.layer.cornerRadius = 6
        
        self.mainView.layer.cornerRadius = 35
        
        self.bottomView = UIView(frame: CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 100, width: UIScreen.main.bounds.size.width, height: 100))
        self.bottomView.backgroundColor = Constants.darkBlueColor
        self.bottomView.layer.cornerRadius = 23
        self.view.addSubview(self.bottomView)
        
        
        // Middle View : Add Property
        self.viewAddProperty = UIView(frame: CGRect(x: UIScreen.main.bounds.size.width/2, y: 15.0, width: 80.0, height: 60.0))
        self.viewAddProperty.backgroundColor = Constants.darkGrayColor
        self.viewAddProperty.layer.cornerRadius = 15
        self.bottomView.addSubview(self.viewAddProperty)
        
        self.imgViewAddProperty = UIImageView(frame: CGRect(x: 30.0, y: 0.0, width: 25.0, height: 60.0))
        self.imgViewAddProperty.image = UIImage(named: "createPropertyGray")
        self.imgViewAddProperty.contentMode = .scaleAspectFit
        self.viewAddProperty.addSubview(self.imgViewAddProperty)
        
//        self.lblAddProperty = UILabel(frame: CGRect(x: self.imgViewAddProperty.frame.origin.x + self.imgViewAddProperty.frame.size.width + 5, y: 0, width: 120, height: 60.0))
        self.lblAddProperty = UILabel(frame: CGRect(x: self.imgViewAddProperty.frame.origin.x + self.imgViewAddProperty.frame.size.width + 5, y: 0, width: 0, height: 60.0))
        self.lblAddProperty.text = "Add Property"
        self.lblAddProperty.font = UIFont(name: "SofiaPro", size: 15)
        self.lblAddProperty.textAlignment = .left
        self.viewAddProperty.addSubview(self.lblAddProperty)
        
        self.btnAddProperty = UIButton(type: .custom)
        self.btnAddProperty.frame = CGRect(x: 0.0, y: 0.0, width: self.viewAddProperty.frame.width, height: self.viewAddProperty.frame.height)
        self.btnAddProperty.backgroundColor = .clear
        self.btnAddProperty.addTarget(self, action: #selector(self.addPropertyButtonClicked(sender:)), for: .touchUpInside)
        self.viewAddProperty.addSubview(self.btnAddProperty)
        
        
        // First View : Discover
        self.viewDiscover = UIView(frame: CGRect(x: 15, y: 15.0, width: self.viewAddProperty.frame.origin.x - 30, height: 60.0))
        self.viewDiscover.backgroundColor = .white
        self.viewDiscover.layer.cornerRadius = 15
        self.bottomView.addSubview(self.viewDiscover)
        
        self.imgViewDiscover = UIImageView(frame: CGRect(x: 30.0, y: 0.0, width: 25.0, height: 60.0))
        self.imgViewDiscover.image = UIImage(named: "discoverBlue")
        self.imgViewDiscover.contentMode = .scaleAspectFit
//        self.imgViewDiscover.backgroundColor = .yellow
        self.viewDiscover.addSubview(self.imgViewDiscover)
        
        self.lblDiscover = UILabel(frame: CGRect(x: self.imgViewDiscover.frame.origin.x + self.imgViewDiscover.frame.size.width + 5, y: 7, width: 100, height: 50.0))
        self.lblDiscover.text = "Home"
        self.lblDiscover.font = UIFont(name: "SofiaPro", size: 16)
        self.lblDiscover.textAlignment = .left
        self.lblDiscover.textColor = Constants.darkBlueColor
//        self.lblDiscover.backgroundColor = .orange
        self.viewDiscover.addSubview(self.lblDiscover)
        
        self.btnDiscover = UIButton(type: .custom)
        self.btnDiscover.frame = CGRect(x: 0.0, y: 0.0, width: self.viewDiscover.frame.width, height: self.viewDiscover.frame.height)
        self.btnDiscover.backgroundColor = .clear
        self.btnDiscover.addTarget(self, action: #selector(self.discoverButtonClicked(sender:)), for: .touchUpInside)
        self.viewDiscover.addSubview(self.btnDiscover)
        
        
        // Third View : Notifications
//        self.viewNotifications = UIView(frame: CGRect(x: self.viewAddProperty.frame.origin.x + self.viewAddProperty.frame.size.width + 15, y: 15.0, width: 80.0, height: 60.0))
        self.viewNotifications = UIView(frame: CGRect(x: self.bottomView.frame.width - 80 - 15, y: 15.0, width: 80.0, height: 60.0))
        self.viewNotifications.backgroundColor = Constants.darkGrayColor
        self.viewNotifications.layer.cornerRadius = 15
        self.bottomView.addSubview(self.viewNotifications)
        
        self.imgViewNotifications = UIImageView(frame: CGRect(x: 30.0, y: 0.0, width: 25.0, height: 60.0))
        self.imgViewNotifications.image = UIImage(named: "notificationGray")
        self.imgViewNotifications.contentMode = .scaleAspectFit
        self.viewNotifications.addSubview(self.imgViewNotifications)
        
        self.btnNotifications = UIButton(type: .custom)
        self.btnNotifications.frame = CGRect(x: 0.0, y: 0.0, width: self.viewNotifications.frame.width, height: self.viewNotifications.frame.height)
        self.btnNotifications.backgroundColor = .clear
        self.btnNotifications.addTarget(self, action: #selector(self.notificationButtonClicked(sender:)), for: .touchUpInside)
        self.viewNotifications.addSubview(self.btnNotifications)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.searchBarObj.searchTextField.text = ""
        self.searchBarObj.searchTextField.resignFirstResponder()
        self.isSearch = false;
      
        let searchTextAppearance = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        searchTextAppearance.font = UIFont.systemFont(ofSize: 14)
        
        
        if Reachability.isConnectedToNetwork()
        {
            self.getUserProfile()
            self.getPropertyList()
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Refresh Data
    @objc func pullToRefreshData(_ sender: Any)
    {
        print("Pull To Refresh")
        
        if Reachability.isConnectedToNetwork()
        {
            self.getUserProfile()
            self.getPropertyList()
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
        self.refreshControl!.endRefreshing()
    }
    
    //MARK: - Notification - Update Profile Pic
    @objc func receiveUpdateProfileNotification(notification : NSNotification)
    {
        DispatchQueue.main.async {
            self.profileImgView.sd_setImage(with: (NSURL(string: "\(notification.value(forKey: "object") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
        }
    }
    
    //MARK: - Button Actions
    @objc func discoverButtonClicked(sender : UIButton)
    {
        print("discoverButtonClicked")
        
        UIView.animate(withDuration: 0.7) {
            
            self.viewAddProperty.frame = CGRect(x: UIScreen.main.bounds.size.width/2, y: 15.0, width: 80.0, height: 60.0)
            self.viewAddProperty.backgroundColor = Constants.darkGrayColor
            self.lblAddProperty.frame.size.width = 0
            self.btnAddProperty.frame = CGRect(x: 0.0, y: 0.0, width: self.viewAddProperty.frame.width, height: self.viewAddProperty.frame.height)
            self.imgViewAddProperty.image = UIImage(named: "createPropertyGray")
            
            
            self.viewDiscover.backgroundColor = .white
            self.viewDiscover.frame = CGRect(x: 15, y: 15, width: self.viewAddProperty.frame.origin.x - 30, height: 60.0)
            self.lblDiscover.frame = CGRect(x: self.imgViewDiscover.frame.origin.x + self.imgViewDiscover.frame.size.width + 5, y: 7, width: 100, height: 50.0)
            self.lblDiscover.textColor = Constants.darkBlueColor
            self.btnDiscover.frame = CGRect(x: 0.0, y: 0.0, width: self.viewDiscover.frame.width, height: self.viewDiscover.frame.height)
            self.imgViewDiscover.image = UIImage(named: "discoverBlue")
            
            

        } completion: { (_) in
            
        }
    }
    
    @objc func addPropertyButtonClicked(sender : UIButton)
    {
        print("addPropertyButtonClicked")
        
        UIView.animate(withDuration: 0.7) {
            self.lblDiscover.frame.size.width = 0
            self.viewDiscover.backgroundColor = Constants.darkGrayColor
            self.viewDiscover.frame = CGRect(x: 15, y: self.viewDiscover.frame.origin.y, width: 80, height: 60)
            self.imgViewDiscover.frame = CGRect(x: 27.5, y: 0, width: 25, height: 60)
            self.imgViewDiscover.image = UIImage(named: "discoverGray")
            self.btnDiscover.frame = CGRect(x: 0.0, y: 0.0, width: self.viewDiscover.frame.width, height: self.viewDiscover.frame.height)
            
            
            self.viewAddProperty.backgroundColor = .white//Constants.darkBlueColor
            self.viewAddProperty.frame = CGRect(x: 15 + 80 + 15, y: self.viewAddProperty.frame.origin.y, width: self.viewNotifications.frame.origin.x - (15 + 15 + 15 + 80), height: 60)
            self.lblAddProperty.frame = CGRect(x: self.imgViewAddProperty.frame.origin.x + self.imgViewAddProperty.frame.size.width + 5, y: 7, width: 100, height: 50.0)
            self.lblAddProperty.textColor = Constants.darkBlueColor
            self.btnAddProperty.frame = CGRect(x: 0.0, y: 0.0, width: self.viewAddProperty.frame.width, height: self.viewAddProperty.frame.height)
            self.imgViewAddProperty.image = UIImage(named: "createPropertyBlue")
            
        } completion: { (_) in
            let createPropertyVC = CreatePropertyViewController(nibName: "CreatePropertyViewController", bundle: nil)
            self.navigationController?.pushViewController(createPropertyVC, animated: true)
        }

    }
    
    @objc func notificationButtonClicked(sender : UIButton)
    {
        print("notificationButtonClicked")
        
        let activityVC = ActivityViewController(nibName: "ActivityViewController", bundle: nil)
        activityVC.isFromDashboard = true
        self.navigationController?.pushViewController(activityVC, animated: true)
    }
    
    @IBAction func menuButtonClicked(_ sender: UIButton)
    {
        let alertViewController = UIAlertController(title: "Logout", message: "Are you sure to Logout?", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) in
           
            AppUtility.deleteAllUserDefaults()

            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            let baseVC = BaseViewController(nibName: "BaseViewController", bundle: nil)

            appDelegate.window?.rootViewController = baseVC
            appDelegate.window?.makeKeyAndVisible()
        }))
        
        alertViewController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) in
            
        }))
        
        DispatchQueue.main.async {
            self .present(alertViewController, animated: true, completion: nil)
        }
        
//        self.deleteUserProfile()
        
        
    }
    
    @IBAction func profileButtonClicked(_ sender: UIButton)
    {
//        let userProfileVC = UserProfileViewController(nibName: "UserProfileViewController", bundle: nil)
//        userProfileVC.profileDict = self.profileDict
//        self.navigationController?.pushViewController(userProfileVC, animated: true)
        
        let editProfileVC = CreateProfileViewController(nibName: "CreateProfileViewController", bundle: nil)
        editProfileVC.isFromEditProfile = true
        editProfileVC.profileDict = self.profileDict
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
    @IBAction func filterButtonClicked(_ sender: UIButton)
    {
        let filterVC = FilterViewController(nibName: "FilterViewController", bundle: nil)
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    @objc func shareButtonClicked(sender : UIButton)
    {
        print("shareButtonClicked")
        self.createPDF(propertyDict: (self.responseArray?.object(at: sender.tag) as! NSDictionary))
    }
    
    //MARK: - Create PDF
    func createPDF(propertyDict : NSDictionary)
    {
        let html = "<html><body><font size=6><h1> \(propertyDict.value(forKey: "propertyName") ?? "") </h1><h3>Property Details </h3><b> Building Name :</b> \(propertyDict.value(forKey: "bldg_name") ?? "") <br><b> Address : </b> \(propertyDict.value(forKey: "wing") ?? "")-\(propertyDict.value(forKey: "flat_no") ?? ""),\(propertyDict.value(forKey: "floor") ?? ""),\(propertyDict.value(forKey: "bldg_name") ?? ""), \(propertyDict.value(forKey: "address1") ?? ""), \(propertyDict.value(forKey: "address,") ?? ""), \(propertyDict.value(forKey: "locality") ?? ""), \(propertyDict.value(forKey: "city") ?? ""), \(propertyDict.value(forKey: "state") ?? "")-\(propertyDict.value(forKey: "zipCode") ?? ""), \(propertyDict.value(forKey: "country") ?? "") <br><b> BuildUp Area : </b> \(propertyDict.value(forKey: "builtup_area") ?? "") <br><b> Carpet Area : </b> \(propertyDict.value(forKey: "carpet_area") ?? "") <br><b> Amenities : </b> \(propertyDict.value(forKey: "Amenities") ?? "") <h3> Lease Details </h3> <b> Security Deposite : </b> ₹\(propertyDict.value(forKey: "security_deposit_amt") ?? "") <br><b> Monthly Rent : </b> ₹\(propertyDict.value(forKey: "monthly_rent") ?? "") /month </font></body> </html>"
        let pdfFormatter = UIMarkupTextPrintFormatter(markupText: html)
        
        // 2. Assign print formatter to UIPrintPageRenderer
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(pdfFormatter, startingAtPageAt: 0)
        
        // 3. Assign paperRect and printableRect
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, .zero, nil)
        
        for i in 1...render.numberOfPages
        {
            UIGraphicsBeginPDFPage();
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext();
        
        // 5. Save PDF file
        let documentsPath = self.getDocumentsDirectory().appendingPathComponent("\("\(propertyDict.value(forKey: "propertyName") ?? "")".replacingOccurrences(of: " ", with: "")).pdf")
        
        pdfData.write(to: documentsPath, atomically: true)
        
        let urlPath = URL(fileURLWithPath: documentsPath.absoluteString)
        
        let activityVC = UIActivityViewController(activityItems: [urlPath], applicationActivities: nil)
        
//        activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
//
//                do {
//                    let fileManager = FileManager.default
//
//                    // Check if file exists
//                    if fileManager.fileExists(atPath: urlPath.absoluteString) {
//                        // Delete file
//                        try fileManager.removeItem(atPath: urlPath.absoluteString)
//                    } else {
//                        print("File does not exist")
//                    }
//
//                }
//                catch let error as NSError {
//                    print("An error took place: \(error)")
//                }
//        }
        
        self.present(activityVC, animated: true, completion: nil)
        
        if let popOver = activityVC.popoverPresentationController {
            popOver.sourceView = self.view
        }
    }
    
    
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isSearch
        {
            if self.filterArray.count > 0
            {
                return self.filterArray.count
            }
        }
        else
        {
            if self.responseArray != nil
            {
                if self.responseArray!.count > 0
                {
                    return self.responseArray!.count
                }
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            return 700
        }
        
        if self.isSearch
        {
            if "\(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))".isEmpty == true || "\(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))" == "0"
            {
                return 430
            }
            else
            {
                return 390
            }
        }
        else
        {
            if "\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))".isEmpty == true || "\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))" == "0"
            {
                return 440
            }
            else
            {
                return 415
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! HomeTableViewCell
        
        myCell.selectionStyle = .none
        myCell.backgroundColor = .clear
        
        DispatchQueue.main.async {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                myCell.lblName.font = UIFont(name: "SofiaPro-Medium", size: 22)
                myCell.lblAddress.font = UIFont(name: "SofiaPro", size: 19)
                
                myCell.lblTitleBrokerName.font = UIFont(name: "SofiaPro", size: 18)
                myCell.lblBrokerName.font = UIFont(name: "SofiaPro-Medium", size: 22)
                
                myCell.lblTitleTenantName.font = UIFont(name: "SofiaPro", size: 18)
                myCell.lblTenantName.font = UIFont(name: "SofiaPro-Medium", size: 22)
                
                myCell.lblTitleRentDueDate.font = UIFont(name: "SofiaPro", size: 18)
                myCell.lblRentDueDate.font = UIFont(name: "SofiaPro-Medium", size: 22)
                
                myCell.lblTitleRentAmount.font = UIFont(name: "SofiaPro", size: 18)
                myCell.lblRentAmount.font = UIFont(name: "SofiaPro-Medium", size: 22)
            }
            myCell.mainView.layer.cornerRadius = 20
            myCell.imgView.layer.cornerRadius = 20
            
            myCell.lblGreen.layer.cornerRadius = 7.5
            myCell.lblGreen.layer.masksToBounds = true
            
            if self.isSearch
            {
                if "\(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))".isEmpty == true || "\(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))" == "0"
                {
                    myCell.lblGreenTop.constant = 15
                    myCell.lblGreenHeight.constant = 15
                    myCell.lblAvailableTop.constant = 15
                    myCell.lblAvailableHeight.constant = 20
                }
                else
                {
                    myCell.lblGreenTop.constant = 0
                    myCell.lblGreenHeight.constant = 0
                    myCell.lblAvailableTop.constant = 0
                    myCell.lblAvailableHeight.constant = 0

                }
               
                myCell.lblContactType.text = "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")"
                myCell.lblContactType.textColor = .white //Constants.darkBlueColor
                
                myCell.btnShare.layer.cornerRadius = 8
                myCell.btnShare.setRadiusWithShadow(8)
                
//                myCell.mainView.setRadiusWithShadow(20)
                myCell.mainView.dropShadowForDashBoard()
                
                myCell.imgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "prop_photo") as? String ?? "").components(separatedBy: ",").first ?? "")")! as URL), placeholderImage: UIImage(named: "signupHomeLogo"))
                
                myCell.lblName.text = "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "propertyName") ?? "")"
                          
                myCell.lblAddress.text =   "\(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "wing") ?? "")) - \(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "flat_no") ?? "")), \(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "bldg_name") ?? "")), \(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "address1") ?? "")), \(((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "address2") ?? ""))"
                
                myCell.lblRentDueDate.text = ("\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "rent_due_date") ?? "")" == "<null>" || "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "rent_due_date") ?? "")".isEmpty == true) ? "NA" : "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "rent_due_date") ?? "")"
                
                myCell.lblRentAmount.text = "₹\(("\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "monthly_rent") ?? "")") == "<null>" ? "NA" : "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "monthly_rent") ?? "")")/m"
                //
                myCell.lblBrokerName.text = ("\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")" == "<null>" || "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")".isEmpty == true) ? "NA" : "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")"
                
                myCell.lblTenantName.text = ("\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")" == "<null>" || "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")".isEmpty == true) ? "NA" : "\((self.filterArray.object(at: indexPath.row) as AnyObject).value(forKey: "tenantLastName") ?? "")"
                
                myCell.baseViewObj.roundCorners(corners: [.topRight, .bottomLeft], radius: 20)
            }
            else
            {
                if "\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))".isEmpty == true || "\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantNumber") as? String ?? ""))" == "0"
                {
                    myCell.lblGreenTop.constant = 15
                    myCell.lblGreenHeight.constant = 15
                    myCell.lblAvailableTop.constant = 15
                    myCell.lblAvailableHeight.constant = 20
                }
                else
                {
                    myCell.lblGreenTop.constant = 0
                    myCell.lblGreenHeight.constant = 0
                    myCell.lblAvailableTop.constant = 0
                    myCell.lblAvailableHeight.constant = 0

                }
               
                myCell.lblContactType.text = "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")"
                myCell.lblContactType.textColor = .white //Constants.darkBlueColor
                
                if "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")" == "Owner"
                {
                    myCell.lblTitleBrokerName.text = "Broker Name"
                    myCell.lblBrokerName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")".isEmpty == true) ? "NA" : "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")"
                    
                    myCell.lblTitleTenantName.text = "Tenant Name"
                    myCell.lblTenantName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")".isEmpty == true) ? "NA" : "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantLastName") ?? "")"
                }
                else if "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")" == "Broker"
                {
                    myCell.lblTitleBrokerName.text = "Owner Name"
                    myCell.lblBrokerName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "")".isEmpty == true) ? "NA" : ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "") " + "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerLastName") ?? "")")
                    
                    myCell.lblTitleTenantName.text = "Tenant Name"
                    myCell.lblTenantName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")".isEmpty == true) ? "NA" : ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "") " + "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantLastName") ?? "")")
                }
                else if "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")" == "Tenant"
                {
                    myCell.lblTitleBrokerName.text = "Owner Name"
                    myCell.lblBrokerName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "")".isEmpty == true) ? "NA" : ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "") " + "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerLastName") ?? "")")
                    
                    myCell.lblTitleTenantName.text = "Broker Name"
                    myCell.lblTenantName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")".isEmpty == true) ? "NA" : ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "") " + "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerLastName") ?? "")")
                }
                else if "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "contact_type") ?? "")" == "Property Manager"
                {
                    myCell.lblTitleBrokerName.text = "Owner Name"
                    myCell.lblBrokerName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "")".isEmpty == true) ? "NA" : ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerFirstName") ?? "") " + "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "ownerLastName") ?? "")")
                    
                    myCell.lblTitleTenantName.text = "Tenant Name"
                    myCell.lblTenantName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")".isEmpty == true) ? "NA" : ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "") " + "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantLastName") ?? "")")
                }
                
                myCell.btnShare.layer.cornerRadius = 8
                myCell.btnShare.setRadiusWithShadow(8)
                
//                myCell.mainView.setRadiusWithShadow(20)
                myCell.mainView.dropShadowForDashBoard()
                
                
//                if ("\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "prop_photo") as? String ?? ""))").isEmpty != true
//                {
//                    myCell.imgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "prop_photo") as? String ?? "").components(separatedBy: ",").first ?? "")")! as URL), placeholderImage: UIImage(named: "signupHomeLogo"))
//                }
                
                myCell.imgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "prop_photo") as? String ?? "").components(separatedBy: ",").first ?? "")")! as URL), placeholderImage: UIImage(named: "signupHomeLogo"))
                
                myCell.lblName.text = "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "propertyName") ?? "")"
                          
                myCell.lblAddress.text =   "\(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "wing") ?? "")) - \(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "flat_no") ?? "")), \(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "bldg_name") ?? "")), \(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "address1") ?? "")), \(((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "address2") ?? ""))"
                
                myCell.lblRentDueDate.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "rent_due_date") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "rent_due_date") ?? "")".isEmpty == true) ? "NA" : "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "rent_due_date") ?? "")"
                
                myCell.lblRentAmount.text = "₹\(("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "monthly_rent") ?? "")") == "<null>" ? "NA" : "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "monthly_rent") ?? "")")/m"
                //
//                myCell.lblBrokerName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")".isEmpty == true) ? "NA" : "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "brokerFirstName") ?? "")"
                
//                myCell.lblTenantName.text = ("\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")" == "<null>" || "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantFirstName") ?? "")".isEmpty == true) ? "NA" : "\((self.responseArray?.object(at: indexPath.row) as AnyObject).value(forKey: "tenantLastName") ?? "")"
                
                myCell.baseViewObj.roundCorners(corners: [.topRight, .bottomLeft], radius: 20)
            }
        }
        
        myCell.btnShare.tag = indexPath.row
        myCell.btnShare.addTarget(self, action: #selector(self.shareButtonClicked(sender:)), for: .touchUpInside)
        
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let propertyDetailVC = PropertyDetailViewController(nibName: "PropertyDetailViewController", bundle: nil)
        
        if self.isSearch
        {
            propertyDetailVC.detailDict = (self.filterArray.object(at: indexPath.row) as! NSDictionary)
        }
        else
        {
            propertyDetailVC.detailDict = (self.responseArray?.object(at: indexPath.row) as! NSDictionary)
        }
        
        self.navigationController?.pushViewController(propertyDetailVC, animated: true)
    }
    
    //MARK: - UISearchBar Delegates
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.count == 0
        {
            self.isSearch = false;
            self.lblNoData.isHidden = true
            
            DispatchQueue.main.async {
                self.tableViewObj.reloadData()
            }
        }
        else
        {
            self.filterArray = self.responseArray!.filter({ (text) -> Bool in
                let tmp: NSString = "\((text as AnyObject).value(forKey: "propertyName") as? String ?? "")" as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            }) as NSArray
            
            if(self.filterArray.count == 0)
            {
                self.lblNoData.isHidden = false
            }
            else
            {
                self.lblNoData.isHidden = true
            }
            
            isSearch = true
            
            DispatchQueue.main.async {
                self.tableViewObj.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        print("searchBarSearchButtonClicked")
        searchBar.searchTextField.resignFirstResponder()
    }
    
    //MARK: - Send Device Token
    func sendDeviceToken()
    {
        if Reachability.isConnectedToNetwork()
        {
            let paramDict = [
                "mobileno":(Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "device_id":0,
                "device_token" : "\(AppUtility.getUserData(keyVal: "fcmToken"))",
                "otp" : 0,

            ] as [String : Any]

            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/sendDeviceToken", headerVal: "", parameters: paramDict) { (status, response) in

                    if status
                    {
                        print("Send Device Token Response : \(response)")
                    }
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - Get Property List
    func getPropertyList()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = [
                "mobileno":(Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                "searchResult" : "",
                "emailId" : "",
                "property_name" : "",
                "property_id" : "0",
                "data" : "start",
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getPropertyList", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("Property List Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableArray)
                        {
                            self.responseArray = NSMutableArray()
                            self.responseArray = responseObj
                            
                            DispatchQueue.main.async {
                                self.tableViewObj.isHidden = false
                                self.lblNoData.isHidden = true
                                
                                self.tableViewObj.reloadData()
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.tableViewObj.isHidden = true
                                self.lblNoData.isHidden = false
                                
                                self.tableViewObj.reloadData()
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.lblNoData.isHidden = false
                            self.tableViewObj.isHidden = true
                            
                            self.tableViewObj.reloadData()
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Get User Profile
    func getUserProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno":(Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                             "searchResult" : "",
                             "emailId" : "",
                             "property_name" : "",
                             "property_id" : "",
                             "data" : "",
                             
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("User Profile Response : \(response)")
                        
                        if let responseObj = ((response as AnyObject).value(forKey: "data") as? NSMutableDictionary)
                        {
                            self.profileDict = NSMutableDictionary()
                            self.profileDict = responseObj
                        }
                        
                        DispatchQueue.main.async {

                            self.profileImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "profile_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            
                            self.lblProfileName.text = "Hi, \(self.profileDict?.value(forKey: "firstName") ?? "") \(self.profileDict?.value(forKey: "lastName") ?? "")"
                            
                            AppUtility.setUserData(keyString: "name", valueString: "\(self.profileDict?.value(forKey: "firstName") ?? "") \(self.profileDict?.value(forKey: "lastName") ?? "")")
                        }
                    }
                    
                    self.sendDeviceToken()
//                    else
//                    {
//                        DispatchQueue.main.async {
//                            self.lblNoData.isHidden = false
//                            self.tableViewObj.isHidden = true
//
//                            self.tableViewObj.reloadData()
//                        }
//                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Delete User Profile
    func deleteUserProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno":(Int64("\(AppUtility.getUserProfileMobileNumber())") ?? 0),
                             "searchResult" : "",
                             "firstName":"hardik",
                             "emailId" : "",
                             "property_name" : "",
                             "property_id" : "",
                             "data" : "",
                             
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/deleteUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        print("User Profile Response : \(response)")
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.lblNoData.isHidden = false
                            self.tableViewObj.isHidden = true
                            
                            self.tableViewObj.reloadData()
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
        
    }
    
    //MARK: - Location Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let currentLocation : CLLocation = locations.last!
        //        let latitude = String(format: "%f", currentLocation.coordinate.latitude)
        //        let longitude = String(format: "%f", currentLocation.coordinate.longitude)
        
        AppUtility.setUserData(keyString: "lat", valueString: "\(currentLocation.coordinate.latitude)")
        
        AppUtility.setUserData(keyString: "lon", valueString: "\(currentLocation.coordinate.longitude)")
        
        
        
        //            print("Latitude : \(AppUtility().getUserData(keyVal: "lat")), Longitude : \(AppUtility().getUserData(keyVal: "lon"))")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Location Error : \(error.localizedDescription)")
    }
}
