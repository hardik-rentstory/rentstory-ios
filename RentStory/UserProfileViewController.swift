//
//  UserProfileViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 19/01/21.
//

import UIKit
import SDWebImage

class UserProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var profileDict : NSMutableDictionary?
    
    let tableArray = ["Edit Profile", "My Rewards", "My Transaction", "My Favourites"]
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var tableViewObj: UITableView!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.mainView.backgroundColor = Constants.bgColor
        
        self.tableViewObj.delegate = self
        self.tableViewObj.dataSource = self
        self.tableViewObj.backgroundColor = .clear
        self.tableViewObj.separatorColor = .white
        
        let myNib = UINib(nibName: "UserProfileTableViewCell", bundle: nil)
        self.tableViewObj.register(myNib, forCellReuseIdentifier: "userProfileCell")
       
        
        self.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\(self.profileDict?.value(forKey: "profile_path") ?? "")")! as URL), placeholderImage: UIImage(named: "avatar"))
        self.nameLabel.text = "\(self.profileDict?.value(forKey: "firstName") ?? "") \(self.profileDict?.value(forKey: "lastName") ?? "")"
        self.numberLabel.text = "\(self.profileDict?.value(forKey: "mobileno") ?? "")"
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
    
        self.mainView.layer.cornerRadius = 40
        self.viewImage.layer.cornerRadius = 15
        self.viewImage.layer.masksToBounds = true
    }

    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func inviteButtonClicked(_ sender: UIButton)
    {
    }
    
    @IBAction func logoutButtonClicked(_ sender: UIButton)
    {
//        AppUtility.deleteAllUserDefaults()
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        let baseVC = BaseViewController(nibName: "BaseViewController", bundle: nil)
//
//        appDelegate.window?.rootViewController = baseVC
//        appDelegate.window?.makeKeyAndVisible()
        
        let alertViewController = UIAlertController(title: "Logout", message: "Are you sure to Logout?", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) in
         
            AppUtility.deleteAllUserDefaults()

            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            let baseVC = BaseViewController(nibName: "BaseViewController", bundle: nil)

            appDelegate.window?.rootViewController = baseVC
            appDelegate.window?.makeKeyAndVisible()
        }))
        
        alertViewController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) in
         
            
        }))
        
        DispatchQueue.main.async {
            self .present(alertViewController, animated: true, completion: nil)
        }
    }
    
    //MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.tableArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "userProfileCell", for: indexPath) as! UserProfileTableViewCell
        
        myCell.selectionStyle = .none
        
        myCell.nameLabel.text = "\(self.tableArray[indexPath.row])"
        
        DispatchQueue.main.async {
            myCell.mainView.layer.cornerRadius = 20
            myCell.mainView.backgroundColor = .white
        }
        
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if "\(self.tableArray[indexPath.row])" == "Edit Profile"
        {
            let editProfileVC = CreateProfileViewController(nibName: "CreateProfileViewController", bundle: nil)
            editProfileVC.isFromEditProfile = true
            editProfileVC.profileDict = self.profileDict
            self.navigationController?.pushViewController(editProfileVC, animated: true)
        }
        
    }
}
