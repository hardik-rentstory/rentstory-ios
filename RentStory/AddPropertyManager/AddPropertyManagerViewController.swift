//
//  AddPropertyManagerViewController.swift
//  RentStory
//
//  Created by Hardik Pithadia on 21/01/21.
//

import UIKit
import KRProgressHUD
import AWSS3
import PromiseKit
import AddressBook
import AddressBookUI
import Contacts
import ContactsUI
import DatePickerDialog
import SDWebImage
import MobileCoreServices

class AddPropertyManagerViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ABPeoplePickerNavigationControllerDelegate, CNContactPickerDelegate, UIDocumentPickerDelegate
{
    var isEditPM = false
    
    var isProfileSelected = false
    var isAadharSelected = false
    var isPanSelected = false
    
    var profileImagePath = ""
    var aadharImagePath = ""
    var panImagePath = ""
    var genderString = ""
    var isTenantTemporary = ""
    var propertyIdStr = ""
    var propertyName = ""
    
    var detailDict : NSDictionary?
    
    
    @IBOutlet weak var topHeaderLabel: UILabel!
    var aadharImage : UIImage?
    var panImage : UIImage?
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet var propertyManagerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtAadhar: UITextField!
    @IBOutlet weak var txtPan: UITextField!
    
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var aadharButton: UIButton!
    @IBOutlet weak var panButton: UIButton!

    
    @IBOutlet weak var mobileLabelTop: NSLayoutConstraint!
    @IBOutlet weak var mobileLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneBookButton: UIButton!
    
    @IBOutlet weak var mobileHeight: NSLayoutConstraint!
    @IBOutlet weak var mobileTop: NSLayoutConstraint!
    
    @IBOutlet weak var profileButton: UIButton!
    
    //MARK: - init
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.bgColor
        
        self.propertyManagerView.backgroundColor = .clear
        self.propertyManagerView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 1300)
        self.mainScrollView.addSubview(self.propertyManagerView)
        
        self.mainScrollView.contentSize.height = self.propertyManagerView.frame.height
        
        self.txtMobileNumber.delegate = self
        let txtMobileNumberViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtMobileNumber.leftView = txtMobileNumberViewObj
        self.txtMobileNumber.leftViewMode = .always
        
        self.txtFirstName.delegate = self
        let txtFirstNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtFirstName.leftView = txtFirstNameViewObj
        self.txtFirstName.leftViewMode = .always
        
        self.txtLastName.delegate = self
        let txtLastNameViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtLastName.leftView = txtLastNameViewObj
        self.txtLastName.leftViewMode = .always
        
        self.txtEmail.delegate = self
        let txtEmailViewObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtEmail.leftView = txtEmailViewObj
        self.txtEmail.leftViewMode = .always
        
        self.txtDOB.delegate = self
        let txtDOBObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtDOB.leftView = txtDOBObj
        self.txtDOB.leftViewMode = .always
        
        self.txtAadhar.delegate = self
        let txtAadharObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtAadhar.leftView = txtAadharObj
        self.txtAadhar.leftViewMode = .always
        
        self.txtPan.delegate = self
        let txtPanObj = UIView(frame: CGRect(x: 0, y: 0.0, width: 15.0, height: 50.0))
        self.txtPan.leftView = txtPanObj
        self.txtPan.leftViewMode = .always
        
        if self.detailDict != nil
        {
            self.profileImageView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "contact_photo") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
            self.profileImagePath = "\(self.detailDict?.value(forKey: "contact_photo") ?? "")"
            self.isProfileSelected = ("\(self.detailDict?.value(forKey: "contact_photo") ?? "")".isEmpty == true) ? false : true
            
            let aadharImgView = UIImageView()
            aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "adhar_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.aadharButton.setImage(aadharImgView.image, for: .normal)
            self.aadharImagePath = "\(self.detailDict?.value(forKey: "adhar_doc") ?? "")"
            self.isAadharSelected = ("\(self.detailDict?.value(forKey: "adhar_doc") ?? "")".isEmpty == true) ? false : true
            self.aadharButton.imageView?.contentMode = .scaleAspectFill
            self.aadharButton.layer.masksToBounds = true
            
            let panImgView = UIImageView()
            panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")")")! as URL), placeholderImage: UIImage(named: "roundPlus"))
            self.panButton.setImage(panImgView.image, for: .normal)
            self.panImagePath = "\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")"
            self.isPanSelected = ("\(self.detailDict?.value(forKey: "pan_card_doc") ?? "")".isEmpty == true) ? false : true
            self.panButton.imageView?.contentMode = .scaleAspectFill
            self.panButton.layer.masksToBounds = true
            
            self.txtMobileNumber.text = "\(self.detailDict?.value(forKey: "mobileno") ?? "")"
            self.txtFirstName.text = "\(self.detailDict?.value(forKey: "firstName") ?? "")"
            self.txtLastName.text = "\(self.detailDict?.value(forKey: "lastName") ?? "")"
            self.txtEmail.text = "\(self.detailDict?.value(forKey: "email") ?? "")"
            self.txtDOB.text = "\(self.detailDict?.value(forKey: "dob") ?? "")"
            
            self.txtAadhar.text = "\(self.detailDict?.value(forKey: "adhar_number") ?? "")"
            self.txtPan.text = "\(self.detailDict?.value(forKey: "pan_number") ?? "")"
            
            if "\(self.detailDict?.value(forKey: "gender") ?? "")" == "male"
            {
                self.genderString = "male"
                self.maleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.maleButton.isSelected = true
                
            }
            else if "\(self.detailDict?.value(forKey: "gender") ?? "")" == "female"
            {
                self.genderString = "female"
                self.femaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.femaleButton.isSelected = true
            }
            else
            {
                self.genderString = "other"
                self.otherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                self.otherButton.isSelected = true
            }
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if self.isEditPM
        {
            self.topHeaderLabel.text = "Edit Property Manager"
            
            self.mobileLabelTop.constant = 0
            self.mobileLabelHeight.constant = 0
            self.mobileTop.constant = 0
            self.mobileHeight.constant = 0
            self.phoneBookButton.isHidden = true
        }
        else
        {
            self.topHeaderLabel.text = "Add Property Manager"
        }
    }
    
    
    //MARK: - Button Actions
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profileButtonClicked(_ sender: UIButton)
    {
        self.isProfileSelected = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func phoneButtonClicked(_ sender: UIButton)
    {
        let peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        peoplePicker.displayedPropertyKeys = [CNContactPhoneNumbersKey, CNContactEmailAddressesKey]
        
        DispatchQueue.main.async {
            self.present(peoplePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func aadharButtonClicked(_ sender: UIButton)
    {
        self.isAadharSelected = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func panButtonClicked(_ sender: UIButton)
    {
        self.isPanSelected = true
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            print("Camera Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action) in
            print("Photo Clicked")
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: false, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Documents", style: .default, handler: { (action) in
            print("Documents")
            
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    
    @IBAction func maleButtonClicked(_ sender: UIButton)
    {
        self.femaleButton.isSelected = false
        self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.otherButton.isSelected = false
        self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.maleButton.isSelected
        {
            self.maleButton.isSelected = false
            self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderString = ""
        }
        else
        {
            self.maleButton.isSelected = true
            self.maleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderString = "male"
        }
    }
    
    
    @IBAction func femaleButtonClicked(_ sender: UIButton)
    {
        self.maleButton.isSelected = false
        self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.otherButton.isSelected = false
        self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.femaleButton.isSelected
        {
            self.femaleButton.isSelected = false
            self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderString = ""
        }
        else
        {
            self.femaleButton.isSelected = true
            self.femaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderString = "female"
        }
    }
    
    @IBAction func otherButtonClicked(_ sender: UIButton)
    {
        self.maleButton.isSelected = false
        self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        self.femaleButton.isSelected = false
        self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
        
        if self.otherButton.isSelected
        {
            self.otherButton.isSelected = false
            self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
            
            self.genderString = ""
        }
        else
        {
            self.otherButton.isSelected = true
            self.otherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
            
            self.genderString = "other"
        }
    }
    
    
    @IBAction func submitButtonClicked(_ sender: UIButton)
    {
        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.propertyIdStr.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Property Not Created", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }

        else if self.txtMobileNumber.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's Mobile Number", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtFirstName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's First Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtLastName.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's Last Name", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.txtEmail.text!.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Property Manager's Email Id", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else if self.genderString.isEmpty == true
        {
            let alertViewController = UIAlertController(title: "Warning", message: "Please Select Property Manager's Gender", preferredStyle: .alert)

            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

            }))

            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
        }
        else
        {
            if self.txtEmail.text?.isEmpty != true
            {
                if !AppUtility.isValidEmail(emailString: self.txtEmail.text ?? "")
                {
                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)

                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in

                    }))

                    DispatchQueue.main.async {
                        self .present(alertViewController, animated: true, completion: nil)
                    }
                    return
                }
            }
            if self.isEditPM
            {
                self.updatePropertyManagerResponse()
            }
            else
            {
                self.addPropertyManagerDetailsResponse()
            }
            
        }

        
//        if AppUtility.getUserData(keyVal: "mobileNo").isEmpty == true
//        {
//            let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Mobile Number", preferredStyle: .alert)
//
//            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//            }))
//
//            DispatchQueue.main.async {
//                self .present(alertViewController, animated: true, completion: nil)
//            }
//        }
//        else if self.propertyIdStr.isEmpty == true
//        {
//
//        }
//
//        else
//        {
//            if self.txtEmail.text?.isEmpty != true
//            {
//                if !AppUtility.isValidEmail(emailString: self.txtEmail.text ?? "")
//                {
//                    let alertViewController = UIAlertController(title: "Warning", message: "Please Enter Valid Email Id", preferredStyle: .alert)
//
//                    alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
//
//                    }))
//
//                    DispatchQueue.main.async {
//                        self .present(alertViewController, animated: true, completion: nil)
//                    }
//                    return
//                }
//            }
//        }
//
//        self.addPropertyManagerDetailsResponse()
    }
    
    //MARK: - Phonebook Delegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact])
    {
        if contacts.count > 0
        {
            let selectedPhoneNumber = contacts[0].phoneNumbers[0].value.stringValue
          
            self.txtMobileNumber.text = "\(selectedPhoneNumber)"
//            self.txtFirstName.text = "\(contacts[0].givenName)"
//            self.txtLastName.text = "\(contacts[0].familyName)"
            self.getPropertyManagerProfile()
        }
    }
    
    //MARK: - Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])
    {
        if urls.first != nil
        {
            if self.isAadharSelected
            {
                self.isAadharSelected = false
                
                self.aadharImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.aadharButton.setImage(self.aadharImage, for: .normal)
                self.aadharButton.imageView?.contentMode = .scaleAspectFill
                self.aadharButton.layer.masksToBounds = true
                
                self.aadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
            else if self.isPanSelected
            {
                self.isPanSelected = false
                
                self.panImage = AppUtility.drawPDFfromURL(url: urls.first!)
                
                self.panButton.setImage(self.panImage, for: .normal)
                self.panButton.imageView?.contentMode = .scaleAspectFill
                self.panButton.layer.masksToBounds = true
                
                self.panImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
    }
    
    //MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if self.isProfileSelected == true
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isProfileSelected = false
                self.profileImageView.image = pickedImage
                
                self.profileImagePath = Constants.profileImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isAadharSelected
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isAadharSelected = false
                
                self.aadharImage = pickedImage
                
                self.aadharButton.setImage(pickedImage, for: .normal)
                self.aadharButton.layer.masksToBounds = true
                
                self.aadharImagePath = Constants.aadharImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        else if self.isPanSelected
        {
            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                self.isPanSelected = false
                
                self.panImage = pickedImage
                
                self.panButton.setImage(pickedImage, for: .normal)
                self.panButton.layer.masksToBounds = true
                
                self.panImagePath = Constants.panCardImageFolder+"/"+AppUtility.getUserData(keyVal: "mobileNo")+AppUtility.getCurrentTS()+".png"
            }
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.isProfileSelected = false
        self.isAadharSelected = false
        self.isPanSelected = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if self.txtDOB == textField
        {
//            textField.resignFirstResponder()
//            self.txtDOB.isEditing = false
            
            let datePicker = DatePickerDialog()
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            datePicker.show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
                
                if let dt = date
                {
                    let formatter = DateFormatter()
    //                formatter.dateFormat = "dd MMM - yyyy"
                    formatter.dateFormat = "yyyy-MM-dd"
                    print("Selected Date : \(formatter.string(from: dt))")
                    
                    self.txtDOB.text = "\(formatter.string(from: dt))"
                }
            }
            
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if self.txtMobileNumber == textField
        {
            self.getPropertyManagerProfile()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if textField == self.txtMobileNumber ||  textField == self.txtAadhar
        {
            if string == ""
            {
                return true
            }
            else if (textField.text?.count)! > 11
            {
                return false
            }
            
            return true
        }
        else if textField == self.txtPan
        {
            if string == ""
            {
                return true
            }
            else if (textField.text?.count)! > 9
            {
                return false
            }
            
            return true
        }
        
        return true
    }
    
    //MARK: - Get Property Manager Profile
    func getPropertyManagerProfile()
    {
        if Reachability.isConnectedToNetwork()
        {
            KRProgressHUD.show()
            
            let paramDict = ["mobileno": (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtMobileNumber.text ?? ""))") ?? 0),
                             "searchResult":"",
                             "emailId":"",
                             "property_name":"",
                             "property_id":"",
                             "data":""
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/getUserProfile", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        DispatchQueue.main.async {
                           
                            self.profileImageView!.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "profile_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            self.profileButton.isEnabled = false
                            
                            self.txtFirstName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "firstName") ?? "")"
                            self.txtFirstName.isEnabled = false
                            
                            self.txtLastName.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "lastName") ?? "")"
                            self.txtLastName.isEnabled = false
                            
                            self.txtEmail.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "email") ?? "")"
                            self.txtEmail.isEnabled = false
                            
                            self.txtDOB.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "dob") ?? "")"
                            self.txtDOB.isEnabled = false
                            
                            if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "male"
                            {
                                self.genderString = "male"
                                self.maleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else if ("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "gender") ?? "")").lowercased() == "female"
                            {
                                self.genderString = "female"
                                self.femaleButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            else
                            {
                                self.genderString = "other"
                                self.otherButton.setBackgroundImage(UIImage(named: "selectedRadio"), for: .normal)
                            }
                            
                            self.maleButton.isEnabled = false
                            self.femaleButton.isEnabled = false
                            self.otherButton.isEnabled = false
                            
                            self.txtAadhar.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_no") ?? "")"
                            self.txtAadhar.isEnabled = false
                            
                            let aadharImgView = UIImageView()
                            aadharImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "adhar_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            self.aadharButton.setImage(aadharImgView.image, for: .normal)
                            self.aadharButton.imageView?.contentMode = .scaleAspectFill
                            self.aadharButton.layer.masksToBounds = true
                            self.aadharButton.isEnabled = false
                            
                            self.txtPan.text = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_no") ?? "")"
                            self.txtPan.isEnabled = false
                            
                            let panImgView = UIImageView()
                            panImgView.sd_setImage(with: (NSURL(string: "\(Constants.imagePath)/\("\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "pan_doc_path") ?? "")")")! as URL), placeholderImage: UIImage(named: "avatar"))
                            self.panButton.setImage(panImgView.image, for: .normal)
                            self.panButton.imageView?.contentMode = .scaleAspectFill
                            self.panButton.layer.masksToBounds = true
                            self.panButton.isEnabled = false
                            
                            self.isTenantTemporary = "\(((response as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "is_temporary") ?? "")"
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                           
                            self.profileImageView!.image = UIImage(named: "avatar")
                            self.profileImagePath = ""
                            self.isProfileSelected = false
                            self.profileButton.isEnabled = true
                            
                            self.txtFirstName.text = ""
                            self.txtFirstName.isEnabled = true
                            
                            self.txtLastName.text = ""
                            self.txtLastName.isEnabled = true
                            
                            self.txtEmail.text = ""
                            self.txtEmail.isEnabled = true
                            
                            self.txtDOB.text = ""
                            self.txtDOB.isEnabled = true
                            
                            self.txtAadhar.text = ""
                            self.txtAadhar.isEnabled = true
                            
                            self.txtPan.text = ""
                            self.txtPan.isEnabled = true
                            
                            self.genderString = ""
                            
                            self.maleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.maleButton.isSelected = false
                            self.maleButton.isEnabled = true
                            
                            self.femaleButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.femaleButton.isSelected = false
                            self.femaleButton.isEnabled = true
                            
                            self.otherButton.setBackgroundImage(UIImage(named: "uncheckRadio"), for: .normal)
                            self.otherButton.isSelected = false
                            self.otherButton.isEnabled = true
                            
                            self.aadharButton.setImage(UIImage(named: "roundPlus"), for: .normal)
                            self.aadharButton.isSelected = false
                            self.aadharButton.isEnabled = true
                            
                            self.panButton.setBackgroundImage(UIImage(named: "roundPlus"), for: .normal)
                            self.panButton.isSelected = false
                            self.panButton.isEnabled = true
                            
                            
                            self.isTenantTemporary = "0"
                        }
                    }

                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Upload Registration Images
    func uploadImageToServer(uploadImg : UIImage, imageName : String)
    {
        KRProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            
            let req = NetworkManager.api.uploadImage(image: uploadImg, imageName: "\(imageName)")
            
            req.done { (url) in
                print("Success URL : \(url)")
                
            }.catch { (error) in
                print("Error : \(error.localizedDescription)")
            }
            
            KRProgressHUD.dismiss()
        }
    }
    
    //MARK: - Add Property Manager Details
    func addPropertyManagerDetailsResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.profileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.profileImageView.image!, imageName: "\(self.profileImagePath)")
            }
            
            if self.aadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.aadharImage!, imageName: "\(self.aadharImagePath)")
            }
            
            if self.panImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.panImage!, imageName: "\(self.panImagePath)")
            }
            
            KRProgressHUD.show()
                
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_type":"Property Manager", //String
                "mobileno":(Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno":(Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtMobileNumber.text ?? ""))") ?? 0) as Any, //Long
                "firstName":"\(self.txtFirstName.text ?? "")", //String
                "lastName":"\(self.txtLastName.text ?? "")", //String
                "email":"\(self.txtEmail.text ?? "")", //String
                "adhar_doc":"\(self.aadharImagePath)", //String
                "pan_card_doc":"\(self.panImagePath)", //String
                "contact_photo":"\(self.profileImagePath)", //String
                "Nationality":"",
                "istenantbroker":false, //Boolean
                "iscompanyoccupant":false, //Boolean
                "dob":"\(self.txtDOB.text ?? "")", //String
                "pan_number":"\(self.txtPan.text ?? "")", //String
                "adhar_number":"\(self.txtAadhar.text ?? "")", //String
                "gender":"\(self.genderString)", //String
                "doa":"", //String
                "propertyName":"\(self.propertyName)", //String
                "property_progress_state":Int(0), //Int
                "is_temporary":"\(self.isTenantTemporary)", //String
//                "broker_type":"\(self.brokerTypeStr)"
            ] as [String : Any]
            
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/addPropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
    
    //MARK: - Update Property Manager Response
    func updatePropertyManagerResponse()
    {
        if Reachability.isConnectedToNetwork()
        {
            if self.profileImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.profileImageView.image!, imageName: self.profileImagePath)
            }
            
            if self.aadharImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.aadharImage!, imageName: self.aadharImagePath)
            }
            
            if self.panImagePath.isEmpty != true
            {
                self.uploadImageToServer(uploadImg: self.panImage!, imageName: self.panImagePath)
            }
            
            KRProgressHUD.show()

            
            let paramDict = [
                "property_id":(Int64(self.propertyIdStr) ?? 0), //Long
                "contact_id" : Int64("\(self.detailDict?.value(forKey: "contact_id") ?? "")") as Any, //Long
                "contact_type" : "Property Manager", //String
                "mobileno" : (Int64(AppUtility.getUserProfileMobileNumber()) ?? 0) as Any, //Long
                "contactno" : (Int64("\(AppUtility.getMobileNumber(mobileNumber: self.txtMobileNumber.text ?? ""))") ?? 0) as Any, //Long
                "firstName" : "\(self.txtFirstName.text ?? "")", //String
                "lastName" : "\(self.txtLastName.text ?? "")", //String
                "email" : "\(self.txtEmail.text ?? "")", //String
                "Nationality" : "", //String
                "adhar_doc" : "\(self.aadharImagePath)", //String
                "pan_card_doc" : "\(self.panImagePath)", //String
                "contact_photo" : "\(self.profileImagePath)", //String
//                "companyinfo" : [String](), //String Array
                "istenantbroker" : false, //Boolean
                "iscompanyoccupant" : false, //Boolean
//                "bank_info" : [String](), //String Array
                "dob" : "\(self.txtDOB.text ?? "")", //String
                "pan_number" : "\(self.txtPan.text ?? "")", //String
                "adhar_number" : "\(self.txtAadhar.text ?? "")", //String
                "gender" : "\(self.genderString)", //String
                "doa" : "", //String
                "propertyName" : "\(self.propertyName)", //String
                "property_progress_state" : Int(0), //Int
                "is_temporary" : "\(self.detailDict?.value(forKey: "is_temporary") ?? "")", //String
                "broker_type" : "\(self.detailDict?.value(forKey: "broker_type") ?? "")", //String
            ] as [String : Any]
            
            DispatchQueue.global(qos: .background).async {
               
                WebRequestManager.parseJsonWebService(urlString: "\(Constants.mainURL)/updatePropertycontact", headerVal: "", parameters: paramDict) { (status, response) in
                    
                    if status
                    {
                        let alertViewController = UIAlertController(title: "Success", message: "\((response as AnyObject).value(forKey: "message") as? String ?? "")", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alertViewController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                        
                        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                            
                        }))
                        
                        DispatchQueue.main.async {
                            self .present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    
                    KRProgressHUD.dismiss()
                }
            }
            
        }
        else
        {
            let alertViewController = UIAlertController(title: "No Internet", message: "Network Not Available.\nPlease Check Settings", preferredStyle: .alert)
            
            alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (alert: UIAlertAction!) in
                
            }))
            
            DispatchQueue.main.async {
                self .present(alertViewController, animated: true, completion: nil)
            }
            
        }
    }
}
